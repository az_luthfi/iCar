
package com.app.icarindo.services;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.utility.FileOpen;
import com.app.icarindo.utility.Logs;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * Created by Creasif on 10/27/2016.
 */
public class DownloadService extends IntentService {
    private static final String TAG = DownloadService.class.getSimpleName();

    public DownloadService() {
        super("Download Service");
    }

    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    private int totalFileSize;
    private String filename;
    private String url;
    private final String DOCUMENT_URL = "media/document/";

    @Override
    protected void onHandleIntent(Intent intent) {

        Log.e("download", "here");
        filename = intent.getExtras().getString("filename");
        url = intent.getExtras().getString("url");



        initDownload();

    }

    private void initDownload() {
        App.apiDownload().downloadFile(url)
                .flatMap(processResponse())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(handleResult());


//        ApiInterface apiInterface = Client.getClient().create(ApiInterface.class);
//        Call<ResponseBody> request = apiInterface.downloadFile(DOCUMENT_URL+filename);
//        try {
//            downloadFile(request.execute().body());
//
//        } catch (IOException e) {
//            e.printStackTrace();
//            Toast.makeText(getApplicationContext(),e.getMessage(), Toast.LENGTH_SHORT).show();
//
//        }
    }

    private Observable<File> saveToDiskRx(final Response<ResponseBody> response) {
        return Observable.create(new ObservableOnSubscribe<File>() {
            @Override public void subscribe(@NonNull ObservableEmitter<File> subscriber) throws Exception {
                Logs.d(TAG, "subscribe ==> saveToDiskRx");
                try {
                    int count;
                    byte data[] = new byte[1024 * 4];
                    long fileSize = response.body().contentLength();
                    Logs.d(TAG, "subscribe ==> fileSize => " + fileSize);
                    InputStream bis = new BufferedInputStream(response.body().byteStream(), 1024 * 8);
                    File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), filename);
                    OutputStream output = new FileOutputStream(outputFile);
                    long total = 0;
                    long startTime = System.currentTimeMillis();
                    int timeCount = 1;
                    while ((count = bis.read(data)) != -1) {

                        total += count;
                        totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
                        double current = Math.round(total / (Math.pow(1024, 2)));

                        int progress = (int) ((total * 100) / fileSize);

                        long currentTime = System.currentTimeMillis() - startTime;

                        MrCatzDownload download = new MrCatzDownload();
                        download.setTotalFileSize(totalFileSize);

                        if (currentTime > 1000 * timeCount) {

                            download.setCurrentFileSize((int) current);
                            download.setProgress(progress);
                            sendNotification(download);
                            timeCount++;
                        }

                        output.write(data, 0, count);
                    }
                    onDownloadComplete();
                    output.flush();
                    output.close();
                    bis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });
    }

    private Observer<File> handleResult() {
        return new Observer<File>() {
            @Override
            public void onError(Throwable e) {
                Toast.makeText(getApplicationContext(), getString(R.string.error_network_light), Toast.LENGTH_SHORT).show();
                e.printStackTrace();
                Log.d(TAG, "Error " + e.getMessage());
                onDownloadError();
            }

            @Override public void onComplete() {
                Log.d(TAG, "onCompleted");
            }

            @Override public void onSubscribe(Disposable d) {
                notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                notificationBuilder = new NotificationCompat.Builder(DownloadService.this)
                        .setSmallIcon(android.R.drawable.stat_sys_download)
                        .setContentTitle("Download")
                        .setContentText("Downloading File")
                        .setAutoCancel(true);
                notificationManager.notify(0, notificationBuilder.build());
            }

            @Override
            public void onNext(File file) {
                Log.d(TAG, "File downloaded to " + file.getAbsolutePath());

            }
        };
    }

    private Function<Response<ResponseBody>, Observable<File>> processResponse() {
        return new Function<Response<ResponseBody>, Observable<File>>() {
            @Override public Observable<File> apply(@NonNull Response<ResponseBody> responseBodyResponse) throws Exception {
                return saveToDiskRx(responseBodyResponse);
            }
        };
    }

    private void sendNotification(MrCatzDownload download) {

        sendIntent(download, false);
        notificationBuilder.setProgress(100, download.getProgress(), false);
        notificationBuilder.setContentText("Downloading file " + download.getCurrentFileSize() + "/" + totalFileSize + " MB");
        notificationManager.notify(0, notificationBuilder.build());
    }

    private void sendIntent(MrCatzDownload download, boolean status) {

        Intent intent = new Intent("message_progress");
        intent.putExtra("download", download);
        intent.putExtra("filename", filename);
        intent.putExtra("status", status);
        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }

    private void onDownloadComplete() {
        Logs.d(TAG, "onDownloadComplete ==> ");
        MrCatzDownload download = new MrCatzDownload();
        download.setProgress(100);
        sendIntent(download, true);

        File outputFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), filename);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, FileOpen.getIntent(this, outputFile),
                PendingIntent.FLAG_ONE_SHOT);

        notificationManager.cancel(0);
        notificationBuilder.setSmallIcon(android.R.drawable.stat_sys_download_done);
        notificationBuilder.setProgress(0, 0, false);
        notificationBuilder.setContentText("File Downloaded");
        notificationBuilder.setContentIntent(pendingIntent);
        notificationManager.notify(0, notificationBuilder.build());


    }

    private void onDownloadError() {
        MrCatzDownload download = new MrCatzDownload();
        download.setProgress(100);
        sendIntent(download, false);

        notificationManager.cancel(0);
        notificationBuilder.setSmallIcon(android.R.drawable.stat_sys_download_done);
        notificationBuilder.setProgress(0, 0, false);
        notificationBuilder.setContentText("Error Download File");
        notificationManager.notify(0, notificationBuilder.build());

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        notificationManager.cancel(0);
    }

}