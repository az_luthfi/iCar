package com.app.icarindo.services;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Creasif on 10/27/2016.
 */
public class MrCatzDownload implements Parcelable {

    public MrCatzDownload() {

    }

    private int progress;
    private int currentFileSize;
    private int totalFileSize;

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getCurrentFileSize() {
        return currentFileSize;
    }

    public void setCurrentFileSize(int currentFileSize) {
        this.currentFileSize = currentFileSize;
    }

    public int getTotalFileSize() {
        return totalFileSize;
    }

    public void setTotalFileSize(int totalFileSize) {
        this.totalFileSize = totalFileSize;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

        dest.writeInt(progress);
        dest.writeInt(currentFileSize);
        dest.writeInt(totalFileSize);
    }

    private MrCatzDownload(Parcel in) {

        progress = in.readInt();
        currentFileSize = in.readInt();
        totalFileSize = in.readInt();
    }

    public static final Creator<MrCatzDownload> CREATOR = new Creator<MrCatzDownload>() {
        public MrCatzDownload createFromParcel(Parcel in) {
            return new MrCatzDownload(in);
        }

        public MrCatzDownload[] newArray(int size) {
            return new MrCatzDownload[size];
        }
    };
}