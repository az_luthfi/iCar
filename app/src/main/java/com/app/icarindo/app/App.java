package com.app.icarindo.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.app.icarindo.apis.ApiService;


/**
 * Created by j3p0n on 12/3/2016.
 */
public class App extends Application {

    private static ApiService apiService;
    private static ApiService apiDownload;

    @Override
    public void onCreate() {
        super.onCreate();
        RestClient restClient = new RestClient();
        apiService = restClient.getApiService();
        apiDownload = restClient.getApiDownload();


    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static ApiService getService() {
        return apiService;
    }

    public static ApiService apiDownload() {
        return apiDownload;
    }
}
