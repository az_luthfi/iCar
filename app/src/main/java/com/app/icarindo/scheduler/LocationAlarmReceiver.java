package com.app.icarindo.scheduler;

import android.app.AlarmManager;
import android.app.IntentService;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.util.Log;

import com.app.icarindo.utility.Config;
import com.app.icarindo.utility.Preferences;

import java.util.Calendar;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class LocationAlarmReceiver extends WakefulBroadcastReceiver {
    private static final String TAG = LocationAlarmReceiver.class.getSimpleName();
    private AlarmManager alarmMgr;

    private PendingIntent alarmIntent;

    private long UPDATE_INTERVAL = 30000;
    private int START_DELAY = 2;

    @Override public void onReceive(Context context, Intent intent) {
        String customerId = intent.getStringExtra(LocationUpdateService.CUSTOMER_ID);
        if (customerId != null){
            Intent service = new Intent(context, LocationUpdateService.class);
            service.putExtra(LocationUpdateService.CUSTOMER_ID, customerId);
            Log.d(TAG, "onReceive: startWakefulservice");
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                startWakefulService(context, service);
            }


        }else{
            setAlarm(context);
        }

    }

    public void setAlarm(Context context) {
        Preferences pref = new Preferences(context);


        Log.d(TAG, "setAlarm: start");
        alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, LocationAlarmReceiver.class);
        if (pref.getPreferencesInt(Config.CUSTOMER_ID) > 0) {
            intent.putExtra(LocationUpdateService.CUSTOMER_ID, String.valueOf(pref.getPreferencesInt(Config.CUSTOMER_ID)));
        }else{
            return;
        }

        alarmIntent = PendingIntent.getBroadcast(context, Config.ALARM_ID, intent, PendingIntent.FLAG_CANCEL_CURRENT);

        Calendar cal = Calendar.getInstance();
        // add 2 hours to the calendar object
        cal.add(Calendar.HOUR, START_DELAY);
//        cal.add(Calendar.SECOND, START_DELAY);

        alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), UPDATE_INTERVAL, alarmIntent);

        // Enable {@code SampleBootReceiver} to automatically restart the alarm when the
        // device is rebooted.
        ComponentName receiver = new ComponentName(context, LocationBootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                PackageManager.DONT_KILL_APP);
//        }
    }

    public void cancelAlarm(Context context) {
        // If the alarm has been set, cancel it.
        if (alarmMgr == null) {
            alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        }
        if (alarmMgr != null) {
            if (alarmIntent == null) {
                Intent intent = new Intent(context, LocationAlarmReceiver.class);
                alarmIntent = PendingIntent.getBroadcast(context, Config.ALARM_ID, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            }
            alarmMgr.cancel(alarmIntent);
        }

        // Disable {@code SampleBootReceiver} so that it doesn't automatically restart the
        // alarm when the device is rebooted.
        ComponentName receiver = new ComponentName(context, LocationBootReceiver.class);
        PackageManager pm = context.getPackageManager();

        pm.setComponentEnabledSetting(receiver,
                PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
                PackageManager.DONT_KILL_APP);
    }
}
