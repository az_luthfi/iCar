package com.app.icarindo.scheduler;

import android.app.IntentService;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.app.icarindo.utility.SendLocation;


/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class LocationUpdateService extends IntentService {
    public static final String CUSTOMER_ID = "customer_id";
    private static final String TAG = LocationUpdateService.class.getSimpleName();

    public LocationUpdateService() {
        super("LocationUpdateService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "Location update broooooo");
        // Release the wake lock provided by the BroadcastReceiver.
        String customerId = intent.getStringExtra(CUSTOMER_ID);
        if (customerId != null) {
            new SendLocation(getApplicationContext(), customerId);
        }
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            LocationAlarmReceiver.completeWakefulIntent(intent);
        }
    }
}
