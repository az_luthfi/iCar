package com.app.icarindo.scheduler;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.app.icarindo.utility.UtilScheduler;

public class LocationBootReceiver extends BroadcastReceiver {
    LocationAlarmReceiver locationalarm = new LocationAlarmReceiver();
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (action != null && action.equals("android.intent.action.BOOT_COMPLETED")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                UtilScheduler.scheduleJob(context);
            } else {
                locationalarm.cancelAlarm(context);
                locationalarm.setAlarm(context);
            }
        }
    }
}
