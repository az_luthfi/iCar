package com.app.icarindo.scheduler;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.app.icarindo.utility.Config;
import com.app.icarindo.utility.Logs;
import com.app.icarindo.utility.Preferences;
import com.app.icarindo.utility.SendLocation;
import com.app.icarindo.utility.UtilScheduler;


@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
public class LocationJobService extends JobService {
    private static final String TAG = LocationJobService.class.getSimpleName();

    @Override public boolean onStartJob(JobParameters params) {
        Logs.d(TAG, "onStartJob ==> ");

        Preferences pref = new Preferences(this);
        if (pref.getPreferencesInt(Config.CUSTOMER_ID) > 0) {
            new SendLocation(getApplicationContext(), String.valueOf(pref.getPreferencesInt(Config.CUSTOMER_ID)));
        }
//        Intent service = new Intent(getApplicationContext(), LocationJobService.class);
//        getApplicationContext().startService(service);
        UtilScheduler.scheduleJob(getApplicationContext());
        return true;
    }

    @Override public boolean onStopJob(JobParameters params) {
        Logs.d(TAG, "onStopJob ==> ");
        return true;
    }
}
