package com.app.icarindo.modules.carwis.detail;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.carwis.WisataCartPriceAdapter;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.carwis.RequestCarwis;
import com.app.icarindo.models.carwis.Transaction;
import com.app.icarindo.models.eventbus.EventRefreshListCarwis;
import com.app.icarindo.models.eventbus.EventUpdateProfile;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Config;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.Str;
import com.app.icarindo.utility.Validation;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@FragmentWithArgs
public class DetailCarwisFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestCarwis, XDetailCarwisView, DetailCarwisPresenter>
        implements XDetailCarwisView, AlertListener {

    @Arg String bookingCode;
    @BindView(R.id.tvBookingCode) TextView tvBookingCode;
    @BindView(R.id.tvStatusText) TextView tvStatusText;
    @BindView(R.id.tvStatusInfo) TextView tvStatusInfo;
    @BindView(R.id.tvPaymentMethod) TextView tvPaymentMethod;
    @BindView(R.id.tvPemesan) TextView tvPemesan;
    @BindView(R.id.tvNote) TextView tvNote;
    @BindView(R.id.layNote) CardView layNote;
    @BindView(R.id.tvPenjemputanAddress) TextView tvPenjemputanAddress;
    @BindView(R.id.tvDepartNotes) TextView tvDepartNotes;
    @BindView(R.id.tvKepulanganAddress) TextView tvKepulanganAddress;
    @BindView(R.id.tvReturnNotes) TextView tvReturnNotes;
    @BindView(R.id.layKepulangan) CardView layKepulangan;
    @BindView(R.id.tvAdultText) TextView tvAdultText;
    @BindView(R.id.tvJmlKursiAdult) TextView tvJmlKursiAdult;
    @BindView(R.id.layAdult) LinearLayout layAdult;
    @BindView(R.id.tvJmlKursiChild) TextView tvJmlKursiChild;
    @BindView(R.id.layChild) LinearLayout layChild;
    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.tvTime) TextView tvTime;
    @BindView(R.id.layForm) CardView layForm;
    @BindView(R.id.recyclerView2) RecyclerView recyclerView2;
    @BindView(R.id.layWisata) LinearLayout layWisata;
    @BindView(R.id.tvCarName) TextView tvCarName;
    @BindView(R.id.tvCarAmount) TextView tvCarAmount;
    @BindView(R.id.layRentCar) LinearLayout layRentCar;
    @BindView(R.id.tvPaketWisataName) TextView tvPaketWisataName;
    @BindView(R.id.tvAmountPaketWisata) TextView tvAmountPaketWisata;
    @BindView(R.id.layPaketWisata) LinearLayout layPaketWisata;
    @BindView(R.id.tvTotalPrice) TextView tvTotalPrice;
    @BindView(R.id.tvTotalPayment) TextView tvTotalPayment;
    @BindView(R.id.btnSubmit) Button btnSubmit;
    @BindView(R.id.layActionBottom) LinearLayout layActionBottom;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.layDepart) CardView layDepart;
    @BindView(R.id.etBookingCode) EditText etBookingCode;
    @BindView(R.id.tvExtraAmount) TextView tvExtraAmount;

    private Transaction transaction;
    private WisataCartPriceAdapter priceAdapter;

    public DetailCarwisFragment() {

    }

    @Override public DetailCarwisPresenter createPresenter() {
        return new DetailCarwisPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_detail_carwis;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
        priceAdapter = new WisataCartPriceAdapter();
        recyclerView2.setHasFixedSize(true);
        recyclerView2.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView2.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(recyclerView2, false);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @SuppressLint("SetTextI18n") @Override public void setData(RequestCarwis data) {
        if (data.getStatus()) {
            this.transaction = data.getTransaction();
            tvStatusText.setText(transaction.getCtStatusText());
            tvStatusInfo.setText(transaction.getCtStatusInfoText());
            if (transaction.getCtStatus().equals("finish")) {
                tvBookingCode.setVisibility(View.VISIBLE);
                tvBookingCode.setText("KODE BOOKING : " + transaction.getCtBookingCode());
            } else {
                tvBookingCode.setVisibility(View.GONE);
            }
            if (transaction.getRental() != null) {
                layRentCar.setVisibility(View.VISIBLE);
                tvCarName.setText(new StringBuilder("- ").append(transaction.getRental().getCcrName()));
                tvCarAmount.setText(CommonUtilities.toRupiahNumberFormat(transaction.getRental().getCcpAmount()));
                tvExtraAmount.setText(CommonUtilities.toRupiahNumberFormat(transaction.getRental().getCcrExtra()));
            } else {
                layRentCar.setVisibility(View.GONE);
            }
            if (!TextUtils.isEmpty(transaction.getCtNote())) {
                layNote.setVisibility(View.VISIBLE);
                tvNote.setText(transaction.getCtNote());
            } else {
                layNote.setVisibility(View.GONE);
            }
            if (transaction.getWisatas().size() > 0) {
                layWisata.setVisibility(View.VISIBLE);
                if (priceAdapter.getItemCount() > 0) {
                    priceAdapter.clear();
                }
                recyclerView2.setAdapter(priceAdapter);
                priceAdapter.setItems(transaction.getWisatas());
            } else {
                layWisata.setVisibility(View.GONE);
            }
            if (transaction.getPaket() != null) {
                layPaketWisata.setVisibility(View.VISIBLE);
                tvPaketWisataName.setText(new StringBuilder("- ").append(transaction.getPaket().getCpwName()));
                tvAmountPaketWisata.setText(CommonUtilities.toRupiahNumberFormat(transaction.getPaket().getCpwAmount()));
            } else {
                layPaketWisata.setVisibility(View.GONE);
            }

            tvTotalPrice.setText(CommonUtilities.toRupiahNumberFormat(transaction.getCtAmountSubtotal()));
            tvTotalPayment.setText(CommonUtilities.toRupiahNumberFormat(transaction.getCtAmountTotal()));


            tvPaymentMethod.setText(transaction.getPaymentMethodGroup());

            tvPemesan.setText(transaction.getContactName() + "\n" + transaction.getContactEmail() + "\n" + transaction.getContactPhone());

            tvPenjemputanAddress.setText(Str.getText(transaction.getPickupAddress(), ""));
            if (!TextUtils.isEmpty(transaction.getPickupNote())) {
                tvDepartNotes.setVisibility(View.VISIBLE);
                tvDepartNotes.setText(transaction.getPickupNote());
            } else {
                tvDepartNotes.setVisibility(View.GONE);
            }

            if (!transaction.getReturnLat().equals("0")) {
                layKepulangan.setVisibility(View.VISIBLE);
                tvKepulanganAddress.setText(Str.getText(transaction.getReturnAddress(), ""));
                if (!TextUtils.isEmpty(transaction.getReturnNote())) {
                    tvReturnNotes.setVisibility(View.VISIBLE);
                    tvReturnNotes.setText(transaction.getReturnNote());
                }
            } else {
                layKepulangan.setVisibility(View.GONE);
            }


            switch (transaction.getCtType()) {
                case "wisata":
                    tvAdultText.setText("Dewasa (Umur 12+)");
                    tvJmlKursiChild.setText(String.valueOf(transaction.getChVisitorChild()));
                    tvJmlKursiAdult.setText(String.valueOf(transaction.getChVisitorAdult()));
                    break;
                case "paket":
                    layChild.setVisibility(View.GONE);
                    layAdult.setVisibility(View.GONE);
                    break;
                case "rental":
                    layChild.setVisibility(View.GONE);
                    tvAdultText.setText("Jumlah Kursi");
                    tvJmlKursiAdult.setText(String.valueOf(transaction.getChVisitorAdult()));
                    break;
                default:
                    mListener.back();
                    break;
            }

            tvDate.setText(CommonUtilities.getFormatedDate(transaction.getCtBookingTime(), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy"));
            tvTime.setText(CommonUtilities.getFormatedDate(transaction.getCtBookingTime(), "yyyy-MM-dd HH:mm:ss", "HH:mm"));

            if (transaction.getCtStatus().equals("process")) {
                layActionBottom.setVisibility(View.VISIBLE);
            } else {
                layActionBottom.setVisibility(View.GONE);
            }
        } else {
            showToast(data.getText());
            mListener.back();
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh, bookingCode);
    }


    @OnClick({R.id.layDepart, R.id.layKepulangan, R.id.btnSubmit}) public void onViewClicked(View view) {
        String url;
        Intent i;
        switch (view.getId()) {
            case R.id.layDepart:
                url = "https://www.google.com/maps/dir/?api=1&destination=" + transaction.getPickupLat() + "," + transaction.getPickupLong();
                i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.layKepulangan:
                url = "https://www.google.com/maps/dir/?api=1&destination=" + transaction.getReturnLat() + "," + transaction.getReturnLong();
                i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                break;
            case R.id.btnSubmit:
                if (Validation.isEmpty(etBookingCode, "Kode Booking wajib diisi")) return;
                presenter.validationBooking(etBookingCode.getText().toString());
                break;
        }
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            showLoading("Loading...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void showError(String string) {
        showToast(string);
    }

    @Override public void onNextValidationBooking(RequestCarwis data) {
        if (data.getStatus()) {
            presenter.getPrefs().savePreferences(Config.CUSTOMER_SALDO, data.getCustomerSaldo());
            EventBus.getDefault().post(new EventUpdateProfile());
            EventBus.getDefault().post(new EventRefreshListCarwis());
            loadData(false);
        } else {
            showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", data.getText(), "OK", null, "validation_booking_false", true);
        }
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {

    }
}