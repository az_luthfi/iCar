package com.app.icarindo.modules.youtube;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.YouTubeFailureRecoveryActivity;
import com.app.icarindo.utility.Config;
import com.app.icarindo.utility.Logs;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;

/**
 * Created by MrCatz on 11/12/16.
 */

public class YoutubeActivity extends YouTubeFailureRecoveryActivity implements
        CompoundButton.OnCheckedChangeListener,
        YouTubePlayer.OnFullscreenListener {
    private static final String TAG = YoutubeActivity.class.getSimpleName();

    private static final int PORTRAIT_ORIENTATION = Build.VERSION.SDK_INT < 9
            ? ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
            : ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT;
    @BindView(R.id.player)
    YouTubePlayerView playerView;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.landscape_fullscreen_checkbox)
    CheckBox checkbox;
    @BindView(R.id.layout)
    RelativeLayout layout;

    private YouTubePlayer player;
    private String videoURL, titleVideo;
    private RelativeLayout.LayoutParams paramsNotFullscreen;

    private boolean fullscreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_youtube);
        ButterKnife.bind(this);


        Bundle extra = getIntent().getExtras();
        videoURL = "mkualZPRZCs";//extra.getString("url");
        titleVideo = extra.getString("title");
        title.setText(titleVideo);
        Logs.d(TAG, "onCreate ==> url => " + videoURL);

        checkbox.setOnCheckedChangeListener(this);

        playerView.initialize(Config.YOUTUBE_API_KEY, this);

        doLayout();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player,
                                        boolean wasRestored) {
        this.player = player;
        setControlsEnabled();
        player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
        player.setOnFullscreenListener(this);
        //player.setFullscreen(true);
        if (!wasRestored) {
            player.loadVideo(videoURL);
        }
    }

    @Override
    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return playerView;
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        int controlFlags = player.getFullscreenControlFlags();
        if (isChecked) {
            // If you use the FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE, your activity's normal UI
            // should never be laid out in landscape mode (since the video will be fullscreen whenever the
            // activity is in landscape orientation). Therefore you should set the activity's requested
            // orientation to portrait. Typically you would do this in your AndroidManifest.xml, we do it
            // programmatically here since this activity demos fullscreen behavior both with and without
            // this flag).
            setRequestedOrientation(PORTRAIT_ORIENTATION);
            controlFlags |= YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE;
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
            controlFlags &= ~YouTubePlayer.FULLSCREEN_FLAG_ALWAYS_FULLSCREEN_IN_LANDSCAPE;
        }
        player.setFullscreenControlFlags(controlFlags);
    }

    private void doLayout() {
        RelativeLayout.LayoutParams playerParams =
                (RelativeLayout.LayoutParams) playerView.getLayoutParams();
        if (fullscreen) {
            // When in fullscreen, the visibility of all other views than the player should be set to
            // GONE and the player should be laid out across the whole screen.
            playerParams.width = RelativeLayout.LayoutParams.MATCH_PARENT;
            playerParams.height = RelativeLayout.LayoutParams.MATCH_PARENT;

            title.setVisibility(View.GONE);
            checkbox.setVisibility(View.GONE);
        } else {

            title.setVisibility(View.VISIBLE);
            checkbox.setVisibility(View.GONE);
            if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                playerParams.width  = MATCH_PARENT;
                playerParams.height = WRAP_CONTENT;

            } else {
                playerParams.width = MATCH_PARENT;
                playerParams.height = WRAP_CONTENT;

            }
            setControlsEnabled();
        }
    }

    private void setControlsEnabled() {
        checkbox.setEnabled(player != null
                && getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT);
    }

    @Override
    public void onFullscreen(boolean isFullscreen) {
        fullscreen = isFullscreen;
        doLayout();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        doLayout();
        //setViewOnOrientation(newConfig);
    }



}
