package com.app.icarindo.modules.content.list;


import com.app.icarindo.models.content.RequestContent;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XContentListView extends MvpLceView<RequestContent> {

    void resetAdapter();

    void showLoadingNextPage(boolean show);

    void showErrorNextPage(String msg);

    void setNextData(RequestContent data);
}
