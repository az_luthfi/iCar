package com.app.icarindo.modules.training.materi;

import android.Manifest;
import android.app.Activity;
import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.training.RequestTraining;
import com.tbruyelle.rxpermissions2.RxPermissions;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;

public class MateriPresenter extends BaseRxLcePresenter<XMateriView, RequestTraining>
        implements XMateriPresenter {

    private RxPermissions rxPermissions;

    public MateriPresenter(Context context, Activity activity) {
        super(context);
        rxPermissions = new RxPermissions(activity);
    }

    @Override public void loadMateri(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "materi");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));

        subscribe(App.getService().apiTraining(params), pullToRefresh);
    }

    @Override public void requestPermissionsStorage() {
        rxPermissions
                .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(new Observer<Boolean>() {
                    @Override public void onSubscribe(Disposable d) {
                    }

                    @Override public void onNext(Boolean aBoolean) {
                        if (isViewAttached()) {
                            if (aBoolean) {
                                getView().onAllowStoragePermissions();
                            } else {
                                getView().onDeniedStoragePermissions();
                            }
                        }
                    }

                    @Override public void onError(Throwable e) {
                    }

                    @Override public void onComplete() {
                    }
                });
    }
}