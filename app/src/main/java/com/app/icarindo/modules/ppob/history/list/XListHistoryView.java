package com.app.icarindo.modules.ppob.history.list;

import com.app.icarindo.models.ppob.RequestHistory;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XListHistoryView extends MvpLceView<RequestHistory> {

}