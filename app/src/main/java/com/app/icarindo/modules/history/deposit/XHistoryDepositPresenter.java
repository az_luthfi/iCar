package com.app.icarindo.modules.history.deposit;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XHistoryDepositPresenter extends MvpPresenter<XHistoryDepositView> {

    void loadData(String filterDate, boolean pullToRefresh);

    void loadDataNextPage(String filterDate, int page);
}