package com.app.icarindo.modules.account.upgrade;

import com.app.icarindo.models.content.RequestContent;
import com.app.icarindo.models.network.RequestNetwork;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XUpgradePlanView extends MvpLceView<RequestNetwork> {

    void showProgressDialog(boolean show);

    void shoDialogTermAndCon(RequestContent data);

    void showError(String text);

    void onNextUpgradePlan(RequestNetwork data);
}