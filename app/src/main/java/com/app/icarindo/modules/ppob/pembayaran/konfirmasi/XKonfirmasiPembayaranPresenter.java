package com.app.icarindo.modules.ppob.pembayaran.konfirmasi;

import android.support.annotation.Nullable;

import com.app.icarindo.models.ppob.RequestInquiry;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XKonfirmasiPembayaranPresenter extends MvpPresenter<XKonfirmasiPembayaranView> {

    void inquiry(String idPelanggan, String noTelephone, String typeProduk, String idProduk, @Nullable String nominal);

    void payment(String idPelanggan, String noTelephone, String typeProduk, String pin, RequestInquiry responsInquiry);
}