package com.app.icarindo.modules.splash.login;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.customer.RequestCustomer;
import com.app.icarindo.models.eventbus.EventFacebookLogin;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Logs;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.content.ContentValues.TAG;
import static com.app.icarindo.utility.Config.CUSTOMER_REG_ID;
import static com.app.icarindo.utility.Config.NO_DATA;
import static com.app.icarindo.utility.Config.TRAINING;

public class LoginPresenter extends BasePresenter<XLoginView>
        implements XLoginPresenter {

    private CallbackManager callbackManager;
    private Activity activity;

    public LoginPresenter(Context context, Activity activity) {
        super(context);
        this.activity = activity;
    }

    @Override public void resetPassword(String emailReset) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "forget_pin_before_login");
        params.put("email", emailReset);

        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        Observer<RequestCustomer> observer = new Observer<RequestCustomer>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCustomer requestCustomer) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (requestCustomer.getStatus()) {
                        Toast.makeText(context, requestCustomer.getText(), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, requestCustomer.getText(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    Toast.makeText(context, context.getString(R.string.error_network_light), Toast.LENGTH_SHORT).show();
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiCustomer(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override public void requestLogin(String email, @Nullable String password, boolean isSocial) {
        HashMap<String, String> params = new HashMap<>();
        params.put("regId", getRegId());
        params.put("email", email);
        params.put("handset", CommonUtilities.getDeviceName());
        params.put("app_version", String.valueOf(CommonUtilities.getAppVersion(context)));
        params.put("email", email);
        if (!isSocial){
            params.put("act", "doLogin");
            params.put("pass", CommonUtilities.secure("doLogin", password));
        }else{
            params.put("act", "isRegister");
        }

        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        Observer<RequestCustomer> observer = new Observer<RequestCustomer>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCustomer requestCustomer) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (requestCustomer.getStatus()) {
                        prefs.savePreferences(NO_DATA, requestCustomer.getNoData());
                        prefs.savePreferences(TRAINING, requestCustomer.getTraining());
                        getView().onSuccessAuth(requestCustomer.getCustomer());
                    } else {
                        getView().showError(requestCustomer.getText());
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiCustomer(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    private String getRegId() {
        return prefs.getPreferencesString(CUSTOMER_REG_ID) == null ? "" : prefs.getPreferencesString(CUSTOMER_REG_ID);
    }

    @Override public void onActivityResultFacebook(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override public void facebookLogin() {
        callbackManager = CallbackManager.Factory.create();
        FacebookCallback<LoginResult> callback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Logs.d(TAG, " onSuccess ==> Login Autentikasi");
                getUserDetailsFromFB(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Logs.d(TAG, " onCancel ==> Login Cancelled");
            }

            @Override
            public void onError(FacebookException error) {
                Logs.d(TAG, "onError ==> Facebook login Error : " + error.getMessage());
            }
        };
        if (isViewAttached()) {
            LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList("public_profile", "email"));
        }
        LoginManager.getInstance().registerCallback(callbackManager, callback);
    }

    private void getUserDetailsFromFB(AccessToken accessToken) {
        GraphRequest req = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    requestLogin(object.getString("email"), null, true);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)");
        req.setParameters(parameters);
        req.executeAsync();
    }

    @Override public void attachView(XLoginView view) {
        super.attachView(view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventFacebookLogin(EventFacebookLogin event) {
        onActivityResultFacebook(event.getRequestCode(), event.getResultCode(), event.getData());
    }
}