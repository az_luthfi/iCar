package com.app.icarindo.modules.ppob.history.struck;

import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.RequestBase;

import java.util.HashMap;

public class StruckPPOBPresenter extends BaseRxLcePresenter<XStruckPPOBView, RequestBase>
        implements XStruckPPOBPresenter {

    public StruckPPOBPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(String transactionId) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("act", "struk");
        params.put("trxId", transactionId);
        params.put("type", "html");


        subscribe(App.getService().apiStruckRequest(params), false);
    }
}