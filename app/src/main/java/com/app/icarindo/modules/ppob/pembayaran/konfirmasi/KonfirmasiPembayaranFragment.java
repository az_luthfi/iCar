package com.app.icarindo.modules.ppob.pembayaran.konfirmasi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.ppob.InquiryDataAdapter;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.ppob.RequestInquiry;
import com.app.icarindo.models.ppob.RequestPay;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.modules.ppob.history.struck.StruckPPOBFragmentBuilder;
import com.app.icarindo.utility.CommonUtilities;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.app.icarindo.utility.Config.CUSTOMER_PIN;

@FragmentWithArgs
public class KonfirmasiPembayaranFragment extends BaseMvpLceFragment<NestedScrollView, RequestInquiry, XKonfirmasiPembayaranView, KonfirmasiPembayaranPresenter>
        implements XKonfirmasiPembayaranView, AlertListener, MainActivity.OnChangeToolbar {
    private static final String PAYMENT_SUCCESS = "success";
    private static final String INQUIRY = "inquiry";
    private static final String PAYMENT_ERROR = "error";
    @Arg String idPelanggan;
    @Arg(required = false) String noTelephone;
    @Arg(required = false) String idProduk;
    @Arg(required = false) String typeProduk;
    @Arg(required = false) String nominal;
    @Arg(required = false) String jsonProduk;
    @BindView(R.id.tvLabel) TextView tvLabel;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.layoutConfirm) CardView layoutConfirm;
    @BindView(R.id.etPin) MaterialEditText etPin;
    @BindView(R.id.layoutPin) TextInputLayout layoutPin;
    @BindView(R.id.btnSubmit) Button btnSubmit;
    @BindView(R.id.contentView) NestedScrollView contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.dataView) RelativeLayout dataView;
    @BindView(R.id.emptyView) TextView emptyView;
    private InquiryDataAdapter adapter;
    private RequestInquiry responsInquiry;
    private String struckData;

    public KonfirmasiPembayaranFragment() {

    }

    @Override public KonfirmasiPembayaranPresenter createPresenter() {
        return new KonfirmasiPembayaranPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_konfirmasi_pembayaran;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        adapter = new InquiryDataAdapter();
        recyclerView.setAdapter(adapter);

        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        e.printStackTrace();
        return getString(R.string.error_network);
    }

    @Override public void setData(RequestInquiry data) {
        responsInquiry = data;
        if (data.getStatus()) {
            dataView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            adapter.setItems(data.getData());
        } else {
            dataView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.inquiry(idPelanggan, noTelephone, typeProduk, idProduk, nominal);
    }

    @OnTextChanged(R.id.etPin) public void onTextChanged() {
        btnSubmit.setEnabled(etPin.getText().length() > 5);
    }

    @OnEditorAction(R.id.etPin) public boolean onActionGo(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_GO) {
            payNow();
        }
        return true;
    }

    @OnClick(R.id.btnSubmit) public void onViewClicked() {
        payNow();
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action) {
            case PAYMENT_SUCCESS:
                //show struck
                mListener.back();
                mListener.gotoPage(new StruckPPOBFragmentBuilder(responsInquiry.getTrxId()).struckHTML(struckData).build(),false,"Struck "+ responsInquiry.getTrxId());
                break;
            case PAYMENT_ERROR:
                payNow();
                break;
        }
    }

    private void payNow() {
        if (!CommonUtilities.md5(CommonUtilities.md5(etPin.getText().toString())).equalsIgnoreCase(presenter.getPrefs().getPreferencesString(CUSTOMER_PIN))) {
            showToast("Pin salah");
            return;
        }
        presenter.payment(idPelanggan, noTelephone, typeProduk, etPin.getText().toString(), responsInquiry);
    }

    @Override public void shoProgressDialog(boolean show) {
        if (show) {
            showLoading("Transaksi Anda sedang diproses...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void onErrorPayment(String contextString) {
        showSnackBar(contextString);
    }

    @Override public void successPayment(RequestPay requestPay) {
        struckData = requestPay.getDataStruk();
        showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Transaksi Berhasil", requestPay.getText(), "Lihat Struk", "Transaksi Lagi", PAYMENT_SUCCESS, false);
    }

    @Override public void onErrorPaymentStatus(String text) {
        showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Transaksi Gagal", text, "Coba Lagi", "Tutup", PAYMENT_ERROR, false);
    }

    @Nullable @Override public String getTitle() {
        return "Konfirmasi Pembayaran";
    }
}