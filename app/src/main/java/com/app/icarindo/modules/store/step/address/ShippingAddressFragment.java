package com.app.icarindo.modules.store.step.address;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.store.ShippingAdapter;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.store.CustomerShipping;
import com.app.icarindo.models.store.Product;
import com.app.icarindo.models.store.RequestShipping;
import com.app.icarindo.modules.store.product.detail.ProductDetailFragment;
import com.app.icarindo.modules.store.product.detail.ProductDetailFragmentBuilder;
import com.app.icarindo.modules.store.step.confirm.ConfirmFragmentBuilder;
import com.app.icarindo.modules.store.step.updateaddress.AddShippingAddressFragmentBuilder;
import com.app.icarindo.utility.FragmentStack;
import com.app.icarindo.utility.listview.ItemClickSupport;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@FragmentWithArgs
public class ShippingAddressFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestShipping, XShippingAddressView, ShippingAddressPresenter>
        implements XShippingAddressView, AlertListener, ShippingAdapter.ShippingListener, FragmentStack.OnBackPressedHandlingFragment {
    @Arg boolean isPurchase = false; // .modules.store.purchase
    @Arg(required = false) String jsonProduct;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.ivAddNew) ImageView ivAddNew;
    @BindView(R.id.addNewView) RelativeLayout addNewView;
    @BindView(R.id.itemsView) NestedScrollView itemsView;
    @BindView(R.id.btnCreate) Button btnCreate;
    @BindView(R.id.emptyView) LinearLayout emptyView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.container) FrameLayout container;
    @BindView(R.id.tvAddress) TextView tvAddress;
    @BindView(R.id.tvConfirm) TextView tvConfirm;
    @BindView(R.id.layHeaderPurchase) LinearLayout layHeaderPurchase;
    @BindView(R.id.btnShipping) Button btnShipping;
    @BindView(R.id.layBtnShipping) FrameLayout layBtnShipping;

    private ShippingAdapter adapter;
    private int deletePosition;
    private CustomerShipping shippingSelected;
    private Product product;

    public ShippingAddressFragment() {

    }

    @Override public ShippingAddressPresenter createPresenter() {
        return new ShippingAddressPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_shipping_address;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
        if (isPurchase) {
            layHeaderPurchase.setVisibility(View.VISIBLE);
            layBtnShipping.setVisibility(View.VISIBLE);
            tvAddress.setEnabled(true);
            product = new Gson().fromJson(jsonProduct, Product.class);
        } else {
            layHeaderPurchase.setVisibility(View.GONE);
            layBtnShipping.setVisibility(View.GONE);
        }
        adapter = new ShippingAdapter(this, isPurchase);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);

        recyclerView.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                if (isPurchase) {
                    if (shippingSelected != null) {
                        adapter.getItem(shippingSelected.getPositionAdapter()).setSelected(false);
                    }
                    adapter.getItem(position).setSelected(true);
                    adapter.setSelected(true);
                    adapter.notifyDataSetChanged();
                }

            }
        });
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        if (pullToRefresh) {
            return getContext().getString(R.string.error_network_light);
        }
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestShipping data) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
            adapter.setSelected(false);
        }
        if (data.getStatus()) {
            itemsView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            adapter.setItems(data.getCustomerShipping());
        } else {
            itemsView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.listShipping(pullToRefresh);
    }

    @OnClick({R.id.addNewView, R.id.btnCreate, R.id.btnShipping}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.addNewView:
            case R.id.btnCreate:
                mListener.gotoPage(new AddShippingAddressFragmentBuilder(adapter.getItemCount()).build(), false, null);
                break;
            case R.id.btnShipping:
                if (shippingSelected != null){
                    mListener.gotoPage(new ConfirmFragmentBuilder(new Gson().toJson(shippingSelected), jsonProduct).build(), false, null);
                }else{
                    showToast("Pilih alamat pengiriman");
                }
                break;
        }
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action) {
            case "delete":
                presenter.deleteShippingAddress(adapter.getItem(deletePosition).getShippingId());
                break;
        }
    }

    @Override public void onEdit(String jsonShipping) {
        mListener.gotoPage(new AddShippingAddressFragmentBuilder(adapter.getItemCount()).jsonAddress(jsonShipping).build(), false, null);
    }

    @Override public void onDelete(int position) {
        deletePosition = position;
        showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan!", "Apakah anda yakin akan menghapus alamat ini?", "Hapus", "Batal", "delete", true);
    }

    @Override public void onSelectedAddress(CustomerShipping shipping) {
        shippingSelected = shipping;
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            showLoading("Loading...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void showDialogError(String string) {
        final Snackbar snackbar = Snackbar.make(container, string, Snackbar.LENGTH_LONG);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override public boolean onBackPressed() {
        if (isPurchase) {
            mListener.gotoPage(new ProductDetailFragmentBuilder(product.getProductId()).build(), false, ProductDetailFragment.class.getSimpleName() + product.getProductId());
            return true;
        }
        return false;
    }
}