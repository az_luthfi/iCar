package com.app.icarindo.modules.home;

import com.app.icarindo.models.RequestHome;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XHomeView extends MvpLceView<RequestHome> {

}