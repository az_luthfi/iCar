package com.app.icarindo.modules.account.password;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XChangePasswordView extends MvpView {

    void showProgressDialog(boolean show);

    void showDialogError(String action, String message);

    void onSuccessChangePassword(String text);


    void onErrorPassword(String text);

    void onForgetPasswordSuccess(String text);
}