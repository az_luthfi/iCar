package com.app.icarindo.modules.training.parent;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.training.RequestTraining;
import com.app.icarindo.models.training.Soal;
import com.app.icarindo.modules.home.HomeFragment;
import com.app.icarindo.modules.training.page.TrainingPageFragmentBuilder;
import com.app.icarindo.utility.NonSwipeableViewPager;
import com.app.icarindo.utility.SmartFragmentStatePagerAdapter;

import java.util.ArrayList;

import butterknife.BindView;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class TrainingFragment extends BaseMvpLceFragment<FrameLayout, RequestTraining, XTrainingView, TrainingPresenter>
        implements XTrainingView, AlertListener {

    @BindView(R.id.contentView) FrameLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.viewPager) NonSwipeableViewPager viewPager;
    @BindView(R.id.emptyView) TextView emptyView;
//    @BindView(R.id.button_nav_prev) ImageButton buttonNavPrev;
//    @BindView(R.id.button_nav_next) ImageButton buttonNavNext;
    @BindView(R.id.page_number) Button pageNumber;

    private ArrayList<Soal> soals = new ArrayList<>();
    private TrainingPagerAdapter pagerAdapter;

    public TrainingFragment() {

    }

    @Override public TrainingPresenter createPresenter() {
        return new TrainingPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_training;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        if (pullToRefresh) {
            return getContext().getString(R.string.error_network_light);
        }
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestTraining data) {
        if (data.getStatus()) {
            soals = data.getSoal();
            viewPager.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
//            buttonNavNext.setVisibility(View.VISIBLE);
//            buttonNavPrev.setVisibility(View.VISIBLE);
            pageNumber.setVisibility(View.VISIBLE);
            setCurrentPageNumber(1);

            pagerAdapter = new TrainingPagerAdapter(getChildFragmentManager());
            viewPager.setAdapter(pagerAdapter);
            viewPager.setCurrentItem(0);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override public void onPageSelected(int position) {
//                    if (position < soals.size() - 1) {
//                        buttonNavNext.setVisibility(View.VISIBLE);
//                    } else {
//                        buttonNavNext.setVisibility(View.GONE);
//                    }
//
//                    if (position > 0) {
//                        buttonNavPrev.setVisibility(View.VISIBLE);
//                    } else {
//                        buttonNavPrev.setVisibility(View.GONE);
//                    }

                    setCurrentPageNumber(position + 1);
                }

                @Override public void onPageScrollStateChanged(int state) {

                }
            });

        } else {
            viewPager.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
//            buttonNavNext.setVisibility(View.GONE);
//            buttonNavPrev.setVisibility(View.GONE);
            pageNumber.setVisibility(View.GONE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadSoal(pullToRefresh);
    }

    private void setCurrentPageNumber(int position) {
        pageNumber.setText(position + " / " + soals.size());
    }

//    @OnClick({R.id.btnFinish, R.id.button_nav_prev, R.id.button_nav_next}) public void onViewClicked(View view) {
//        switch (view.getId()) {
//            case R.id.btnFinish:
//                validateAnswer();
//                break;
//            case R.id.button_nav_prev:
//                int prev = viewPager.getCurrentItem() - 1;
//                if (prev >= 0) {
//                    viewPager.setCurrentItem(prev, true);
//                }
//                break;
//            case R.id.button_nav_next:
//                int next = viewPager.getCurrentItem() + 1;
//                if (next < soals.size()) {
//                    viewPager.setCurrentItem(next, true);
//                }
//                break;
//        }
//    }

    private void validateAnswer() {
        int countWrong = 0;
        for (int i = 0; i < soals.size(); i++) {
            if (TextUtils.isEmpty(soals.get(i).getJawaban())){
                viewPager.setCurrentItem(i, true);
                return;
            }
            if (!soals.get(i).getJawaban().toLowerCase().equals(soals.get(i).getStBenar().toLowerCase())){
                countWrong += 1;
            }
        }
        if (countWrong > 0){
            showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Belum tepat", "Maaf anda belum berhasil menjawab semua pertanyaan dengan benar. " + String.valueOf(countWrong) + " jawaban anda belum benar.", "Ulanig", "Tutup", "wrong_answer", false);
        }else{
            presenter.saveAnswer(soals);
        }
    }

    public void setAnswer(int position, String jawaban) {
        if (position < soals.size() - 1) {
            viewPager.setCurrentItem(position + 1, true);
        }
        soals.get(position).setJawaban(jawaban);
        if (position == soals.size() -1){
            validateAnswer();
        }
    }

    @Override public void onCancel(String action) {
        switch (action){
            case "wrong_answer":
                mListener.back();
                break;
            case "success_answer":
                break;
        }
    }

    @Override public void onSubmit(String action) {
        switch (action){
            case "wrong_answer":
                loadData(false);
                break;
            case "success_answer":
                mListener.gotoPage(new HomeFragment(), false, null);
                break;
        }
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            showLoading("Loading...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void showError(String text) {
        final Snackbar snackbar = Snackbar.make(contentView, text, Snackbar.LENGTH_LONG);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override public void onSuccessAnswer(String text) {
        showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Selamat", text, "Tutup", null, "success_answer", false);
    }

    public class TrainingPagerAdapter extends SmartFragmentStatePagerAdapter {

        public TrainingPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override public Fragment getItem(int position) {
            return new TrainingPageFragmentBuilder(position, soals.get(position)).build();
        }

        @Override public int getCount() {
            return soals.size();
        }
    }
}