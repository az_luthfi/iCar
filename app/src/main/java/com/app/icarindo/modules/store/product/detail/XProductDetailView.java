package com.app.icarindo.modules.store.product.detail;

import com.app.icarindo.models.store.RequestProduct;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XProductDetailView extends MvpLceView<RequestProduct> {

}