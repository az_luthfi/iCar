package com.app.icarindo.modules.ppob.pembelian.pulsa;

import android.content.Intent;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XPulsaPresenter extends MvpPresenter<XPulsaView> {

    void loadProduct(String msisdn,String type);
    void getContact();
    void onActivityResult(int requestCode, int resultCode, Intent data);

}