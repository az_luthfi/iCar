package com.app.icarindo.modules.ppob.pembelian.voucher;

import android.content.Intent;

import com.app.icarindo.models.ppob.RequestProductCategory;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XVoucherView extends MvpLceView<RequestProductCategory> {
    void startActivityForResult(Intent pickContactIntent, int requestContact);

    void setPhoneNumber(String number);
}