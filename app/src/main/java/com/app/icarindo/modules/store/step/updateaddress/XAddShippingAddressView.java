package com.app.icarindo.modules.store.step.updateaddress;

import com.app.icarindo.models.datalist.SimpleList;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.ArrayList;

public interface XAddShippingAddressView extends MvpView {

    void showDialog(ArrayList<? extends SimpleList> data, String title);

    void showProgressDialog(boolean show);

    void showDialogSuccess(String text);

    void showError(String message);
}