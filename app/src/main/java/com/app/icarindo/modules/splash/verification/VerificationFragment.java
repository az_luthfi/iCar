package com.app.icarindo.modules.splash.verification;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.BankAdapter;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.models.customer.Customer;
import com.app.icarindo.models.ppob.Deposit;
import com.app.icarindo.modules.splash.verificationconfirm.VerificationConfirmFragment;
import com.app.icarindo.modules.splash.verificationconfirm.VerificationConfirmFragmentBuilder;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.listview.ItemClickSupport;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@FragmentWithArgs
public class VerificationFragment extends BaseMvpFragment<XVerificationView, VerificationPresenter>
        implements XVerificationView {
    @Arg String jsonCustomer;
    @BindView(R.id.tvPrice) TextView tvPrice;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.btnTransfer) Button btnTransfer;

    private BankAdapter adapter;
    private int positionSelected = -1;
    private Customer customer;

    public VerificationFragment() {

    }

    @Override public VerificationPresenter createPresenter() {
        return new VerificationPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_verification;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        customer = new Gson().fromJson(jsonCustomer, Customer.class);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new BankAdapter();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        tvPrice.setText(CommonUtilities.toRupiahFormat(customer.getRegisterPrice()));

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                if (!adapter.getItem(position).isSelected()){
                    if (positionSelected > -1){
                        adapter.getItem(positionSelected).setSelected(false);
                    }
                    adapter.getItem(position).setSelected(true);
                    adapter.notifyDataSetChanged();
                    positionSelected = position;
                }
            }
        });

        adapter.setItems(customer.getBanks());
    }

    @OnClick(R.id.btnTransfer) public void onViewClicked() {
        if (positionSelected > -1 && adapter != null && adapter.getItem(positionSelected) != null){
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
            String currentDateandTime = sdf.format(new Date());
            try {
                if (CommonUtilities.isTimeBetweenTwoTime("08:00:00", "20:00:00", currentDateandTime)) {
                    CommonUtilities.hideSoftKeyboard(getActivity());
                    presenter.depositRegister(customer.getProductCode(), adapter.getItem(positionSelected).getBankName(), customer.getCustomerId(), customer.getRegisterPrice());
                } else {
                    showConfirmDialog(SweetAlertDialog.WARNING_TYPE,"Informasi", "Deposit hanya dapat dilakukan pada jam 08:00 sampai 20:00 WIB","OK",null,"",true);
                }
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            showLoading("Loading...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void showError(String text) {
        showSnackBar(text);
    }

    @Override public void onSuccessDepositRegister(Deposit deposit) {
        VerificationConfirmFragment fragment = new VerificationConfirmFragmentBuilder(String.valueOf(customer.getCustomerId()), new Gson().toJson(deposit)).build();
        mListener.gotoPage(fragment, false, null);
    }

    @Override public void showErrorDepositRegister(String text) {
        showConfirmDialog(SweetAlertDialog.WARNING_TYPE,"Informasi", text,"OK",null,"",true);
    }
}