package com.app.icarindo.modules.absensi.extra;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.Validation;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class ExtraFragment extends BaseMvpLceFragment<FrameLayout, RequestNetwork, XExtraView, ExtraPresenter>
        implements XExtraView, AlertListener {

    @BindView(R.id.etDate) MaterialEditText etDate;
    @BindView(R.id.tvSubmit) TextView tvSubmit;
    @BindView(R.id.contentView) FrameLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.tvCountCanUseExtra) TextView tvCountCanUseExtra;
    @BindView(R.id.dataView) RelativeLayout dataView;
    @BindView(R.id.emptyView) TextView emptyView;

    public ExtraFragment() {

    }

    @Override public ExtraPresenter createPresenter() {
        return new ExtraPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_extra;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestNetwork data) {
        if (data.getStatus()) {
            dataView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            tvCountCanUseExtra.setText(new StringBuilder("Bonus Extra : ").append(String.valueOf(data.getCountCanUseExtra())).append(" hari"));
        } else {
            dataView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.getCountExtra(pullToRefresh);
    }

    @OnClick({R.id.etDate, R.id.tvSubmit}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etDate:
                Calendar calendar = Calendar.getInstance();
                Calendar minDate = Calendar.getInstance();
                minDate.add(Calendar.DAY_OF_MONTH, 1);
                if (!TextUtils.isEmpty(etDate.getText())){
                    Date date = CommonUtilities.getDateFromString(etDate.getText().toString(), "yyyy-MM-dd");
                    if (date != null){
                        calendar.setTime(date);
                    }
                }
                DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @SuppressLint("DefaultLocale") @Override public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        etDate.setText(year + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", dayOfMonth));
                    }
                };
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        getActivity(),
                        dateSetListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
                datePickerDialog.getDatePicker().setMinDate(minDate.getTimeInMillis());
                datePickerDialog.show();
                break;
            case R.id.tvSubmit:
                CommonUtilities.hideSoftKeyboard(getActivity());
                if (Validation.isEmpty(etDate, "Tanggal belum di pilih")) return;
                presenter.requestExtra(etDate.getText().toString());
                break;
        }
    }

    @Override public void showProgressDialog(boolean show) {
        if (show){
            showLoading("Loading..", false);
        }else{
            hideLoading();
        }
    }

    @Override public void showError(String text) {
        final Snackbar snackbar = Snackbar.make(contentView, text, Snackbar.LENGTH_LONG);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override public void onNextRequestExtra(RequestNetwork data) {
        if (data.getStatus()){
            showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Berhasil", data.getText(), "OK", null, "sucess-extra", false);
        }else{
            showConfirmDialog(SweetAlertDialog.ERROR_TYPE, "Gagal", data.getText(), "Tutup", null, "error-extra", true);
        }
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action){
            case "sucess-extra":
                mListener.back();
                break;
        }
    }
}