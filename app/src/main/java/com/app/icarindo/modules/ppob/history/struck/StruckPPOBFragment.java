package com.app.icarindo.modules.ppob.history.struck;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.models.RequestBase;
import com.app.icarindo.modules.main.MainActivity;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import butterknife.BindView;

@FragmentWithArgs
public class StruckPPOBFragment extends BaseMvpLceFragment<RelativeLayout, RequestBase, XStruckPPOBView, StruckPPOBPresenter>
        implements XStruckPPOBView, MainActivity.OnChangeToolbar {

    @Arg String transactionId;
    @Arg(required = false) String struckHTML;
    @BindView(R.id.webView) WebView webView;
    @BindView(R.id.contentView) RelativeLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    public StruckPPOBFragment() {

    }

    @Override public StruckPPOBPresenter createPresenter() {
        return new StruckPPOBPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_struck_ppob;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WebSettings ws = webView.getSettings();
        ws.setDefaultFontSize(16);
        ws.setBuiltInZoomControls(false);
        ws.setDisplayZoomControls(false);
        ws.setJavaScriptEnabled(true);
        if (struckHTML != null) {
            webView.loadData(struckHTML, "text/html", "UTF-8");
            showContent();
        } else {
            loadData(false);
        }
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestBase data) {
        webView.loadData(data.getText(), "text/html", "UTF-8");
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(transactionId);
    }

    @Nullable @Override public String getTitle() {
        return "Struk";
    }
}