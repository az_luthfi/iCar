package com.app.icarindo.modules.ppob.history.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.ppob.HistoryAdapter;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.models.ppob.HistoryFilterMonthly;
import com.app.icarindo.models.ppob.HistoryFilterYearly;
import com.app.icarindo.models.ppob.RequestHistory;
import com.app.icarindo.modules.ppob.history.detail.deposit.DetailDepositFragmentBuilder;
import com.app.icarindo.modules.ppob.history.detail.transaction.DetailTransactionFragmentBuilder;
import com.app.icarindo.utility.listview.DividerItemDecoration;
import com.app.icarindo.utility.listview.ItemClickSupport;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

public class ListHistoryFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestHistory, XListHistoryView, ListHistoryPresenter>
        implements XListHistoryView {
    private static final String FILTER_TYPE_ALL = "all";
    private static final String FILTER_TYPE_DEPOSIT = "deposit";
    private static final String FILTER_TYPE_TRANSACTION = "transaction";
    private static final String FILTER_TYPE_CASHBACK = "cashback";
    @BindView(R.id.ivTransaction) ImageView ivTransaction;
    @BindView(R.id.vDivider) View vDivider;
    @BindView(R.id.layFilter) LinearLayout layFilter;
    @BindView(R.id.tvTypeLabel) TextView tvTypeLabel;
    @BindView(R.id.spFilterYear) Spinner spFilterYear;
    @BindView(R.id.layFilterYear) FrameLayout layFilterYear;
    @BindView(R.id.tvFilterLabelMonth) TextView tvFilterLabelMonth;
    @BindView(R.id.spFilterMonth) Spinner spFilterMonth;
    @BindView(R.id.layFilterMonth) FrameLayout layFilterMonth;
    @BindView(R.id.tvLabelTotal) TextView tvLabelTotal;
    @BindView(R.id.tvTotalNominal) TextView tvTotalNominal;
    @BindView(R.id.tvLabelFilter) TextView tvLabelFilter;
    @BindView(R.id.tvFilterNominal) TextView tvFilterNominal;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.layFilterDate) LinearLayout layFilterDate;
    @BindView(R.id.layStatistic) RelativeLayout layStatistic;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.tvTitle) TextView tvTitle;

    private String filterType = FILTER_TYPE_TRANSACTION;
    private String filterDate = "";

    private HistoryAdapter adapter;

    private ArrayList<HistoryFilterYearly> historyFilterYearlies;
    private ArrayList<HistoryFilterMonthly> historyFilterMonthlies;

    private boolean firstLoad = true;

    public ListHistoryFragment() {

    }

    @Override public ListHistoryPresenter createPresenter() {
        return new ListHistoryPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_list_history;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new HistoryAdapter();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));
        recyclerView.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
        recyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                switch (adapter.getItem(position).getHistoryType().toLowerCase()) {
                    case "deposit":
                        mListener.gotoPage(new DetailDepositFragmentBuilder(adapter.getItem(position).getHistoryId()).build(), false, null);
                        break;
                    case "transaksi":
                    case "cashback":
                        mListener.gotoPage(new DetailTransactionFragmentBuilder(adapter.getItem(position).getHistoryId()).build(), false, null);
                        break;
                }
//                mListener.gotoPage(new ProfileFragmentBuilder(adapter.getItem(position).getCustomerId()).build(), false, null);
            }
        });
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        if (pullToRefresh) {
            return getContext().getString(R.string.error_network_light);
        }
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestHistory data) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        // reset Adapter
        layFilterDate.setVisibility(View.VISIBLE);
        if (filterType.equals(FILTER_TYPE_ALL)) {
            layStatistic.setVisibility(View.GONE);
        } else {
            layStatistic.setVisibility(View.VISIBLE);
        }
        if (historyFilterYearlies == null && data.getYearly().size() > 0) {
            historyFilterYearlies = new ArrayList<>(data.getYearly());
            setupSpinnerYear();
        }
        tvTotalNominal.setText(new StringBuilder("Rp ").append(data.getTotal()));
        tvFilterNominal.setText(new StringBuilder("Rp ").append(data.getTotalMonthly()));
        if (data.getStatus()) {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            switch (filterType) {
                case FILTER_TYPE_DEPOSIT:
                    tvLabelTotal.setText("Total Deposit");
                    break;
                case FILTER_TYPE_TRANSACTION:
                    tvLabelTotal.setText("Total Transaksi");
                    break;
                case FILTER_TYPE_CASHBACK:
                    tvLabelTotal.setText("Total Cashback");
                    break;
            }
            adapter.setItems(data.getHistories());
        } else {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    private void setupSpinnerYear() {
        String[] year = new String[historyFilterYearlies.size()];
        for (int i = 0; i < historyFilterYearlies.size(); i++) {
            year[i] = historyFilterYearlies.get(i).getText();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, year);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFilterYear.setAdapter(adapter);
        spFilterYear.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                setupSpinnerMonth(position);
            }

            @Override public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void setupSpinnerMonth(final int positionYearly) {
        historyFilterMonthlies = historyFilterYearlies.get(positionYearly).getHistoryFilterMonthlies();
        String[] month = new String[historyFilterMonthlies.size()];
        for (int i = 0; i < historyFilterMonthlies.size(); i++) {
            month[i] = historyFilterMonthlies.get(i).getText();
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_spinner_item, month);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spFilterMonth.setAdapter(adapter);
        spFilterMonth.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (!firstLoad) {
                    filterDate = historyFilterMonthlies.get(position).getLink();
                    loadData(false);
                }
                firstLoad = false;
                tvLabelFilter.setText(historyFilterMonthlies.get(position).getText() + " " + historyFilterYearlies.get(positionYearly).getText());
            }

            @Override public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(filterType, filterDate, pullToRefresh);
    }

    private void showMenuFilterType() {
        PopupMenu popupMenu = new PopupMenu(getContext(), layFilter);
        popupMenu.getMenuInflater().inflate(R.menu.filter_type_transaction_popup_menu, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.option_all:
                        filterType = FILTER_TYPE_ALL;
                        tvTitle.setText(new StringBuilder("SEMUA"));
                        loadData(false);
                        return true;
                    case R.id.option_depossit:
                        filterType = FILTER_TYPE_DEPOSIT;
                        tvTitle.setText(new StringBuilder("DEPOSIT"));
                        loadData(false);
                        return true;
                    case R.id.option_transaction:
                        filterType = FILTER_TYPE_TRANSACTION;
                        tvTitle.setText(new StringBuilder("TRANSAKSI"));
                        loadData(false);
                        return true;
                    case R.id.option_cashback:
                        filterType = FILTER_TYPE_CASHBACK;
                        tvTitle.setText(new StringBuilder("CASHBACK"));
                        loadData(false);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }

    @OnClick(R.id.layFilter) public void onViewClicked() {
        showMenuFilterType();
    }

}