package com.app.icarindo.modules.history.product;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.adapter.HistoryNonTransactionAdapter;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.store.RequestOrder;
import com.app.icarindo.modules.history.HistoryFragment;
import com.app.icarindo.modules.store.order.detail.OrderDetailFragment;
import com.app.icarindo.modules.store.order.detail.OrderDetailFragmentBuilder;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.listview.DividerItemDecoration;
import com.app.icarindo.utility.listview.EndlessRecyclerOnScrollListener;
import com.app.icarindo.utility.listview.ItemClickSupport;

import butterknife.BindView;

public class HistoryProductFragment extends BaseMvpLceFragment<SwipeRefreshLayout, RequestOrder, XHistoryProductView, HistoryProductPresenter>
        implements XHistoryProductView, ItemLoadingHolder.ItemViewLoadingListener {

    @BindView(R.id.tvSubheadMonth) TextView tvSubheadMonth;
    @BindView(R.id.tvNominalMonth) TextView tvNominalMonth;
    @BindView(R.id.tvNominalTotal) TextView tvNominalTotal;
    @BindView(R.id.layDividerFilter) FrameLayout layDividerFilter;
    @BindView(R.id.layFilter) FrameLayout layFilter;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private String filterDate = "";

    private HistoryNonTransactionAdapter adapter;
    private int maxCountItems;
    private EndlessRecyclerOnScrollListener scrollListener;

    public HistoryProductFragment() {

    }

    @Override public HistoryProductPresenter createPresenter() {
        return new HistoryProductPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_history_item;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        super.onViewCreated(view, savedInstanceState);
        adapter = new HistoryNonTransactionAdapter(this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                mListener.gotoPage(new OrderDetailFragmentBuilder(adapter.getItem(position).getHistoryId()).build(), false, OrderDetailFragment.class.getSimpleName() + adapter.getItem(position).getHistoryId());
            }
        });

        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    if (adapter.getItemCount() < maxCountItems) {
                        presenter.loadDataNextPage(filterDate, page);
                    }
                }
            }
        };

        recyclerView.addOnScrollListener(scrollListener);

        layDividerFilter.setVisibility(View.GONE);
        layFilter.setVisibility(View.GONE);
        tvSubheadMonth.setText(new StringBuilder("Produk"));
    }

    public void updateData() {
        String filterDateParent = ((HistoryFragment) getParentFragment()).getFilterDate();
        if (!filterDateParent.equals(filterDate)) {
            filterDate = filterDateParent;
            loadData(false);
        }
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestOrder data) {
        tvNominalMonth.setText(data.getNominalMonth());
        tvNominalTotal.setText(data.getNominalTotal());
        if (adapter.getItemCount() > 0){
            adapter.clear();
            scrollListener.resetState();
        }
        if (data.getStatus()){
            maxCountItems = data.getCount();
            adapter.setItems(data.getItemHistories());
        }else{
            maxCountItems = 0;
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(filterDate, pullToRefresh);
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadDataNextPage(filterDate, scrollListener.getCurrentPage());
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void setNextData(RequestOrder data) {
        if (data.getStatus()) {
            adapter.setItems(data.getItemHistories());
        }
    }

    @Override public void showErrorNextPage(String string) {
        Toast.makeText(getContext(), string, Toast.LENGTH_SHORT).show();
        adapter.setErrorNextPage("Tap to retry");
        scrollListener.backToPreviousPage();
    }
}