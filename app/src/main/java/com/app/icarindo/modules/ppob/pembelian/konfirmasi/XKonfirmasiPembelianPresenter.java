package com.app.icarindo.modules.ppob.pembelian.konfirmasi;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XKonfirmasiPembelianPresenter extends MvpPresenter<XKonfirmasiPembelianView> {

    void purchaseProcess(String customerNumber, String ppobProductId, String password);
}