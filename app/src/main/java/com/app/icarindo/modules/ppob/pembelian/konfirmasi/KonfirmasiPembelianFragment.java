package com.app.icarindo.modules.ppob.pembelian.konfirmasi;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.widget.AppCompatEditText;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.ppob.PpobProduct;
import com.app.icarindo.models.ppob.RequestPayment;
import com.app.icarindo.modules.deposit.add.DepositFragment;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.utility.CommonUtilities;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

import static cn.pedant.SweetAlert.SweetAlertDialog.SUCCESS_TYPE;
import static com.app.icarindo.utility.Config.CUSTOMER_PIN;
import static com.app.icarindo.utility.Config.CUSTOMER_SALDO;

@FragmentWithArgs
public class KonfirmasiPembelianFragment extends BaseMvpFragment<XKonfirmasiPembelianView, KonfirmasiPembelianPresenter>
        implements XKonfirmasiPembelianView, AlertListener, MainActivity.OnChangeToolbar {
    @Arg String productJson;
    @Arg String productCategory;
    @Arg String customerNumber;
    @BindView(R.id.ivProductImage) ImageView ivProductImage;
    @BindView(R.id.tvNoPelanggan) TextView tvNoPelanggan;
    @BindView(R.id.tvProductName) TextView tvProductName;
    @BindView(R.id.tvProductPrice) TextView tvProductPrice;
    @BindView(R.id.tvLabelMemberSaldo) TextView tvLabelMemberSaldo;
    @BindView(R.id.tvMemberSaldo) TextView tvMemberSaldo;
    @BindView(R.id.tvLabelTotalPayment) TextView tvLabelTotalPayment;
    @BindView(R.id.tvTotalPayment) TextView tvTotalPayment;
    @BindView(R.id.tvLabelSisaMemberSaldo) TextView tvLabelSisaMemberSaldo;
    @BindView(R.id.tvSisaMemberSaldo) TextView tvSisaMemberSaldo;
    @BindView(R.id.tvInfo) TextView tvInfo;
    @BindView(R.id.buttonAddDeposit) Button buttonAddDeposit;
    @BindView(R.id.rlActionDepositContainer) RelativeLayout rlActionDepositContainer;
    @BindView(R.id.etPin) AppCompatEditText etPin;
    @BindView(R.id.pinLayout) TextInputLayout pinLayout;
    @BindView(R.id.buttonBuy) Button buttonBuy;
    @BindView(R.id.rlActionContainer) RelativeLayout rlActionContainer;
    private PpobProduct product;

    public KonfirmasiPembelianFragment() {

    }

    @Override public KonfirmasiPembelianPresenter createPresenter() {
        return new KonfirmasiPembelianPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_konfirmasi_pembelian;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
        product = new Gson().fromJson(productJson, PpobProduct.class);

        int resId = getResources().getIdentifier(productCategory.replace("data","").trim(), "drawable", getContext().getApplicationInfo().packageName);
        int productPrice = Integer.parseInt(product.getPpobProductPrice()) + Integer.parseInt(product.getPpobProductModal());
        int memberSaldo = presenter.getPrefs().getPreferencesInt(CUSTOMER_SALDO);
        if (resId != 0){
            Picasso.with(getContext()).load(resId).into(ivProductImage);
        }

        tvProductName.setText(product.getPpobProductName());
        tvNoPelanggan.setText(customerNumber.replaceAll("(.{4})(?!$)", "$1-"));
        tvProductPrice.setText(CommonUtilities.toRupiahFormat(String.valueOf(productPrice)));
        tvMemberSaldo.setText(CommonUtilities.toRupiahFormat(memberSaldo));
        tvTotalPayment.setText(CommonUtilities.toRupiahFormat(productPrice));

        if(memberSaldo != 0){
            tvSisaMemberSaldo.setText(CommonUtilities.toRupiahFormat(String.valueOf(memberSaldo-productPrice)));
        }

        if (memberSaldo < productPrice) {
            rlActionDepositContainer.setVisibility(View.VISIBLE);
        } else {
            rlActionContainer.setVisibility(View.VISIBLE);
        }
    }

    @OnClick({R.id.buttonAddDeposit, R.id.buttonBuy}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonAddDeposit:
                mListener.gotoPage(new DepositFragment(), false, null);
                break;
            case R.id.buttonBuy:
                if (!CommonUtilities.md5(CommonUtilities.md5(etPin.getText().toString())).equalsIgnoreCase(presenter.getPrefs().getPreferencesString(CUSTOMER_PIN))) {
                    showSnackBar("Pin salah");
                }else{
                    presenter.purchaseProcess(customerNumber, product.getPpobProductId(), etPin.getText().toString());
                }
                break;
        }
    }

    @Override public void shoProgressDialog(boolean show) {
        if (show){
            showLoading("Transaksi Anda sedang diproses...", false);
        }else{
            hideLoading();
        }
    }

    @Override public void onError(String text) {
        showSnackBar(text);
    }

    @Override public void onSuccessPayment(RequestPayment data) {
        if (data.getUserStatus().equals("success")){
            showConfirmDialog(SUCCESS_TYPE,"Transaksi Berhasil", data.getText(),"OK",null,"success", false);
        }else if (data.getUserStatus().equals("pending")){
            showConfirmDialog(SUCCESS_TYPE,"Transaksi Pending", data.getText(),"OK",null,"success", false);
        }
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action){
            case "success":
                mListener.back();
                break;
        }
    }

    @Nullable @Override public String getTitle() {
        return "Konfirmasi Pembelian";
    }
}