package com.app.icarindo.modules.splash.verificationwaiting;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XVerificationWaitingPresenter extends MvpPresenter<XVerificationWaitingView> {

}