package com.app.icarindo.modules.store.product.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.store.ProductStoreAdapter;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.models.store.RequestProduct;
import com.app.icarindo.modules.store.product.detail.ProductDetailFragment;
import com.app.icarindo.modules.store.product.detail.ProductDetailFragmentBuilder;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.listview.DividerItemDecoration;
import com.app.icarindo.utility.listview.ItemClickSupport;

import butterknife.BindView;

public class ProductListFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestProduct, XProductListView, ProductListPresenter>
        implements XProductListView {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private ProductStoreAdapter adapter;


    public ProductListFragment() {

    }

    @Override public ProductListPresenter createPresenter() {
        return new ProductListPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_product_list;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new ProductStoreAdapter();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));
        recyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                mListener.gotoPage(new ProductDetailFragmentBuilder(adapter.getItem(position).getProductId()).build(), false, ProductDetailFragment.class.getSimpleName() + adapter.getItem(position).getProductId());
            }
        });

        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestProduct data) {
        if (data.getStatus()) {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            if (adapter.getItemCount() > 0) {
                adapter.clear();
            }
            adapter.setItems(data.getProducts());
        } else {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh);
    }
}