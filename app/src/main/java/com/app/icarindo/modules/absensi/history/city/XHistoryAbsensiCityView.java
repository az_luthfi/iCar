package com.app.icarindo.modules.absensi.history.city;

import com.app.icarindo.models.network.RequestNetwork;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XHistoryAbsensiCityView extends MvpLceView<RequestNetwork> {
    void showLoadingNextPage(boolean show);

    void setNextData(RequestNetwork data);

    void showErrorNextPage(String string);

}