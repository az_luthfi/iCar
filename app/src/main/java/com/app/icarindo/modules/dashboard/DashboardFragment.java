package com.app.icarindo.modules.dashboard;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.models.eventbus.EventDashboard;
import com.app.icarindo.modules.account.account.AccountFragment;
import com.app.icarindo.modules.home.HomeFragment;
import com.app.icarindo.modules.pesan.list.MessageListFragment;
import com.app.icarindo.modules.ppob.history.list.ListHistoryFragment;
import com.app.icarindo.utility.SmartFragmentStatePagerAdapter;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;

@FragmentWithArgs
public class DashboardFragment extends BaseMvpFragment<IDashboardView, DashboardPresenter> implements IDashboardView {
    @Arg int position;
    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.appBar) AppBarLayout appBar;
    @BindView(R.id.viewPager) ViewPager viewPager;


    private DashboardPagerAdapter pagerAdapter;

    public DashboardFragment() {
        // Required empty public constructor
    }

    @Override public DashboardPresenter createPresenter() {
        return new DashboardPresenter();
    }


    @Override protected int getLayoutRes() {
        return R.layout.fragment_dashboard;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pagerAdapter = new DashboardPagerAdapter(getChildFragmentManager(), getContext());
        viewPager.setAdapter(pagerAdapter);

        viewPager.setCurrentItem(position);
        tabLayout.setupWithViewPager(viewPager);
        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (tab != null) {
                tab.setCustomView(pagerAdapter.getTabView(i));
            }
        }
    }

    private class DashboardPagerAdapter extends SmartFragmentStatePagerAdapter {
        private Context context;
        private String tabTitle[] = new String[]{"Home", "History", "Help", "Account"};
        private Integer tabIcons[] = new Integer[]{
                R.drawable.tab_item_home,
                R.drawable.tab_item_history,
                R.drawable.tab_item_help,
                R.drawable.tab_item_account
        };


        DashboardPagerAdapter(FragmentManager fm, Context context) {
            super(fm);
            this.context = context;
        }

        @Override
        public int getCount() {
            return tabIcons.length;
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 1:
                    return new ListHistoryFragment();
                case 2:
                    return new MessageListFragment();
                case 3:
                    return new AccountFragment();
                default:
                    return new HomeFragment();
            }
        }

        View getTabView(int position) {
            View view = LayoutInflater.from(context).inflate(R.layout.tab_layout_item_home, null);
            ImageView imgItem = (ImageView) view.findViewById(R.id.ivTabImage);
            TextView tvTabTitle = (TextView) view.findViewById(R.id.tvTabTitle);
            imgItem.setImageResource(tabIcons[position]);
            tvTabTitle.setText(tabTitle[position]);
            return view;
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDashboardEvent(EventDashboard event) {
        if (event.getPosition() != -1 && viewPager != null) {
            viewPager.setCurrentItem(event.getPosition());
        }

    }

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override public void onDetach() {
        super.onDetach();
        EventBus.getDefault().unregister(this);
    }
}
