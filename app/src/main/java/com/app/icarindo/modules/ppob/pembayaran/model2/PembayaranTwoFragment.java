package com.app.icarindo.modules.ppob.pembayaran.model2;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.ImageButton;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.modules.ppob.pembayaran.konfirmasi.KonfirmasiPembayaranFragment;
import com.app.icarindo.modules.ppob.pembayaran.konfirmasi.KonfirmasiPembayaranFragmentBuilder;
import com.app.icarindo.utility.CommonUtilities;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static com.app.icarindo.utility.Config.PT_PLN_PASCA;

/**
 * Pembayaran PLN PASCA
 */
@FragmentWithArgs
public class PembayaranTwoFragment extends BaseMvpFragment<XPembayaranTwoView, PembayaranTwoPresenter>
        implements XPembayaranTwoView, MainActivity.OnChangeToolbar {
    @Arg String title;
    @BindView(R.id.etIdPelanggan) MaterialEditText etIdPelanggan;
    @BindView(R.id.btnSubmit) Button btnSubmit;
    @BindView(R.id.btnGetContact) ImageButton btnGetContact;

    public PembayaranTwoFragment() {

    }

    @Override public PembayaranTwoPresenter createPresenter() {
        return new PembayaranTwoPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_pembayaran_two;
    }


    @OnClick(R.id.btnSubmit) public void onViewClicked() {
        if (!etIdPelanggan.isCharactersCountValid()) {
            etIdPelanggan.setError("ID pelanggan tidak valid");
            etIdPelanggan.requestFocus();
            return;
        }
//        showToast("Masih dalam pengembangan");
        CommonUtilities.hideSoftKeyboard(getActivity());
        KonfirmasiPembayaranFragment fragment = new KonfirmasiPembayaranFragmentBuilder(etIdPelanggan.getText().toString())
                .typeProduk(PT_PLN_PASCA)
                .noTelephone("")
                .build();

        mListener.gotoPage(fragment, false, null);

        etIdPelanggan.setText(null);
    }

    @OnTextChanged(R.id.etIdPelanggan) public void onTextChanged() {
        btnSubmit.setEnabled(etIdPelanggan.getText().length() > 10);
    }

    @Nullable @Override public String getTitle() {
        return title;
    }


    @OnClick(R.id.btnGetContact) public void onContactClicked() {
        presenter.getContact();
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override public void setPhoneNumber(String number) {
        etIdPelanggan.setText(number);
    }
}