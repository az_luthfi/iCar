package com.app.icarindo.modules.account.password;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.utility.CommonUtilities;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.app.icarindo.utility.Config.MIN_LENGHT_PASSWORD;

public class ChangePasswordFragment extends BaseMvpFragment<XChangePasswordView, ChangePasswordPresenter>
        implements XChangePasswordView, AlertListener, MainActivity.OnChangeToolbar {
    @BindView(R.id.etPassword) MaterialEditText etPassword;
    @BindView(R.id.etNewPassword) MaterialEditText etNewPassword;
    @BindView(R.id.etConfirmNewPassword) MaterialEditText etConfirmNewPassword;
    @BindView(R.id.btnChangePassword) Button btnChangePassword;
    @BindView(R.id.tvForgetPassword) TextView tvForgetPassword;

    public ChangePasswordFragment() {

    }

    @Override public ChangePasswordPresenter createPresenter() {
        return new ChangePasswordPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_change_password;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
    }

    @OnClick({R.id.btnChangePassword, R.id.tvForgetPassword}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnChangePassword:
                validatePassword();
                break;
            case R.id.tvForgetPassword:
                showConfirmDialog(SweetAlertDialog.NORMAL_TYPE, "Lupa password?", "Password baru anda akan dikirim ke email anda", "Kirim", "Batal", ChangePasswordPresenter.ACT_FORGET_PASSWORD, true);
                break;
        }
    }

    private void validatePassword() {
        CommonUtilities.hideSoftKeyboard(getActivity());
        if (TextUtils.isEmpty(etPassword.getText())){
            etPassword.setError("Password saat ini wajib diisi");
            etPassword.requestFocus();
            return;
        }
        if (etPassword.length() < MIN_LENGHT_PASSWORD){
            etPassword.setError("Passsword minimal " + MIN_LENGHT_PASSWORD);
            etPassword.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(etNewPassword.getText())){
            etNewPassword.setError("Password baru wajib diisi");
            etNewPassword.requestFocus();
            return;
        }
        if (etNewPassword.length() < 6){
            etNewPassword.setError("Passsword minimal 6 karakter");
            etNewPassword.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(etConfirmNewPassword.getText())){
            etConfirmNewPassword.setError("Konfirmasi password baru wajib diisi");
            etConfirmNewPassword.requestFocus();
            return;
        }
        if (etConfirmNewPassword.length() < 6){
            etConfirmNewPassword.setError("Passsword minimal 6 karakter");
            etConfirmNewPassword.requestFocus();
            return;
        }

        if (!etNewPassword.getText().toString().equals(etConfirmNewPassword.getText().toString())){
            etConfirmNewPassword.setError("Konfirmasi Password baru tidak sama dengan password baru");
            etConfirmNewPassword.requestFocus();
            return;
        }

        presenter.requestChangePassword(etPassword.getText().toString(), etNewPassword.getText().toString());
    }

    @Override public void showProgressDialog(boolean show) {
        if (show){
            showLoading("Loading...", false);
        }else{
            hideLoading();
        }
    }

    @Override public void showDialogError(String action, String message) {
        showConfirmDialog(SweetAlertDialog.ERROR_TYPE, "Error!", message, "Ulangi", "Batal", action, false);
    }

    @Override public void onSuccessChangePassword(String text) {
        Toast.makeText(getContext(), text, Toast.LENGTH_LONG).show();
        mListener.back();
    }

    @Override public void onErrorPassword(String text) {
        etPassword.setError(text);
        etPassword.requestFocus();
    }

    @Override public void onForgetPasswordSuccess(String text) {
        showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Success", text, "OK", null, "success-forget", false);
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action){
            case ChangePasswordPresenter.ACT_CHANGE_PASSWORD:
                validatePassword();
                break;
            case ChangePasswordPresenter.ACT_FORGET_PASSWORD:
                presenter.requestForgetPassword();
                break;
        }
    }


    @Nullable @Override public String getTitle() {
        return "Ubah Password";
    }
}