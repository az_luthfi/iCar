package com.app.icarindo.modules.account.dataakun;

import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.utility.Config;

import java.util.HashMap;

public class DataAkunPresenter extends BaseRxLcePresenter<XDataAkunView, RequestNetwork>
        implements XDataAkunPresenter {

    public DataAkunPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "customer-akun");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));

        subscribe(App.getService().apiNetwork(params), pullToRefresh);
    }
}