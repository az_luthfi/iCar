package com.app.icarindo.modules.ppob.pembelian.pulsa;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.ppob.RequestProductCategory;
import com.app.icarindo.utility.Logs;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class PulsaPresenter extends BasePresenter<XPulsaView>
        implements XPulsaPresenter {

    private static final int REQUEST_CONTACT = 454;

    public PulsaPresenter(Context context) {
        super(context);
    }

    @Override public void loadProduct(String msisdn, String type) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("act", "getProduct");
        params.put("msisdn", msisdn);
        params.put("type", type);

        Logs.d("PulsaPresenter => loadProduct ==> " + params.toString());

        Observable<RequestProductCategory> observable = App.getService().apiPPOBProductCategory(params);
        Observer<RequestProductCategory> observer = new Observer<RequestProductCategory>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestProductCategory requestProductCategory) {
                if (requestProductCategory.getStatus()) {
                    if (getView() != null) {
                        getView().showProduct(requestProductCategory);
                    }
                }else{
                    getView().showError(requestProductCategory.getText());
                    getView().resetProduct();
                }
            }

            @Override public void onError(Throwable e) {

            }

            @Override public void onComplete() {

            }
        };

        observable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override public void getContact() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
        pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        getView().startActivityForResult(pickContactIntent, REQUEST_CONTACT);

    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case (REQUEST_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactUri = data.getData();
                    String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

                    @SuppressLint("Recycle") Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        // Retrieve the phone number from the NUMBER column
                        int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                        String number = cursor.getString(column);
                        number = number.replace("-", "");
                        number = number.replace("+", "");

                        if (number.substring(0,3).equalsIgnoreCase("620")) {
                            number = number.replaceFirst("(?:620)+", "0");
                        }else if (number.substring(0,2).equalsIgnoreCase("62")) {
                            number = number.replaceFirst("(?:62)+", "0");
                        }

                        number = number.replace(" ", "");
                        getView().setPhoneNumber(number);
                        Logs.d("FragmentPulsaPresenter => onActivityResult ==> " + number);
                        getView().checkMsisdn(number.substring(0, 4));
                    }


                }
                break;
        }
    }
}