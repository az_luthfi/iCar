package com.app.icarindo.modules.account.bank;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XBankAccountPresenter extends MvpPresenter<XBankAccountView> {

    void loadData(boolean pullToRefresh);

    void saveBank(String bankName, String bankNumber, String bankAccountName);
}