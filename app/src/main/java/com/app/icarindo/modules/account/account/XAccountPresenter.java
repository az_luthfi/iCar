package com.app.icarindo.modules.account.account;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XAccountPresenter extends MvpPresenter<XAccountView> {

    void loadDataAccount();
}