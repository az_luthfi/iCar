package com.app.icarindo.modules.training.parent;

import com.app.icarindo.models.training.RequestTraining;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XTrainingView extends MvpLceView<RequestTraining> {

    void showProgressDialog(boolean show);

    void showError(String text);

    void onSuccessAnswer(String text);
}