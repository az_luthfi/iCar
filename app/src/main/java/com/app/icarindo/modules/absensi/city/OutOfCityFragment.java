package com.app.icarindo.modules.absensi.city;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Validation;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class OutOfCityFragment extends BaseMvpFragment<XOutOfCityView, OutOfCityPresenter>
        implements XOutOfCityView, AlertListener {

    @BindView(R.id.etDate) MaterialEditText etDate;
    @BindView(R.id.etDesc) MaterialEditText etDesc;
    @BindView(R.id.tvSubmit) TextView tvSubmit;

    private Calendar minDate;

    public OutOfCityFragment() {

    }

    @Override public OutOfCityPresenter createPresenter() {
        return new OutOfCityPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_out_of_city;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
        minDate = Calendar.getInstance();
        minDate.add(Calendar.DAY_OF_MONTH, 1);
    }

    @OnClick({R.id.etDate, R.id.tvSubmit}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etDate:
                showDateDialog();
                break;
            case R.id.tvSubmit:
                CommonUtilities.hideSoftKeyboard(getActivity());
                if (Validation.isEmpty(etDate, "Tanggal wajib diisi")) return;
                if (Validation.isEmpty(etDesc, "Keterangan wajib diisi")) return;
                presenter.requestInput(etDate.getText().toString(), etDesc.getText().toString());
                break;
        }
    }

    private void showDateDialog(){
        Calendar calendar2 = Calendar.getInstance();
        if (!TextUtils.isEmpty(etDate.getText())){
            Date date = CommonUtilities.getDateFromString(etDate.getText().toString(), "yyyy-MM-dd");
            if (date != null){
                calendar2.setTime(date);
            }
        }
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("DefaultLocale") @Override public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                etDate.setText(year + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", dayOfMonth));
            }
        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                getActivity(),
                dateSetListener,
                calendar2.get(Calendar.YEAR),
                calendar2.get(Calendar.MONTH),
                calendar2.get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.getDatePicker().setMinDate(minDate.getTimeInMillis());
        datePickerDialog.show();
    }

    @Override public void showProgressDialog(boolean show) {
        if (show){
            showLoading("Loading...", false);
        }else{
            hideLoading();
        }
    }

    @Override public void onNextRequestInput(RequestNetwork data) {
        if (data.getStatus()){
            showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Berhasil", data.getText(), "OK", null, "success-input", false);
        }else{
            showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", data.getText(), "Tutup", null, "fale-input", false);
        }
    }

    @Override public void showError(String string) {
        showSnackBar(string);
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action){
            case "success-input":
                mListener.back();
                break;
        }
    }
}