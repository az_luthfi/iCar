package com.app.icarindo.modules.ppob.history.struck;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XStruckPPOBPresenter extends MvpPresenter<XStruckPPOBView> {

    void loadData(String transactionId);

}