package com.app.icarindo.modules.history.product;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XHistoryProductPresenter extends MvpPresenter<XHistoryProductView> {
    void loadData(String filterDate, boolean pullToRefresh);

    void loadDataNextPage(String filterDate, int page);
}