package com.app.icarindo.modules.account.edit;

import com.app.icarindo.models.customer.RequestCustomer;
import com.app.icarindo.models.datalist.SimpleList;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.ArrayList;

public interface XEditAccountView extends MvpLceView<RequestCustomer> {

    void showDialog(ArrayList<? extends SimpleList> data, String title);

    void imageFromGallery();

    void showLoadingUpdate(boolean show);

    void gotoBack();
}