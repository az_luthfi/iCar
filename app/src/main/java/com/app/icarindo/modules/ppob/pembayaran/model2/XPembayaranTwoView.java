package com.app.icarindo.modules.ppob.pembayaran.model2;

import android.content.Intent;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XPembayaranTwoView extends MvpView {
    void startActivityForResult(Intent pickContactIntent, int requestContact);

    void setPhoneNumber(String number);
}