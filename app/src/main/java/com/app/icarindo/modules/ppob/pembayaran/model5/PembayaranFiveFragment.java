package com.app.icarindo.modules.ppob.pembayaran.model5;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.ppob.PpobProductAdapter;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.models.ppob.PpobProduct;
import com.app.icarindo.models.ppob.PpobProductCategory;
import com.app.icarindo.models.ppob.RequestProductCategory;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.modules.ppob.pembayaran.konfirmasi.KonfirmasiPembayaranFragment;
import com.app.icarindo.modules.ppob.pembayaran.konfirmasi.KonfirmasiPembayaranFragmentBuilder;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Logs;
import com.app.icarindo.utility.listview.DividerItemDecoration;
import com.app.icarindo.utility.listview.ItemClickSupport;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static com.app.icarindo.utility.Config.HP_PASCA;
import static com.app.icarindo.utility.Config.TV_CABLE;

/**
 * Pembayaran TV CABLE
 */
@FragmentWithArgs
public class PembayaranFiveFragment extends BaseMvpLceFragment<RelativeLayout, RequestProductCategory, XPembayaranFiveView, PembayaranFivePresenter>
        implements XPembayaranFiveView, MainActivity.OnChangeToolbar {
    private static final int REQUEST_CONTACT_ID = 455;
    @Arg String title;
    @Arg String type;
    @BindView(R.id.etIdPelanggan) MaterialEditText etIdPelanggan;
    @BindView(R.id.etJenisProduct) MaterialEditText etJenisProduct;
    @BindView(R.id.etProduct) MaterialEditText etProduct;
    @BindView(R.id.btnSubmit) Button btnSubmit;
    @BindView(R.id.dataView) LinearLayout dataView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) RelativeLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private ArrayList<PpobProductCategory> ppobProductCategory = new ArrayList<>();
    private List<PpobProduct> productList;
    private PpobProduct productSelected;

    public PembayaranFiveFragment() {

    }

    @Override public PembayaranFivePresenter createPresenter() {
        return new PembayaranFivePresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_pembayaran_five;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getString(R.string.error_network);
    }

    @Override public void setData(RequestProductCategory data) {
        if (data.getStatus()) {
            dataView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            etProduct.setClickable(true);
            ppobProductCategory = data.getPpobProductCategories();
        } else {
            dataView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(type);
    }

    @Nullable @Override public String getTitle() {
        return title;
    }

    @OnClick({R.id.etJenisProduct, R.id.etProduct, R.id.btnSubmit, R.id.btnGetContactIdPel}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etJenisProduct:
                registerForContextMenu(etJenisProduct);
                etJenisProduct.showContextMenu();
                break;
            case R.id.etProduct:
                showDialog();
                break;
            case R.id.btnSubmit:
                if (validateInput()) {
                    CommonUtilities.hideSoftKeyboard(getActivity());
                    KonfirmasiPembayaranFragment fragment = new KonfirmasiPembayaranFragmentBuilder(etIdPelanggan.getText().toString())
                            .idProduk(productSelected.getPpobProductId())
                            .typeProduk(TV_CABLE)
                            .build();

                    etIdPelanggan.setText(null);
                    mListener.gotoPage(fragment, false, "Konfirmasi Pembayaran");
                }
                break;
            case R.id.btnGetContactIdPel:
                Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
                pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(pickContactIntent, REQUEST_CONTACT_ID);
                break;
        }
    }

    private boolean validateInput() {

        if (TextUtils.isEmpty(etIdPelanggan.getText())) {
            etIdPelanggan.setError("Silahkan masukkan ID Pelanggan");
            etIdPelanggan.requestFocus();
            return false;
        }
        return true;
    }

    @OnTextChanged(R.id.etIdPelanggan) public void onTextChanged() {
        btnSubmit.setEnabled(etIdPelanggan.getText().length() > 4 && productSelected != null);
    }

    @Override public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        for (int i = 0; i < ppobProductCategory.size(); i++) {
            menu.setHeaderTitle("Pilih Voucher");
            menu.add(0, i, i, ppobProductCategory.get(i).getCatName());
        }
    }

    @Override public boolean onContextItemSelected(MenuItem item) {
        productList = ppobProductCategory.get(item.getItemId()).getPpobProducts();
        etJenisProduct.setText(ppobProductCategory.get(item.getItemId()).getCatName());
        productSelected = null;
        etProduct.setText("");
        return true;
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(getActivity(), android.R.style.DeviceDefault_Light_ButtonBar);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setContentView(R.layout.dialog_ppob_product_chooser);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
        EditText searchEditText = (EditText) dialog.findViewById(R.id.tvSearch);

        final PpobProductAdapter adapter = new PpobProductAdapter();
        adapter.setType(HP_PASCA);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));
        adapter.addAll(productList);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                productSelected = adapter.getItem(position);
                etProduct.setText(productSelected.getPpobProductName());
                btnSubmit.setEnabled(etIdPelanggan.getText().length() > 5 && productSelected != null);
                dialog.dismiss();
            }
        });

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Logs.d("FragmentPayment => onTextChanged ==> " + s);
                adapter.filter(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        dialog.show();
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CONTACT_ID) {
            if (resultCode == Activity.RESULT_OK) {
                String number = CommonUtilities.getNumberContact(getContext(), data.getData());
                if (!TextUtils.isEmpty(number)) {
                    etIdPelanggan.setText(number);
                }
            }
        }
    }
}