package com.app.icarindo.modules.store.step.updateaddress;

import android.content.Context;
import android.text.TextUtils;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.datalist.Province;
import com.app.icarindo.models.eventbus.EventShippingAddress;
import com.app.icarindo.models.store.City;
import com.app.icarindo.models.store.RequestShipping;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;


public class AddShippingAddressPresenter extends BasePresenter<XAddShippingAddressView>
        implements XAddShippingAddressPresenter {

    private ArrayList<Province> provinces = null;
    private ArrayList<City> cities = null;

    private boolean isLoad = false;

    public AddShippingAddressPresenter(Context context) {
        super(context);
    }

    @Override public void loadProvince() {
        if (!isLoad){
            if (provinces == null) {
                isLoad = true;
                if (isViewAttached()) {
                    getView().showProgressDialog(true);
                }
                HashMap<String, String> params = new HashMap<>();
                params.put("customer_id", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
                params.put("act", "province");

                Observer<ArrayList<Province>> observer = new Observer<ArrayList<Province>>() {
                    @Override public void onSubscribe(Disposable d) {
                    }

                    @Override public void onNext(ArrayList<Province> provinces) {
                        onProvinceUpdate(provinces);
                        isLoad = false;
                        if (isViewAttached()) {
                            getView().showProgressDialog(false);
                        }
                    }

                    @Override public void onError(Throwable e) {
                        isLoad = false;
                        if (isViewAttached()) {
                            getView().showProgressDialog(false);
                        }
                    }

                    @Override public void onComplete() {

                    }
                };

                App.getService().apiShippingProvince(params)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(observer);
            } else {
                onProvinceUpdate(provinces);
            }
        }
    }

    @Override public void loadCity(String provinceId) {
        if (!TextUtils.isEmpty(provinceId) && !isLoad) {
            if (cities == null) {
                isLoad = true;
                if (isViewAttached()) {
                    getView().showProgressDialog(true);
                }
                HashMap<String, String> params = new HashMap<>();
                params.put("customer_id", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
                params.put("provinceId", provinceId);
                params.put("act", "city");


                Observer<ArrayList<City>> observer = new Observer<ArrayList<City>>() {
                    @Override public void onSubscribe(Disposable d) {
                    }

                    @Override public void onNext(ArrayList<City> cities) {
                        onCityUpdate(cities);
                        isLoad = false;
                        if (isViewAttached()) {
                            getView().showProgressDialog(false);
                        }
                    }

                    @Override public void onError(Throwable e) {
                        isLoad = false;
                        if (isViewAttached()) {
                            getView().showProgressDialog(false);
                        }
                    }

                    @Override public void onComplete() {

                    }
                };

                App.getService().apiShippingCity(params)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(observer);
            } else {
                onCityUpdate(cities);
            }
        }
    }


    @Override public void resetCity() {
        this.cities = null;
    }

    @Override public void saveAddress(HashMap<String, String> params, String action) {
        params.put("customer_id", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("act", action);
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        Observer<RequestShipping> observer = new Observer<RequestShipping>() {

            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestShipping data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (data.getStatus()) {
                        getView().showDialogSuccess(data.getText());
                        EventBus.getDefault().post(new EventShippingAddress());
                    } else {
                        Toast.makeText(context, data.getText(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiShipping(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    private void onProvinceUpdate(ArrayList<Province> provinces) {
        this.provinces = provinces;
        if (isViewAttached()) {
            getView().showDialog(provinces, "Provinsi");
        }
    }

    private void onCityUpdate(ArrayList<City> cities) {
        this.cities = cities;
        if (isViewAttached()) {
            getView().showDialog(cities, "Kabupaten");
        }
    }
}