package com.app.icarindo.modules.store.order.detail;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.store.RequestOrder;
import com.app.icarindo.utility.Config;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class OrderDetailPresenter extends BaseRxLcePresenter<XOrderDetailView, RequestOrder>
        implements XOrderDetailPresenter {

    public OrderDetailPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(String orderId, boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "orderDetail");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("orderId", orderId);

        subscribe(App.getService().apiOrder(params), pullToRefresh);
    }

    @Override public void cancelOrder(String orderId) {
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("orderId", orderId);
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("act", "canceledOrder");

        Observer<RequestOrder> observer = new Observer<RequestOrder>() {

            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestOrder data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (data.getStatus()){
                        prefs.savePreferences(Config.CUSTOMER_SALDO, data.getCustomerSaldo());
                    }
                    getView().setData(data);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiOrder(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}