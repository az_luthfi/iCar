package com.app.icarindo.modules.absensi.history;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseFragment;
import com.app.icarindo.modules.absensi.history.city.HistoryAbsensiCityFragment;
import com.app.icarindo.modules.absensi.history.extra.HistoryExtraFragment;
import com.app.icarindo.modules.absensi.history.ijin.HistoryIjinFragment;
import com.app.icarindo.utility.SmartFragmentStatePagerAdapter;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryAbsensiFragment extends BaseFragment {


    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;

    public HistoryAbsensiFragment() {
        // Required empty public constructor
    }


    @Override protected int getLayoutRes() {
        return R.layout.fragment_with_tab_layout;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AbsensiViewPager pagerAdapter = new AbsensiViewPager(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(0);
        tabLayout.setupWithViewPager(viewPager);
//        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
    }

    private class AbsensiViewPager extends SmartFragmentStatePagerAdapter {
        private String tabTitles[] =  new String[]{"EXTRA", "IJIN", "KELUAR KOTA"};

        public AbsensiViewPager(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override public Fragment getItem(int position) {
            switch (position) {
                case 1:
                    return new HistoryIjinFragment();
                case 2:
                    return new HistoryAbsensiCityFragment();
                default:
                    return new HistoryExtraFragment();
            }
        }

        @Override public int getCount() {
            return tabTitles.length;
        }

        @Override public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }
}
