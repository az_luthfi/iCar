package com.app.icarindo.modules.absensi.history.ijin;

import com.app.icarindo.models.network.RequestNetwork;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XHistoryIjinView extends MvpLceView<RequestNetwork> {
    void showLoadingNextPage(boolean show);

    void setNextData(RequestNetwork data);

    void showErrorNextPage(String string);

    void onSuccessCancel();

    void showProgressDialog(boolean show);

    void showError(String text);
}