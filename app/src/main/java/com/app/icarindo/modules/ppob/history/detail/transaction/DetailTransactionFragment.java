package com.app.icarindo.modules.ppob.history.detail.transaction;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.models.ppob.RequestHistory;
import com.app.icarindo.models.ppob.Transaction;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.modules.ppob.history.struck.StruckPPOBFragmentBuilder;
import com.app.icarindo.utility.CommonUtilities;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class DetailTransactionFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestHistory, XDetailTransactionView, DetailTransactionPresenter>
        implements XDetailTransactionView, MainActivity.OnChangeToolbar {
    @Arg String transId;
    @BindView(R.id.tvProductName) TextView tvProductName;
    @BindView(R.id.tvTrxId) TextView tvTrxId;
    @BindView(R.id.tvTransactionDate) TextView tvTransactionDate;
    @BindView(R.id.ivIcon) ImageView ivIcon;
    @BindView(R.id.ivCopy) ImageView ivCopy;
    @BindView(R.id.tvCustomerNumber) TextView tvCustomerNumber;
    @BindView(R.id.tvProductType) TextView tvProductType;
    @BindView(R.id.tvTransactionStatus) TextView tvTransactionStatus;
    @BindView(R.id.tvTansactionPrice) TextView tvTansactionPrice;
    @BindView(R.id.tvCashback) TextView tvCashback;
    @BindView(R.id.tvTotalPayment) TextView tvTotalPayment;
    @BindView(R.id.tvInfoText) TextView tvInfoText;
    @BindView(R.id.rl_view_struct) RelativeLayout rlViewStruct;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.dataView) RelativeLayout dataView;

    private Transaction transaction;
    private String productType;
    public DetailTransactionFragment() {

    }

    @Override public DetailTransactionPresenter createPresenter() {
        return new DetailTransactionPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_detail_transaction;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        registerForContextMenu(ivCopy);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        if (pullToRefresh) {
            return getContext().getString(R.string.error_network_light);
        }
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestHistory data) {
        if (data.getStatus()) {
            transaction = data.getTransaction();
            dataView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            if (!TextUtils.isEmpty(transaction.getPpobProductImage())){
                Picasso.with(getContext()).load(transaction.getPpobProductImage()).into(ivIcon, new Callback() {
                    @Override public void onSuccess() {
                        ivIcon.setVisibility(View.VISIBLE);
                    }

                    @Override public void onError() {

                    }
                });
            }
            tvTrxId.setText(new StringBuilder("Trx. ID : ").append(transaction.getTransTrxId()));
            tvTransactionDate.setText(transaction.getTransCreateDate());
            tvProductType.setText(transaction.getPpobProductType().toUpperCase());
            tvProductName.setText(transaction.getPpobProductName());
            tvCustomerNumber.setText(transaction.getTransCustomerNumber());
            tvTansactionPrice.setText(CommonUtilities.toRupiahFormat(transaction.getTransPrice()));
            tvCashback.setText(CommonUtilities.toRupiahFormat(transaction.getTransFee()));
            tvTransactionStatus.setText(transaction.getTransStatus().toUpperCase());
            tvInfoText.setText(transaction.getTransResponNote());
            tvTotalPayment.setText(CommonUtilities.toRupiahFormat(transaction.getTotal()));
            switch (transaction.getTransStatus().toLowerCase()) {
                case "pending":
                    tvTransactionStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.status_pending));
                    break;
                case "failed":
                    tvTransactionStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.status_cancel));
                    break;
                case "success":
                    tvTransactionStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.status_success));
                    break;
                default:
                    tvTransactionStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.status_new));
                    break;
            }
            if (transaction.getTransType().equalsIgnoreCase("pembayaran") && transaction.getTransStatus().equalsIgnoreCase("success")) {

                switch (transaction.getPpobProductType().toLowerCase()) {
                    case "pln_pascabayar":
                        productType = "Pembayaran PLN Pasca Bayar";
                        break;
                    case "pln_prabayar":
                        productType = "Pembelian Token PLN";
                        break;
                    case "telkom":
                        productType = "Pembayaran Telkom / Speedy";
                        break;
                    case "multi_finance":
                        productType = "Pembayaran Finance";
                        break;
                    case "pdam":
                    case "pdam_surabaya":
                        productType = "Pembayaran PDAM";
                        break;
                    case "telepon_pascabayar":
                        productType = "Pembayaran HP Pasca Bayar";
                        break;
                    case "bpjs":
                        productType = "Pembayaran BPJS Kesehatan";
                        break;
                    default:
                        if (transaction.getPpobProductType().contains("tv_")) {
                            productType = "Pembayaran TV Cable";
                        }
                        break;
                }
                if (productType != null) {
                    rlViewStruct.setVisibility(View.VISIBLE);
                    tvProductType.setText(productType);
                }
            }
        } else {
            dataView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh, transId);
    }

    @OnClick({R.id.ivCopy, R.id.rl_view_struct}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivCopy:
                ivCopy.showContextMenu();
                break;
            case R.id.rl_view_struct:
                mListener.gotoPage(new StruckPPOBFragmentBuilder(transaction.getTransTrxId()).build(), false, "Struck "+ transaction.getTransTrxId());
                break;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.context_menu_copy, menu);
        menu.findItem(R.id.copy_token).setVisible(false);
        menu.findItem(R.id.copy_trx_id).setVisible(true);
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip;
        switch (item.getItemId()) {
            case R.id.copy_trx_id:
                clip = ClipData.newPlainText(tvTrxId.getText(), tvTrxId.getText());
                clipboard.setPrimaryClip(clip);
                showToast("Trx Id telah disalin");
                return true;
            case R.id.copy_id_pelanggan:
                clip = ClipData.newPlainText(tvCustomerNumber.getText(), tvCustomerNumber.getText());
                clipboard.setPrimaryClip(clip);
                showToast("ID Pelanggan telah disalin");
                return true;
            default:
                return true;
        }
    }

    @Nullable @Override public String getTitle() {
        return "Detail Transaksi";
    }
}