package com.app.icarindo.modules.splash.verification;

import com.app.icarindo.models.ppob.Deposit;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XVerificationView extends MvpView {

    void showProgressDialog(boolean show);

    void showError(String text);

    void onSuccessDepositRegister(Deposit deposit);

    void showErrorDepositRegister(String text);
}