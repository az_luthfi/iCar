package com.app.icarindo.modules.pesan.list;

import com.app.icarindo.models.pesan.Message;
import com.app.icarindo.models.pesan.RequestMessage;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XMessageListView extends MvpLceView<RequestMessage> {

    void showLoadingNextPage(boolean show);

    void showErrorNextPage(String msg);

    void setNextData(RequestMessage data);

    void addNewItem(Message message);
}