package com.app.icarindo.modules.home;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XHomePresenter extends MvpPresenter<XHomeView> {

    void loadData(boolean pullToRefresh);
}