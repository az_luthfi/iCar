package com.app.icarindo.modules.splash.verificationconfirm;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.customer.RequestCustomer;
import com.app.icarindo.models.ppob.Deposit;
import com.app.icarindo.modules.splash.BaseSplashFragment;
import com.app.icarindo.modules.splash.verificationwaiting.VerificationWaitingFragment;
import com.app.icarindo.utility.CommonUtilities;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;

@FragmentWithArgs
public class VerificationConfirmFragment extends BaseSplashFragment<XVerificationConfirmView, VerificationConfirmPresenter>
        implements XVerificationConfirmView {
    @Arg String jsonDeposit;
    @Arg String customerId;
    @BindView(R.id.ivProductImage) ImageView ivProductImage;
    @BindView(R.id.tvNominalDeposit) TextView tvNominalDeposit;
    @BindView(R.id.tvBankName) TextView tvBankName;
    @BindView(R.id.tvBankAccountName) TextView tvBankAccountName;
    @BindView(R.id.tvBankAccountNumber) TextView tvBankAccountNumber;
    @BindView(R.id.etPesan) AppCompatEditText etPesan;
    private Deposit deposit;
    public VerificationConfirmFragment() {

    }

    @Override public VerificationConfirmPresenter createPresenter() {
        return new VerificationConfirmPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_verification_confirm;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deposit = new Gson().fromJson(jsonDeposit, Deposit.class);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (deposit != null) {
            tvNominalDeposit.setText(CommonUtilities.toRupiahFormat(CommonUtilities.toPlain(deposit.getDepositAmount())));
            tvBankName.setText(deposit.getDepositBankTo());
            tvBankAccountName.setText(deposit.getDepositBankToAcc());
            tvBankAccountNumber.setText(deposit.getDepositBankToNumber());
            if (!TextUtils.isEmpty(deposit.getBank().getBankImage())){
                Picasso.with(getContext()).load(deposit.getBank().getBankImage()).into(ivProductImage);
            }
        }
    }

    @OnClick({R.id.buttonConfirmDeposit}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonConfirmDeposit:
                presenter.confirmDepositRegister(deposit.getDepositId(),etPesan.getText().toString(), customerId);
                break;
        }
    }

    @Override public void showProgressDialog(boolean show) {
            if (show) {
                showLoading("Loading...", false);
            } else {
                hideLoading();
            }
    }

    @Override public void showError(String text) {
        showSnackBar(text);
    }

    @Override public void onDepositRegisterConfirm(RequestCustomer requestCustomer) {
        if (requestCustomer.getStatus()){
            splashListener.OnSuccessAuth(requestCustomer.getCustomer());
        }else{
            mListener.gotoPage(new VerificationWaitingFragment(), false, null);
        }
    }

}