package com.app.icarindo.modules.ppob.history.detail.deposit;

import com.app.icarindo.models.ppob.RequestHistory;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XDetailDepositView extends MvpLceView<RequestHistory> {

}