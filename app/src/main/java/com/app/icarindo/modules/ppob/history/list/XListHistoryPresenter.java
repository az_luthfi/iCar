package com.app.icarindo.modules.ppob.history.list;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XListHistoryPresenter extends MvpPresenter<XListHistoryView> {

    void loadData(String filterType, String filterDate, boolean pullToRefresh);
}