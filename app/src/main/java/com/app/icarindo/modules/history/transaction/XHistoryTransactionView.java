package com.app.icarindo.modules.history.transaction;

import com.app.icarindo.models.ppob.RequestHistory;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XHistoryTransactionView extends MvpLceView<RequestHistory> {
    void showLoadingNextPage(boolean show);

    void setNextData(RequestHistory data);

    void showErrorNextPage(String string);
}