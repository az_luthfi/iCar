package com.app.icarindo.modules.history.withdraw;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XHistoryWithdrawPresenter extends MvpPresenter<XHistoryWithdrawView> {

    void loadData(String filterDate, boolean pullToRefresh);

    void loadDataNextPage(String filterDate, int page);
}