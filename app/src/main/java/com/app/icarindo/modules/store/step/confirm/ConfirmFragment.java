package com.app.icarindo.modules.store.step.confirm;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.store.CustomerShipping;
import com.app.icarindo.models.store.Product;
import com.app.icarindo.models.store.RequestOrder;
import com.app.icarindo.modules.deposit.add.DepositFragment;
import com.app.icarindo.modules.store.order.detail.OrderDetailFragment;
import com.app.icarindo.modules.store.order.detail.OrderDetailFragmentBuilder;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Config;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@FragmentWithArgs
public class ConfirmFragment extends BaseMvpFragment<XConfirmView, ConfirmPresenter>
        implements XConfirmView, AlertListener {

    @Arg String jsonProduct;
    @Arg String jsonCusShipping;

    @BindView(R.id.tvAddress) TextView tvAddress;
    @BindView(R.id.tvConfirm) TextView tvConfirm;
    @BindView(R.id.layHeaderPurchase) LinearLayout layHeaderPurchase;
    @BindView(R.id.tvEditShippingAddress) TextView tvEditShippingAddress;
    @BindView(R.id.tvLabel) TextView tvLabel;
    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.tvFullAddress) TextView tvFullAddress;
    @BindView(R.id.tvAddressShipping) TextView tvAddressShipping;
    @BindView(R.id.tvPhone) TextView tvPhone;
    @BindView(R.id.layShippingAddress) LinearLayout layShippingAddress;
    @BindView(R.id.tvShippingPrice) TextView tvShippingPrice;
    @BindView(R.id.tvTotalPayment) TextView tvTotalPayment;
    @BindView(R.id.tvSaldo) TextView tvSaldo;
    @BindView(R.id.tvSisaSaldo) TextView tvSisaSaldo;
    @BindView(R.id.btnNext) Button btnNext;
    @BindView(R.id.layBtnNext) LinearLayout layBtnNext;
    @BindView(R.id.productImage) ImageView productImage;
    @BindView(R.id.productName) TextView productName;
    @BindView(R.id.productPrice) TextView productPrice;
    @BindView(R.id.productQty) TextView productQty;
    @BindView(R.id.tvSaldoMinus) TextView tvSaldoMinus;
    @BindView(R.id.etNotes) EditText etNotes;
    @BindView(R.id.layNote) LinearLayout layNote;

    private Product product;
    private CustomerShipping shipping;
    private String orderId;

    public ConfirmFragment() {

    }

    @Override public ConfirmPresenter createPresenter() {
        return new ConfirmPresenter(getContext());
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        product = new Gson().fromJson(jsonProduct, Product.class);
        shipping = new Gson().fromJson(jsonCusShipping, CustomerShipping.class);
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_confirm;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
        tvAddress.setEnabled(true);
        tvConfirm.setEnabled(true);

        tvLabel.setText(CommonUtilities.getText(shipping.getShippingLabel(), ""));
        tvName.setText(CommonUtilities.getText(shipping.getShippingName(), ""));
        tvPhone.setText(CommonUtilities.getText(shipping.getShippingPhone(), ""));
        tvFullAddress.setText(CommonUtilities.getText(shipping.getShippingAddress(), ""));
        String addressShipping = "";
        if (!TextUtils.isEmpty(shipping.getShippingProvinceName())) {
            addressShipping = shipping.getShippingProvinceName();
        }
        if (!TextUtils.isEmpty(shipping.getShippingCityName())) {
            addressShipping = addressShipping + ", " + shipping.getShippingCityName();
        }
        tvAddressShipping.setText(addressShipping);

        setTotal();
        Picasso.with(getContext()).load(product.getProductImageLink()).into(productImage);
        productName.setText(product.getProductName());
        productPrice.setText(CommonUtilities.toRupiahNumberFormat(product.getProductPricePublish()));
        productQty.setText(new StringBuilder("x ").append(product.getQtyBuy()));
    }

    @Override public void setTotal() {
        Integer saldo = presenter.getPrefs().getPreferencesInt(Config.CUSTOMER_SALDO);
        tvSaldo.setText(CommonUtilities.toRupiahNumberFormat(saldo));
        Integer qty = product.getQtyBuy();
        Integer productPrice = Integer.parseInt(product.getProductPricePublish());
        Integer total = productPrice * qty;
        tvTotalPayment.setText(CommonUtilities.toRupiahNumberFormat(total));
        if (total > saldo) {
            tvSaldoMinus.setVisibility(View.VISIBLE);
            tvSisaSaldo.setText("Rp 0");
            btnNext.setText(new StringBuilder("Tambah Saldo"));
        } else {
            tvSaldoMinus.setVisibility(View.GONE);
            Integer sisaSaldo = saldo - total;
            btnNext.setText(new StringBuilder("Bayar Sekarang"));
            tvSisaSaldo.setText(CommonUtilities.toRupiahNumberFormat(sisaSaldo));
        }
    }

    @Override public void showProgressDialog(boolean show) {
        if (show){
            showLoading("Loading...", false);
        }else{
            hideLoading();
        }
    }

    @Override public void showError(String string) {
        showSnackBar(string);
    }

    @Override public void setData(RequestOrder data) {
        if (data.getStatus()){
            orderId = data.getOrderId();
            showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Berhasil", data.getText(), "OK", null, "success_buy", false);
        }else{
            showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", data.getText(), "Tutup", null, "unsuccess_buy", true);
        }
    }

    @OnClick({R.id.tvEditShippingAddress, R.id.btnNext}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvEditShippingAddress:
                mListener.back();
                break;
            case R.id.btnNext:
                if (btnNext.getText().toString().equals("Tambah Saldo")) {
                    mListener.gotoPage(new DepositFragment(), false, null);
                } else {
                    String note = "";
                    if (!TextUtils.isEmpty(etNotes.getText())){
                        note = etNotes.getText().toString();
                    }
                   presenter.buy(note, jsonProduct, jsonCusShipping);
                }
                break;
        }
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action){
            case "success_buy":
                mListener.gotoPage(new OrderDetailFragmentBuilder(orderId).build(), false, OrderDetailFragment.class.getSimpleName() + orderId);
                break;
        }
    }
}