package com.app.icarindo.modules.ppob.pembayaran.model1;

import android.content.Intent;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XPembayaranOnePresenter extends MvpPresenter<XPembayaranOneView> {

    void loadData(boolean pullToRefresh, String type);

    void getContact();

    void onActivityResult(int requestCode, int resultCode, Intent data);
}