package com.app.icarindo.modules.network;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseFragment;
import com.app.icarindo.modules.network.jaringan.NetworkTreeFragment;
import com.app.icarindo.modules.network.sponsor.SponsorFragment;
import com.app.icarindo.utility.SmartFragmentStatePagerAdapter;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class NetworkFragment extends BaseFragment {


    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;

    public NetworkFragment() {
        // Required empty public constructor
    }


    @Override protected int getLayoutRes() {
        return R.layout.fragment_with_tab_layout;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        NetworkViewPager pagerAdapter = new NetworkViewPager(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(0);
        tabLayout.setupWithViewPager(viewPager);
    }

    private class NetworkViewPager extends SmartFragmentStatePagerAdapter {
        private String tabTitles[] =  new String[]{"SPONSOR", "JARINGAN"};

        public NetworkViewPager(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override public Fragment getItem(int position) {
            switch (position) {
                case 1:
                    return new NetworkTreeFragment();
                default:
                    return new SponsorFragment();
            }
        }

        @Override public int getCount() {
            return tabTitles.length;
        }

        @Override public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }
}
