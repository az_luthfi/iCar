package com.app.icarindo.modules.deposit.confirm;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.eventbus.EventUpdateProfile;
import com.app.icarindo.models.ppob.RequestDeposit;
import com.app.icarindo.utility.Logs;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;
import static com.app.icarindo.utility.Config.CUSTOMER_SALDO;


public class DepositConfirmationPresenter extends BasePresenter<XDepositConfirmationView>
        implements XDepositConfirmationPresenter {

    public DepositConfirmationPresenter(Context context) {
        super(context);
    }


    @Override public void cancelDeposit(String depositId) {
        if (isViewAttached()){
            getView().showProgressDialog("Membatalkan deposit...", false);
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("act", "cancel_last_deposit");
        params.put("depositId", depositId);

        final Observable<RequestDeposit> requestDepositObservable = App.getService().apiDepositRequest(params);
        Observer<RequestDeposit> observer = new Observer<RequestDeposit>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestDeposit requestDeposit) {
                if (isViewAttached()) {
                    getView().showProgressDialog(null, false);
                    if (requestDeposit.getStatus()) {
                        getView().showConfirmDialogView(SweetAlertDialog.SUCCESS_TYPE, "Berhasil", requestDeposit.getText(), "Tutup", null, "back");
                    } else {
                        getView().showConfirmDialogView(SweetAlertDialog.ERROR_TYPE, "Peringatan", requestDeposit.getText(), "Tutup", null, "close");
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()){
                    getView().showProgressDialog(null, false);
                    getView().onError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {
            }
        };

        requestDepositObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }

    @Override public void confirmDeposit(String depositId, String comment) {
        if (isViewAttached()){
            getView().showProgressDialog("Mengirim data...", false);
        }
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("act", "deposit_confirm");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("depositId", depositId);
        params.put("bankFrom", "");
        params.put("bankAcc", "");
        params.put("depositText", comment);

        Logs.d("FragmentPulsaPresenter => requestVoucherProduct ==> " + params);

        final Observable<RequestDeposit> requestDepositObservable = App.getService().apiDepositRequest(params);
        Observer<RequestDeposit> observer = new Observer<RequestDeposit>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestDeposit requestDeposit) {
                if (isViewAttached()) {
                    getView().showProgressDialog(null, false);
                    if (requestDeposit.getStatus()) {
                        getView().showConfirmDialogView(SweetAlertDialog.SUCCESS_TYPE, "Berhasil", requestDeposit.getText(), "Tutup", null, "back");
                        prefs.savePreferences(CUSTOMER_SALDO, requestDeposit.getCustomerSaldo());
                        EventBus.getDefault().post(new EventUpdateProfile());
                    } else {
                        getView().showConfirmDialogView(SweetAlertDialog.ERROR_TYPE, "Peringatan", requestDeposit.getText(), "Tutup", null, "close");
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()){
                    getView().showProgressDialog(null, false);
                    getView().onError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {
            }
        };

        requestDepositObservable.subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}
