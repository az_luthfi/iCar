package com.app.icarindo.modules.pesan.add;

import android.Manifest;
import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.eventbus.EventMessage;
import com.app.icarindo.models.pesan.RequestMessage;
import com.app.icarindo.utility.Config;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddMessagePresenter extends BasePresenter<XAddMessageView>
        implements XAddMessagePresenter {

    private RxPermissions rxPermissions;

    public AddMessagePresenter(Context context, FragmentActivity activity) {
        super(context);
        rxPermissions = new RxPermissions(activity);
    }

    @Override public void requestPermissionGallery() {
        rxPermissions
                .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                .subscribe(new Observer<Boolean>() {
                    @Override public void onSubscribe(Disposable d) {
                    }

                    @Override public void onNext(Boolean aBoolean) {
                        if (isViewAttached()) {
                            if (aBoolean) {
                                getView().imageFromGallery();
                            } else {
                                Toast.makeText(context, "Permission denied, can't open the gallery", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override public void onError(Throwable e) {
                    }

                    @Override public void onComplete() {
                    }
                });
    }

    @Override public void sendMessage(MultipartBody.Part requestImage, String devisi, String subject, String pesan) {
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }
        Map<String, RequestBody> params = new HashMap<>();
        params.put("act", RequestBody.create(MediaType.parse("multipart/form-data"), "new"));
        params.put("customerId", RequestBody.create(MediaType.parse("multipart/form-data"), String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID))));
        params.put("divisi", RequestBody.create(MediaType.parse("multipart/form-data"), devisi));
        params.put("sub", RequestBody.create(MediaType.parse("multipart/form-data"), subject));
        params.put("isi", RequestBody.create(MediaType.parse("multipart/form-data"), pesan));


        Observer<RequestMessage> observer = new Observer<RequestMessage>() {

            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestMessage m) {
                if (isViewAttached()){
                    getView().showProgressDialog(false);
                    if (m.getStatus()){
                        getView().showDialogSuccess(m.getText());
                        EventBus.getDefault().post(new EventMessage(EventMessage.NEW, m.getMessage()));
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()){
                    getView().showProgressDialog(false);
                    getView().showDialogError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().sendMessage(params, requestImage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}