package com.app.icarindo.modules.absensi.history.extra;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.adapter.HistoryAbsensiExtraAdapter;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.network.Extra;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.listview.EndlessRecyclerOnScrollListener;
import com.app.icarindo.utility.listview.ItemClickSupport;

import butterknife.BindView;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class HistoryExtraFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestNetwork, XHistoryExtraView, HistoryExtraPresenter>
        implements XHistoryExtraView, ItemLoadingHolder.ItemViewLoadingListener, AlertListener {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private HistoryAbsensiExtraAdapter adapter;
    private int maxCountItems;
    private EndlessRecyclerOnScrollListener scrollListener;

    private int positionDelete = -1;

    public HistoryExtraFragment() {

    }

    @Override public HistoryExtraPresenter createPresenter() {
        return new HistoryExtraPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_with_recyclerview_refresh;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
        adapter = new HistoryAbsensiExtraAdapter(this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Extra extra = adapter.getItem(position);
                if (extra.getCanCancel()){
                    positionDelete = position;
                    showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", "Apakah anda yakin akan membatalkan bonus extra pada tanggal " + extra.getBeDatePakai(), "Ya", "Tutup", "click-cancel", true);
                }
            }
        });

        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    if (adapter.getItemCount() < maxCountItems) {
                        presenter.loadDataNextPage(page);
                    }
                }
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestNetwork data) {
        if (data.getStatus()) {
            if (adapter != null && adapter.getItemCount() > 0){
                adapter.clear();
            }
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            maxCountItems = data.getCount();
            adapter.setItems(data.getExtras());
        } else {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadHistoryExtra(pullToRefresh);
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadDataNextPage(scrollListener.getCurrentPage());
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void setNextData(RequestNetwork data) {
        if (data.getStatus()) {
            adapter.setItems(data.getExtras());
        }
    }

    @Override public void showErrorNextPage(String string) {
        Toast.makeText(getContext(), string, Toast.LENGTH_SHORT).show();
        adapter.setErrorNextPage("Tap to retry");
        scrollListener.backToPreviousPage();
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action){
            case "click-cancel":
                if (positionDelete > -1){
                    presenter.requestCancel(adapter.getItem(positionDelete).getBeId());
                }
                break;
        }
    }

    @Override public void onSuccessCancel() {
        adapter.removeItem(positionDelete);
        positionDelete = -1;
    }

    @Override public void showProgressDialog(boolean show) {
        if (show){
            showLoading("Loading..", false);
        }else{
            hideLoading();
        }
    }

    @Override public void showError(String text) {
        final Snackbar snackbar = Snackbar.make(contentView, text, Snackbar.LENGTH_LONG);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }
}