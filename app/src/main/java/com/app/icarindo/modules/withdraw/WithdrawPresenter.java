package com.app.icarindo.modules.withdraw;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.eventbus.EventAddBank;
import com.app.icarindo.models.eventbus.EventUpdateProfile;
import com.app.icarindo.models.withdraw.RequestWithdraw;
import com.app.icarindo.utility.Config;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;
import static com.app.icarindo.utility.Config.CUSTOMER_SALDO;

public class WithdrawPresenter extends BaseRxLcePresenter<XWithdrawView, RequestWithdraw>
        implements XWithdrawPresenter {

    public WithdrawPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "getBank");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));

        subscribe(App.getService().apiWithdraw(params), pullToRefresh);
    }

    @Override public void requestWithdraw(String nominal, String desc) {
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("act", "withdraw");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("nominal", nominal);
        params.put("desc", desc);
        Observer<RequestWithdraw> observer = new Observer<RequestWithdraw>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestWithdraw data) {
                if (data.getStatus()){
                    prefs.savePreferences(CUSTOMER_SALDO, data.getCustomerSaldo());
                    EventBus.getDefault().post(new EventUpdateProfile());
                }
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().onNextWithdraw(data);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiWithdraw(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProfileUpdateEvent(EventAddBank event) {
        loadData(false);
    }

    @Override public void attachView(XWithdrawView view) {
        super.attachView(view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }
}