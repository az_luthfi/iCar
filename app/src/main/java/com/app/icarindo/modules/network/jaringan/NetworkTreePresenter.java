package com.app.icarindo.modules.network.jaringan;

import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.eventbus.EventUpdateJaringan;
import com.app.icarindo.models.network.RequestNetwork;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

public class NetworkTreePresenter extends BaseRxLcePresenter<XNetworkTreeView, RequestNetwork>
        implements XNetworkTreePresenter {

    public NetworkTreePresenter(Context context) {
        super(context);
    }

    private String customerId;
    @Override public void loadData(boolean pullToRefresh, String customerId) {
        this.customerId = customerId;
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "tree");
        params.put("customerId", customerId);

        subscribe(App.getService().apiNetwork(params), pullToRefresh);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProfileUpdateEvent(EventUpdateJaringan event) {
        loadData(true, customerId);
    }

    @Override public void attachView(XNetworkTreeView view) {
        super.attachView(view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }
}