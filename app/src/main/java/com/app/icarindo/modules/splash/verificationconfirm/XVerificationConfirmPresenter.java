package com.app.icarindo.modules.splash.verificationconfirm;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XVerificationConfirmPresenter extends MvpPresenter<XVerificationConfirmView> {

    void confirmDepositRegister(String depositId, String pesan, String customerId);
}