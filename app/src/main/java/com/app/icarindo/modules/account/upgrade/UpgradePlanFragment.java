package com.app.icarindo.modules.account.upgrade;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.content.RequestContent;
import com.app.icarindo.models.network.Plan;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.Validation;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.app.icarindo.utility.Config.CUSTOMER_PIN;
import static com.app.icarindo.utility.Config.CUSTOMER_SALDO;

public class UpgradePlanFragment extends BaseMvpLceFragment<FrameLayout, RequestNetwork, XUpgradePlanView, UpgradePlanPresenter>
        implements XUpgradePlanView, AlertListener {
    private static final String TAG = UpgradePlanFragment.class.getSimpleName();
    @BindView(R.id.dataView) RelativeLayout dataView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) FrameLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.cbTerm) CheckBox cbTerm;
    @BindView(R.id.tvTermAndCon) TextView tvTermAndCon;
    @BindView(R.id.etPin) MaterialEditText etPin;
    @BindView(R.id.layoutPin) TextInputLayout layoutPin;
    @BindView(R.id.tvSubmit) TextView tvSubmit;
    @BindView(R.id.tvSaldo) TextView tvSaldo;
    @BindView(R.id.radioPlanB) RadioButton radioPlanB;
    @BindView(R.id.radioPlanPlus) RadioButton radioPlanPlus;
    @BindView(R.id.radioGender) RadioGroup radioGender;

    private Plan plan;

    private ArrayList<Plan> plans;
    public UpgradePlanFragment() {

    }

    @Override public UpgradePlanPresenter createPresenter() {
        return new UpgradePlanPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_upgrade_plan;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
        tvSaldo.setText(CommonUtilities.toRupiahFormat(presenter.getPrefs().getPreferencesInt(CUSTOMER_SALDO)));
        loadData(false);

        SpannableString span = new SpannableString(tvTermAndCon.getText().toString().trim());
        span.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.colorAccent)), 34, 54, 0);
        span.setSpan(new RelativeSizeSpan(1.2f), 34, 54, 0);
        span.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), 34, 54, 0);
        span.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                presenter.loadTermAndCon();
            }
        }, 34, 54, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvTermAndCon.setText(span);
        tvTermAndCon.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestNetwork data) {
        if (data.getStatus()) {
            plans = data.getPlans();
            dataView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            if (plans.size() == 1){
                radioPlanB.setVisibility(View.GONE);
                radioPlanPlus.setText(plans.get(0).getPpNama() + " (" + CommonUtilities.toRupiahFormat(plans.get(0).getPpHarga()) + ")");
                radioPlanPlus.setTag(plans.get(0).getPpCode());
            }else if (plans.size() == 2){
                radioPlanB.setVisibility(View.VISIBLE);
                radioPlanB.setText(plans.get(0).getPpNama() + " (" + CommonUtilities.toRupiahFormat(plans.get(0).getPpHarga()) + ")");
                radioPlanB.setTag(plans.get(0).getPpCode());
                radioPlanPlus.setText(plans.get(1).getPpNama() + " (" + CommonUtilities.toRupiahFormat(plans.get(1).getPpHarga()) + ")");
                radioPlanPlus.setTag(plans.get(1).getPpCode());
            }else{
                showToast("Error");
                mListener.back();
            }
//            tvPlanName.setText(plan.getPpNama());
//            tvPrice.setText(CommonUtilities.toRupiahFormat(plan.getPpHarga()));
        } else {
            dataView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh);
    }


    @OnClick(R.id.tvSubmit) public void onViewClicked() {
        validateForm();
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            showLoading("Loading...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void shoDialogTermAndCon(RequestContent data) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alertsyarat, null);
        final TextView ket = (TextView) alertLayout.findViewById(R.id.tvKet);
        Button btnok = (Button) alertLayout.findViewById(R.id.btOk);
        final AlertDialog alert = new AlertDialog.Builder(getActivity()).create();
        alert.setView(alertLayout);
        alert.setCancelable(false);
        ket.setText(CommonUtilities.toHtml(data.getContent().getContentDesc()));
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.setView(alertLayout);
        alert.show();
    }

    @Override public void showError(String text) {
        final Snackbar snackbar = Snackbar.make(contentView, text, Snackbar.LENGTH_LONG);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override public void onNextUpgradePlan(RequestNetwork data) {
        if (data.getStatus()) {
            showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Berhasil", data.getText(), "OK", null, "success-upgrade", false);
        } else {
            showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", data.getText(), "Tutup", null, "false-upgrade", true);
        }
    }

    @OnTextChanged(R.id.etPin) public void onTextChanged() {
        tvSubmit.setEnabled(etPin.getText().length() > 5);
    }

    @OnEditorAction(R.id.etPin) public boolean onActionGo(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_GO) {
            validateForm();
        }
        return true;
    }

    private void validateForm() {
        CommonUtilities.hideSoftKeyboard(getActivity());
        if (Validation.isEmpty(etPin, "Password wajib diisi")) return;
        if (!CommonUtilities.md5(CommonUtilities.md5(etPin.getText().toString())).equalsIgnoreCase(presenter.getPrefs().getPreferencesString(CUSTOMER_PIN))) {
            showToast("Pin salah");
            return;
        }
        if (!cbTerm.isChecked()) {
            showToast("Anda belum menyutujui syarat dan ketentuan");
            return;
        }
        String planCode = "";
        if (radioPlanB.isChecked()){
            planCode = radioPlanB.getTag().toString();
        }else if (radioPlanPlus.isChecked()){
            planCode = radioPlanPlus.getTag().toString();
        }
        if (TextUtils.isEmpty(planCode)){
            showToast("Anda belum memilih plan upgrade");
            return;
        }
        presenter.upgradePlan(planCode);
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action) {
            case "success-upgrade":
                mListener.back();
                break;
        }
    }
}