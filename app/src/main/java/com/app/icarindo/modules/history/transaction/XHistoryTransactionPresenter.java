package com.app.icarindo.modules.history.transaction;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XHistoryTransactionPresenter extends MvpPresenter<XHistoryTransactionView> {

    void loadData(String filterDate, boolean pullToRefresh);

    void loadDataNextPage(String filterDate, int page);
}