package com.app.icarindo.modules.withdraw.detail;

import com.app.icarindo.models.withdraw.RequestWithdraw;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XWithdrawDetailView extends MvpLceView<RequestWithdraw> {

}