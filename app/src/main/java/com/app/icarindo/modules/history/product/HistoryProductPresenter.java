package com.app.icarindo.modules.history.product;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.store.RequestOrder;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;

public class HistoryProductPresenter extends BaseRxLcePresenter<XHistoryProductView, RequestOrder>
        implements XHistoryProductPresenter {

    public HistoryProductPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(String filterDate, boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "history_order");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("page", "1");
        params.put("filterDate", filterDate);
        subscribe(App.getService().apiOrder(params), pullToRefresh);
    }

    @Override public void loadDataNextPage(String filterDate, int page) {
        if (isViewAttached()) {
            getView().showLoadingNextPage(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "history_order");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("page", String.valueOf(page));
        params.put("filterDate", filterDate);

        Observer<RequestOrder> observer1 = new Observer<RequestOrder>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestOrder data) {
                if (isViewAttached()) {
                    getView().setNextData(data);
                    getView().showLoadingNextPage(false);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showErrorNextPage(context.getString(R.string.error_network));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiOrder(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer1);
    }
}