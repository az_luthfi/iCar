package com.app.icarindo.modules.deposit.confirm;

import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XDepositConfirmationView extends MvpView {

    void showProgressDialog(@Nullable  String msg, boolean show);

    void onError(String contextString);

    void showConfirmDialogView(int warningType, String peringatan, String s, String s1, String tidak, String cancelDeposit);

}