package com.app.icarindo.modules.absensi.city;

import com.app.icarindo.models.network.RequestNetwork;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XOutOfCityView extends MvpView {

    void showProgressDialog(boolean show);

    void onNextRequestInput(RequestNetwork data);

    void showError(String string);
}