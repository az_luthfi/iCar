package com.app.icarindo.modules.splash.verificationwaiting;

import android.widget.Button;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpFragment;

import butterknife.BindView;
import butterknife.OnClick;

public class VerificationWaitingFragment extends BaseMvpFragment<XVerificationWaitingView, VerificationWaitingPresenter>
        implements XVerificationWaitingView {

    @BindView(R.id.btnClose) Button btnClose;

    public VerificationWaitingFragment() {

    }

    @Override public VerificationWaitingPresenter createPresenter() {
        return new VerificationWaitingPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_verification_waiting;
    }


    @OnClick(R.id.btnClose) public void onViewClicked() {
        getActivity().finish();
    }
}