package com.app.icarindo.modules.ppob.history.detail.transaction;

import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.ppob.RequestHistory;

import java.util.HashMap;

public class DetailTransactionPresenter extends BaseRxLcePresenter<XDetailTransactionView, RequestHistory>
        implements XDetailTransactionPresenter {

    public DetailTransactionPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh, String transId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "transaction_detail");
        params.put("transId", transId);

        subscribe(App.getService().apiHistoryRequest(params), pullToRefresh);
    }
}