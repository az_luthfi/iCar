package com.app.icarindo.modules.splash.verification;

import android.content.Context;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.customer.RequestCustomer;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class VerificationPresenter extends BasePresenter<XVerificationView>
        implements XVerificationPresenter {

    public VerificationPresenter(Context context) {
        super(context);
    }

    @Override public void depositRegister(String productCode, String bankName, Integer customerId, String registerPrice) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "deposit_register");
        params.put("bankTo", bankName);
        params.put("productCode", productCode);
        params.put("customerId", String.valueOf(customerId));
        params.put("depositAmount", registerPrice);

        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        Observer<RequestCustomer> observer = new Observer<RequestCustomer>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCustomer requestCustomer) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (requestCustomer.getStatus()) {
                        Toast.makeText(context, requestCustomer.getText(), Toast.LENGTH_SHORT).show();
                        getView().onSuccessDepositRegister(requestCustomer.getDeposit());
                    } else {
                        getView().showErrorDepositRegister(requestCustomer.getText());
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiCustomer(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}