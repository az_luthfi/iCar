package com.app.icarindo.modules.ppob.pembelian.pulsa;

import android.content.Intent;

import com.app.icarindo.models.ppob.RequestProductCategory;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XPulsaView extends MvpView {

    void showProduct(RequestProductCategory requestProductCategory);

    void startActivityForResult(Intent pickContactIntent, int requestContact);

    void setPhoneNumber(String number);

    void checkMsisdn(String msisdn);

    void showError(String text);

    void resetProduct();
}