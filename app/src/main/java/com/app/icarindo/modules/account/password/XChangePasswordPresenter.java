package com.app.icarindo.modules.account.password;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XChangePasswordPresenter extends MvpPresenter<XChangePasswordView> {

    void requestChangePassword(String currentPassword, String newPassword);

    void requestForgetPassword();
}