package com.app.icarindo.modules.withdraw.detail;

import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.withdraw.RequestWithdraw;
import com.app.icarindo.utility.Config;

import java.util.HashMap;

public class WithdrawDetailPresenter extends BaseRxLcePresenter<XWithdrawDetailView, RequestWithdraw>
        implements XWithdrawDetailPresenter {

    public WithdrawDetailPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(String withdrawId, boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("withdrawId", withdrawId);
        params.put("act", "withdraw_detail");

        subscribe(App.getService().apiWithdraw(params), pullToRefresh);
    }
}