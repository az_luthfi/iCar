package com.app.icarindo.modules.register;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.SimpleAdapter;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.content.RequestContent;
import com.app.icarindo.models.customer.RequestCustomer;
import com.app.icarindo.models.datalist.SimpleList;
import com.app.icarindo.models.network.Plan;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.models.network.Tree;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.DialogListOption;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnEditorAction;
import butterknife.OnTextChanged;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.app.icarindo.utility.Config.CUSTOMER_KODE;
import static com.app.icarindo.utility.Config.CUSTOMER_PIN;
import static com.app.icarindo.utility.Config.MIN_LENGHT_PASSWORD;

@FragmentWithArgs
public class RegisterFragment extends BaseMvpLceFragment<NestedScrollView, RequestNetwork, XRegisterView, RegisterPresenter>
        implements XRegisterView, AlertListener {

    @Arg Tree tree;
    @Arg String kaki; // 1 = kanan, 2 = kiri

    @BindView(R.id.etName) MaterialEditText etName;
    @BindView(R.id.etEmail) MaterialEditText etEmail;
    @BindView(R.id.etPassword) MaterialEditText etPassword;
    @BindView(R.id.etPhone) MaterialEditText etPhone;
    @BindView(R.id.etAddress) MaterialEditText etAddress;
    @BindView(R.id.etProvince) MaterialEditText etProvince;
    @BindView(R.id.cbTerm) CheckBox cbTerm;
    @BindView(R.id.btnRegister) Button btnRegister;
    @BindView(R.id.tvTermAndCon) TextView tvTermAndCon;
    @BindView(R.id.etSponsor) MaterialEditText etSponsor;
    @BindView(R.id.etUpLine) MaterialEditText etUpLine;
    @BindView(R.id.radioPlanA) RadioButton radioPlanA;
    @BindView(R.id.radioPlanB) RadioButton radioPlanB;
    @BindView(R.id.radioGender) RadioGroup radioGender;
    @BindView(R.id.contentView) NestedScrollView contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.etPin) MaterialEditText etPin;
    @BindView(R.id.etBirtDate) MaterialEditText etBirtDate;
    @BindView(R.id.layoutPin) TextInputLayout layoutPin;
    @BindView(R.id.etKTP) MaterialEditText etKTP;
    @BindView(R.id.etAhliWaris) MaterialEditText etAhliWaris;

    private String[] email;
    private SimpleAdapter adapter;
    private String birthDate;

    public RegisterFragment() {

    }

    @Override public RegisterPresenter createPresenter() {
        return new RegisterPresenter(getContext(), getActivity());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_register;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new SimpleAdapter();
        setAlertListener(this);

        loadData(false);

        SpannableString span = new SpannableString(tvTermAndCon.getText().toString().trim());
        span.setSpan(new ForegroundColorSpan(ContextCompat.getColor(getActivity(), R.color.colorAccent)), 34, 54, 0);
        span.setSpan(new RelativeSizeSpan(1.2f), 34, 54, 0);
        span.setSpan(new StyleSpan(Typeface.BOLD_ITALIC), 34, 54, 0);
        span.setSpan(new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                presenter.loadTermAndCon();
            }
        }, 34, 54, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        tvTermAndCon.setText(span);
        tvTermAndCon.setMovementMethod(LinkMovementMethod.getInstance());
        String upLine = tree.getCustomerName() + "\n";
        if (kaki.equals("1")) {
            upLine += "Kanan";
        }
        if (kaki.equals("2")) {
            upLine += "Kiri";
        }
        etUpLine.setText(upLine);
        etSponsor.setText(CommonUtilities.getText(presenter.getPrefs().getPreferencesString(CUSTOMER_KODE), ""));
    }

    @Override public void setData(RequestNetwork data) {
        boolean existPlanA = false;
        boolean existPlanB = false;
        for (Plan plan : data.getPlans()) {
            if (plan.getPpCode().equals(Plan.CODE_PLANA1)) {
                existPlanA = true;
                radioPlanA.setText(plan.getPpNama() + " (" + CommonUtilities.toRupiahFormat(plan.getPpHarga()) + ")");
                continue;
            }
            if (plan.getPpCode().equals(Plan.CODE_PLANB1)) {
                existPlanB = true;
                radioPlanB.setText(plan.getPpNama() + " (" + CommonUtilities.toRupiahFormat(plan.getPpHarga()) + ")");
            }
        }
        if (!existPlanA) {
            radioPlanA.setVisibility(View.GONE);
        }
        if (!existPlanB) {
            radioPlanB.setVisibility(View.GONE);
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadPlan(pullToRefresh);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        if (pullToRefresh) {
            return getContext().getString(R.string.error_network_light);
        }
        return getContext().getString(R.string.error_network);
    }

    @OnClick({R.id.etEmail, R.id.etProvince, R.id.btnRegister}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etEmail:
                presenter.requestPermissionsAccounts();
                break;
            case R.id.etProvince:
                presenter.loadProvince();
                break;
            case R.id.btnRegister:
                validateForm();
                break;

        }
    }

    @OnTextChanged(R.id.etPin) public void onTextChanged() {
        btnRegister.setEnabled(etPin.getText().length() > 5);
    }

    @OnEditorAction(R.id.etPin) public boolean onActionGo(TextView v, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_ACTION_GO) {
            validateForm();
        }
        return true;
    }

    private void validateForm() {
        CommonUtilities.hideSoftKeyboard(getActivity());
        if (!CommonUtilities.md5(CommonUtilities.md5(etPin.getText().toString())).equalsIgnoreCase(presenter.getPrefs().getPreferencesString(CUSTOMER_PIN))) {
            showToast("Pin salah");
            return;
        }
        if (isEmpty(etName, "Nama wajib diisi")) return;
        if (isEmpty(etKTP, "No. KTP / NIK wajib diisi")) return;
        if (etKTP.length() < 16) {
            setError(etKTP, "No. KTP / NIK tidak sesuai");
            return;
        }
        if (isEmpty(etEmail, "Email wajib diisi")) return;
        if (isEmpty(etPassword, "Password wajib diisi")) return;
        if (etPassword.length() < MIN_LENGHT_PASSWORD) {
            setError(etPassword, "Password minimal " + MIN_LENGHT_PASSWORD);
            return;
        }
        if (isEmpty(etPhone, "No. Telp / HP wajib diisi")) return;
        if (etPhone.length() < 10) {
            setError(etPhone, "No. Telp / HP tidak sesuai");
            return;
        }
        if (isEmpty(etSponsor, "Sponsor wajib diisi")) return;
        if (isEmpty(etBirtDate, "Tanggal lahir wajib diisi")) return;
        if (isEmpty(etProvince, "Provinsi wajib diisi")) return;
        if (isEmpty(etAddress, "Alamat wajib diisi")) return;
        String planChoose = null;
        if (radioPlanA.isChecked()) {
            planChoose = Plan.CODE_PLANA1;
        }
        if (radioPlanB.isChecked()) {
            planChoose = Plan.CODE_PLANB1;
        }
        if (TextUtils.isEmpty(planChoose)) {
            showToast("Anda belum memilih PLAN");
            return;
        }
        if (isEmpty(etAhliWaris, "Ahli waris wajib diisi")) return;

        if (!cbTerm.isChecked()) {
            showToast("Anda belum menyutujui syarat dan ketentuan");
            return;
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("name", etName.getText().toString());
        params.put("email", etEmail.getText().toString());
        params.put("cusPassword", etPassword.getText().toString());
        params.put("msisdn", etPhone.getText().toString());
        params.put("sponsor", etSponsor.getText().toString());
        params.put("plan_code", planChoose);
        params.put("upline_id", tree.getCustomerId());
        params.put("upline_kaki", kaki);
        params.put("province", etProvince.getText().toString());
        params.put("address", etAddress.getText().toString());
        params.put("birthDate", etBirtDate.getText().toString());
        params.put("ktp", etKTP.getText().toString());
        params.put("ahliwaris", etAhliWaris.getText().toString());

        presenter.requestRegister(params);
    }

    private boolean isEmpty(EditText editText, String errorMsg) {
        if (TextUtils.isEmpty(editText.getText())) {
            setError(editText, errorMsg);
            return true;
        }
        return false;
    }

    private boolean isEmpty(TextView textView, String errorMsg) {
        if (TextUtils.isEmpty(textView.getText())) {
            showToast(errorMsg);
            return true;
        }
        return false;
    }

    private void setError(EditText editText, String errorMsg) {
        editText.setError(errorMsg);
        editText.requestFocus();
    }

    @Override public void showAccountsDialog() {
        final ArrayList<String> emailList = CommonUtilities.getEmailOnPhone(getActivity());
        email = new String[emailList.size()];
        for (int a = 0; a < emailList.size(); a++) {
            email[a] = emailList.get(a);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pilih Email");
        builder.setItems(email, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                etEmail.setText(email[item]);
            }
        });
        builder.show();
    }

    @Override public void showDialog(ArrayList<? extends SimpleList> data, final String title) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        adapter.addAll(data);
        new DialogListOption(getContext(), title, adapter) {
            @Override public void onItemClicked(int position) {
                if (position != -1) {
                    switch (title.toLowerCase()) {
                        case "provinsi":
                            etProvince.setText(adapter.getItem(position).getName());
                            break;
                    }

                }
            }

            @Override public void onFilter(CharSequence s) {
                adapter.filter(s);
            }
        };
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            showLoading("Loading...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void showError(String text) {
        final Snackbar snackbar = Snackbar.make(contentView, text, Snackbar.LENGTH_LONG);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override public void onSuccessRegister(RequestCustomer data) {
        etName.setText("");
        etEmail.setText("");
        etPassword.setText("");
        etPhone.setText("");
        etSponsor.setText("");
        etUpLine.setText("");
        etProvince.setText("");
        etAddress.setText("");
        etPin.setText("");
        showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Berhasil", data.getText(), "OK", null, "success", true);
    }

    @Override public void shoDialogTermAndCon(RequestContent data) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View alertLayout = inflater.inflate(R.layout.alertsyarat, null);
        final TextView ket = (TextView) alertLayout.findViewById(R.id.tvKet);
        Button btnok = (Button) alertLayout.findViewById(R.id.btOk);
        final AlertDialog alert = new AlertDialog.Builder(getActivity()).create();
        alert.setView(alertLayout);
        alert.setCancelable(false);
        ket.setText(CommonUtilities.toHtml(data.getContent().getContentDesc()));
        btnok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alert.dismiss();
            }
        });

        alert.setView(alertLayout);
        alert.show();
    }

    @Override public void gotoBack() {
        mListener.back();
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action) {
            case "success":
                mListener.back();
                break;
        }
    }

    @OnClick(R.id.etBirtDate) public void onViewBirtDate() {
        final Calendar calendar = Calendar.getInstance();
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                DecimalFormat mFormat = new DecimalFormat("00");
                mFormat.setRoundingMode(RoundingMode.DOWN);
                String day = mFormat.format(Double.valueOf(dayOfMonth));
                String monthFormat = mFormat.format(Double.valueOf(month + 1));
                birthDate = year + "-" + monthFormat + "-" + day;
                etBirtDate.setText(new StringBuilder().append(day)
                        .append(" / ").append(monthFormat).append(" / ").append(year)
                        .append(" "));
            }
        };

        new DatePickerDialog(
                getActivity(),
                dateSetListener,
                calendar.get(Calendar.YEAR),
                calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
        ).show();
    }
}