package com.app.icarindo.modules.splash.login;

import com.app.icarindo.models.customer.Customer;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XLoginView extends MvpView {

    void showProgressDialog(boolean show);


    void showError(String text);

    void onSuccessAuth(Customer customer);

    void onSuccessResetPassword(String text);
}