package com.app.icarindo.modules.store.step.confirm;

import com.app.icarindo.models.store.RequestOrder;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XConfirmView extends MvpView {

    void setTotal();

    void showProgressDialog(boolean show);

    void showError(String string);

    void setData(RequestOrder data);
}