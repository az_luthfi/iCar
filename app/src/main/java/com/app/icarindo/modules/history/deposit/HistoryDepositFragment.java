package com.app.icarindo.modules.history.deposit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.adapter.HistoryNonTransactionAdapter;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.ppob.RequestHistory;
import com.app.icarindo.modules.history.HistoryFragment;
import com.app.icarindo.modules.ppob.history.detail.deposit.DetailDepositFragmentBuilder;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.listview.DividerItemDecoration;
import com.app.icarindo.utility.listview.EndlessRecyclerOnScrollListener;
import com.app.icarindo.utility.listview.ItemClickSupport;

import butterknife.BindView;

public class HistoryDepositFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestHistory, XHistoryDepositView, HistoryDepositPresenter>
        implements XHistoryDepositView, ItemLoadingHolder.ItemViewLoadingListener {

    @BindView(R.id.tvSubheadMonth) TextView tvSubheadMonth;
    @BindView(R.id.tvNominalMonth) TextView tvNominalMonth;
    @BindView(R.id.tvNominalTotal) TextView tvNominalTotal;
    @BindView(R.id.layDividerFilter) FrameLayout layDividerFilter;
    @BindView(R.id.layFilter) FrameLayout layFilter;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private String filterDate = "";

    private HistoryNonTransactionAdapter adapter;
    private int maxCountItems;
    private EndlessRecyclerOnScrollListener scrollListener;

    public HistoryDepositFragment() {

    }

    @Override public HistoryDepositPresenter createPresenter() {
        return new HistoryDepositPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_history_item;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new HistoryNonTransactionAdapter(this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                mListener.gotoPage(new DetailDepositFragmentBuilder(adapter.getItem(position).getHistoryId()).build(), false, null);
            }
        });

        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    if (adapter.getItemCount() < maxCountItems) {
                        presenter.loadDataNextPage(filterDate, page);
                    }
                }
            }
        };

        recyclerView.addOnScrollListener(scrollListener);

        layDividerFilter.setVisibility(View.GONE);
        layFilter.setVisibility(View.GONE);
        tvSubheadMonth.setText(new StringBuilder("Deposit"));
    }

    public void updateData() {
        String filterDateParent = ((HistoryFragment) getParentFragment()).getFilterDate();
        if (!filterDateParent.equals(filterDate)) {
            filterDate = filterDateParent;
            loadData(false);
        }
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestHistory data) {
        tvNominalMonth.setText(data.getNominalMonth());
        tvNominalTotal.setText(data.getNominalTotal());
        if (adapter.getItemCount() > 0){
            adapter.clear();
            scrollListener.resetState();
        }
        if (data.getStatus()){
            maxCountItems = data.getCount();
            adapter.setItems(data.getItemHistories());
        }else{
            maxCountItems = 0;
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(filterDate, pullToRefresh);
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadDataNextPage(filterDate, scrollListener.getCurrentPage());
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void setNextData(RequestHistory data) {
        if (data.getStatus()) {
            adapter.setItems(data.getItemHistories());
        }
    }

    @Override public void showErrorNextPage(String string) {
        Toast.makeText(getContext(), string, Toast.LENGTH_SHORT).show();
        adapter.setErrorNextPage("Tap to retry");
        scrollListener.backToPreviousPage();
    }
}