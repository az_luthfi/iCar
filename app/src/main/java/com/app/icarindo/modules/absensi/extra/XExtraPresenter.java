package com.app.icarindo.modules.absensi.extra;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XExtraPresenter extends MvpPresenter<XExtraView> {

    void getCountExtra(boolean pullToRefresh);

    void requestExtra(String date);
}