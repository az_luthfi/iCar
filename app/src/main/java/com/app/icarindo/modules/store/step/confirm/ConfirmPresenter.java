package com.app.icarindo.modules.store.step.confirm;

import android.content.Context;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.eventbus.EventUpdateProfile;
import com.app.icarindo.models.store.RequestOrder;
import com.app.icarindo.utility.Config;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ConfirmPresenter extends BasePresenter<XConfirmView>
        implements XConfirmPresenter {

    public ConfirmPresenter(Context context) {
        super(context);
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProfileUpdateEvent(EventUpdateProfile event) {
        if (isViewAttached()) {
            getView().setTotal();
        }
    }

    @Override public void attachView(XConfirmView view) {
        super.attachView(view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }

    @Override public void buy(String note, String jsonProduct, String jsonCusShipping) {
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("product", jsonProduct);
        params.put("cus_shipping", jsonCusShipping);
        params.put("orderNote", note);
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("act", "confirm");

        Observer<RequestOrder> observer = new Observer<RequestOrder>() {

            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestOrder data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    Toast.makeText(context, data.getText(), Toast.LENGTH_SHORT).show();
                    if (data.getStatus()){
                        prefs.savePreferences(Config.CUSTOMER_SALDO, data.getCustomerSaldo());
                        getView().setData(data);
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiOrder(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }
}