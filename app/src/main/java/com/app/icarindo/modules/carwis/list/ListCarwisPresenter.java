package com.app.icarindo.modules.carwis.list;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.DataNotification;
import com.app.icarindo.models.carwis.RequestCarwis;
import com.app.icarindo.models.eventbus.EventRefreshListCarwis;
import com.app.icarindo.utility.Config;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ListCarwisPresenter extends BaseRxLcePresenter<XListCarwisView, RequestCarwis>
        implements XListCarwisPresenter {

    public ListCarwisPresenter(Context context) {
        super(context);
    }

    private String status;

    @Override public void loadData(boolean pullToRefresh, String status, int page) {
        this.status = status;
        HashMap<String, String> params = new HashMap<>();
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("page", String.valueOf(page));
        params.put("act", "history");
        params.put("status", status);

        Observable<RequestCarwis> observable = App.getService().apiCarwis(params);
        if (page == 1) {
            subscribe(observable, pullToRefresh);
        } else {
            if (isViewAttached()) {
                getView().showLoadingNextPage(true);
            }
            Observer<RequestCarwis> observer1 = new Observer<RequestCarwis>() {
                @Override public void onSubscribe(Disposable d) {

                }

                @Override public void onNext(RequestCarwis data) {
                    if (isViewAttached()) {
                        getView().showLoadingNextPage(false);
                        getView().setNextData(data);
                    }
                }

                @Override public void onError(Throwable e) {
                    if (isViewAttached()) {
                        getView().showErrorNextPage(context.getString(R.string.error_network));
                    }
                }

                @Override public void onComplete() {

                }
            };

            observable.subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observer1);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventRefresh(EventRefreshListCarwis event) {
        loadData(false, status, 1);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventNotification(DataNotification event) {
        if (event.getMethod().equals("carwis")){
            loadData(false, status, 1);
        }
    }

    @Override public void attachView(XListCarwisView view) {
        super.attachView(view);
        EventBus.getDefault().register(this);
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }
}