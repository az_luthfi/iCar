package com.app.icarindo.modules.carwis.detail;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.DataNotification;
import com.app.icarindo.models.carwis.RequestCarwis;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;

public class DetailCarwisPresenter extends BaseRxLcePresenter<XDetailCarwisView, RequestCarwis>
        implements XDetailCarwisPresenter {

    private String bookingCode;

    public DetailCarwisPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh, String bookingCode) {
        this.bookingCode = bookingCode;
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "detail");
        params.put("bookingCode", bookingCode);

        subscribe(App.getService().apiCarwis(params), pullToRefresh);
    }

    @Override public void validationBooking(String bookingCode) {
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("bookingCode", bookingCode);
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("act", "validation_booking");

        Observer<RequestCarwis> observer = new Observer<RequestCarwis>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCarwis data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().onNextValidationBooking(data);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiCarwis(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEventNotification(DataNotification event) {
        if (event.getMethod().equals("carwis") && event.getDataId().equals(bookingCode)){
            loadData(false, bookingCode);
        }
    }

    @Override public void attachView(XDetailCarwisView view) {
        super.attachView(view);
        EventBus.getDefault().register(this);
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }
}