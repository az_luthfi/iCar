package com.app.icarindo.modules.store.step.updateaddress;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.HashMap;

public interface XAddShippingAddressPresenter extends MvpPresenter<XAddShippingAddressView> {

    void loadProvince();

    void loadCity(String provinceName);

    void resetCity();

    void saveAddress(HashMap<String, String> params, String action);
}