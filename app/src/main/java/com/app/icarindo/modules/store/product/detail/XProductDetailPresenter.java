package com.app.icarindo.modules.store.product.detail;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XProductDetailPresenter extends MvpPresenter<XProductDetailView> {

    void loadData(String productId, boolean pullToRefresh);
}