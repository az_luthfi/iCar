package com.app.icarindo.modules.history.income;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.network.RequestNetwork;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;

public class HistoryIncomePresenter extends BaseRxLcePresenter<XHistoryIncomeView, RequestNetwork>
        implements XHistoryIncomePresenter {

    public HistoryIncomePresenter(Context context) {
        super(context);
    }

    @Override public void loadData(String filterType, String filterDate, boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "history-income-" + filterType);
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("page", "1");
        params.put("filterDate", filterDate);

        subscribe(App.getService().apiNetwork(params), pullToRefresh);
    }

    @Override public void loadDataNextPage(String filterType, String filterDate, int page) {
        if (isViewAttached()) {
            getView().showLoadingNextPage(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "history-income-" + filterType);
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("page", String.valueOf(page));
        params.put("filterDate", filterDate);

        Observer<RequestNetwork> observer1 = new Observer<RequestNetwork>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestNetwork data) {
                if (isViewAttached()) {
                    getView().setNextData(data);
                    getView().showLoadingNextPage(false);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showErrorNextPage(context.getString(R.string.error_network));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiNetwork(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer1);
    }
}