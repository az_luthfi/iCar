package com.app.icarindo.modules.content.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.models.content.Content;
import com.app.icarindo.modules.main.MainActivity;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

@FragmentWithArgs
public class DetailContentFragment extends BaseMvpFragment<XDetailContentView, DetailContentPresenter>
        implements XDetailContentView, MainActivity.OnChangeToolbar {
    @Arg String jsonContent;
    @BindView(R.id.tvContentTitle) TextView tvContentTitle;
    @BindView(R.id.tvContentDate) TextView tvContentDate;
    @BindView(R.id.ivImage) ImageView ivImage;
    @BindView(R.id.wvContentDesc) WebView wvContentDesc;
    private Content content;

    public DetailContentFragment() {

    }

    @Override public DetailContentPresenter createPresenter() {
        return new DetailContentPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_detail_content;
    }

    @Override public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        content = new Gson().fromJson(jsonContent, Content.class);
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        WebSettings ws = wvContentDesc.getSettings();
        ws.setDefaultTextEncodingName("utf-8");
        ws.setBuiltInZoomControls(true);
        ws.setDisplayZoomControls(true);
        ws.setJavaScriptEnabled(true);
        wvContentDesc.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }
        });

        wvContentDesc.loadData(content.getContentDesc(), "text/html", "UTF-8");
        tvContentTitle.setText(content.getContentName());
        tvContentDate.setText(content.getContentCreateDate());
        if (!content.getContentImage().isEmpty() && content.getContentImage().contains("http")) {
            Picasso.with(getContext()).load(content.getContentImage()).into(ivImage);
        }
    }

    @Nullable @Override public String getTitle() {
        return "Detail Content";
    }
}