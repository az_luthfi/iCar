package com.app.icarindo.modules.ppob.pembayaran.model1;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.ppob.PpobProductAdapter;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.models.ppob.PpobProduct;
import com.app.icarindo.models.ppob.PpobProductCategory;
import com.app.icarindo.models.ppob.RequestProductCategory;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.modules.ppob.pembayaran.konfirmasi.KonfirmasiPembayaranFragment;
import com.app.icarindo.modules.ppob.pembayaran.konfirmasi.KonfirmasiPembayaranFragmentBuilder;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Logs;
import com.app.icarindo.utility.listview.DividerItemDecoration;
import com.app.icarindo.utility.listview.ItemClickSupport;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static com.app.icarindo.utility.Config.FINANCE;
import static com.app.icarindo.utility.Config.HP_PASCA;
import static com.app.icarindo.utility.Config.PDAM;

/**
 * Pembayaran PDAM, HP PASCABAYAR, FINANCE
 */
@FragmentWithArgs
public class PembayaranOneFragment extends BaseMvpLceFragment<RelativeLayout, RequestProductCategory, XPembayaranOneView, PembayaranOnePresenter>
        implements XPembayaranOneView, MainActivity.OnChangeToolbar {
    @Arg String title;
    @Arg String type;
    @BindView(R.id.etIdPelanggan) MaterialEditText etIdPelanggan;
    @BindView(R.id.etProduct) MaterialEditText etProduct;
    @BindView(R.id.btnSubmit) Button btnSubmit;
    @BindView(R.id.contentView) NestedScrollView contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.btnGetContact) ImageButton btnGetContact;

    private PpobProductCategory ppobProductCategory;
    private PpobProduct productSelected;

    public PembayaranOneFragment() {

    }

    @Override public PembayaranOnePresenter createPresenter() {
        return new PembayaranOnePresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_pembayaran_one;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        switch (type) {
            case PDAM:
                etProduct.setHint("Pilih Wilayah PDAM");
                etIdPelanggan.setInputType(InputType.TYPE_CLASS_TEXT);
                break;
            case HP_PASCA:
                etProduct.setHint("Pilih Operator");
                break;
            case FINANCE:
                etProduct.setHint("Pilih Layanan");
                break;
        }
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestProductCategory data) {
        if (data.getStatus()) {
            etProduct.setClickable(true);
        }
        ppobProductCategory = data.getPpobProductCategory();
        productSelected = ppobProductCategory.getPpobProducts().get(0);
        etProduct.setText(productSelected.getPpobProductName());
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh, type);
    }

    @Nullable @Override public String getTitle() {
        return title;
    }

    @OnTextChanged(R.id.etIdPelanggan) public void onTextChanged() {
        btnSubmit.setEnabled(etIdPelanggan.getText().length() > 4 && productSelected != null);
    }


    @OnClick({R.id.etProduct, R.id.btnSubmit}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etProduct:
                showDialog();
                break;
            case R.id.btnSubmit:
                CommonUtilities.hideSoftKeyboard(getActivity());
                KonfirmasiPembayaranFragment fragment = new KonfirmasiPembayaranFragmentBuilder(etIdPelanggan.getText().toString())
                        .idProduk(productSelected.getPpobProductId())
                        .typeProduk(type)
                        .build();
                mListener.gotoPage(fragment, false, null);
                etIdPelanggan.setText(null);
                break;
        }
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(getActivity(), android.R.style.DeviceDefault_Light_ButtonBar);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setContentView(R.layout.dialog_ppob_product_chooser);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
        EditText searchEditText = (EditText) dialog.findViewById(R.id.tvSearch);

        final PpobProductAdapter adapter = new PpobProductAdapter();
        adapter.setType(type);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));
        adapter.addAll(ppobProductCategory.getPpobProducts());

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                productSelected = adapter.getItem(position);
                etProduct.setText(productSelected.getPpobProductName());

                dialog.dismiss();
            }
        });

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Logs.d("FragmentPayment => onTextChanged ==> " + s);
                adapter.filter(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        dialog.show();
    }

    @OnClick(R.id.btnGetContact) public void onViewClicked() {
        presenter.getContact();
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override public void setPhoneNumber(String number) {
        etIdPelanggan.setText(number);
    }
}