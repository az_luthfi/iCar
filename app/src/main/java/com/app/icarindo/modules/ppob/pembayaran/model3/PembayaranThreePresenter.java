package com.app.icarindo.modules.ppob.pembayaran.model3;

import android.content.Context;

import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.ppob.Product;
import com.app.icarindo.utility.CommonUtilities;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import static com.app.icarindo.utility.Config.BPJS;
import static com.app.icarindo.utility.Config.PT_PLN_TOKEN;

public class PembayaranThreePresenter extends BasePresenter<XPembayaranThreeView>
        implements XPembayaranThreePresenter {
    private static final int REQUEST_CONTACT = 454;
    private  ArrayList<Product> products;
    public PembayaranThreePresenter(Context context) {
        super(context);
    }

    @Override public void loadProductList(String type) {
        if (products == null){
            Type mapType = new TypeToken<ArrayList<Product>>() {
            }.getType();
            switch (type){
                case BPJS:
                    products = new Gson().fromJson(CommonUtilities.loadJSONFromAsset(context, "periode_bulan.json"), mapType);
                    break;
                case PT_PLN_TOKEN:
                    products = new Gson().fromJson(CommonUtilities.loadJSONFromAsset(context, "pln_token.json"), mapType);
                    break;
            }
        }
        if (isViewAttached()) {
            getView().showProduct(products);
        }
    }
}