package com.app.icarindo.modules.absensi.city;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.utility.Config;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class OutOfCityPresenter extends BasePresenter<XOutOfCityView>
        implements XOutOfCityPresenter {

    public OutOfCityPresenter(Context context) {
        super(context);
    }

    @Override public void requestInput(String date, String desc) {
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("date", date);
        params.put("desc", desc);
        params.put("act", "keluar-kota");
        Observer<RequestNetwork> observer = new Observer<RequestNetwork>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestNetwork data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().onNextRequestInput(data);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiNetwork(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}