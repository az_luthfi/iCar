package com.app.icarindo.modules.account.bank;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.eventbus.EventAddBank;
import com.app.icarindo.models.withdraw.RequestWithdraw;
import com.app.icarindo.utility.Config;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;

public class BankAccountPresenter extends BaseRxLcePresenter<XBankAccountView, RequestWithdraw>
        implements XBankAccountPresenter {

    public BankAccountPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "getBank");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));

        subscribe(App.getService().apiWithdraw(params), pullToRefresh);
    }

    @Override public void saveBank(String bankName, String bankNumber, String bankAccountName) {

        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("act", "saveBank");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("bankName", bankName);
        params.put("bankNumber", bankNumber);
        params.put("bankAccountName", bankAccountName);
        Observer<RequestWithdraw> observer = new Observer<RequestWithdraw>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestWithdraw data) {
                if (isViewAttached()) {
                    if (data.getStatus()){
                        EventBus.getDefault().post(new EventAddBank());
                    }
                    getView().showProgressDialog(false);
                    getView().onNextSave(data);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiWithdraw(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }
}