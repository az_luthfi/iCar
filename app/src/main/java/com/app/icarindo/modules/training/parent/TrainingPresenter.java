package com.app.icarindo.modules.training.parent;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.eventbus.EventUpdateProfile;
import com.app.icarindo.models.training.RequestTraining;
import com.app.icarindo.models.training.Soal;
import com.app.icarindo.utility.Config;
import com.google.gson.Gson;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;

public class TrainingPresenter extends BaseRxLcePresenter<XTrainingView, RequestTraining>
        implements XTrainingPresenter {

    public TrainingPresenter(Context context) {
        super(context);
    }

    @Override public void loadSoal(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "list_soal");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));

        subscribe(App.getService().apiTraining(params), pullToRefresh);
    }

    @Override public void saveAnswer(ArrayList<Soal> soals) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "answer");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("answer", new Gson().toJson(soals));

        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        Observer<RequestTraining> observer = new Observer<RequestTraining>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestTraining data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (data.getStatus()){
                        prefs.savePreferences(Config.TRAINING, false);
                        EventBus.getDefault().post(new EventUpdateProfile());
                        getView().onSuccessAnswer(data.getText());
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiTraining(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}