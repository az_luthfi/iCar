package com.app.icarindo.modules.store.product.detail;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.text.TextUtils;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.holder.SliderItem;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.store.Product;
import com.app.icarindo.models.store.RequestProduct;
import com.app.icarindo.modules.store.product.zoomfoto.ZoomPhotosActivity;
import com.app.icarindo.modules.store.step.address.ShippingAddressFragmentBuilder;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.Str;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@FragmentWithArgs
public class ProductDetailFragment extends BaseMvpLceFragment<RelativeLayout, RequestProduct, XProductDetailView, ProductDetailPresenter>
        implements XProductDetailView, AlertListener, BaseSliderView.OnSliderClickListener {

    @Arg String productId;
    @BindView(R.id.slider) SliderLayout slider;
    @BindView(R.id.progressBar) ProgressBar progressBar;
    @BindView(R.id.tvProductName) TextView tvProductName;
    @BindView(R.id.tvProductPrice) TextView tvProductPrice;
    @BindView(R.id.tvStock) TextView tvStock;
    @BindView(R.id.layMinusStock) FrameLayout layMinusStock;
    @BindView(R.id.tvStockBuy) TextView tvStockBuy;
    @BindView(R.id.layPlusStock) FrameLayout layPlusStock;
    @BindView(R.id.layStockBuy) LinearLayout layStockBuy;
    @BindView(R.id.btnBuyNow) TextView btnBuyNow;
    @BindView(R.id.wvProductDesc) WebView wvProductDesc;
    @BindView(R.id.contentView) NestedScrollView contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.tvSubtotal) TextView tvSubtotal;

    private ArrayList<SliderItem> sliderItems = new ArrayList<>();
    private ArrayList<String> imageUrls = new ArrayList<>();
    private Product product;

    public ProductDetailFragment() {

    }

    @Override public ProductDetailPresenter createPresenter() {
        return new ProductDetailPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_product_detail;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
        WebSettings ws = wvProductDesc.getSettings();
        ws.setDefaultTextEncodingName("utf-8");
        ws.setBuiltInZoomControls(true);
        ws.setDisplayZoomControls(true);
        ws.setJavaScriptEnabled(true);
        wvProductDesc.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                return true;
            }
        });
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestProduct data) {
        if (data.getStatus()) {
            this.product = data.getProduct();
            sliderItems = new ArrayList<>();
            SliderItem sliderItem;
            for (int i = 0; i < product.getMedias().size(); i++) {
                sliderItem = new SliderItem(getContext());
                sliderItem.image(product.getMedias().get(i).getMediaValue()).setScaleType(BaseSliderView.ScaleType.CenterInside);
                imageUrls.add(product.getMedias().get(i).getMediaValue());
                sliderItem.setOnSliderClickListener(this);
                sliderItems.add(sliderItem);
            }
            initSlider();
            tvProductName.setText(CommonUtilities.toHtml(Str.getText(product.getProductName(), "")));
            tvProductPrice.setText(CommonUtilities.toRupiahNumberFormat(Str.getText(product.getProductPricePublish(), "0")));
            tvStock.setText(new StringBuilder("Stok ").append(Str.getText(product.getProductStock(), "0")));
            wvProductDesc.loadData(product.getProductDesc(), "text/html", "UTF-8");
            tvSubtotal.setText(tvProductPrice.getText().toString());
            tvStockBuy.setText("1");
        } else {
            showConfirmDialog(SweetAlertDialog.ERROR_TYPE, "Error.", data.getText(), "OK", null, "load_false", false);
        }
    }

    private void initSlider() {
        for (int i = 0; i < sliderItems.size(); i++) {
            slider.addSlider(sliderItems.get(i));
        }
        progressBar.setVisibility(View.GONE);
        slider.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in));
        slider.setVisibility(View.VISIBLE);
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(productId, pullToRefresh);
    }

    @OnClick({R.id.layMinusStock, R.id.layPlusStock, R.id.btnBuyNow}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layMinusStock:
                if (!TextUtils.isEmpty(tvStockBuy.getText())) {
                    Integer stockBuy = Integer.parseInt(tvStockBuy.getText().toString());
                    if (stockBuy <= 0) {
                        tvStockBuy.setText("0");
                        tvSubtotal.setText("Rp 0");
                        return;
                    } else {
                        tvStockBuy.setText(String.valueOf(stockBuy - 1));
                        Integer productPrice = Integer.parseInt(product.getProductPricePublish());
                        tvSubtotal.setText(CommonUtilities.toRupiahNumberFormat(productPrice * (stockBuy - 1)));
                    }
                }
                break;
            case R.id.layPlusStock:
                if (TextUtils.isEmpty(product.getProductStock())) {
                    tvStockBuy.setText("0");
                    tvSubtotal.setText("Rp 0");
                    return;
                }
                if (!TextUtils.isEmpty(tvStockBuy.getText())) {
                    Integer productStock = Integer.parseInt(product.getProductStock());
                    Integer stockBuy = Integer.parseInt(tvStockBuy.getText().toString());
                    if (stockBuy >= productStock) {
                        tvStockBuy.setText(String.valueOf(productStock));
                        Integer productPrice = Integer.parseInt(product.getProductPricePublish());
                        tvSubtotal.setText(CommonUtilities.toRupiahNumberFormat(productPrice * stockBuy));
                        return;
                    } else {
                        tvStockBuy.setText(String.valueOf(stockBuy + 1));
                        Integer productPrice = Integer.parseInt(product.getProductPricePublish());
                        tvSubtotal.setText(CommonUtilities.toRupiahNumberFormat(productPrice * (stockBuy + 1)));
                    }
                }
                break;
            case R.id.btnBuyNow:
                if (TextUtils.isEmpty(tvStockBuy.getText())) {
                    return;
                }
                Integer stockBuy = Integer.parseInt(tvStockBuy.getText().toString());
                if (stockBuy <= 0) {
                    return;
                }
                product.setQtyBuy(Integer.parseInt(tvStockBuy.getText().toString()));
                mListener.gotoPage(new ShippingAddressFragmentBuilder(true).jsonProduct(new Gson().toJson(product)).build(), false, null);
                break;
        }
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action) {
            case "load_false":
                mListener.back();
                break;
        }
    }

    @Override public void onSliderClick(BaseSliderView slider) {
        Intent intent = new Intent(getActivity(), ZoomPhotosActivity.class);
        String[] imageArray = new String[product.getMedias().size()];
        for (int i = 0; i < product.getMedias().size(); i++) {
            imageArray[i] = product.getMedias().get(i).getMediaValue();
        }
        intent.putExtra("media", imageArray);
        startActivity(intent);
    }
}