package com.app.icarindo.modules.splash.verification;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XVerificationPresenter extends MvpPresenter<XVerificationView> {

    void depositRegister(String productCode, String bankName, Integer customerId, String registerPrice);
}