package com.app.icarindo.modules.carwis.detail;

import com.app.icarindo.models.carwis.RequestCarwis;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XDetailCarwisView extends MvpLceView<RequestCarwis> {

    void showProgressDialog(boolean show);

    void showError(String string);

    void onNextValidationBooking(RequestCarwis data);
}