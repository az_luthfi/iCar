package com.app.icarindo.modules.account.bank;

import com.app.icarindo.models.withdraw.RequestWithdraw;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XBankAccountView extends MvpLceView<RequestWithdraw> {

    void showProgressDialog(boolean show);

    void onNextSave(RequestWithdraw data);

    void showError(String string);
}