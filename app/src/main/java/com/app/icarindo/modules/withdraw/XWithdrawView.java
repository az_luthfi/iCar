package com.app.icarindo.modules.withdraw;

import com.app.icarindo.models.withdraw.RequestWithdraw;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XWithdrawView extends MvpLceView<RequestWithdraw> {

    void showProgressDialog(boolean show);

    void onNextWithdraw(RequestWithdraw data);

    void showError(String string);
}