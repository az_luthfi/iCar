package com.app.icarindo.modules.withdraw;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.withdraw.RequestWithdraw;
import com.app.icarindo.modules.account.bank.BankAccountFragment;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Config;
import com.app.icarindo.utility.GeneralErrorMessage;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class WithdrawFragment extends BaseMvpLceFragment<RelativeLayout, RequestWithdraw, XWithdrawView, WithdrawPresenter>
        implements XWithdrawView, AlertListener {

    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.tvSaldo) TextView tvSaldo;
    @BindView(R.id.etNominal) EditText etNominal;
    @BindView(R.id.etDesc) EditText etDesc;
    @BindView(R.id.buttonNext) Button buttonNext;
    @BindView(R.id.dataView) LinearLayout dataView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.bntAkunBank) Button bntAkunBank;
    @BindView(R.id.emptyBank) LinearLayout emptyBank;
    @BindView(R.id.contentView) ScrollView contentView;

    public WithdrawFragment() {

    }

    @Override public WithdrawPresenter createPresenter() {
        return new WithdrawPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_withdraw;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        tvSaldo.setText(CommonUtilities.toRupiahFormat(presenter.getPrefs().getPreferencesInt(Config.CUSTOMER_SALDO)));
        setAlertListener(this);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestWithdraw data) {
        if (data.getStatus()){
            dataView.setVisibility(View.VISIBLE);
            emptyBank.setVisibility(View.GONE);
            etNominal.setHint("Min " + CommonUtilities.toRupiahNumberFormat(Config.MINIMAL_WITHDRAW));
        }else{
            dataView.setVisibility(View.GONE);
            emptyBank.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh);
    }


    @OnClick({R.id.buttonNext, R.id.bntAkunBank}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonNext:
                CommonUtilities.hideSoftKeyboard(getActivity());
                if (TextUtils.isEmpty(etNominal.getText())) {
                    showToast("Silahkan masukkan nominal deposit");
                    return;
                }
                if (!TextUtils.isDigitsOnly(etNominal.getText())){
                    showToast("Nominal bukan angka");
                    return;
                }
                Integer nominal = 0;
                try {
                    nominal = Integer.parseInt(etNominal.getText().toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (nominal > presenter.getPrefs().getPreferencesInt(Config.CUSTOMER_SALDO)){
                    showToast("Nominal melebihi maksimal penarikan saldo");
                    return;
                }
                if (Integer.parseInt(etNominal.getText().toString()) < Config.MINIMAL_WITHDRAW) {
                    etNominal.requestFocus();
                    etNominal.setError("Nominal penarikan saldo minimal " + CommonUtilities.toRupiahFormat(Config.MINIMAL_WITHDRAW));
                    return;
                }
                if (TextUtils.isEmpty(etDesc.getText())){
                    showToast("Keterangan wajib diisi");
                    return;
                }
                presenter.requestWithdraw(etNominal.getText().toString(), etDesc.getText().toString());
                break;
            case R.id.bntAkunBank:
                mListener.gotoPage(new BankAccountFragment(), false, null);
                break;
        }
    }

    @Override public void showProgressDialog(boolean show) {
        if (show){
            showLoading("Loading...", false);
        }else{
            hideLoading();
        }
    }

    @Override public void onNextWithdraw(RequestWithdraw data) {
        if (data.getStatus()){
            showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Berhasil", data.getText(), "OK", null, "true_withdraw", true);
        }else{
            showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", data.getText(), "Tutup", null, "false_withdraw", true);
        }
    }

    @Override public void showError(String string) {
        showSnackBar(string);
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action){
            case "true_withdraw":
                mListener.back();
                break;
        }
    }
}