package com.app.icarindo.modules.ppob.history.detail.transaction;

import com.app.icarindo.models.ppob.RequestHistory;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XDetailTransactionView extends MvpLceView<RequestHistory> {

}