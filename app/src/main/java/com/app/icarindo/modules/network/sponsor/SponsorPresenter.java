package com.app.icarindo.modules.network.sponsor;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.network.RequestNetwork;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;

public class SponsorPresenter extends BaseRxLcePresenter<XSponsorView, RequestNetwork>
        implements XSponsorPresenter {

    public SponsorPresenter(Context context) {
        super(context);
    }

    @Override public void loadBonusSponsor(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "bonus-sponsor");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("page", "1");

        subscribe(App.getService().apiNetwork(params), pullToRefresh);
    }

    @Override public void loadDataNextPage(int page) {
        if (isViewAttached()) {
            getView().showLoadingNextPage(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "bonus-sponsor");
        params.put("page", String.valueOf(page));
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));

        Observer<RequestNetwork> observer1 = new Observer<RequestNetwork>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestNetwork data) {
                if (isViewAttached()) {
                    getView().setNextData(data);
                    getView().showLoadingNextPage(false);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showErrorNextPage(context.getString(R.string.error_network));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiNetwork(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer1);
    }


}