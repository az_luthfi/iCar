package com.app.icarindo.modules.store.step.address;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XShippingAddressPresenter extends MvpPresenter<XShippingAddressView> {

    void listShipping(boolean pullToRefresh);

    void deleteShippingAddress(String caId);
}