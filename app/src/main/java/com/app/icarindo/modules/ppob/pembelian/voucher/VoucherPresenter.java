package com.app.icarindo.modules.ppob.pembelian.voucher;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.ppob.RequestProductCategory;

import java.util.HashMap;

public class VoucherPresenter extends BaseRxLcePresenter<XVoucherView, RequestProductCategory>
        implements XVoucherPresenter {
    private static final int REQUEST_CONTACT = 454;

    public VoucherPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(String type) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "getProduct");
        params.put("type", type);
        subscribe(App.getService().apiPPOBProductCategory(params), false);
    }

    @Override public void getContact() {
        Intent pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
        pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
        getView().startActivityForResult(pickContactIntent, REQUEST_CONTACT);
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case (REQUEST_CONTACT):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactUri = data.getData();
                    String[] projection = {ContactsContract.CommonDataKinds.Phone.NUMBER};

                    @SuppressLint("Recycle") Cursor cursor = context.getContentResolver().query(contactUri, projection, null, null, null);
                    if (cursor != null) {
                        cursor.moveToFirst();
                        // Retrieve the phone number from the NUMBER column
                        int column = cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
                        String number = cursor.getString(column);
                        number = number.replace("-", "");
                        number = number.replace("+", "");

                        if (number.substring(0,3).equalsIgnoreCase("620")) {
                            number = number.replaceFirst("(?:620)+", "0");
                        }else if (number.substring(0,2).equalsIgnoreCase("62")) {
                            number = number.replaceFirst("(?:62)+", "0");
                        }

                        number = number.replace(" ", "");
                        getView().setPhoneNumber(number);
                    }


                }
                break;
        }
    }
}