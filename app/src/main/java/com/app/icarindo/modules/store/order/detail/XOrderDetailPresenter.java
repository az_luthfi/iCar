package com.app.icarindo.modules.store.order.detail;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XOrderDetailPresenter extends MvpPresenter<XOrderDetailView> {

    void loadData(String orderId, boolean pullToRefresh);

    void cancelOrder(String orderId);
}