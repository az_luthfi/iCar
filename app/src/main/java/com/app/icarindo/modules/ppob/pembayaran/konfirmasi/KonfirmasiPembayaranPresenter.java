package com.app.icarindo.modules.ppob.pembayaran.konfirmasi;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.ppob.RequestInquiry;
import com.app.icarindo.models.ppob.RequestPay;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Logs;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.BPJS;
import static com.app.icarindo.utility.Config.CUSTOMER_ID;
import static com.app.icarindo.utility.Config.CUSTOMER_PIN;
import static com.app.icarindo.utility.Config.TELKOM;

public class KonfirmasiPembayaranPresenter extends BaseRxLcePresenter<XKonfirmasiPembayaranView, RequestInquiry>
        implements XKonfirmasiPembayaranPresenter {
        private static final String TAG = KonfirmasiPembayaranPresenter.class.getSimpleName();
    public KonfirmasiPembayaranPresenter(Context context) {
        super(context);
    }

    @Override public void inquiry(String idPelanggan, String noTelephone, String typeProduk, String idProduk, @Nullable String nominal) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("act", "inquiry");
        params.put("product", typeProduk);
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("idPelanggan1", idPelanggan);
        params.put("idPelanggan2", "");
        if (noTelephone == null){
            params.put("idPelanggan3", "");
        }else{
            params.put("idPelanggan3", noTelephone);
        }
//        if (typeProduk.equals(HP_PASCA) || typeProduk.equals(PDAM) || typeProduk.equals(TELKOM) || typeProduk.equals(FINANCE)){
//            params.put("idPelanggan3", "");
//        }else{
//            params.put("idPelanggan3", noTelephone);
//        }

        if (idProduk != null) {
            params.put("productId", idProduk);
        }
        if (noTelephone == null) {
            params.put("idPelanggan2", "");
        }
        if (nominal != null) {
            params.put("nominal", nominal);
        }
        if (typeProduk.equals(BPJS)){
            params.put("periodebulan", nominal);
        }
        if (typeProduk.equals(TELKOM) && !TextUtils.isEmpty(noTelephone)){
            params.put("idPelanggan2", noTelephone);
        }
        Logs.d(TAG + "inquiry = " + params.toString());

        subscribe(App.getService().apiInquiryRequest(params), false);
    }

    @Override public void payment(String idPelanggan, String noTelephone, String typeProduk, String pin, RequestInquiry responsInquiry) {
        if (isViewAttached()){
            getView().shoProgressDialog(true);
        }

        HashMap<String, String> params = new HashMap<String, String>();
        params.put("act", "payment");
        params.put("ref2", responsInquiry.getRef2());
        params.put("trxId", responsInquiry.getTrxId());
        params.put("productId", responsInquiry.getProductId());
        params.put("biaya", String.valueOf(responsInquiry.getBiaya()));
        params.put("nominal", String.valueOf(responsInquiry.getNominal()));
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("pass", CommonUtilities.secure("payment", prefs.getPreferencesString(CUSTOMER_PIN)));
        params.put("idPelanggan1", idPelanggan);
        params.put("idPelanggan2", noTelephone);
        params.put("idPelanggan3", "");

        if (noTelephone == null) {
            params.put("idPelanggan2", "");
        }

        Observer<RequestPay> observer = new Observer<RequestPay>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestPay requestPay) {
                if (isViewAttached()) {
                    getView().shoProgressDialog(false);
                    if (requestPay.getStatus()) {
                        getView().successPayment(requestPay);
                    } else {
                        getView().onErrorPaymentStatus(requestPay.getText());
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()){
                    getView().shoProgressDialog(false);
                    getView().onErrorPayment(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {
            }
        };

        App.getService().apiPaymentRequest(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}