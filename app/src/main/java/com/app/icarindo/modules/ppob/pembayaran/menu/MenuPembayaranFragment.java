package com.app.icarindo.modules.ppob.pembayaran.menu;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.app.icarindo.R;
import com.app.icarindo.adapter.ppob.MenuPembayaranAdapter;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.models.ppob.MenuPembayaran;
import com.app.icarindo.modules.ppob.pembayaran.model1.PembayaranOneFragmentBuilder;
import com.app.icarindo.modules.ppob.pembayaran.model2.PembayaranTwoFragmentBuilder;
import com.app.icarindo.modules.ppob.pembayaran.model3.PembayaranThreeFragmentBuilder;
import com.app.icarindo.modules.ppob.pembayaran.model4.PembayaranFourFragmentBuilder;
import com.app.icarindo.modules.ppob.pembayaran.model5.PembayaranFiveFragmentBuilder;
import com.app.icarindo.modules.ppob.pembelian.pulsa.PulsaFragmentBuilder;
import com.app.icarindo.modules.ppob.pembelian.voucher.VoucherFragmentBuilder;
import com.app.icarindo.modules.store.product.list.ProductListFragment;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.listview.ItemClickSupport;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;

import static com.app.icarindo.utility.Config.BPJS;
import static com.app.icarindo.utility.Config.FINANCE;
import static com.app.icarindo.utility.Config.HP_PASCA;
import static com.app.icarindo.utility.Config.PAKET_DATA;
import static com.app.icarindo.utility.Config.PDAM;
import static com.app.icarindo.utility.Config.PT_PLN_TOKEN;
import static com.app.icarindo.utility.Config.PULSA;
import static com.app.icarindo.utility.Config.TV_CABLE;
import static com.app.icarindo.utility.Config.VOUCHER;

public class MenuPembayaranFragment extends BaseMvpFragment<XMenuPembayaranView, MenuPembayaranPresenter>
        implements XMenuPembayaranView{

    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    private MenuPembayaranAdapter adapter;
    public MenuPembayaranFragment() {

    }

    @Override public MenuPembayaranPresenter createPresenter() {
        return new MenuPembayaranPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_menu_pembayaran;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new MenuPembayaranAdapter();
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);
        recyclerView.setAdapter(adapter);
        Type mapType = new TypeToken<ArrayList<MenuPembayaran>>() {
        }.getType();
        ArrayList<MenuPembayaran> menuPembayaren = new Gson().fromJson(CommonUtilities.loadJSONFromAsset(getContext(), "menu_transaksi.json"), mapType);

        adapter.setItems(menuPembayaren);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                MenuPembayaran menu = adapter.getItem(position);
                switch (menu.getIcon()){
                    case "pulsa":
                        mListener.gotoPage(new PulsaFragmentBuilder(PULSA).build(), false, null);
                        break;
                    case "voucher":
                        mListener.gotoPage(new VoucherFragmentBuilder(VOUCHER).build(), false, null);
                        break;
                    case "paket_data":
                        mListener.gotoPage(new PulsaFragmentBuilder(PAKET_DATA).build(), false, null);
                        break;
                    case "pln_pasca": // model 2
                        mListener.gotoPage(new PembayaranTwoFragmentBuilder(menu.getTitle()).build(), false, null);
                        break;
                    case "pln": // model 3
                        mListener.gotoPage(new PembayaranThreeFragmentBuilder(menu.getTitle(), PT_PLN_TOKEN).build(), false, null);
                        break;
                    case "pdam": // model 1
                        mListener.gotoPage(new PembayaranOneFragmentBuilder(menu.getTitle(), PDAM).build(), false, null);
                        break;
                    case "phone": // model 1
                        mListener.gotoPage(new PembayaranOneFragmentBuilder(menu.getTitle(), HP_PASCA).build(), false, null);
                        break;
                    case "finance": // model 1
                        mListener.gotoPage(new PembayaranOneFragmentBuilder(menu.getTitle(), FINANCE).build(), false, null);
                        break;
                    case "telkom": // model 4
                        mListener.gotoPage(new PembayaranFourFragmentBuilder(menu.getTitle(), BPJS).build(), false, null);
                        break;
                    case "tv": // model 5
                        mListener.gotoPage(new PembayaranFiveFragmentBuilder(menu.getTitle(), TV_CABLE).build(), false, null);
                        break;
                    case "bpjs": // model 3
                        mListener.gotoPage(new PembayaranThreeFragmentBuilder(menu.getTitle(), BPJS).build(), false, null);
                        break;
                    case "product":
                        mListener.gotoPage(new ProductListFragment(), false, null);
                        break;
                }
            }
        });

    }

//    @Nullable @Override public String getTitle() {
//        return "Pembayaran";
//    }
}