package com.app.icarindo.modules.carwis.list;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.carwis.BookingAdapter;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.carwis.RequestCarwis;
import com.app.icarindo.modules.carwis.detail.DetailCarwisFragment;
import com.app.icarindo.modules.carwis.detail.DetailCarwisFragmentBuilder;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.listview.EndlessRecyclerOnScrollListener;
import com.app.icarindo.utility.listview.ItemClickSupport;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import butterknife.BindView;

import static com.app.icarindo.utility.Config.PAGINATION_LIMIT;

@FragmentWithArgs
public class ListCarwisFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestCarwis, XListCarwisView, ListCarwisPresenter>
        implements XListCarwisView, ItemLoadingHolder.ItemViewLoadingListener {

    @Arg String status;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private BookingAdapter adapter;
    private EndlessRecyclerOnScrollListener scrollListener;

    public ListCarwisFragment() {

    }

    @Override public ListCarwisPresenter createPresenter() {
        return new ListCarwisPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_with_recyclerview_refresh;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new BookingAdapter(this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                DetailCarwisFragment fragment = new DetailCarwisFragmentBuilder(adapter.getItem(position).getCtBookingCode()).build();
                String tagName = DetailCarwisFragment.class.getSimpleName() + adapter.getItem(position).getCtBookingCode();
                mListener.gotoPage(fragment, false, tagName);
            }
        });

        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    presenter.loadData(false, status, page);
                }
            }
        };
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestCarwis data) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        if (data.getStatus()) {
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            adapter.setItems(data.getTransactions());
            if (data.getTransactions().size() < PAGINATION_LIMIT) {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        } else {
            if (adapter.getItemCount() == 0) {
                emptyView.setVisibility(View.VISIBLE);
                emptyView.setText(data.getText());
                recyclerView.setVisibility(View.GONE);
            } else {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        scrollListener.resetState();
        recyclerView.addOnScrollListener(scrollListener);
        presenter.loadData(pullToRefresh, status, 1);
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadData(false, status, scrollListener.getCurrentPage());
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void setNextData(RequestCarwis data) {
        if (data.getStatus()) {
            adapter.setItems(data.getTransactions());
            if (data.getTransactions().size() < PAGINATION_LIMIT) {
                recyclerView.removeOnScrollListener(scrollListener);
            }
        } else {
            recyclerView.removeOnScrollListener(scrollListener);
        }
    }

    @Override public void showErrorNextPage(String string) {
        showToast(string);
        adapter.setErrorNextPage("Tap to retry");
        scrollListener.backToPreviousPage();
    }
}