package com.app.icarindo.modules.splash.verificationconfirm;

import com.app.icarindo.models.customer.RequestCustomer;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XVerificationConfirmView extends MvpView {

    void showProgressDialog(boolean show);

    void showError(String text);

    void onDepositRegisterConfirm(RequestCustomer requestCustomer);
}