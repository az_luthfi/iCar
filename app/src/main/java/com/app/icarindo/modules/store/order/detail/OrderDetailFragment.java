package com.app.icarindo.modules.store.order.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.store.ProductOrder;
import com.app.icarindo.models.store.ProductOrderDetail;
import com.app.icarindo.models.store.RequestOrder;
import com.app.icarindo.modules.history.HistoryFragment;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.FragmentStack;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.Str;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@FragmentWithArgs
public class OrderDetailFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestOrder, XOrderDetailView, OrderDetailPresenter>
        implements XOrderDetailView, AlertListener, FragmentStack.OnBackPressedHandlingFragment {

    @Arg String orderId;

    @BindView(R.id.tvInvoiceNumber) TextView tvInvoiceNumber;
    @BindView(R.id.tvOrderStatusInfo) TextView tvOrderStatusInfo;
    @BindView(R.id.tvFullAddress) TextView tvFullAddress;
    @BindView(R.id.layShippingAddress) LinearLayout layShippingAddress;
    @BindView(R.id.tvDesc) TextView tvDesc;
    @BindView(R.id.layNotes) LinearLayout layNotes;
    @BindView(R.id.tvShippingPrice) TextView tvShippingPrice;
    @BindView(R.id.tvTotalPayment) TextView tvTotalPayment;
    @BindView(R.id.tvCashback) TextView tvCashback;
    @BindView(R.id.productImage) ImageView productImage;
    @BindView(R.id.productName) TextView productName;
    @BindView(R.id.productPrice) TextView productPrice;
    @BindView(R.id.productQty) TextView productQty;
    @BindView(R.id.tvInvoiceCreateDate) TextView tvInvoiceCreateDate;
    @BindView(R.id.layInvoiceCreateDate) LinearLayout layInvoiceCreateDate;
    @BindView(R.id.tvInvoicePaidDate) TextView tvInvoicePaidDate;
    @BindView(R.id.layInvoicePaidDate) LinearLayout layInvoicePaidDate;
    @BindView(R.id.tvInvoiceProcessDate) TextView tvInvoiceProcessDate;
    @BindView(R.id.layInvoiceProcessDate) LinearLayout layInvoiceProcessDate;
    @BindView(R.id.tvInvoiceFinishDate) TextView tvInvoiceFinishDate;
    @BindView(R.id.layInvoiceFinishDate) LinearLayout layInvoiceFinishDate;
    @BindView(R.id.tvInvoiceCancelDate) TextView tvInvoiceCancelDate;
    @BindView(R.id.layInvoiceCancelDate) LinearLayout layInvoiceCancelDate;
    @BindView(R.id.tvInvoiceRefundDate) TextView tvInvoiceRefundDate;
    @BindView(R.id.layInvoiceRefundDate) LinearLayout layInvoiceRefundDate;
    @BindView(R.id.btnCancelOrder) Button btnCancelOrder;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private ProductOrder productOrder;
    private ProductOrderDetail productOrderDetail;

    public OrderDetailFragment() {

    }

    @Override public OrderDetailPresenter createPresenter() {
        return new OrderDetailPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_order_detail;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestOrder data) {
        if (data.getStatus()) {
            productOrder = data.getProductOrder();
            productOrderDetail = data.getProductOrder().getProductOrderDetails().get(0);

            tvInvoiceNumber.setText(new StringBuilder("Order Code : ").append(Str.getText(productOrder.getOrderCode(), "")));
            tvOrderStatusInfo.setText(Str.getText(productOrder.getOrderStatus().toUpperCase(), ""));

            tvFullAddress.setText(productOrder.getOrderShippingAddress());
            tvDesc.setText(Str.getText(productOrder.getOrderDesc(), ""));
            tvTotalPayment.setText(CommonUtilities.toRupiahNumberFormat(Str.getText(productOrder.getOrderTotal(), "0")));
            tvCashback.setText(CommonUtilities.toRupiahNumberFormat(Str.getText(productOrder.getOrderCashback(), "0")));
            Picasso.with(getContext()).load(productOrderDetail.getProductImage()).into(productImage);
            productName.setText(productOrderDetail.getProductName());
            productPrice.setText(CommonUtilities.toRupiahNumberFormat(Str.getText(productOrderDetail.getOdItemPrice(), "0")));
            productQty.setText(new StringBuilder("x ").append(Str.getText(productOrderDetail.getOdQty(), "")));

            if (productOrder.getOrderStatus().equals("paid")) {
                btnCancelOrder.setVisibility(View.VISIBLE);
            } else {
                btnCancelOrder.setVisibility(View.GONE);
            }

            tvInvoiceCreateDate.setText(productOrder.getOrderCreateDate());
            if (!TextUtils.isEmpty(productOrder.getOrderProcessDate())) {
                layInvoiceProcessDate.setVisibility(View.VISIBLE);
                tvInvoiceProcessDate.setText(productOrder.getOrderProcessDate());
            }
            if (!TextUtils.isEmpty(productOrder.getOrderCancelDate())) {
                layInvoiceCancelDate.setVisibility(View.VISIBLE);
                tvInvoiceCancelDate.setText(productOrder.getOrderCancelDate());
            }

        } else {
            showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Error", data.getText(), "Tutup", null, "error_data", false);
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(orderId, pullToRefresh);
    }

    @OnClick(R.id.btnCancelOrder) public void onViewClicked() {
        showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", "Apakah Anda yakin akan membatalkan pesanan ini?", "YA", "Tidak", "cancel_order", true);
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action) {
            case "error_data":
                mListener.back();
                break;
            case "cancel_order":
                presenter.cancelOrder(productOrder.getOrderId());
                break;
        }
    }

    @Override public void showProgressDialog(boolean show) {
        if (show){
            showLoading("Loading...", false);
        }else{
            hideLoading();
        }
    }

    @Override public void showError(String string) {
        showSnackBar(string);
    }

    @Override public boolean onBackPressed() {
        mListener.gotoPage(new HistoryFragment(), false, null);
        return true;
    }
}