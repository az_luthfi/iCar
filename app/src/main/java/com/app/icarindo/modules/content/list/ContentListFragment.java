package com.app.icarindo.modules.content.list;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.content.ContentListAdapter;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.content.RequestContent;
import com.app.icarindo.modules.content.detail.DetailContentFragmentBuilder;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.utility.listview.EndlessRecyclerOnScrollListener;
import com.app.icarindo.utility.listview.ItemClickSupport;
import com.google.gson.Gson;

import butterknife.BindView;

public class ContentListFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestContent, XContentListView, ContentListPresenter>
        implements XContentListView, MainActivity.OnChangeToolbar, ItemLoadingHolder.ItemViewLoadingListener {

    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private ContentListAdapter adapter;
    private int maxCountItems;
    private EndlessRecyclerOnScrollListener scrollListener;

    public ContentListFragment() {

    }

    @Override public ContentListPresenter createPresenter() {
        return new ContentListPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_content_list;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new ContentListAdapter(this);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, final int position, View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {
                        mListener.gotoPage(new DetailContentFragmentBuilder(new Gson().toJson(adapter.getItem(position))).build(), false, String.valueOf(position));
                    }
                }, 300);
            }
        });

        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    if (adapter.getItemCount() < maxCountItems) {
                        presenter.loadDataNextPage(page);
                    }
                }
            }
        };

        recyclerView.addOnScrollListener(scrollListener);

        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        if (pullToRefresh) {
            return getContext().getString(R.string.error_network_light);
        }
        return getContext().getString(R.string.error_network);
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh);
    }

    @Override public void setData(final RequestContent data) {
        resetAdapter();
        if (data.getStatus()) {
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            maxCountItems = data.getCount();
            adapter.setItems(data.getContents());
        } else {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void resetAdapter() {
        if (adapter != null && adapter.getItemCount() > 0) {
            adapter.clear();
            scrollListener.resetState();
        }
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void showErrorNextPage(String msg) {
        showToast(msg);
        adapter.setErrorNextPage("Tap to retry");
        scrollListener.backToPreviousPage();
    }

    @Override public void setNextData(RequestContent data) {
        if (data.getStatus()) {
            adapter.setItems(data.getContents());
        }
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadDataNextPage(scrollListener.getCurrentPage());
    }

    @Nullable @Override public String getTitle() {
        return "Informasi";
    }
}