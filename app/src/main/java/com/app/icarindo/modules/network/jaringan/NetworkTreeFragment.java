package com.app.icarindo.modules.network.jaringan;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.models.network.Tree;
import com.app.icarindo.modules.register.RegisterFragmentBuilder;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;

public class NetworkTreeFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestNetwork, XNetworkTreeView, NetworkTreePresenter>
        implements XNetworkTreeView {

    @BindView(R.id.ivImageIsset1) ImageView ivImageIsset1;
    @BindView(R.id.tvNameIsset1) TextView tvNameIsset1;
    @BindView(R.id.tvUserIdIsset1) TextView tvUserIdIsset1;
    @BindView(R.id.layIsset1) LinearLayout layIsset1;
    @BindView(R.id.layEmpty2) LinearLayout layEmpty2;
    @BindView(R.id.ivImageIsset2) ImageView ivImageIsset2;
    @BindView(R.id.tvNameIsset2) TextView tvNameIsset2;
    @BindView(R.id.tvUserIdIsset2) TextView tvUserIdIsset2;
    @BindView(R.id.layIsset2) LinearLayout layIsset2;
    @BindView(R.id.layEmpty3) LinearLayout layEmpty3;
    @BindView(R.id.ivImageIsset3) ImageView ivImageIsset3;
    @BindView(R.id.tvNameIsset3) TextView tvNameIsset3;
    @BindView(R.id.tvUserIdIsset3) TextView tvUserIdIsset3;
    @BindView(R.id.layIsset3) LinearLayout layIsset3;
    @BindView(R.id.layEmpty4) LinearLayout layEmpty4;
    @BindView(R.id.ivImageIsset4) ImageView ivImageIsset4;
    @BindView(R.id.tvNameIsset4) TextView tvNameIsset4;
    @BindView(R.id.tvUserIdIsset4) TextView tvUserIdIsset4;
    @BindView(R.id.layIsset4) LinearLayout layIsset4;
    @BindView(R.id.layEmpty5) LinearLayout layEmpty5;
    @BindView(R.id.ivImageIsset5) ImageView ivImageIsset5;
    @BindView(R.id.tvNameIsset5) TextView tvNameIsset5;
    @BindView(R.id.tvUserIdIsset5) TextView tvUserIdIsset5;
    @BindView(R.id.layIsset5) LinearLayout layIsset5;
    @BindView(R.id.layEmpty6) LinearLayout layEmpty6;
    @BindView(R.id.ivImageIsset6) ImageView ivImageIsset6;
    @BindView(R.id.tvNameIsset6) TextView tvNameIsset6;
    @BindView(R.id.tvUserIdIsset6) TextView tvUserIdIsset6;
    @BindView(R.id.layIsset6) LinearLayout layIsset6;
    @BindView(R.id.layEmpty7) LinearLayout layEmpty7;
    @BindView(R.id.ivImageIsset7) ImageView ivImageIsset7;
    @BindView(R.id.tvNameIsset7) TextView tvNameIsset7;
    @BindView(R.id.tvUserIdIsset7) TextView tvUserIdIsset7;
    @BindView(R.id.layIsset7) LinearLayout layIsset7;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.ivImageIsset0) ImageView ivImageIsset0;
    @BindView(R.id.tvNameIsset0) TextView tvNameIsset0;
    @BindView(R.id.tvUserIdIsset0) TextView tvUserIdIsset0;
    @BindView(R.id.layIsset0) LinearLayout layIsset0;

    private ArrayList<Tree> trees;

    public NetworkTreeFragment() {

    }

    @Override public NetworkTreePresenter createPresenter() {
        return new NetworkTreePresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_network_tree;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh, String.valueOf(presenter.getPrefs().getPreferencesInt(CUSTOMER_ID)));
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        if (pullToRefresh) {
            return getContext().getString(R.string.error_network_light);
        }
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestNetwork data) {
        if (data.getStatus()) {
            resetLayout();
            trees = data.getTrees();
            if (trees.get(0) != null) {
                layIsset0.setVisibility(View.VISIBLE);
                setImage(trees.get(0).getCustomerImage(), ivImageIsset0);
                tvNameIsset0.setText(trees.get(0).getCustomerName());
                tvUserIdIsset0.setText(trees.get(0).getCustomerKode());
            }else{
                layIsset0.setVisibility(View.INVISIBLE);
            }
            setImage(trees.get(1).getCustomerImage(), ivImageIsset1);
            tvNameIsset1.setText(trees.get(1).getCustomerName());
            tvUserIdIsset1.setText(trees.get(1).getCustomerKode());
            setData2(trees);
            setData3(trees);
        } else {
            showToast(data.getText());
        }
    }

    private void setData2(ArrayList<Tree> trees) {
        if (trees.get(2) != null) {
            layIsset2.setVisibility(View.VISIBLE);
            layEmpty2.setVisibility(View.GONE);
            setImage(trees.get(2).getCustomerImage(), ivImageIsset2);
            tvNameIsset2.setText(trees.get(2).getCustomerName());
            tvUserIdIsset2.setText(trees.get(2).getCustomerKode());
            setData4(trees);
            setData5(trees);
        } else {
            layIsset2.setVisibility(View.GONE);
            layEmpty2.setVisibility(View.VISIBLE);
            layEmpty4.setVisibility(View.GONE);
            layIsset4.setVisibility(View.GONE);
            layEmpty5.setVisibility(View.GONE);
            layIsset5.setVisibility(View.GONE);
        }
    }

    private void setData3(ArrayList<Tree> trees) {
        if (trees.get(3) != null) {
            layIsset3.setVisibility(View.VISIBLE);
            layEmpty3.setVisibility(View.GONE);
            setImage(trees.get(3).getCustomerImage(), ivImageIsset3);
            tvNameIsset3.setText(trees.get(3).getCustomerName());
            tvUserIdIsset3.setText(trees.get(3).getCustomerKode());
            setData6(trees);
            setData7(trees);
        } else {
            layIsset3.setVisibility(View.GONE);
            layEmpty3.setVisibility(View.VISIBLE);
            layEmpty6.setVisibility(View.GONE);
            layIsset6.setVisibility(View.GONE);
            layEmpty7.setVisibility(View.GONE);
            layIsset7.setVisibility(View.GONE);
        }
    }

    private void setData4(ArrayList<Tree> trees) {
        if (trees.get(4) != null) {
            layIsset4.setVisibility(View.VISIBLE);
            layEmpty4.setVisibility(View.GONE);
            setImage(trees.get(4).getCustomerImage(), ivImageIsset4);
            tvNameIsset4.setText(trees.get(4).getCustomerName());
            tvUserIdIsset4.setText(trees.get(4).getCustomerKode());
        } else {
            layIsset4.setVisibility(View.GONE);
            layEmpty4.setVisibility(View.VISIBLE);
        }
    }

    private void setData5(ArrayList<Tree> trees) {
        if (trees.get(5) != null) {
            layIsset5.setVisibility(View.VISIBLE);
            layEmpty5.setVisibility(View.GONE);
            setImage(trees.get(5).getCustomerImage(), ivImageIsset5);
            tvNameIsset5.setText(trees.get(5).getCustomerName());
            tvUserIdIsset5.setText(trees.get(5).getCustomerKode());
        } else {
            layIsset5.setVisibility(View.GONE);
            layEmpty5.setVisibility(View.VISIBLE);
        }
    }

    private void setData6(ArrayList<Tree> trees) {
        if (trees.get(6) != null) {
            layIsset6.setVisibility(View.VISIBLE);
            layEmpty6.setVisibility(View.GONE);
            setImage(trees.get(6).getCustomerImage(), ivImageIsset6);
            tvNameIsset6.setText(trees.get(6).getCustomerName());
            tvUserIdIsset6.setText(trees.get(6).getCustomerKode());
        } else {
            layIsset6.setVisibility(View.GONE);
            layEmpty6.setVisibility(View.VISIBLE);
        }
    }

    private void setData7(ArrayList<Tree> trees) {
        if (trees.get(7) != null) {
            layIsset7.setVisibility(View.VISIBLE);
            layEmpty7.setVisibility(View.GONE);
            setImage(trees.get(7).getCustomerImage(), ivImageIsset7);
            tvNameIsset7.setText(trees.get(7).getCustomerName());
            tvUserIdIsset7.setText(trees.get(7).getCustomerKode());
        } else {
            layIsset7.setVisibility(View.GONE);
            layEmpty7.setVisibility(View.VISIBLE);
        }
    }

    private void setImage(String url, ImageView imageView) {
        if (!TextUtils.isEmpty(url)) {
            Picasso.with(getContext()).load(url).placeholder(R.drawable.person_default).into(imageView);
        }
    }

    private void resetLayout() {
        layIsset2.setVisibility(View.INVISIBLE);

        layIsset2.setVisibility(View.GONE);
        layEmpty2.setVisibility(View.VISIBLE);

        layIsset3.setVisibility(View.GONE);
        layEmpty3.setVisibility(View.VISIBLE);

        layIsset4.setVisibility(View.GONE);
        layEmpty4.setVisibility(View.GONE);

        layIsset5.setVisibility(View.GONE);
        layEmpty5.setVisibility(View.GONE);

        layIsset6.setVisibility(View.GONE);
        layEmpty6.setVisibility(View.GONE);

        layIsset7.setVisibility(View.GONE);
        layEmpty7.setVisibility(View.GONE);
    }


    @OnClick({R.id.layIsset0, R.id.layIsset1, R.id.layIsset2, R.id.layIsset3, R.id.layIsset4, R.id.layIsset5, R.id.layIsset6, R.id.layIsset7}) public void onIssetClicked(final View view) {
        if (contentView.isRefreshing()){
            return;
        }
        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                switch (view.getId()) {
                    case R.id.layIsset0:
                        if (trees.get(0) == null) {
                            return;
                        }
                        if (trees.get(1) != null && trees.get(1).getCustomerId().equals(String.valueOf(presenter.getPrefs().getPreferencesInt(CUSTOMER_ID)))) {
                            return;
                        }
                        presenter.loadData(true, trees.get(0).getCustomerId());
                        break;
                    case R.id.layIsset1:
//                if (!trees.get(0).getCustomerId().equals(String.valueOf(presenter.getPrefs().getPreferencesInt(CUSTOMER_ID)))) {
//
//                }
                        break;
                    case R.id.layIsset2:
//                if (trees.get(3) == null && trees.get(4) == null) {
//                    return;
//                }
//                presenter.loadData(false, trees.get(1).getCustomerId());
                        break;
                    case R.id.layIsset3:
//                if (trees.get(5) == null && trees.get(6) == null) {
//                    return;
//                }
//                presenter.loadData(false, trees.get(2).getCustomerId());
                        break;
                    case R.id.layIsset4:
                        presenter.loadData(true, trees.get(4).getCustomerId());
                        break;
                    case R.id.layIsset5:
                        presenter.loadData(true, trees.get(5).getCustomerId());
                        break;
                    case R.id.layIsset6:
                        presenter.loadData(true, trees.get(6).getCustomerId());
                        break;
                    case R.id.layIsset7:
                        presenter.loadData(true, trees.get(7).getCustomerId());
                        break;
                }
            }
        }, 100);

    }

    @OnClick({R.id.layEmpty2, R.id.layEmpty3, R.id.layEmpty4, R.id.layEmpty5, R.id.layEmpty6, R.id.layEmpty7}) public void onEmptyClicked(View view) {
        switch (view.getId()) {
            case R.id.layEmpty2:
                mListener.gotoPage(new RegisterFragmentBuilder("2", trees.get(1)).build(), false, null);
                break;
            case R.id.layEmpty3:
                mListener.gotoPage(new RegisterFragmentBuilder("1", trees.get(1)).build(), false, null);
                break;
            case R.id.layEmpty4:
                mListener.gotoPage(new RegisterFragmentBuilder("2", trees.get(2)).build(), false, null);
                break;
            case R.id.layEmpty5:
                mListener.gotoPage(new RegisterFragmentBuilder("1", trees.get(2)).build(), false, null);
                break;
            case R.id.layEmpty6:
                mListener.gotoPage(new RegisterFragmentBuilder("2", trees.get(3)).build(), false, null);
                break;
            case R.id.layEmpty7:
                mListener.gotoPage(new RegisterFragmentBuilder("1", trees.get(3)).build(), false, null);
                break;
        }
    }
}