package com.app.icarindo.modules.home;

import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.RequestHome;
import com.app.icarindo.models.eventbus.EventUpdateProfile;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;

public class HomePresenter extends BaseRxLcePresenter<XHomeView, RequestHome>
        implements XHomePresenter {

    public HomePresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "home");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("section", "berita");

        subscribe(App.getService().apiHomeRequest(params), pullToRefresh);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProfileUpdateEvent(EventUpdateProfile event) {
        loadData(false);
    }

    @Override public void attachView(XHomeView view) {
        super.attachView(view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }
}