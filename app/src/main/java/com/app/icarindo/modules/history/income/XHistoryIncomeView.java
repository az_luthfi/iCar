package com.app.icarindo.modules.history.income;

import com.app.icarindo.models.network.RequestNetwork;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XHistoryIncomeView extends MvpLceView<RequestNetwork> {
    void showLoadingNextPage(boolean show);

    void setNextData(RequestNetwork data);

    void showErrorNextPage(String string);
}