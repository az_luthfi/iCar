package com.app.icarindo.modules.network.jaringan;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XNetworkTreePresenter extends MvpPresenter<XNetworkTreeView> {

    void loadData(boolean pullToRefresh, String customerId);
}