package com.app.icarindo.modules.ppob.pembayaran.konfirmasi;

import com.app.icarindo.models.ppob.RequestInquiry;
import com.app.icarindo.models.ppob.RequestPay;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XKonfirmasiPembayaranView extends MvpLceView<RequestInquiry> {

    void shoProgressDialog(boolean show);

    void onErrorPayment(String contextString);

    void successPayment(RequestPay requestPay);

    void onErrorPaymentStatus(String text);
}