package com.app.icarindo.modules.training.materi;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.MateriAdapter;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.models.training.RequestTraining;
import com.app.icarindo.modules.training.parent.TrainingFragment;
import com.app.icarindo.services.DownloadService;
import com.app.icarindo.services.MrCatzDownload;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.listview.DividerItemDecoration;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class MateriFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestTraining, XMateriView, MateriPresenter>
        implements XMateriView, MateriAdapter.MateriListener {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.btnStart) Button btnStart;
    @BindView(R.id.layAction) RelativeLayout layAction;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private MateriAdapter adapter;

    public MateriFragment() {

    }

    @Override public MateriPresenter createPresenter() {
        return new MateriPresenter(getContext(), getActivity());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_materi;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new MateriAdapter(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));
        recyclerView.setNestedScrollingEnabled(false);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);

        presenter.requestPermissionsStorage();
        registerReceiver();
    }

    @Override public void onAllowStoragePermissions() {
        loadData(false);
    }

    @Override public void onDeniedStoragePermissions() {
        mListener.back();
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestTraining data) {
        if (data.getStatus()){
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            if (adapter.getItemCount() > 0){
                adapter.clear();
            }
            adapter.setItems(data.getMedias());
        }else{
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadMateri(pullToRefresh);
    }


    @OnClick(R.id.btnStart) public void onViewClicked() {
        mListener.gotoPage(new TrainingFragment(), false, null);
    }

    @Override public void downloadFile(String mediaValueUrl, String mediaValue) {
        Intent intent = new Intent(getContext(), DownloadService.class);
        intent.putExtra("filename", mediaValue);
        intent.putExtra("url", mediaValueUrl);
        getContext().startService(intent);
    }

    private void registerReceiver() {

        LocalBroadcastManager bManager = LocalBroadcastManager.getInstance(getContext());
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("message_progress");
        bManager.registerReceiver(broadcastReceiver, intentFilter);

    }

    private BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals("message_progress")) {
                if (MateriFragment.this.getContext() != null) {
                    MrCatzDownload download = intent.getParcelableExtra("download");
                    String filename = intent.getExtras().getString("filename");
                    boolean status = intent.getExtras().getBoolean("status");
                    if (download.getProgress() == 100) {
                        hideLoading();
                        if (status) {
                            adapter.notifyDataSetChanged();
                            showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Success", "File telah tersimpan di " + Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + filename, "OK", null, "success-download", true);
                        }
                    }
                }

            }
        }
    };
}