package com.app.icarindo.modules.account.account;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.models.eventbus.EventDashboard;
import com.app.icarindo.models.network.Plan;
import com.app.icarindo.modules.account.bank.BankAccountFragment;
import com.app.icarindo.modules.account.dataakun.DataAkunFragment;
import com.app.icarindo.modules.account.edit.EditAccountFragment;
import com.app.icarindo.modules.account.password.ChangePasswordFragment;
import com.app.icarindo.modules.account.upgrade.UpgradePlanFragment;
import com.app.icarindo.modules.content.list.ContentListFragment;
import com.app.icarindo.modules.content.page.StaticPageFragmentBuilder;
import com.app.icarindo.modules.deposit.add.DepositFragment;
import com.app.icarindo.modules.pesan.list.MessageListFragment;
import com.app.icarindo.modules.splash.activity.SplashActivity;
import com.app.icarindo.modules.training.materi.MateriFragment;
import com.app.icarindo.scheduler.LocationAlarmReceiver;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Config;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class AccountFragment extends BaseMvpFragment<XAccountView, AccountPresenter>
        implements XAccountView {
    private static final String TAG = AccountFragment.class.getSimpleName();
    @BindView(R.id.tvCustomerName) TextView tvCustomerName;
    @BindView(R.id.layEditProfile) LinearLayout layEditProfile;
    @BindView(R.id.layTransactionHistory) LinearLayout layTransactionHistory;
    @BindView(R.id.layDeposit) LinearLayout layDeposit;
    @BindView(R.id.layAbout) LinearLayout layAbout;
    @BindView(R.id.layCustomerService) LinearLayout layCustomerService;
    @BindView(R.id.layInformation) LinearLayout layInformation;
    @BindView(R.id.laySignOut) LinearLayout laySignOut;
    @BindView(R.id.ivCustomerImage) CircleImageView ivCustomerImage;
    @BindView(R.id.tvCustomerId) TextView tvCustomerId;
    @BindView(R.id.tvCustomerSaldo) TextView tvCustomerSaldo;
    @BindView(R.id.tvNoDataCount) TextView tvNoDataCount;
    @BindView(R.id.layEditPassword) LinearLayout layEditPassword;
    @BindView(R.id.layTraining) LinearLayout layTraining;
    @BindView(R.id.tvTrainingCount) TextView tvTrainingCount;
    @BindView(R.id.layUpgrade) LinearLayout layUpgrade;
    @BindView(R.id.tvCustomerPlan) TextView tvCustomerPlan;

    private LocationAlarmReceiver locationAlarmReceiver;

    public AccountFragment() {

    }

    @Override public AccountPresenter createPresenter() {
        return new AccountPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_account;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            locationAlarmReceiver = new LocationAlarmReceiver();
        }

        presenter.loadDataAccount();
    }

    @Override public void setAccount(String image, String name, String id, Integer saldo) {
        if (!TextUtils.isEmpty(image)) {
            Picasso.with(getContext()).load(image).placeholder(R.drawable.person_default).into(ivCustomerImage);
        }
        tvCustomerName.setText(name);
        tvCustomerId.setText(new StringBuilder().append("ID : ").append(id));
        tvCustomerSaldo.setText(new StringBuilder().append("SALDO : ").append(CommonUtilities.toRupiahFormat(saldo)));

        if (presenter.getPrefs().getPreferencesBoolean(Config.NO_DATA)) {
            tvNoDataCount.setVisibility(View.VISIBLE);
        } else {
            tvNoDataCount.setVisibility(View.GONE);
        }
        if (presenter.getPrefs().getPreferencesBoolean(Config.TRAINING)) {
            layTraining.setVisibility(View.VISIBLE);
        } else {
            layTraining.setVisibility(View.GONE);
        }
        String planCode = presenter.getPrefs().getPreferencesString(Config.PLAN_CODE);
        switch (planCode){
            case Plan.CODE_PLANA1:
                tvCustomerPlan.setText(Plan.NAME_PLANA1);
                break;
            case Plan.CODE_PLANA2:
                tvCustomerPlan.setText(Plan.NAME_PLANA2);
                break;
            case Plan.CODE_PLANB1:
                tvCustomerPlan.setText(Plan.NAME_PLANB1);
                break;
            case Plan.CODE_PLANB2:
                tvCustomerPlan.setText(Plan.NAME_PLANB2);
                break;
        }
        if (planCode.equals(Plan.CODE_PLANA1) || planCode.equals(Plan.CODE_PLANB1)) {
            layUpgrade.setVisibility(View.VISIBLE);
        } else {
            layUpgrade.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.layEditProfile, R.id.layEditPassword, R.id.layTransactionHistory, R.id.layDeposit, R.id.layBankAccount, R.id.layAbout, R.id.layCustomerService, R.id.layInformation, R.id.laySignOut, R.id.layTraining, R.id.layUpgrade, R.id.layDataAccount, R.id.layBtpnWow, R.id.layInsurance}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layEditProfile:
                mListener.gotoPage(new EditAccountFragment(), false, null);
                break;
            case R.id.layEditPassword:
                mListener.gotoPage(new ChangePasswordFragment(), false, null);
                break;
            case R.id.layTransactionHistory:
                EventBus.getDefault().post(new EventDashboard(1));
                break;
            case R.id.layDeposit:
                mListener.gotoPage(new DepositFragment(), false, null);
                break;
            case R.id.layBankAccount:
                mListener.gotoPage(new BankAccountFragment(), false, null);
                break;
            case R.id.layAbout:
                mListener.gotoPage(new StaticPageFragmentBuilder("Tentang-Kami").build(), false, null);
                break;
            case R.id.layCustomerService:
                mListener.gotoPage(new MessageListFragment(), false, null);
                break;
            case R.id.layInformation:
                mListener.gotoPage(new ContentListFragment(), false, null);
                break;
            case R.id.laySignOut:
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                    locationAlarmReceiver.cancelAlarm(getContext());
                }
                presenter.getPrefs().clearAllPreferences();
                Intent intent = new Intent(getActivity(), SplashActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
                getActivity().finish();
                break;
            case R.id.layTraining:
                mListener.gotoPage(new MateriFragment(), false, null);
                break;
            case R.id.layDataAccount:
                mListener.gotoPage(new DataAkunFragment(), false, null);
                break;
            case R.id.layUpgrade:
                mListener.gotoPage(new UpgradePlanFragment(), false, null);
                break;
            case R.id.layBtpnWow:
                mListener.gotoPage(new StaticPageFragmentBuilder("Btpn-Wow").build(), false, null);
                break;
            case R.id.layInsurance:
                mListener.gotoPage(new StaticPageFragmentBuilder("Asuransi").build(), false, null);
                break;
        }
    }
}