package com.app.icarindo.modules.account.account;

import android.content.Context;

import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.eventbus.EventUpdateProfile;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import static com.app.icarindo.utility.Config.CUSTOMER_AVATAR;
import static com.app.icarindo.utility.Config.CUSTOMER_KODE;
import static com.app.icarindo.utility.Config.CUSTOMER_NAME;
import static com.app.icarindo.utility.Config.CUSTOMER_SALDO;

public class AccountPresenter extends BasePresenter<XAccountView>
        implements XAccountPresenter {

    public AccountPresenter(Context context) {
        super(context);
    }

    @Override public void loadDataAccount() {
        if (isViewAttached()){
            String image = prefs.getPreferencesString(CUSTOMER_AVATAR);
            String name  = prefs.getPreferencesString(CUSTOMER_NAME);
            String id = prefs.getPreferencesString(CUSTOMER_KODE);
            Integer saldo = prefs.getPreferencesInt(CUSTOMER_SALDO);
            getView().setAccount(image, name, id, saldo);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onProfileUpdateEvent(EventUpdateProfile event) {
       loadDataAccount();
    }

    @Override public void attachView(XAccountView view) {
        super.attachView(view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }
}