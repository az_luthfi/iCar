package com.app.icarindo.modules.absensi.ijin;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.network.Cuti;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.Validation;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class CutiFragment extends BaseMvpLceFragment<RelativeLayout, RequestNetwork, XCutiView, CutiPresenter>
        implements XCutiView, AlertListener {

    @BindView(R.id.etStartDate) MaterialEditText etStartDate;
    @BindView(R.id.etEndDate) MaterialEditText etEndDate;
    @BindView(R.id.etDesc) MaterialEditText etDesc;
    @BindView(R.id.tvSubmit) TextView tvSubmit;
    @BindView(R.id.dataView) RelativeLayout dataView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) FrameLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private String startDateCanCuti;
    private Integer countCutiAvailableInMonth;
    private Calendar minDate;
    private ArrayList<Cuti> cutis = new ArrayList<>();

    public CutiFragment() {

    }

    @Override public CutiPresenter createPresenter() {
        return new CutiPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_cuti;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        minDate = Calendar.getInstance();
        setAlertListener(this);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestNetwork data) {
        if (data.getStatus()) {
            dataView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            startDateCanCuti = data.getDateCanCuti();
            countCutiAvailableInMonth = data.getCountCutiAvailableInMonth();
            cutis = data.getCutis();

            Date date = CommonUtilities.getDateFromString(startDateCanCuti, "yyyy-MM-dd");
            if (date != null){
                minDate.setTime(date);
            }
        } else {
            dataView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadDataCuti(pullToRefresh);
    }

    @OnClick({R.id.etStartDate, R.id.etEndDate, R.id.tvSubmit}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etStartDate:
                showDateDialog(etStartDate);
                break;
            case R.id.etEndDate:
                if (Validation.isEmpty(etStartDate, "Tanggal mulai wajib diisi")) return;
                showDateDialog(etEndDate);
                break;
            case R.id.tvSubmit:
                CommonUtilities.hideSoftKeyboard(getActivity());
                if (Validation.isEmpty(etStartDate, "Tanggal mulai wajib diisi")) return;
                if (Validation.isEmpty(etEndDate, "Tanggal sampai wajib diisi")) return;
                if (Validation.isEmpty(etDesc, "Keterangan wajib diisi")) return;
                presenter.requestCuti(etStartDate.getText().toString(), etEndDate.getText().toString(), etDesc.getText().toString());
                break;
        }
    }


    private void showDateDialog(final MaterialEditText editText){
        Calendar calendar2 = Calendar.getInstance();
        if (!TextUtils.isEmpty(editText.getText())){
            Date date = CommonUtilities.getDateFromString(editText.getText().toString(), "yyyy-MM-dd");
            if (date != null){
                calendar2.setTime(date);
            }
        }
        DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("DefaultLocale") @Override public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                if (editText.getId() == R.id.etStartDate){
                    etEndDate.setText("");
                }
                editText.setText(year + "-" + String.format("%02d", month + 1) + "-" + String.format("%02d", dayOfMonth));
            }
        };
        DatePickerDialog datePickerDialog = new DatePickerDialog(
                getActivity(),
                dateSetListener,
                calendar2.get(Calendar.YEAR),
                calendar2.get(Calendar.MONTH),
                calendar2.get(Calendar.DAY_OF_MONTH)
        );
        datePickerDialog.getDatePicker().setMinDate(minDate.getTimeInMillis());
        
        if (editText.getId() == R.id.etEndDate){
            Date date = CommonUtilities.getDateFromString(etStartDate.getText().toString(), "yyyy-MM-dd");
            if (date != null){
                Calendar maxDate = Calendar.getInstance();
                maxDate.setTime(date);
                maxDate.add(Calendar.DAY_OF_MONTH, 4);
                datePickerDialog.getDatePicker().setMaxDate(maxDate.getTimeInMillis());
                datePickerDialog.getDatePicker().setMinDate(date.getTime());
            }
        }
        datePickerDialog.show();
    }

    @Override public void showProgressDialog(boolean show) {
        if (show){
            showLoading("Loading..", false);
        }else{
            hideLoading();
        }
    }

    @Override public void showError(String text) {
        final Snackbar snackbar = Snackbar.make(contentView, text, Snackbar.LENGTH_LONG);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override public void onNextRequesCuti(RequestNetwork data) {
        if (data.getStatus()){
            showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Berhasil", data.getText(), "OK", null, "sucess-cuti", false);
        }else{
            showConfirmDialog(SweetAlertDialog.ERROR_TYPE, "Gagal", data.getText(), "Tutup", null, "error-cuti", true);
        }
    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action){
            case "sucess-cuti":
                mListener.back();
                break;
        }
    }
}