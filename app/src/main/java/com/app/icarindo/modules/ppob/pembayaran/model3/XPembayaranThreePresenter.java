package com.app.icarindo.modules.ppob.pembayaran.model3;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XPembayaranThreePresenter extends MvpPresenter<XPembayaranThreeView> {

    void loadProductList(String type);
}