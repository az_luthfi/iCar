package com.app.icarindo.modules.splash;

import android.content.Context;

import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.listeners.SplashListener;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.hannesdorfmann.mosby3.mvp.MvpView;

/**
 * Created by luthfi on 21/07/2017.
 */

public abstract class BaseSplashFragment<V extends MvpView, P extends MvpPresenter<V>> extends BaseMvpFragment<V, P> {

    protected SplashListener splashListener;

    @Override public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof SplashListener) {
            splashListener = (SplashListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override public void onDetach() {
        super.onDetach();
        splashListener = null;
    }
}
