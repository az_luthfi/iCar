package com.app.icarindo.modules.content.page;

import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.content.RequestContent;

import java.util.HashMap;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;

public class StaticPagePresenter extends BaseRxLcePresenter<XStaticPageView, RequestContent>
        implements XStaticPagePresenter {

    public StaticPagePresenter(Context context) {
        super(context);
    }

    @Override public void loadContent(String contentAlias) {
        HashMap<String, String> params = new HashMap<String, String>();
        params.put("act", "page");
        params.put("section", "pages");
        params.put("contentAlias", contentAlias.toLowerCase());
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));

        subscribe(App.getService().apiContentRequest(params), false);
    }
}