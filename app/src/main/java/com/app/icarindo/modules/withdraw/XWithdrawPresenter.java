package com.app.icarindo.modules.withdraw;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XWithdrawPresenter extends MvpPresenter<XWithdrawView> {

    void loadData(boolean pullToRefresh);

    void requestWithdraw(String nominal, String desc);
}