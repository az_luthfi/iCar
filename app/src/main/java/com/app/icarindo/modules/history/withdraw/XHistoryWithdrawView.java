package com.app.icarindo.modules.history.withdraw;

import com.app.icarindo.models.withdraw.RequestWithdraw;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XHistoryWithdrawView extends MvpLceView<RequestWithdraw> {
    void showLoadingNextPage(boolean show);

    void setNextData(RequestWithdraw data);

    void showErrorNextPage(String string);
}