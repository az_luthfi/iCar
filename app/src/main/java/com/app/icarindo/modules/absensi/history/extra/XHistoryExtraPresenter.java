package com.app.icarindo.modules.absensi.history.extra;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XHistoryExtraPresenter extends MvpPresenter<XHistoryExtraView> {

    void loadDataNextPage(int page);

    void loadHistoryExtra(boolean pullToRefresh);

    void requestCancel(String beId);
}