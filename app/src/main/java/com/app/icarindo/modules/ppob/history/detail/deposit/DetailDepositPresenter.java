package com.app.icarindo.modules.ppob.history.detail.deposit;

import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.ppob.RequestHistory;

import java.util.HashMap;

public class DetailDepositPresenter extends BaseRxLcePresenter<XDetailDepositView, RequestHistory>
        implements XDetailDepositPresenter {

    public DetailDepositPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh, String depositId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "deposit_detail");
        params.put("depositId", depositId);

        subscribe(App.getService().apiHistoryRequest(params), pullToRefresh);
    }
}