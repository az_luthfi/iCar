package com.app.icarindo.modules.content.page;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.models.content.RequestContent;
import com.app.icarindo.modules.main.MainActivity;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import butterknife.BindView;

@FragmentWithArgs
public class StaticPageFragment extends BaseMvpLceFragment<CoordinatorLayout, RequestContent, XStaticPageView, StaticPagePresenter>
        implements XStaticPageView, MainActivity.OnChangeToolbar {

    @Arg String contentAlias;
    @BindView(R.id.wvContentDesc) WebView wvContentDesc;
    @BindView(R.id.contentView) CoordinatorLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    public StaticPageFragment() {

    }

    @Override public StaticPagePresenter createPresenter() {
        return new StaticPagePresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_static_page;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initView();
        loadData(false);
    }

    private void initView() {
        WebSettings ws = wvContentDesc.getSettings();
        ws.setDefaultFontSize(16);
        ws.setBuiltInZoomControls(false);
        ws.setDisplayZoomControls(false);
        ws.setJavaScriptEnabled(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestContent data) {
        if (data.getStatus()){
            wvContentDesc.loadData(data.getContent().getContentDesc() != null ? data.getContent().getContentDesc() : "", "text/html", "UTF-8");
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadContent(contentAlias);
    }

    @Nullable @Override public String getTitle() {
        return contentAlias.replace("-", " ");
    }
}