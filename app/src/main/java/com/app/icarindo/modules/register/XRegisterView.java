package com.app.icarindo.modules.register;

import com.app.icarindo.models.content.RequestContent;
import com.app.icarindo.models.customer.RequestCustomer;
import com.app.icarindo.models.datalist.SimpleList;
import com.app.icarindo.models.network.RequestNetwork;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.ArrayList;

public interface XRegisterView extends MvpLceView<RequestNetwork> {

    void showAccountsDialog();

    void showDialog(ArrayList<? extends SimpleList> data, String title);

    void showProgressDialog(boolean show);

    void showError(String text);

    void onSuccessRegister(RequestCustomer data);

    void shoDialogTermAndCon(RequestContent data);

    void gotoBack();
}