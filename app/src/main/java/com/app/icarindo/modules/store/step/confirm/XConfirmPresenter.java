package com.app.icarindo.modules.store.step.confirm;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XConfirmPresenter extends MvpPresenter<XConfirmView> {

    void buy(String note, String jsonProduct, String jsonCusShipping);
}