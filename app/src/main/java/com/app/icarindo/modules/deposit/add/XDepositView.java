package com.app.icarindo.modules.deposit.add;


import com.app.icarindo.models.ppob.Bank;
import com.app.icarindo.models.ppob.Deposit;
import com.app.icarindo.models.ppob.RequestDeposit;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

import java.util.ArrayList;

public interface XDepositView extends MvpLceView<RequestDeposit> {

    void initBank(ArrayList<Bank> banks);

    void showDepositConfirmation(Deposit deposit);

    void showProgressDialog(boolean show);

    void onError(String contextString);
}