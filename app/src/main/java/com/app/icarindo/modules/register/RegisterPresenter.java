package com.app.icarindo.modules.register;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.content.RequestContent;
import com.app.icarindo.models.customer.RequestCustomer;
import com.app.icarindo.models.datalist.Province;
import com.app.icarindo.models.eventbus.EventUpdateJaringan;
import com.app.icarindo.models.eventbus.EventUpdateProfile;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.models.ppob.Bank;
import com.app.icarindo.utility.CommonUtilities;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.greenrobot.eventbus.EventBus;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;
import static com.app.icarindo.utility.Config.CUSTOMER_PIN;
import static com.app.icarindo.utility.Config.CUSTOMER_REG_ID;
import static com.app.icarindo.utility.Config.CUSTOMER_SALDO;

public class RegisterPresenter extends BaseRxLcePresenter<XRegisterView, RequestNetwork>
        implements XRegisterPresenter {

    private RxPermissions rxPermissions;
    private ArrayList<Province> provinces;
    private ArrayList<Bank> banks = new ArrayList<>();

    public RegisterPresenter(Context context, Activity activity) {
        super(context);
        rxPermissions = new RxPermissions(activity);
    }

    @Override public void requestPermissionsAccounts() {
        rxPermissions
                .request(Manifest.permission.GET_ACCOUNTS)
                .subscribe(new Observer<Boolean>() {
                    @Override public void onSubscribe(Disposable d) {
                    }

                    @Override public void onNext(Boolean aBoolean) {
                        if (isViewAttached()) {
                            if (aBoolean) {
                                getView().showAccountsDialog();
                            } else {
                                Toast.makeText(context, "Permission denied, can't find email", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }

                    @Override public void onError(Throwable e) {
                    }

                    @Override public void onComplete() {
                    }
                });
    }

    @Override public void loadProvince() {
        if (provinces == null) {
            Type mapType = new TypeToken<ArrayList<Province>>() {
            }.getType();
            provinces = new Gson().fromJson(CommonUtilities.loadJSONFromAsset(context, "province.json"), mapType);
        }

        if (isViewAttached()) {
            getView().showDialog(provinces, "Provinsi");
        }
    }

    @Override public void requestRegister(HashMap<String, String> params) {
        params.put("act", "registering_user");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("pass", CommonUtilities.secure("registering_user", prefs.getPreferencesString(CUSTOMER_PIN)));

        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        Observer<RequestCustomer> observer = new Observer<RequestCustomer>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCustomer requestCustomer) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (requestCustomer.getStatus()) {
                        prefs.savePreferences(CUSTOMER_SALDO, requestCustomer.getCustomerSaldo());
                        EventBus.getDefault().post(new EventUpdateProfile());
                        EventBus.getDefault().post(new EventUpdateJaringan());
                        getView().onSuccessRegister(requestCustomer);
                    } else {
                        if (requestCustomer.getMethod().equals("already-kaki")){
                            EventBus.getDefault().post(new EventUpdateJaringan());
                            getView().gotoBack();
                            Toast.makeText(context, requestCustomer.getText(), Toast.LENGTH_SHORT).show();
                        }else{
                            getView().showError(requestCustomer.getText());
                        }
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiCustomer(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override public void loadTermAndCon() {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "page");
        params.put("section", "pages");
        params.put("contentAlias", "syarat-dan-ketentuan");
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        Observer<RequestContent> observer = new Observer<RequestContent>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestContent data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (data.getStatus()) {
                        getView().shoDialogTermAndCon(data);
                    } else {
                        getView().showError(data.getText());
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiContentRequest(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override public void loadBank() {
        if (banks.size() > 0){
            getView().showDialog(banks, "Bank");
        }else{
            HashMap<String, String> params = new HashMap<>();
            params.put("act", "bank_active");
            if (isViewAttached()) {
                getView().showProgressDialog(true);
            }

            Observer<RequestCustomer> observer = new Observer<RequestCustomer>() {
                @Override public void onSubscribe(Disposable d) {

                }

                @Override public void onNext(RequestCustomer data) {
                    if (isViewAttached()) {
                        banks = data.getBanks();
                        getView().showProgressDialog(false);
                        getView().showDialog(banks, "Bank");

                    }
                }

                @Override public void onError(Throwable e) {
                    if (isViewAttached()) {
                        getView().showProgressDialog(false);
                        getView().showError(context.getString(R.string.error_network_light));
                    }
                }

                @Override public void onComplete() {

                }
            };

            App.getService().apiCustomer(params)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(observer);
        }
    }

    @Override public void loadPlan(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "produk-plan");

        subscribe(App.getService().apiNetwork(params), pullToRefresh);
    }

    public String getRegId() {
        return prefs.getPreferencesString(CUSTOMER_REG_ID) == null ? "" : prefs.getPreferencesString(CUSTOMER_REG_ID);
    }
}