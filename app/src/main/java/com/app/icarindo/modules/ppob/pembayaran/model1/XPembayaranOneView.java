package com.app.icarindo.modules.ppob.pembayaran.model1;

import android.content.Intent;

import com.app.icarindo.models.ppob.RequestProductCategory;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XPembayaranOneView extends MvpLceView<RequestProductCategory> {
    void startActivityForResult(Intent pickContactIntent, int requestContact);

    void setPhoneNumber(String number);
}