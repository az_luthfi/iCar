package com.app.icarindo.modules.ppob.pembelian.konfirmasi;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.eventbus.EventUpdateProfile;
import com.app.icarindo.models.ppob.RequestPayment;
import com.app.icarindo.utility.CommonUtilities;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_EMAIL;
import static com.app.icarindo.utility.Config.CUSTOMER_ID;
import static com.app.icarindo.utility.Config.CUSTOMER_PIN;
import static com.app.icarindo.utility.Config.CUSTOMER_SALDO;

public class KonfirmasiPembelianPresenter extends BasePresenter<XKonfirmasiPembelianView>
        implements XKonfirmasiPembelianPresenter {

    public KonfirmasiPembelianPresenter(Context context) {
        super(context);
    }

    @Override public void purchaseProcess(String customerNumber, String ppobProductId, String password) {
        if (isViewAttached()){
            getView().shoProgressDialog(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "request");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("email", prefs.getPreferencesString(CUSTOMER_EMAIL));
        params.put("productId", ppobProductId);
        params.put("noHp", customerNumber);
        params.put("pin", CommonUtilities.secure("request", prefs.getPreferencesString(CUSTOMER_PIN)));
        params.put("section", "bisnismu");

       Observer<RequestPayment> observer = new Observer<RequestPayment>() {
           @Override public void onSubscribe(Disposable d) {

           }

           @Override public void onNext(RequestPayment requestPayment) {
                if (isViewAttached()){
                    getView().shoProgressDialog(false);
                    if (requestPayment.getStatus()){
                        prefs.savePreferences(CUSTOMER_SALDO, requestPayment.getCustomerSaldo());
                        EventBus.getDefault().post(new EventUpdateProfile());
                        getView().onSuccessPayment(requestPayment);
                    }else{
                        getView().onError(requestPayment.getText());
                    }
                }
           }

           @Override public void onError(Throwable e) {
               if (isViewAttached()){
                   getView().shoProgressDialog(false);
                   getView().onError(context.getString(R.string.error_network_light));
               }
           }

           @Override public void onComplete() {

           }
       };

        App.getService().apiPurchaseRequest(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}