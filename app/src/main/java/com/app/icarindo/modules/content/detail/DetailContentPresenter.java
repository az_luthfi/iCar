package com.app.icarindo.modules.content.detail;

import android.content.Context;

import com.app.icarindo.base.BasePresenter;

public class DetailContentPresenter extends BasePresenter<XDetailContentView>
        implements XDetailContentPresenter {

    public DetailContentPresenter(Context context) {
        super(context);
    }

}