package com.app.icarindo.modules.home;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.HomeMenuAdapter;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.holder.SliderItem;
import com.app.icarindo.models.HomeMenu;
import com.app.icarindo.models.RequestHome;
import com.app.icarindo.models.content.Content;
import com.app.icarindo.models.network.Plan;
import com.app.icarindo.modules.absensi.AbsensiFragment;
import com.app.icarindo.modules.account.account.AccountFragment;
import com.app.icarindo.modules.carwis.CarwisFragment;
import com.app.icarindo.modules.content.detail.DetailContentFragmentBuilder;
import com.app.icarindo.modules.deposit.add.DepositFragment;
import com.app.icarindo.modules.history.HistoryFragment;
import com.app.icarindo.modules.iuran.IuranFragment;
import com.app.icarindo.modules.network.NetworkFragment;
import com.app.icarindo.modules.ppob.pembayaran.menu.MenuPembayaranFragment;
import com.app.icarindo.modules.store.product.list.ProductListFragment;
import com.app.icarindo.modules.withdraw.WithdrawFragment;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Config;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;

import static com.app.icarindo.utility.Config.CUSTOMER_NAME;
import static com.app.icarindo.utility.Config.CUSTOMER_SALDO;
import static com.app.icarindo.utility.Config.NO_DATA;
import static com.app.icarindo.utility.Config.TRAINING;

public class HomeFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestHome, XHomeView, HomePresenter>
        implements XHomeView, BaseSliderView.OnSliderClickListener, HomeMenuAdapter.HomeMenuListener {
    private static final String TAG = HomeFragment.class.getSimpleName();
    @BindView(R.id.slider) SliderLayout slider;
    @BindView(R.id.indicator) PagerIndicator indicator;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.layAccountHeader) RelativeLayout layAccountHeader;
    @BindView(R.id.tvCustomerSaldo) TextView tvCustomerSaldo;
    @BindView(R.id.laySaldoHeader) LinearLayout laySaldoHeader;
    @BindView(R.id.tvAccountCount) TextView tvAccountCount;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;

    private ArrayList<SliderItem> sliderItems = new ArrayList<>();
    private ArrayList<Content> contents = new ArrayList<>();
    private HomeMenuAdapter adapter;
    private ArrayList<HomeMenu> homeMenus = new ArrayList<>();
    int fisrtColumOfRow = -1;

    public HomeFragment() {

    }

    @Override public HomePresenter createPresenter() {
        return new HomePresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_home;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Type mapType = new TypeToken<ArrayList<HomeMenu>>() {
        }.getType();
        homeMenus = new Gson().fromJson(CommonUtilities.loadJSONFromAsset(getContext(), "home_menu.json"), mapType);
        if (presenter.getPrefs().getPreferencesString(Config.PLAN_CODE).equals(Plan.CODE_PLANA1)){
            homeMenus.remove(homeMenus.size() - 1);
        }
        if (presenter.getPrefs().getPreferencesString(Config.PLAN_CODE).equals(Plan.CODE_PLANA1) ||
                presenter.getPrefs().getPreferencesString(Config.PLAN_CODE).equals(Plan.CODE_PLANB1)) {
            homeMenus.remove(1);
        }
        adapter = new HomeMenuAdapter(this);
        recyclerView.setHasFixedSize(true);
        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 6);
        gridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override public int getSpanSize(int position) {
                if ((homeMenus.size() % 3) == 2){
                    if ((homeMenus.size() - position) == 1 || (homeMenus.size() - position) == 2) {
                        return 3;
                    }
                }
                if ((homeMenus.size() % 3) == 1){
                    if ((homeMenus.size() - position) == 1) {
                        return 6;
                    }
                }
                return 2;
            }
        });
        recyclerView.setLayoutManager(gridLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.setNestedScrollingEnabled(false);

        adapter.setItems(homeMenus);
        ViewCompat.setNestedScrollingEnabled(recyclerView, false);


        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        if (pullToRefresh) {
            return getContext().getString(R.string.error_network_light);
        }
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestHome data) {
        tvCustomerSaldo.setText(CommonUtilities.toRupiahNumberFormat(presenter.getPrefs().getPreferencesInt(CUSTOMER_SALDO)));
        tvName.setText(presenter.getPrefs().getPreferencesString(CUSTOMER_NAME));
        int accountRequirCount = 0;
        if (presenter.getPrefs().getPreferencesBoolean(NO_DATA)) {
            accountRequirCount += 1;
        }
        if (presenter.getPrefs().getPreferencesBoolean(TRAINING)) {
            accountRequirCount += 1;
        }
        if (accountRequirCount > 0) {
            tvAccountCount.setVisibility(View.VISIBLE);
            tvAccountCount.setText(String.valueOf(accountRequirCount));
        } else {
            tvAccountCount.setVisibility(View.GONE);
        }
        if (data.getStatus()) {
            contents = new ArrayList<>(data.getContents());
            if (sliderItems.size() == 0) {
                sliderItems = new ArrayList<>();
                SliderItem sliderItem;
                for (int i = 0; i < contents.size(); i++) {
                    sliderItem = new SliderItem(getContext());
                    sliderItem.image(contents.get(i).getContentImage()).setScaleType(BaseSliderView.ScaleType.CenterCrop);
                    sliderItem.description(CommonUtilities.toHtml(contents.get(i).getContentName()).toString());
                    //add your extra information
                    sliderItem.bundle(new Bundle());
                    sliderItem.getBundle().putInt("position", i);
                    sliderItem.setOnSliderClickListener(this);
                    sliderItems.add(sliderItem);
                }
            }
            if (slider != null) {
                slider.removeAllSliders();
                slider.stopAutoCycle();
                for (int i = 0; i < sliderItems.size(); i++) {
                    slider.addSlider(sliderItems.get(i));
                }

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            slider.startAnimation(AnimationUtils.loadAnimation(getActivity(), android.R.anim.fade_in));
                            slider.setVisibility(View.VISIBLE);
                            slider.startAutoCycle();
                        } catch (NullPointerException e) {
                            e.printStackTrace();
                        }
                    }
                }, 1000);

            }
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh);
    }

    @Override public void onSliderClick(BaseSliderView slider) {
        String jsonContent = new Gson().toJson(contents.get(slider.getBundle().getInt("position")));
        mListener.gotoPage(new DetailContentFragmentBuilder(jsonContent).build(), false, null);
    }

    @OnClick({R.id.layAccountHeader, R.id.laySaldoHeader}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layAccountHeader:
                mListener.gotoPage(new AccountFragment(), false, null);
                break;
            case R.id.laySaldoHeader:
                mListener.gotoPage(new DepositFragment(), false, null);
                break;
        }
    }

    @Override public void onMenuClick(int position) {
        switch (adapter.getItem(position).getIcon()) {
            case "topup":
                mListener.gotoPage(new DepositFragment(), false, null);
                break;
            case "absensi":
                mListener.gotoPage(new AbsensiFragment(), false, null);
                break;
            case "etransaksi":
                mListener.gotoPage(new MenuPembayaranFragment(), false, null);
                break;
            case "register":
                break;
            case "product":
                mListener.gotoPage(new ProductListFragment(), false, null);
                break;
            case "history":
                mListener.gotoPage(new HistoryFragment(), false, null);
                break;
            case "network":
                mListener.gotoPage(new NetworkFragment(), false, null);
                break;
            case "withdraw":
                mListener.gotoPage(new WithdrawFragment(), false, null);
                break;
            case "iuran":
                mListener.gotoPage(new IuranFragment(), false, null);
                break;
            case "carwis":
                mListener.gotoPage(new CarwisFragment(), false, null);
                break;
        }
    }
}