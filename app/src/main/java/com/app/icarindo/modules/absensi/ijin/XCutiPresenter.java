package com.app.icarindo.modules.absensi.ijin;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;
import com.rengwuxian.materialedittext.MaterialEditText;

public interface XCutiPresenter extends MvpPresenter<XCutiView> {

    void loadDataCuti(boolean pullToRefresh);

    void requestCuti(String startDate, String endDate, String etDesc);
}