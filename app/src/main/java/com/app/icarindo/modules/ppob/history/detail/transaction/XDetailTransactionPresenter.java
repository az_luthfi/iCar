package com.app.icarindo.modules.ppob.history.detail.transaction;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XDetailTransactionPresenter extends MvpPresenter<XDetailTransactionView> {

    void loadData(boolean pullToRefresh, String transId);
}