package com.app.icarindo.modules.account.bank;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.models.withdraw.CustomerBank;
import com.app.icarindo.models.withdraw.RequestWithdraw;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.Validation;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

public class BankAccountFragment extends BaseMvpLceFragment<RelativeLayout, RequestWithdraw, XBankAccountView, BankAccountPresenter>
        implements XBankAccountView {

    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.etBankName) EditText etBankName;
    @BindView(R.id.etBankNumber) EditText etBankNumber;
    @BindView(R.id.etBankAccountName) EditText etBankAccountName;
    @BindView(R.id.buttonNext) Button buttonNext;
    @BindView(R.id.contentView) ScrollView contentView;

    public BankAccountFragment() {

    }

    @Override public BankAccountPresenter createPresenter() {
        return new BankAccountPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_bank_acccount;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
//        etBankName.setText("BTPN");
//        etBankName.setEnabled(false);
//        etBankName.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        etBankAccountName.setFilters(new InputFilter[]{new InputFilter.AllCaps()});
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestWithdraw data) {
        if (data.getStatus()){
            CustomerBank customerBank = data.getCustomerBank();
            etBankName.setText(customerBank.getCbName());
            etBankNumber.setText(customerBank.getCbNumber());
            etBankAccountName.setText(customerBank.getCbAcc());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh);
    }


    @OnClick(R.id.buttonNext) public void onViewClicked() {
        CommonUtilities.hideSoftKeyboard(getActivity());
        if (Validation.isEmpty(etBankName, "Nama BANK wajib diisi")) return;
        if (Validation.isEmpty(etBankNumber, "Nomor Rekening wajib diisi")) return;
        if (Validation.isEmpty(etBankAccountName, "Atas Nama wajib diisi")) return;
        presenter.saveBank(etBankName.getText().toString(), etBankNumber.getText().toString(), etBankAccountName.getText().toString());
    }

    @Override public void showProgressDialog(boolean show) {
        if (show){
            showLoading("Loading...", false);
        }else{
            hideLoading();
        }
    }

    @Override public void onNextSave(RequestWithdraw data) {
        if (data.getStatus()){
            showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Berhasil", data.getText(), "OK", null, "true_save", true);
        }else{
            showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan", data.getText(), "Tutup", null, "false_save", true);
        }
    }

    @Override public void showError(String string) {
        showSnackBar(string);
    }
}