package com.app.icarindo.modules.account.password;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.customer.RequestCustomer;
import com.app.icarindo.utility.CommonUtilities;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;
import static com.app.icarindo.utility.Config.CUSTOMER_PIN;

public class ChangePasswordPresenter extends BasePresenter<XChangePasswordView>
        implements XChangePasswordPresenter {

    public static final String ACT_CHANGE_PASSWORD = "change_pin";
    public static final String ACT_FORGET_PASSWORD = "forget_pin";

    public ChangePasswordPresenter(Context context) {
        super(context);
    }

    @Override public void requestChangePassword(String currentPassword, String newPassword) {
        if (isViewAttached()){
            getView().showProgressDialog(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("act", ACT_CHANGE_PASSWORD);
        params.put("currentPassword", CommonUtilities.secure(ACT_CHANGE_PASSWORD, currentPassword));
        params.put("newPassword", CommonUtilities.md5(CommonUtilities.md5(newPassword)));
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));

        Observer<RequestCustomer> observer = new Observer<RequestCustomer>() {

            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCustomer m) {
                if (isViewAttached()){
                    getView().showProgressDialog(false);
                    if (m.getStatus()){
                        prefs.savePreferences(CUSTOMER_PIN, m.getCustomerPassword());
                        getView().onSuccessChangePassword(m.getText());
                    }else{
                        getView().onErrorPassword(m.getText());
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()){
                    getView().showProgressDialog(false);
                    getView().showDialogError(ACT_CHANGE_PASSWORD, context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiCustomer(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override public void requestForgetPassword() {
        if (isViewAttached()){
            getView().showProgressDialog(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("act", ACT_FORGET_PASSWORD);
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));

        Observer<RequestCustomer> observer1 = new Observer<RequestCustomer>() {

            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCustomer m) {
                if (isViewAttached()){
                    prefs.savePreferences(CUSTOMER_PIN, m.getCustomerPassword());
                    getView().showProgressDialog(false);
                    getView().onForgetPasswordSuccess(m.getText());
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()){
                    getView().showProgressDialog(false);
                    getView().showDialogError(ACT_FORGET_PASSWORD, context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiCustomer(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer1);
    }
}