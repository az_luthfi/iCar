package com.app.icarindo.modules.ppob.pembayaran.model5;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XPembayaranFivePresenter extends MvpPresenter<XPembayaranFiveView> {

    void loadData(String type);
}