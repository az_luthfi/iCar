package com.app.icarindo.modules.splash.verificationconfirm;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BasePresenter;
import com.app.icarindo.models.customer.RequestCustomer;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class VerificationConfirmPresenter extends BasePresenter<XVerificationConfirmView>
        implements XVerificationConfirmPresenter {

    public VerificationConfirmPresenter(Context context) {
        super(context);
    }

    @Override public void confirmDepositRegister(String depositId, String pesan, String customerId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "deposit_register_confirm");
        params.put("customerId", customerId);
        params.put("depositId", depositId);
        params.put("bankFrom", "");
        params.put("bankAcc", "");
        params.put("depositText", pesan);

        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        Observer<RequestCustomer> observer = new Observer<RequestCustomer>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestCustomer requestCustomer) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().onDepositRegisterConfirm(requestCustomer);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiCustomer(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}