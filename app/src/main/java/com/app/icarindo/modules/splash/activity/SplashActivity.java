package com.app.icarindo.modules.splash.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.listeners.FragmentInteractionListener;
import com.app.icarindo.listeners.SplashListener;
import com.app.icarindo.models.customer.Customer;
import com.app.icarindo.models.eventbus.EventFacebookLogin;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.modules.splash.login.LoginFragment;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.FragmentStack;
import com.app.icarindo.utility.Logs;
import com.app.icarindo.utility.NotificationHelper;
import com.facebook.FacebookSdk;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.hannesdorfmann.mosby3.mvp.MvpActivity;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.app.icarindo.modules.splash.activity.SplashPresenter.ACT_CHECK_VERSION;
import static com.app.icarindo.modules.splash.activity.SplashPresenter.ACT_IS_REGISTERED;


public class SplashActivity extends MvpActivity<XSplashView, XSplashPresenter>
        implements XSplashView, FragmentInteractionListener, SplashListener {

    @BindView(R.id.ivLogo) ImageView ivLogo;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.layLoading) RelativeLayout layLoading;
    @BindView(R.id.container) FrameLayout container;
    private FragmentStack fragmentStack;

    @NonNull @Override public XSplashPresenter createPresenter() {
        return new SplashPresenter(getApplicationContext());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        new NotificationHelper(this);
        FirebaseApp.initializeApp(getApplicationContext());
        layLoading.setVisibility(View.VISIBLE);
        fragmentStack = new FragmentStack(this, getSupportFragmentManager(), R.id.container);
        container.setVisibility(View.GONE);
        registerGcm();
        Logs.d("SplashActivity => onCreate ==> ");
        presenter.checkIsLogin();
//        fragmentStack.push(new SplashFragment(), null);
    }

    private void registerGcm() {
        String regId = FirebaseInstanceId.getInstance().getToken();
        if (regId != null) {
            presenter.saveRegId(regId);
        }
        FirebaseMessaging.getInstance().subscribeToTopic(this.getString(R.string.topic_name));
    }

    @Override public void showContent() {
//        fragmentStack.push(new SplashFragment(), null);
        fragmentStack.push(new LoginFragment(), null);
        layLoading.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);
    }

    @Override public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override public void showConfirmDialog(int alertType, String title, String content, String confirmLabel, String cancelLabel, final String action) {
        SweetAlertDialog alert = CommonUtilities.buildAlert(this, alertType, title, content, confirmLabel, cancelLabel);
        alert.setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                onCancel(action);
                sweetAlertDialog.cancel();
            }
        });
        alert.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sweetAlertDialog) {
                onConfirm(action);
                sweetAlertDialog.cancel();
            }
        });
    }

    private void onConfirm(String action) {
        switch (action) {
            case "error" + ACT_CHECK_VERSION:
                presenter.checkVersion();
                break;
            case "error" + ACT_IS_REGISTERED:
                presenter.checkIsAuth();
                break;
            case "update":
                gotoPlayStore();
                break;
            case "maintenance":
                finish();
                break;
        }
    }

    private void onCancel(String action) {
        switch (action) {
            case "error" + ACT_CHECK_VERSION:
                finish();
                break;
            case "error" + ACT_IS_REGISTERED:
                finish();
                break;
            case "maintenance":
                finish();
                break;
        }
    }

    @Override public void gotoPage(Fragment page, boolean shouldReplace, String tagName) {
        if (shouldReplace) {
            fragmentStack.push(page, tagName);
        } else {
            fragmentStack.pushAdd(page, tagName);
        }
    }

    @Override public void back() {
        onBackPressed();
    }

    @Override public void OnSuccessAuth(Customer customer) {
        Toast.makeText(this, "Anda berhasil masuk", Toast.LENGTH_SHORT).show();
        presenter.saveSession(customer);
        Intent intent = new Intent(this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
        finish();
//
//        if (customer.getCustomerStatus().equals("new")) { // new
//            layLoading.setVisibility(View.GONE);
//            container.setVisibility(View.VISIBLE);
//            presenter.saveSession(customer);
//            if (customer.getDeposit() == null) {
//                Logs.d("SplashAcitvity => On success ==> deposit kosong");
//                gotoPage(new VerificationFragmentBuilder(new Gson().toJson(customer)).build(), false, null);
//            } else {
//                if (customer.getDeposit().getDepositStatus().equals("new")) {
//                    VerificationConfirmFragment fragment = new VerificationConfirmFragmentBuilder(
//                            String.valueOf(customer.getCustomerId()),
//                            new Gson().toJson(customer.getDeposit())
//                    ).build();
//
//                    gotoPage(fragment, false, null);
//                } else {
//                    gotoPage(new VerificationWaitingFragment(), false, null);
//                }
//            }
//        } else {
//
//        }
    }

    @Override protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (FacebookSdk.isFacebookRequestCode(requestCode)) {
            EventBus.getDefault().post(new EventFacebookLogin(requestCode, resultCode, data));
        }
    }

    private void gotoPlayStore() {
        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
        } catch (ActivityNotFoundException anfe) {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
        }
    }
}
