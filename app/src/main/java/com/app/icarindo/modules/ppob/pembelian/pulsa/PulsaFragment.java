package com.app.icarindo.modules.ppob.pembelian.pulsa;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.ppob.PpobProductAdapter;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.models.ppob.PpobProduct;
import com.app.icarindo.models.ppob.PpobProductCategory;
import com.app.icarindo.models.ppob.RequestProductCategory;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.modules.ppob.pembelian.konfirmasi.KonfirmasiPembelianFragment;
import com.app.icarindo.modules.ppob.pembelian.konfirmasi.KonfirmasiPembelianFragmentBuilder;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Logs;
import com.app.icarindo.utility.listview.DividerItemDecoration;
import com.app.icarindo.utility.listview.ItemClickSupport;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.mosby3.mvp.lce.LceAnimator;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;

import static com.app.icarindo.utility.Config.PAKET_DATA;
import static com.app.icarindo.utility.Config.PULSA;

@FragmentWithArgs
public class PulsaFragment extends BaseMvpFragment<XPulsaView, PulsaPresenter>
        implements XPulsaView, MainActivity.OnChangeToolbar{
    @Arg String type;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.etPhone) MaterialEditText etPhone;
    @BindView(R.id.btnGetContact) ImageButton btnGetContact;
    @BindView(R.id.etProduct) MaterialEditText etProduct;
    @BindView(R.id.ivProducticon) ImageView ivProducticon;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.btnSubmit) Button btnSubmit;
    @BindView(R.id.contentView) LinearLayout contentView;

    private PpobProductCategory ppobProductCategory;
    private String tempMsisdn = "";
    private int productIndexSelected;
    private PpobProduct productSelected = null;

    public PulsaFragment() {

    }

    @Override public PulsaPresenter createPresenter() {
        return new PulsaPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_pulsa;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etPhone.addTextChangedListener(new TextWatcher() {
            @Override public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() >= 4) {
                    if (tempMsisdn.isEmpty() || !s.toString().equals(tempMsisdn)) {
                        loadProduct(s.toString().substring(0, 4));
                        tempMsisdn = s.toString();
                    } else {
                        if (ppobProductCategory != null) {
                            showOperatorLogo();
                        }
                    }
                }

//                if (s.length() < 4 && before == 1 && start == 3) {
//                    ivProducticon.setImageResource(0);
//                    setSelectedProduct(-1);
//
//                }
                if (s.length() < 4) {
                    ivProducticon.setImageResource(0);
                    setSelectedProduct(-1);

                }

                if (ppobProductCategory != null) {
                    if (s.toString().length() > 9) {
                        toogleEnableBtnSubmit(true);
                    } else {
                        toogleEnableBtnSubmit(false);
                    }
                }
            }

            @Override public void afterTextChanged(Editable s) {

            }
        });

        if (type.equalsIgnoreCase("data")) {
            etProduct.setHint("Pilih Nominal Paket");
            etProduct.setFloatingLabelText("Nominal Paket");
        }
    }

    private void toogleEnableBtnSubmit(boolean flag) {
        btnSubmit.setEnabled(flag);
    }

    private void loadProduct(String s) {
        LceAnimator.showLoading(loadingView, ivProducticon, errorView);
        presenter.loadProduct(s, type);
    }

    private void setSelectedProduct(int index) {
        loadingView.setVisibility(View.GONE);
        if (index == -1) {
            if (ppobProductCategory != null) {
                ppobProductCategory = null;
                productSelected = null;
                etProduct.setText("");
                btnSubmit.setEnabled(false);
                tempMsisdn = "";
            }
        } else {
            productSelected = ppobProductCategory.getPpobProducts().get(index);
            productIndexSelected = index;
            etProduct.setText(productSelected.getPpobProductName());
            if (etPhone.getText().toString().length() > 9) {
                btnSubmit.setEnabled(true);
            }
        }
    }

    private void showOperatorLogo() {
        int resId = getResources().getIdentifier(ppobProductCategory.getCatName().toLowerCase().replace("data", "").trim(), "drawable", getContext().getApplicationInfo().packageName);
        if (resId != 0) {
            Picasso.with(getContext()).load(resId).into(ivProducticon);
        }
        LceAnimator.showContent(loadingView, ivProducticon, errorView);
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(getActivity(), android.R.style.DeviceDefault_Light_ButtonBar);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setContentView(R.layout.dialog_ppob_product_chooser);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
        EditText searchEditText = (EditText) dialog.findViewById(R.id.tvSearch);
        TextView productAdmin = (TextView) dialog.findViewById(R.id.productAdmin);
        productAdmin.setText("Harga");

        final PpobProductAdapter adapter = new PpobProductAdapter();
        adapter.setType(type);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));
        adapter.addAll(ppobProductCategory.getPpobProducts());

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                setSelectedProduct(position);
                dialog.dismiss();
            }
        });

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Logs.d("FragmentPayment => onTextChanged ==> " + s);
                adapter.filter(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        dialog.show();
    }

    @OnClick({R.id.btnGetContact, R.id.etProduct, R.id.btnSubmit}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnGetContact:
                presenter.getContact();
                break;
            case R.id.etProduct:
                if (ppobProductCategory != null) {
                    if (ppobProductCategory.getPpobProducts() != null) {
                        etPhone.clearFocus();
                        CommonUtilities.hideSoftKeyboard(getActivity());
                        showDialog();
                    }
                }
                break;
            case R.id.btnSubmit:
                if (productSelected != null) {
                    CommonUtilities.hideSoftKeyboard(getActivity());
                    String customerNumber = etPhone.getText().toString();
                    KonfirmasiPembelianFragment page = new KonfirmasiPembelianFragmentBuilder(customerNumber, ppobProductCategory.getCatName().toLowerCase(), new Gson().toJson(productSelected, PpobProduct.class)).build();
                    productSelected = null;
                    etPhone.setText(null);
                    mListener.gotoPage(page, false, "Konfirmasi Pembelian Pulsa");
                }
                break;
        }
    }

    @Override public void showProduct(RequestProductCategory requestProductCategory) {
        ppobProductCategory = requestProductCategory.getPpobProductCategory();
        List<PpobProduct> ppobProducts = ppobProductCategory.getPpobProducts();

        //Set Default product
        setSelectedProduct(0);

        showOperatorLogo();
    }

    @Override public void setPhoneNumber(String number) {
        etPhone.setText(number);
    }

    @Override public void checkMsisdn(String msisdn) {
        loadProduct(msisdn);
    }

    @Override public void showError(String text) {
        showToast(text);
        loadingView.setVisibility(View.GONE);
        etPhone.requestFocus();
        tempMsisdn = "";
    }

    @Override public void resetProduct() {
        setSelectedProduct(-1);
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Nullable @Override public String getTitle() {
        switch (type){
            case PULSA:
                return "Pembelian Pulsa";
            case PAKET_DATA:
                return "Pembelian Paket Data";
            default:
                return null;
        }
    }
}