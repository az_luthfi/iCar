package com.app.icarindo.modules.absensi.ijin;

import com.app.icarindo.models.network.RequestNetwork;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XCutiView extends MvpLceView<RequestNetwork> {

    void showProgressDialog(boolean show);

    void showError(String text);

    void onNextRequesCuti(RequestNetwork data);

}