package com.app.icarindo.modules.content.page;


import com.app.icarindo.models.content.RequestContent;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XStaticPageView extends MvpLceView<RequestContent> {

}