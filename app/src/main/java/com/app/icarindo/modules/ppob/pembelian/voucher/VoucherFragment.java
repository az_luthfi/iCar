package com.app.icarindo.modules.ppob.pembelian.voucher;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.ppob.PpobProductAdapter;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.models.ppob.PpobProduct;
import com.app.icarindo.models.ppob.PpobProductCategory;
import com.app.icarindo.models.ppob.RequestProductCategory;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.modules.ppob.pembelian.konfirmasi.KonfirmasiPembelianFragment;
import com.app.icarindo.modules.ppob.pembelian.konfirmasi.KonfirmasiPembelianFragmentBuilder;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Logs;
import com.app.icarindo.utility.listview.DividerItemDecoration;
import com.app.icarindo.utility.listview.ItemClickSupport;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static com.app.icarindo.utility.Config.HP_PASCA;

@FragmentWithArgs
public class VoucherFragment extends BaseMvpLceFragment<RelativeLayout, RequestProductCategory, XVoucherView, VoucherPresenter>
        implements XVoucherView, MainActivity.OnChangeToolbar {

    @Arg String type;
    @BindView(R.id.etIdPelanggan) MaterialEditText etIdPelanggan;
    @BindView(R.id.etJenisProduct) MaterialEditText etJenisProduct;
    @BindView(R.id.etProduct) MaterialEditText etProduct;
    @BindView(R.id.btnSubmit) Button btnSubmit;
    @BindView(R.id.contentView) RelativeLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.dataView) LinearLayout dataView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.btnGetContact) ImageButton btnGetContact;

    private ArrayList<PpobProductCategory> ppobProductCategory;
    private List<PpobProduct> productList;
    private PpobProduct productSelected;

    public VoucherFragment() {

    }

    @Override public VoucherPresenter createPresenter() {
        return new VoucherPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_voucher;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getString(R.string.error_network);
    }

    @Override public void setData(RequestProductCategory data) {
        if (data.getStatus()) {
            dataView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            etProduct.setClickable(true);
            ppobProductCategory = data.getPpobProductCategories();
        } else {
            dataView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(type);
    }

    @OnClick({R.id.etJenisProduct, R.id.etProduct, R.id.btnSubmit}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etJenisProduct:
                registerForContextMenu(etJenisProduct);
                etJenisProduct.showContextMenu();
                break;
            case R.id.etProduct:
                showDialog();
                break;
            case R.id.btnSubmit:
                if (productSelected != null) {
                    CommonUtilities.hideSoftKeyboard(getActivity());
                    String customerNumber = etIdPelanggan.getText().toString();
                    KonfirmasiPembelianFragment page = new KonfirmasiPembelianFragmentBuilder(customerNumber, etJenisProduct.getText().toString().toLowerCase(), new Gson().toJson(productSelected, PpobProduct.class)).build();
                    productSelected = null;
                    etIdPelanggan.setText(null);
                    mListener.gotoPage(page, false, "Konfirmasi Pembelian Pulsa");
                }

                break;
        }
    }

    @OnTextChanged(R.id.etIdPelanggan) public void onTextChanged() {
        btnSubmit.setEnabled(etIdPelanggan.getText().length() > 4 && productSelected != null);
    }

    @Override public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        for (int i = 0; i < ppobProductCategory.size(); i++) {
            menu.setHeaderTitle("Pilih Voucher");
            menu.add(0, i, i, ppobProductCategory.get(i).getCatName());
        }
    }

    @Override public boolean onContextItemSelected(MenuItem item) {
        productList = ppobProductCategory.get(item.getItemId()).getPpobProducts();
        etJenisProduct.setText(ppobProductCategory.get(item.getItemId()).getCatName());
        productSelected = null;
        etProduct.setText("");
        return true;
    }

    private void showDialog() {
        final Dialog dialog = new Dialog(getActivity(), android.R.style.DeviceDefault_Light_ButtonBar);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setContentView(R.layout.dialog_ppob_product_chooser);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
        EditText searchEditText = (EditText) dialog.findViewById(R.id.tvSearch);

        final PpobProductAdapter adapter = new PpobProductAdapter();
        adapter.setType(HP_PASCA);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));
        adapter.addAll(productList);

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                productSelected = adapter.getItem(position);
                etProduct.setText(productSelected.getPpobProductName());
                btnSubmit.setEnabled(etIdPelanggan.getText().length() > 5 && productSelected != null);
                dialog.dismiss();
            }
        });

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Logs.d("FragmentPayment => onTextChanged ==> " + s);
                adapter.filter(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        dialog.show();
    }

    @Nullable @Override public String getTitle() {
        return "Pembelian Voucher";
    }

    @OnClick(R.id.btnGetContact) public void onViewClicked() {
        presenter.getContact();
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        presenter.onActivityResult(requestCode, resultCode, data);
    }

    @Override public void setPhoneNumber(String number) {
        etIdPelanggan.setText(number);
    }
}