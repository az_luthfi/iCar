package com.app.icarindo.modules.store.step.updateaddress;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.SimpleAdapter;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.datalist.SimpleList;
import com.app.icarindo.models.store.CustomerShipping;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.DialogListOption;
import com.app.icarindo.utility.Validation;
import com.google.gson.Gson;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

@FragmentWithArgs
public class AddShippingAddressFragment extends BaseMvpFragment<XAddShippingAddressView, AddShippingAddressPresenter>
        implements XAddShippingAddressView, AlertListener {
    @Arg int countList;
    @Arg(required = false) String jsonAddress;
    @BindView(R.id.etName) EditText etName;
    @BindView(R.id.etLabel) EditText etLabel;
    @BindView(R.id.etPhone) EditText etPhone;
    @BindView(R.id.tvProvince) TextView tvProvince;
    @BindView(R.id.tvCity) TextView tvCity;
    @BindView(R.id.tvPostCode) TextView tvPostCode;
    @BindView(R.id.etAddress) EditText etAddress;
    @BindView(R.id.cbDefault) CheckBox cbDefault;
    @BindView(R.id.layDefault) RelativeLayout layDefault;
    @BindView(R.id.btnSave) Button btnSave;
    @BindView(R.id.scrollView) ScrollView scrollView;

    private SimpleAdapter adapter;
    private CustomerShipping shipping;
    private boolean isDefault = false;
    private String provinceId;
    private String cityId;

    public AddShippingAddressFragment() {

    }

    @Override public AddShippingAddressPresenter createPresenter() {
        return new AddShippingAddressPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_add_shipping_address;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
        adapter = new SimpleAdapter();
        if (jsonAddress != null) {
            shipping = new Gson().fromJson(jsonAddress, CustomerShipping.class);
            if (shipping.getShippingDefault().equals("1")) isDefault = true;
            provinceId = shipping.getShippingProvince();
            cityId = shipping.getShippingCity();

            etLabel.setText(CommonUtilities.getText(shipping.getShippingLabel(), ""));
            etName.setText(CommonUtilities.getText(shipping.getShippingName(), ""));
            etPhone.setText(CommonUtilities.getText(shipping.getShippingPhone(), ""));
            etAddress.setText(CommonUtilities.getText(shipping.getShippingAddress(), ""));
            if (!TextUtils.isEmpty(shipping.getShippingProvinceName())){
                tvProvince.setText(shipping.getShippingProvinceName());
            }
            if (!TextUtils.isEmpty(shipping.getShippingCityName())){
                tvCity.setText(shipping.getShippingCityName());
            }
            if (!TextUtils.isEmpty(shipping.getShippingZipCode())){
                tvPostCode.setText(shipping.getShippingZipCode());
            }
        }
        if (countList < 1 || isDefault) {
            cbDefault.setChecked(true);
        }
    }

    @OnClick({R.id.tvProvince, R.id.tvCity, R.id.layDefault, R.id.btnSave}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tvProvince:
                presenter.loadProvince();
                break;
            case R.id.tvCity:
                presenter.loadCity(provinceId);
                break;
            case R.id.layDefault:
                if (cbDefault.isChecked() && (countList < 1 || isDefault)) { // && (count < 2 || json address default = 1/true)
                    showConfirmDialog(SweetAlertDialog.WARNING_TYPE, "Peringatan!", "Alamat utama tidak dapat dihapus. Anda dapat mengatur alamat lainnya sebagai alamat utama.", "Ya", null, "delete_address", true);
                } else {
                    cbDefault.setChecked(!cbDefault.isChecked());
                }
                break;
            case R.id.btnSave:
                validateSave();
                break;
        }
    }

    @Override public void showDialog(ArrayList<? extends SimpleList> data, final String title) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        adapter.addAll(data);
        new DialogListOption(getContext(), title, adapter) {
            @Override public void onItemClicked(int position) {
                if (position != -1) {
                    switch (title.toLowerCase()) {
                        case "provinsi":
                            tvProvince.setText(adapter.getItem(position).getName());
                            tvCity.setText("");
                            provinceId = adapter.getItem(position).getId();
                            presenter.resetCity();
                            break;
                        case "kabupaten":
                            tvCity.setText(adapter.getItem(position).getName());
                            tvPostCode.setText(adapter.getItem(position).getOther());
                            cityId = adapter.getItem(position).getId();
                            break;
                    }
                }
            }

            @Override public void onFilter(CharSequence s) {
                adapter.filter(s);
            }
        };
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            showLoading("Loading...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void showDialogSuccess(String text) {
        showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Berhasil", text, "OK", null, "success", false);
        etLabel.setText("");
        etName.setText("");
        etPhone.setText("");
        tvProvince.setText("");
        etAddress.setText("");
    }

    @Override public void showError(String message) {
        final Snackbar snackbar = Snackbar.make(scrollView, message, Snackbar.LENGTH_LONG);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }


    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action) {
            case "success":
                mListener.back();
                break;
        }
    }

    private void validateSave() {
        if (Validation.isEmpty(etName, "Nama penerima wajib diisi.")) return;
        if (Validation.isEmpty(etLabel, "Label wajib diisi")) return;
        if (Validation.isEmpty(etPhone, "No. Telp / HP wajib diisi")) return;
        if (etPhone.length() < 10) {
            etPhone.requestFocus();
            etPhone.setError("No. Telp / HP tidak sesuai");
            return;
        }
        if (Validation.isEmpty(getContext(), tvProvince, "Provinsi wajib diisi")) return;
        if (Validation.isEmpty(getContext(), tvCity, "Kabupaten wajib diisi")) return;
        if (Validation.isEmpty(getContext(), tvPostCode, "Kode pos wajib diisi")) return;
        if (Validation.isEmpty(etAddress, "Alamat wajib diisi")) return;
        String isDefault = "0";
        if (cbDefault.isChecked()) {
            isDefault = "1";
        }

        HashMap<String, String> params = new HashMap<>();
        params.put("shippingLabel", etLabel.getText().toString());
        params.put("shippingName", etName.getText().toString());
        params.put("shippingPhone", etPhone.getText().toString());
        params.put("shippingAddress", etAddress.getText().toString());
        params.put("shippingProvince", provinceId);
        params.put("shippingCity", cityId);
        params.put("shippingZipCode", tvPostCode.getText().toString());
        params.put("shippingDefault", isDefault);

        if (shipping != null) {
            params.put("shippingId", String.valueOf(shipping.getShippingId()));
            presenter.saveAddress(params, "updateAddressBook");
        } else {
            presenter.saveAddress(params, "addAddressBook");
        }
    }
}