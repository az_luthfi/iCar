package com.app.icarindo.modules.ppob.pembelian.konfirmasi;

import com.app.icarindo.models.ppob.RequestPayment;
import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XKonfirmasiPembelianView extends MvpView {

    void shoProgressDialog(boolean show);

    void onError(String text);

    void onSuccessPayment(RequestPayment data);
}