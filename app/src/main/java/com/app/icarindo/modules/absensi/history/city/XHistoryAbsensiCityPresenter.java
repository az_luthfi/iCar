package com.app.icarindo.modules.absensi.history.city;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XHistoryAbsensiCityPresenter extends MvpPresenter<XHistoryAbsensiCityView> {
    void loadHistoryCity(boolean pullToRefresh);

    void loadDataNextPage(int page);
}