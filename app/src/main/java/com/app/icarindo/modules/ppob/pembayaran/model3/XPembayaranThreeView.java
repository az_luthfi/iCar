package com.app.icarindo.modules.ppob.pembayaran.model3;

import com.app.icarindo.models.datalist.SimpleList;
import com.hannesdorfmann.mosby3.mvp.MvpView;

import java.util.ArrayList;

public interface XPembayaranThreeView extends MvpView {

    void showProduct(ArrayList<? extends SimpleList> data);
}