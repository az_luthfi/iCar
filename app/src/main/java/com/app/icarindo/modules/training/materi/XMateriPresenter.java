package com.app.icarindo.modules.training.materi;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XMateriPresenter extends MvpPresenter<XMateriView> {

    void loadMateri(boolean pullToRefresh);

    void requestPermissionsStorage();
}