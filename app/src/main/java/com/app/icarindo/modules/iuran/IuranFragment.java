package com.app.icarindo.modules.iuran;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.adapter.IuranAdapter;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.listview.EndlessRecyclerOnScrollListener;

import butterknife.BindView;

public class IuranFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestNetwork, XIuranView, IuranPresenter>
        implements XIuranView, ItemLoadingHolder.ItemViewLoadingListener {

    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private IuranAdapter adapter;
    private int maxCountItems;
    private EndlessRecyclerOnScrollListener scrollListener;

    public IuranFragment() {

    }

    @Override public IuranPresenter createPresenter() {
        return new IuranPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_iuran;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new IuranAdapter(this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);

        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    if (adapter.getItemCount() < maxCountItems) {
                        presenter.loadDataNextPage(page);
                    }
                }
            }
        };

        recyclerView.addOnScrollListener(scrollListener);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestNetwork data) {
        if (data.getStatus()) {
            if (adapter != null && adapter.getItemCount() > 0){
                adapter.clear();
            }
            recyclerView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            maxCountItems = data.getCount();
            adapter.setItems(data.getIurans());
        } else {
            recyclerView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadIuran(pullToRefresh);
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void setNextData(RequestNetwork data) {
        if (data.getStatus()) {
            adapter.setItems(data.getIurans());
        }
    }

    @Override public void showErrorNextPage(String string) {
        Toast.makeText(getContext(), string, Toast.LENGTH_SHORT).show();
        adapter.setErrorNextPage("Tap to retry");
        scrollListener.backToPreviousPage();
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadDataNextPage(scrollListener.getCurrentPage());
    }
}