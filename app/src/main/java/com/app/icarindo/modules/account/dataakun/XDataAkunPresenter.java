package com.app.icarindo.modules.account.dataakun;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XDataAkunPresenter extends MvpPresenter<XDataAkunView> {

    void loadData(boolean pullToRefresh);
}