package com.app.icarindo.modules.store.product.list;

import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.store.RequestProduct;
import com.app.icarindo.utility.Config;

import java.util.HashMap;

public class ProductListPresenter extends BaseRxLcePresenter<XProductListView, RequestProduct>
        implements XProductListPresenter {

    public ProductListPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("act", "list");

        subscribe(App.getService().apiProduk(params), pullToRefresh);
    }
}