package com.app.icarindo.modules.store.product.list;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XProductListPresenter extends MvpPresenter<XProductListView> {

    void loadData(boolean pullToRefresh);
}