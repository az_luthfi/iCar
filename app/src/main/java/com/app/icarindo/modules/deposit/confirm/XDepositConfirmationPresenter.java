package com.app.icarindo.modules.deposit.confirm;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XDepositConfirmationPresenter extends MvpPresenter<XDepositConfirmationView> {

    void cancelDeposit(String depositId);

    void confirmDeposit(String depositId, String comment);
}