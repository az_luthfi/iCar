package com.app.icarindo.modules.history.register;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XHistoryRegisterPresenter extends MvpPresenter<XHistoryRegisterView> {

    void loadData(String filterDate, boolean pullToRefresh);

    void loadDataNextPage(String filterDate, int page);
}