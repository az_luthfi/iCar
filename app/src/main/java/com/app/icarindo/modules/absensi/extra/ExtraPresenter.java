package com.app.icarindo.modules.absensi.extra;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.utility.Config;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class ExtraPresenter extends BaseRxLcePresenter<XExtraView, RequestNetwork>
        implements XExtraPresenter {

    public ExtraPresenter(Context context) {
        super(context);
    }

    @Override public void getCountExtra(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "absesni-extra-ready");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));

        subscribe(App.getService().apiNetwork(params), pullToRefresh);
    }

    @Override public void requestExtra(String date) {
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("date", date);
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("act", "input-extra");
        Observer<RequestNetwork> observer = new Observer<RequestNetwork>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestNetwork data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().onNextRequestExtra(data);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiNetwork(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }
}