package com.app.icarindo.modules.account.upgrade;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.content.RequestContent;
import com.app.icarindo.models.eventbus.EventUpdateProfile;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.utility.CommonUtilities;

import org.greenrobot.eventbus.EventBus;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;
import static com.app.icarindo.utility.Config.CUSTOMER_PIN;
import static com.app.icarindo.utility.Config.CUSTOMER_SALDO;
import static com.app.icarindo.utility.Config.NO_DATA;
import static com.app.icarindo.utility.Config.PLAN_CODE;
import static com.app.icarindo.utility.Config.TRAINING;

public class UpgradePlanPresenter extends BaseRxLcePresenter<XUpgradePlanView, RequestNetwork>
        implements XUpgradePlanPresenter {

    public UpgradePlanPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "already-upgrade");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));

        subscribe(App.getService().apiNetwork(params), pullToRefresh);
    }

    @Override public void loadTermAndCon() {
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "page");
        params.put("section", "pages");
        params.put("contentAlias", "syarat-dan-ketentuan");

        Observer<RequestContent> observer = new Observer<RequestContent>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestContent data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (data.getStatus()) {
                        getView().shoDialogTermAndCon(data);
                    } else {
                        getView().showError(data.getText());
                    }
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiContentRequest(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Override public void upgradePlan(String planCode) {
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "upgrade_plan");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("planCode", planCode);
        params.put("pass", CommonUtilities.secure("upgrade_plan", prefs.getPreferencesString(CUSTOMER_PIN)));
        Observer<RequestNetwork> observer = new Observer<RequestNetwork>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestNetwork data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    if (data.getStatus()){
                        prefs.savePreferences(NO_DATA, data.getNoData());
                        prefs.savePreferences(TRAINING, data.getTraining());
                        prefs.savePreferences(PLAN_CODE, data.getPlanCode());
                        prefs.savePreferences(CUSTOMER_SALDO, data.getCustomerSaldo());
                        EventBus.getDefault().post(new EventUpdateProfile());
                    }
                    getView().onNextUpgradePlan(data);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiNetwork(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);

    }
}