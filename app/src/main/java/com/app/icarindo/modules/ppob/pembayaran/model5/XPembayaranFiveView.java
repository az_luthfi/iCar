package com.app.icarindo.modules.ppob.pembayaran.model5;

import com.app.icarindo.models.ppob.RequestProductCategory;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XPembayaranFiveView extends MvpLceView<RequestProductCategory> {

}