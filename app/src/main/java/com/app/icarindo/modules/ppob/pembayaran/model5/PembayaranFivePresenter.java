package com.app.icarindo.modules.ppob.pembayaran.model5;

import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.ppob.RequestProductCategory;

import java.util.HashMap;

public class PembayaranFivePresenter extends BaseRxLcePresenter<XPembayaranFiveView, RequestProductCategory>
        implements XPembayaranFivePresenter {

    public PembayaranFivePresenter(Context context) {
        super(context);
    }

    @Override public void loadData(String type) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "getProduct");
        params.put("type", type);
        subscribe(App.getService().apiPPOBProductCategory(params), false);
    }
}