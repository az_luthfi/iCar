package com.app.icarindo.modules.ppob.pembayaran.model2;

import android.content.Intent;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XPembayaranTwoPresenter extends MvpPresenter<XPembayaranTwoView> {
    void getContact();

    void onActivityResult(int requestCode, int resultCode, Intent data);
}