package com.app.icarindo.modules.ppob.pembayaran.model3;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.app.icarindo.R;
import com.app.icarindo.adapter.SimpleAdapter;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.models.datalist.SimpleList;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.modules.ppob.pembayaran.konfirmasi.KonfirmasiPembayaranFragment;
import com.app.icarindo.modules.ppob.pembayaran.konfirmasi.KonfirmasiPembayaranFragmentBuilder;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.DialogListOption;
import com.app.icarindo.utility.Logs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.rengwuxian.materialedittext.MaterialEditText;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static com.app.icarindo.utility.Config.BPJS;
import static com.app.icarindo.utility.Config.PT_PLN_TOKEN;

/**
 * Pembayaran PLN TOKEN, BPJS
 */
@FragmentWithArgs
public class PembayaranThreeFragment extends BaseMvpFragment<XPembayaranThreeView, PembayaranThreePresenter>
        implements XPembayaranThreeView, MainActivity.OnChangeToolbar {
    private static final String TAG = PembayaranThreeFragment.class.getSimpleName();
    private static final int REQUEST_CONTACT = 454;
    private static final int REQUEST_CONTACT_ID = 455;
    @Arg String title;
    @Arg String type;
    @BindView(R.id.etIdPelanggan) MaterialEditText etIdPelanggan;
    @BindView(R.id.etPhone) MaterialEditText etPhone;
    @BindView(R.id.etProduct) MaterialEditText etProduct;
    @BindView(R.id.btnSubmit) Button btnSubmit;
    @BindView(R.id.btnGetContact) ImageButton btnGetContact;
    private SimpleAdapter adapter;
    private String nominal = "0";
    private String titleHint;

    public PembayaranThreeFragment() {

    }

    @Override public PembayaranThreePresenter createPresenter() {
        return new PembayaranThreePresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_pembayaran_three;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new SimpleAdapter();
        switch (type) {
            case BPJS:
                titleHint = "Pilih Periode Bulan";
                etProduct.setFloatingLabelText("Pilih Periode");
                break;
            case PT_PLN_TOKEN:
                titleHint = "Pilih Nominal";
                etProduct.setFloatingLabelText("Pilih Nominal");
                break;
        }
        etProduct.setHint(titleHint);
    }

    @OnTextChanged({R.id.etIdPelanggan, R.id.etPhone}) public void onTextChanged() {
        invalidateSubmitButton();
    }

    private void invalidateSubmitButton() {
        btnSubmit.setEnabled(etPhone.getText().length() > 9 && etIdPelanggan.getText().length() > 10 && !TextUtils.isEmpty(etProduct.getText()));
    }

    @Nullable @Override public String getTitle() {
        return title;
    }

    @OnClick({R.id.btnGetContact, R.id.etProduct, R.id.btnSubmit, R.id.btnGetContactIdPel}) public void onViewClicked(View view) {
        Intent pickContactIntent;
        switch (view.getId()) {
            case R.id.btnGetContact:
                pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
                pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(pickContactIntent, REQUEST_CONTACT);
                break;
            case R.id.etProduct:
                presenter.loadProductList(type);
                break;
            case R.id.btnSubmit:
                if (!validateInput()) {
                    break;
                }
//                showToast("Dalam pengembangan");
                CommonUtilities.hideSoftKeyboard(getActivity());
                Logs.d(TAG + " => nominall => " + nominal);
                KonfirmasiPembayaranFragment fragment = new KonfirmasiPembayaranFragmentBuilder(etIdPelanggan.getText().toString())
                        .nominal(nominal)
                        .noTelephone(etPhone.getText().toString())
                        .typeProduk(type)
                        .build();

                mListener.gotoPage(fragment, false, null);
                etIdPelanggan.setText(null);
                break;
            case R.id.btnGetContactIdPel:
                pickContactIntent = new Intent(Intent.ACTION_PICK, Uri.parse("content://contacts"));
                pickContactIntent.setType(ContactsContract.CommonDataKinds.Phone.CONTENT_TYPE);
                startActivityForResult(pickContactIntent, REQUEST_CONTACT_ID);
                break;
        }
    }

    private boolean validateInput() {
        if (!etIdPelanggan.isCharactersCountValid()) {
            etIdPelanggan.setError("ID pelanggan tidak valid");
            etIdPelanggan.requestFocus();
            return false;
        }

        if (!etPhone.isCharactersCountValid()) {
            etPhone.setError("Nomor Telephone tidak valid");
            etPhone.requestFocus();
            return false;
        }

        if (etProduct.getText().toString().isEmpty()) {
            etProduct.setError("Silahkan pilih nominal");
            return false;
        }

        return true;
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CONTACT) {
            if (resultCode == Activity.RESULT_OK) {
                String number = CommonUtilities.getNumberContact(getContext(), data.getData());
                if (!TextUtils.isEmpty(number)) {
                    etPhone.setText(number);
                }
            }
        }
        if (requestCode == REQUEST_CONTACT_ID) {
            if (resultCode == Activity.RESULT_OK) {
                String number = CommonUtilities.getNumberContact(getContext(), data.getData());
                if (!TextUtils.isEmpty(number)) {
                    etIdPelanggan.setText(number);
                }
            }
        }
    }

    @Override public void showProduct(ArrayList<? extends SimpleList> data) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        adapter.addAll(data);
        new DialogListOption(getContext(), titleHint, adapter) {
            @Override public void onItemClicked(int position) {
                etProduct.setText(adapter.getItem(position).getName());
                nominal = adapter.getItem(position).getId();
                invalidateSubmitButton();
            }

            @Override public void onFilter(CharSequence s) {
                adapter.filter(s);
            }
        };
    }
}