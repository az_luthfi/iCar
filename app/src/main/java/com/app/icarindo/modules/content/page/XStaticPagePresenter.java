package com.app.icarindo.modules.content.page;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XStaticPagePresenter extends MvpPresenter<XStaticPageView> {

    void loadContent(String contentAlias);
}