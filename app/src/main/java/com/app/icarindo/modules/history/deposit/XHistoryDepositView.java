package com.app.icarindo.modules.history.deposit;

import com.app.icarindo.models.ppob.RequestHistory;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XHistoryDepositView extends MvpLceView<RequestHistory> {
    void showLoadingNextPage(boolean show);

    void setNextData(RequestHistory data);

    void showErrorNextPage(String string);
}