package com.app.icarindo.modules.carwis.list;

import com.app.icarindo.models.carwis.RequestCarwis;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XListCarwisView extends MvpLceView<RequestCarwis> {

    void showLoadingNextPage(boolean show);

    void setNextData(RequestCarwis data);

    void showErrorNextPage(String string);
}