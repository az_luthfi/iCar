package com.app.icarindo.modules.ppob.pembayaran.model4;

import android.content.Context;

import com.app.icarindo.base.BasePresenter;

public class PembayaranFourPresenter extends BasePresenter<XPembayaranFourView>
        implements XPembayaranFourPresenter {

    public PembayaranFourPresenter(Context context) {
        super(context);
    }

}