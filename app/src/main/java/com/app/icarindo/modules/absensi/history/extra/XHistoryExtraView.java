package com.app.icarindo.modules.absensi.history.extra;

import com.app.icarindo.models.network.RequestNetwork;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XHistoryExtraView extends MvpLceView<RequestNetwork> {

    void showLoadingNextPage(boolean show);

    void setNextData(RequestNetwork data);

    void showErrorNextPage(String string);

    void onSuccessCancel();

    void showProgressDialog(boolean show);

    void showError(String text);
}