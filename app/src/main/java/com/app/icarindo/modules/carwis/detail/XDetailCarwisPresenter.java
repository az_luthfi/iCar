package com.app.icarindo.modules.carwis.detail;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XDetailCarwisPresenter extends MvpPresenter<XDetailCarwisView> {

    void loadData(boolean pullToRefresh, String bookingCode);

    void validationBooking(String bookingCode);
}