package com.app.icarindo.modules.ppob.history.detail.deposit;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XDetailDepositPresenter extends MvpPresenter<XDetailDepositView> {

    void loadData(boolean pullToRefresh, String depositId);
}