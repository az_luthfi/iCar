package com.app.icarindo.modules.training.parent;

import com.app.icarindo.models.training.Soal;
import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.ArrayList;

public interface XTrainingPresenter extends MvpPresenter<XTrainingView> {

    void loadSoal(boolean pullToRefresh);

    void saveAnswer(ArrayList<Soal> soals);
}