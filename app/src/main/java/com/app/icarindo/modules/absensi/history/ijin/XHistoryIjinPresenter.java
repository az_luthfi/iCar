package com.app.icarindo.modules.absensi.history.ijin;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XHistoryIjinPresenter extends MvpPresenter<XHistoryIjinView> {
    void loadDataNextPage(int page);

    void loadHistoryCuti(boolean pullToRefresh);

    void requestCancel(String ccId);
}