package com.app.icarindo.modules.store.step.address;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.eventbus.EventShippingAddress;
import com.app.icarindo.models.store.RequestShipping;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;


public class ShippingAddressPresenter extends BaseRxLcePresenter<XShippingAddressView, RequestShipping>
        implements XShippingAddressPresenter {

    public ShippingAddressPresenter(Context context) {
        super(context);
    }

    @Override public void listShipping(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("customer_id", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("act", "getAddressBook");

        subscribe(App.getService().apiShipping(params), pullToRefresh);
    }

    @Override public void deleteShippingAddress(String caId) {
        HashMap<String, String> params = new HashMap<>();
        params.put("customer_id", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));
        params.put("shippingId", caId);
        params.put("act", "deleteAddress");

        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }

        Observer<RequestShipping> observer = new Observer<RequestShipping>() {

            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestShipping data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().setData(data);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showDialogError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiShipping(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onShippingAddressEvent(EventShippingAddress event) {
        listShipping(false);
    }

    @Override public void attachView(XShippingAddressView view) {
        super.attachView(view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);

    }
}