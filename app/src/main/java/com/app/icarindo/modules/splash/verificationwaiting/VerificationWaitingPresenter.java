package com.app.icarindo.modules.splash.verificationwaiting;

import android.content.Context;

import com.app.icarindo.base.BasePresenter;

public class VerificationWaitingPresenter extends BasePresenter<XVerificationWaitingView>
        implements XVerificationWaitingPresenter {

    public VerificationWaitingPresenter(Context context) {
        super(context);
    }

}