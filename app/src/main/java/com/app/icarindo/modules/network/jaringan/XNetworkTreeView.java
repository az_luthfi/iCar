package com.app.icarindo.modules.network.jaringan;

import com.app.icarindo.models.network.RequestNetwork;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XNetworkTreeView extends MvpLceView<RequestNetwork> {

}