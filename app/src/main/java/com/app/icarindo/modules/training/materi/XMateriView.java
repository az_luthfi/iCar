package com.app.icarindo.modules.training.materi;

import com.app.icarindo.models.training.RequestTraining;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XMateriView extends MvpLceView<RequestTraining> {

    void onAllowStoragePermissions();

    void onDeniedStoragePermissions();
}