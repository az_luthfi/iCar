package com.app.icarindo.modules.absensi.city;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XOutOfCityPresenter extends MvpPresenter<XOutOfCityView> {

    void requestInput(String date, String desc);
}