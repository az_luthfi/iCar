package com.app.icarindo.modules.ppob.pembayaran.model4;

import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatRadioButton;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpFragment;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.modules.ppob.pembayaran.konfirmasi.KonfirmasiPembayaranFragment;
import com.app.icarindo.modules.ppob.pembayaran.konfirmasi.KonfirmasiPembayaranFragmentBuilder;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Logs;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.hannesdorfmann.mosby3.mvp.lce.LceAnimator;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;
import butterknife.OnTextChanged;

import static com.app.icarindo.utility.Config.TELKOM;

/**
 * Pembayaran TELKOM
 */
@FragmentWithArgs
public class PembayaranFourFragment extends BaseMvpFragment<XPembayaranFourView, PembayaranFourPresenter>
        implements XPembayaranFourView, MainActivity.OnChangeToolbar {
    private static final String TAG = PembayaranFourFragment.class.getSimpleName();
    @Arg String title;
    @Arg String type;
    @BindView(R.id.rbTelkom) AppCompatRadioButton rbTelkom;
    @BindView(R.id.rbSpeedy) AppCompatRadioButton rbSpeedy;
    @BindView(R.id.rbType) RadioGroup rbType;
    @BindView(R.id.etCode) MaterialEditText etCode;
    @BindView(R.id.etIdPelanggan) MaterialEditText etIdPelanggan;
    @BindView(R.id.btnSubmit) Button btnSubmit;

    public PembayaranFourFragment() {

    }

    @Override public PembayaranFourPresenter createPresenter() {
        return new PembayaranFourPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_pembayaran_four;
    }

    @OnCheckedChanged({R.id.rbTelkom, R.id.rbSpeedy}) public void onChecked(RadioButton radioButton, boolean flag) {
        switch (radioButton.getId()) {
            case R.id.rbTelkom:
                Logs.d(TAG, "onChecked ==> telkom");
                etCode.setText("");
                LceAnimator.showContent(new View(getContext()), new View(getContext()), etCode);
                break;
            case R.id.rbSpeedy:
                Logs.d(TAG, "onChecked ==> speedy");
                if (!etCode.isShown()) {
                    LceAnimator.showContent(new View(getContext()), etCode, new View(getContext()));
                }
                break;
        }
    }

    @OnClick(R.id.btnSubmit) public void onViewClicked() {
        if (validateInput()) {
            CommonUtilities.hideSoftKeyboard(getActivity());
            KonfirmasiPembayaranFragment fragment;
            if (etCode.isShown()){
                fragment = new KonfirmasiPembayaranFragmentBuilder(etCode.getText().toString())
                        .noTelephone(etIdPelanggan.getText().toString())
                        .typeProduk(TELKOM)
                        .build();
            }else{
                fragment = new KonfirmasiPembayaranFragmentBuilder(etIdPelanggan.getText().toString())
                        .typeProduk(TELKOM)
                        .build();
            }

            etIdPelanggan.setText(null);
            etCode.setText(null);
            mListener.gotoPage(fragment, false, null);
        }
    }

    private boolean validateInput() {
        if (rbTelkom.isChecked() && etCode.getText().length() < 3) {
            etCode.setError("Kode Area tidak valid");
            etCode.requestFocus();
            return false;
        }

        if (etIdPelanggan.getText().length() < 6) {
            etIdPelanggan.setError("ID Pelanggan tidak valid");
            etIdPelanggan.requestFocus();
            return false;
        }
        return true;
    }

    @OnTextChanged({R.id.etCode, R.id.etIdPelanggan}) public void oTextChanged(CharSequence charSequence, int c1, int c2, int c3) {
        if (rbTelkom.isChecked()){
            btnSubmit.setEnabled(etCode.getText().length() > 2 && etIdPelanggan.getText().length() > 4);
        }else{
            btnSubmit.setEnabled(etIdPelanggan.getText().length() > 4);
        }
    }

    @Nullable @Override public String getTitle() {
        return title;
    }
}