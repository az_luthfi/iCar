package com.app.icarindo.modules.history.product;

import com.app.icarindo.models.store.RequestOrder;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XHistoryProductView extends MvpLceView<RequestOrder> {
    void showLoadingNextPage(boolean show);

    void setNextData(RequestOrder data);

    void showErrorNextPage(String string);
}