package com.app.icarindo.modules.store.order.detail;

import com.app.icarindo.models.store.RequestOrder;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XOrderDetailView extends MvpLceView<RequestOrder> {
    void showProgressDialog(boolean show);

    void showError(String string);
}