package com.app.icarindo.modules.network.sponsor;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XSponsorPresenter extends MvpPresenter<XSponsorView> {

    void loadDataNextPage(int page);

    void loadBonusSponsor(boolean pullToRefresh);
}