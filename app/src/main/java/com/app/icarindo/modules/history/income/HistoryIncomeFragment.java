package com.app.icarindo.modules.history.income;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.PopupMenu;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.adapter.HistoryNonTransactionAdapter;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.network.Plan;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.modules.carwis.detail.DetailCarwisFragment;
import com.app.icarindo.modules.carwis.detail.DetailCarwisFragmentBuilder;
import com.app.icarindo.modules.history.HistoryFragment;
import com.app.icarindo.modules.ppob.history.detail.transaction.DetailTransactionFragmentBuilder;
import com.app.icarindo.modules.store.order.detail.OrderDetailFragment;
import com.app.icarindo.modules.store.order.detail.OrderDetailFragmentBuilder;
import com.app.icarindo.utility.Config;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.app.icarindo.utility.listview.DividerItemDecoration;
import com.app.icarindo.utility.listview.EndlessRecyclerOnScrollListener;
import com.app.icarindo.utility.listview.ItemClickSupport;

import butterknife.BindView;
import butterknife.OnClick;

public class HistoryIncomeFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestNetwork, XHistoryIncomeView, HistoryIncomePresenter>
        implements XHistoryIncomeView, ItemLoadingHolder.ItemViewLoadingListener {

    private static final String FILTER_ALL = "all";
    private static final String FILTER_NETWORK = "network";
    private static final String FILTER_SPONSOR = "sponsor";
    private static final String FILTER_CAHSBACK = "cashback";
    private static final String FILTER_CARWIS = "carwis";

    @BindView(R.id.tvSubheadMonth) TextView tvSubheadMonth;
    @BindView(R.id.tvNominalMonth) TextView tvNominalMonth;
    @BindView(R.id.tvNominalTotal) TextView tvNominalTotal;
    @BindView(R.id.layDividerFilter) FrameLayout layDividerFilter;
    @BindView(R.id.layFilter) FrameLayout layFilter;
    @BindView(R.id.recyclerView) RecyclerView recyclerView;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    private String filterDate = "";
    private String filterType = FILTER_ALL;

    private HistoryNonTransactionAdapter adapter;
    private int maxCountItems;
    private EndlessRecyclerOnScrollListener scrollListener;

    public HistoryIncomeFragment() {

    }

    @Override public HistoryIncomePresenter createPresenter() {
        return new HistoryIncomePresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_history_item;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        adapter = new HistoryNonTransactionAdapter(this);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext(), DividerItemDecoration.LIST_VERTICAL));

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                switch (adapter.getItem(position).getHistoryMethod()){
                    case "trans_cashback":
                        mListener.gotoPage(new DetailTransactionFragmentBuilder(adapter.getItem(position).getHistoryId()).build(), false, null);
                        break;
                    case "order_cashback":
                        mListener.gotoPage(new OrderDetailFragmentBuilder(adapter.getItem(position).getHistoryId()).build(), false, OrderDetailFragment.class.getSimpleName() + adapter.getItem(position).getHistoryId());
                        break;
                    case "carwis":
                        DetailCarwisFragment fragment = new DetailCarwisFragmentBuilder(adapter.getItem(position).getHistoryId()).build();
                        String tagName = DetailCarwisFragment.class.getSimpleName() + adapter.getItem(position).getHistoryId();
                        mListener.gotoPage(fragment, false, tagName);
                        break;
                }
            }
        });

        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                if (!adapter.isLoading()) {
                    if (adapter.getItemCount() < maxCountItems) {
                        presenter.loadDataNextPage(filterType, filterDate, page);
                    }
                }
            }
        };

        recyclerView.addOnScrollListener(scrollListener);

    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestNetwork data) {
        switch (filterType) {
            case FILTER_ALL:
                tvSubheadMonth.setText(new StringBuilder("Income"));
                break;
            case FILTER_NETWORK:
                tvSubheadMonth.setText(new StringBuilder("Bonus Network"));
                break;
            case FILTER_SPONSOR:
                tvSubheadMonth.setText(new StringBuilder("Bonus Sponsor"));
                break;
            case FILTER_CAHSBACK:
                tvSubheadMonth.setText(new StringBuilder("Cashback"));
                break;
            case FILTER_CARWIS:
                tvSubheadMonth.setText(new StringBuilder("Carwis"));
                break;
        }
        tvNominalMonth.setText(data.getNominalMonth());
        tvNominalTotal.setText(data.getNominalTotal());
        if (adapter.getItemCount() > 0) {
            adapter.clear();
            scrollListener.resetState();
        }
        if (data.getStatus()) {
            maxCountItems = data.getCount();
            adapter.setItems(data.getItemHistories());
        } else {
            maxCountItems = 0;
        }
    }

    public void updateData() {
        String filterDateParent = ((HistoryFragment) getParentFragment()).getFilterDate();
        if (!filterDateParent.equals(filterDate)) {
            filterDate = filterDateParent;
            loadData(false);
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(filterType, filterDate, pullToRefresh);
    }

    @OnClick(R.id.layFilter) public void onViewClicked() {
        PopupMenu popupMenu = new PopupMenu(getContext(), layFilter);
        popupMenu.getMenuInflater().inflate(R.menu.filter_history_income, popupMenu.getMenu());
        if (presenter.getPrefs().getPreferencesString(Config.PLAN_CODE).equals(Plan.CODE_PLANA1)) {
            popupMenu.getMenu().getItem(R.id.option_carwis).setVisible(false);
        }
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                switch (menuItem.getItemId()) {
                    case R.id.option_all:
                        filterType = FILTER_ALL;
                        loadData(false);
                        return true;
                    case R.id.option_network:
                        filterType = FILTER_NETWORK;
                        loadData(false);
                        return true;
                    case R.id.option_sponsor:
                        filterType = FILTER_SPONSOR;
                        loadData(false);
                        return true;
                    case R.id.option_cashback:
                        filterType = FILTER_CAHSBACK;
                        loadData(false);
                        return true;
                    case R.id.option_carwis:
                        filterDate = FILTER_CARWIS;
                        loadData(false);
                        return true;
                    default:
                        return false;
                }
            }
        });
        popupMenu.show();
    }

    @Override public void onRetryNextPage() {
        adapter.setErrorNextPage(null);
        presenter.loadDataNextPage(filterType, filterDate, scrollListener.getCurrentPage());
    }

    @Override public void showLoadingNextPage(boolean show) {
        adapter.setLoadingNextPage(show);
    }

    @Override public void setNextData(RequestNetwork data) {
        if (data.getStatus()) {
            adapter.setItems(data.getItemHistories());
        }
    }

    @Override public void showErrorNextPage(String string) {
        Toast.makeText(getContext(), string, Toast.LENGTH_SHORT).show();
        adapter.setErrorNextPage("Tap to retry");
        scrollListener.backToPreviousPage();
    }
}