package com.app.icarindo.modules.carwis.list;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XListCarwisPresenter extends MvpPresenter<XListCarwisView> {

    void loadData(boolean pullToRefresh, String status, int page);
}