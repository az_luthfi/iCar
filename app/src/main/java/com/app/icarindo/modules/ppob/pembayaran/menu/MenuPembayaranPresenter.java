package com.app.icarindo.modules.ppob.pembayaran.menu;

import android.content.Context;

import com.app.icarindo.base.BasePresenter;

public class MenuPembayaranPresenter extends BasePresenter<XMenuPembayaranView>
        implements XMenuPembayaranPresenter {

    public MenuPembayaranPresenter(Context context) {
        super(context);
    }

}