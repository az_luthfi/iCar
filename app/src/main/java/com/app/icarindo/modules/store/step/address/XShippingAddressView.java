package com.app.icarindo.modules.store.step.address;

import com.app.icarindo.models.store.RequestShipping;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XShippingAddressView extends MvpLceView<RequestShipping> {

    void showProgressDialog(boolean show);

    void showDialogError(String string);
}