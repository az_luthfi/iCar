package com.app.icarindo.modules.iuran;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XIuranPresenter extends MvpPresenter<XIuranView> {

    void loadIuran(boolean pullToRefresh);

    void loadDataNextPage(int page);
}