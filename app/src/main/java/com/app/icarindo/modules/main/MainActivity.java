package com.app.icarindo.modules.main;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.listeners.FragmentInteractionListener;
import com.app.icarindo.models.DataNotification;
import com.app.icarindo.models.eventbus.EventDashboard;
import com.app.icarindo.modules.history.HistoryFragment;
import com.app.icarindo.modules.home.HomeFragment;
import com.app.icarindo.modules.network.NetworkFragment;
import com.app.icarindo.scheduler.LocationAlarmReceiver;
import com.app.icarindo.utility.Config;
import com.app.icarindo.utility.FragmentStack;
import com.app.icarindo.utility.Preferences;
import com.app.icarindo.utility.UtilScheduler;
import com.google.gson.Gson;
import com.tbruyelle.rxpermissions2.Permission;
import com.tbruyelle.rxpermissions2.RxPermissions;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class MainActivity extends AppCompatActivity
        implements FragmentInteractionListener {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.ivToolbarImage) ImageView ivToolbarImage;
    @BindView(R.id.tvToolbarTitle) TextView tvToolbarTitle;
    @BindView(R.id.container) FrameLayout container;
    @BindView(R.id.appBar) AppBarLayout appBar;

    private FragmentStack fragmentStack;
    private ActionBar actionBar;
    private Preferences prefs;
    private DataNotification dataNotification;

    private LocationAlarmReceiver locationAlarmReceiver;
    private RxPermissions rxPermissions;
    private boolean gotoSetting = false;
    private AlertDialog alertDialog;

    private static final int TIME_DELAY = 2000;
    private static long back_pressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            locationAlarmReceiver = new LocationAlarmReceiver();
        }
        rxPermissions = new RxPermissions(this);
        fragmentStack = new FragmentStack(this, getSupportFragmentManager(), R.id.container);
        prefs = new Preferences(getApplicationContext());
//        fragmentStack.push(new DashboardFragmentBuilder(0).build(), null);
        fragmentStack.push(new HomeFragment(), null);
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();
        setupToolbar();
        handleIntent(getIntent());

        requestPermissionsLocation();
    }

    private void requestPermissionsLocation() {
        rxPermissions
                .requestEach(android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(new Observer<Permission>() {
                    @Override public void onSubscribe(Disposable d) {

                    }

                    @Override public void onNext(Permission permission) {
                        if (alertDialog != null && alertDialog.isShowing()){
                            alertDialog.dismiss();
                        }
                        if (permission.granted) {
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                UtilScheduler.scheduleJob(MainActivity.this);
                            }else{
                                locationAlarmReceiver.cancelAlarm(MainActivity.this);
                                locationAlarmReceiver.setAlarm(MainActivity.this);
                            }
                        } else if (permission.shouldShowRequestPermissionRationale){
                            requestPermissionsLocation(); // deny without never ask again
                        }else{
                            alertDialog = new AlertDialog.Builder(MainActivity.this).create();
                            alertDialog.setCancelable(false);
                            alertDialog.setTitle("Pengaturan");
                            alertDialog.setMessage("Permisi lokasi wajib di aktifkan");
                            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int which) {
                                            alertDialog.dismiss();
                                            gotoSetting = true;
                                            Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                    Uri.fromParts("package", getPackageName(), null));
                                            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(intent);
                                        }
                                    });
                            alertDialog.show();
                            // deny with never ask again
                        }
                    }

                    @Override public void onError(Throwable e) {

                    }

                    @Override public void onComplete() {

                    }
                });

    }

    private void handleIntent(Intent intent) {
        if (intent == null) {
            return;
        }
        if (intent.getExtras() == null) {
            return;
        }
        String jsonDataNotification = intent.getExtras().getString(Config.JSON_DATA_NOTIFICATION);
        if (jsonDataNotification == null) {
            return;
        }
        dataNotification = new Gson().fromJson(jsonDataNotification, DataNotification.class);
        if (dataNotification == null) {
            return;
        }
        switch (dataNotification.getMethod().toLowerCase()) {
            case "message":
                new Handler().postDelayed(new Runnable() {
                    @Override public void run() {
                        EventBus.getDefault().post(new EventDashboard(2));
                    }
                }, 180);

                break;
            case "bonus_sponsor":
                gotoPage(new NetworkFragment(), false, null);
                break;
            case "bonus_network":
                gotoPage(new HistoryFragment(), false, null);
                break;
        }
    }

    @Override protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        handleIntent(intent);
    }

    private void setupToolbar() {
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayShowHomeEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(false);
        actionBar.setDisplayShowTitleEnabled(false);
    }

    @Override
    public void onBackPressed() {
        if (fragmentStack.size() > 1) {
            fragmentStack.back();
            toggleShowUpNavigation();
        } else {
            if (!fragmentStack.back()) {
                if (back_pressed + TIME_DELAY > System.currentTimeMillis()) {
                    finish();
                } else {
                    Toast.makeText(getBaseContext(), "Tekan lagi untuk keluar",
                            Toast.LENGTH_SHORT).show();
                }
                back_pressed = System.currentTimeMillis();
            }

        }
    }

    private void toggleShowUpNavigation() {
        Fragment top = fragmentStack.peek();
        String toolbarTitle = null;
        if (top instanceof HomeFragment) {
            ivToolbarImage.setVisibility(View.VISIBLE);
            tvToolbarTitle.setVisibility(View.GONE);
        } else {
            ivToolbarImage.setVisibility(View.GONE);
            tvToolbarTitle.setVisibility(View.VISIBLE);
            if (top instanceof OnChangeToolbar) {
                toolbarTitle = ((OnChangeToolbar) top).getTitle();
            }
            if (toolbarTitle == null) {
                ivToolbarImage.setVisibility(View.VISIBLE);
                tvToolbarTitle.setVisibility(View.GONE);
//                tvToolbarTitle.setText(this.getString(R.string.app_name));
            } else {
                tvToolbarTitle.setText(toolbarTitle);
            }
        }
        if (fragmentStack.size() > 1) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        } else {
            setupToolbar();
        }
    }

    @Override public void gotoPage(Fragment page, boolean shouldReplace, String tagName) {
        int countBefore = fragmentStack.size();
        if (shouldReplace) {
            fragmentStack.push(page, tagName);
        } else {
            fragmentStack.pushAdd(page, tagName);
        }

        toggleShowUpNavigation();
//        //ubah back icon ke burger icon pada NavDraw
//        if (page instanceof DashboardFragment) {
//            if (countBefore > 1) {
//                toggleShowUpNavigation();
//            }
//        } else {
//            toggleShowUpNavigation();
//        }
    }

    @Override public void back() {
        onBackPressed();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public interface OnChangeToolbar {
        @Nullable String getTitle();
    }

    @Override protected void onResume() {
        super.onResume();
        if (gotoSetting){
            gotoSetting = false;
            requestPermissionsLocation();
        }
    }
}
