package com.app.icarindo.modules.ppob.pembelian.voucher;

import android.content.Intent;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XVoucherPresenter extends MvpPresenter<XVoucherView> {

    void loadData(String type);

    void getContact();

    void onActivityResult(int requestCode, int resultCode, Intent data);
}