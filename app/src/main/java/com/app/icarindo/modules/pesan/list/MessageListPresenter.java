package com.app.icarindo.modules.pesan.list;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.eventbus.EventMessage;
import com.app.icarindo.models.pesan.RequestMessage;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;


public class MessageListPresenter extends BaseRxLcePresenter<XMessageListView, RequestMessage>
        implements XMessageListPresenter {

    private String customerId;

    public MessageListPresenter(Context context) {
        super(context);
        customerId = String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID));
    }

    @Override public void loadListPesan(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "list");
        params.put("page", "1");
        params.put("customerId", customerId);

        subscribe(App.getService().apiMessageRequest(params), pullToRefresh);
    }

    @Override public void loadDataNextPage(int page) {
        if (isViewAttached()) {
            getView().showLoadingNextPage(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "list");
        params.put("page", String.valueOf(page));
        params.put("customerId", customerId);

        Observer<RequestMessage> observer1 = new Observer<RequestMessage>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestMessage data) {
                if (isViewAttached()) {
                    getView().setNextData(data);
                    getView().showLoadingNextPage(false);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showErrorNextPage(context.getString(R.string.error_network));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiMessageRequest(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer1);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(EventMessage event) {
        switch (event.getType()){
            case EventMessage.NEW:
                if (isViewAttached()){
                    getView().addNewItem(event.getMessage());
                }
                break;
        }

    }

    @Override public void attachView(XMessageListView view) {
        super.attachView(view);
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override public void detachView(boolean retainInstance) {
        super.detachView(retainInstance);
        EventBus.getDefault().unregister(this);
    }
}