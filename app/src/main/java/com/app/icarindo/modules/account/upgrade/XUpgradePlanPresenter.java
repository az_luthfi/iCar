package com.app.icarindo.modules.account.upgrade;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XUpgradePlanPresenter extends MvpPresenter<XUpgradePlanView> {

    void loadData(boolean pullToRefresh);

    void loadTermAndCon();

    void upgradePlan(String planCode);
}