package com.app.icarindo.modules.absensi.extra;

import com.app.icarindo.models.network.RequestNetwork;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XExtraView extends MvpLceView<RequestNetwork> {
    void showProgressDialog(boolean show);

    void showError(String text);

    void onNextRequestExtra(RequestNetwork data);
}