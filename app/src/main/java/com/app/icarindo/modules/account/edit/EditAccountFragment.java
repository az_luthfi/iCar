package com.app.icarindo.modules.account.edit;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.adapter.ImageScanAdapter;
import com.app.icarindo.adapter.SimpleAdapter;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.models.Media;
import com.app.icarindo.models.customer.Customer;
import com.app.icarindo.models.customer.RequestCustomer;
import com.app.icarindo.models.datalist.SimpleList;
import com.app.icarindo.models.network.Plan;
import com.app.icarindo.modules.common.FragmentPhotoViewSingle;
import com.app.icarindo.modules.common.FragmentPhotoViewSingleBuilder;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.DialogListOption;
import com.app.icarindo.utility.Str;
import com.app.icarindo.utility.Validation;
import com.google.gson.Gson;
import com.rengwuxian.materialedittext.MaterialEditText;
import com.squareup.picasso.Picasso;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

import static com.app.icarindo.utility.Config.CUSTOMER_IMAGE_CROP_HEIGHT;
import static com.app.icarindo.utility.Config.CUSTOMER_IMAGE_CROP_WIDTH;
import static com.app.icarindo.utility.Config.DOCUMENT_IMAGE_CROP_HEIGHT;
import static com.app.icarindo.utility.Config.DOCUMENT_IMAGE_CROP_WIDTH;
import static com.app.icarindo.utility.Config.PLAN_CODE;

public class EditAccountFragment extends BaseMvpLceFragment<RelativeLayout, RequestCustomer, XEditAccountView, EditAccountPresenter>
        implements XEditAccountView, MainActivity.OnChangeToolbar {
    private static final int CHOICE_IMAGE_FROM_GALLERY = 8874;
    private static final String CAMERA_FILE_NAME = "cameraFileName";
    private static final String IMAGE_TYPE_PROFILE = "img";
    private static final String IMAGE_TYPE_KTP = "ktp[]";
    private static final String IMAGE_TYPE_SIM = "sim[]";
    private static final String IMAGE_TYPE_SKCK = "skck[]";
    private static final String IMAGE_TYPE_STNK = "stnk[]";
    private static final String IMAGE_TYPE_ETC = "etc[]";

    @BindView(R.id.ivCustomerImage) CircleImageView ivCustomerImage;
    @BindView(R.id.tvCustomerId) TextView tvCustomerId;
    @BindView(R.id.etName) MaterialEditText etName;
    @BindView(R.id.etEmail) MaterialEditText etEmail;
    @BindView(R.id.etPhone) MaterialEditText etPhone;
    @BindView(R.id.etAddress) MaterialEditText etAddress;
    @BindView(R.id.etProvince) MaterialEditText etProvince;
    @BindView(R.id.tvUpdate) TextView tvUpdate;
    @BindView(R.id.contentView) RelativeLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.fabEditPhoto) FloatingActionButton fabEditPhoto;
    @BindView(R.id.btnUploadKTP) Button btnUploadKTP;
    @BindView(R.id.rvKtp) RecyclerView rvKtp;
    @BindView(R.id.btnUploadSIM) Button btnUploadSIM;
    @BindView(R.id.rvSIM) RecyclerView rvSIM;
    @BindView(R.id.btnUploadSKCK) Button btnUploadSKCK;
    @BindView(R.id.rvSKCK) RecyclerView rvSKCK;
    @BindView(R.id.layKtp) LinearLayout layKtp;
    @BindView(R.id.laySIM) LinearLayout laySIM;
    @BindView(R.id.laySkck) LinearLayout laySkck;
    @BindView(R.id.btnUploadSTNK) Button btnUploadSTNK;
    @BindView(R.id.rvSTNK) RecyclerView rvSTNK;
    @BindView(R.id.laySTNK) LinearLayout laySTNK;
    @BindView(R.id.btnUploadEtc) Button btnUploadEtc;
    @BindView(R.id.rvETC) RecyclerView rvETC;
    @BindView(R.id.layEtc) LinearLayout layEtc;
    @BindView(R.id.etKtp) MaterialEditText etKtp;
    @BindView(R.id.etBirthDate) MaterialEditText etBirthDate;

    private SimpleAdapter adapter;
    private File fileImage;
    private String cameraFileName;
    private String imageType = IMAGE_TYPE_PROFILE;
    private Customer customer;
    private ImageScanAdapter adapterKtp;
    private ImageScanAdapter adapterSim;
    private ImageScanAdapter adapterSkck;
    private ImageScanAdapter adapterStnk;
    private ImageScanAdapter adapterEtc;
    private List<MultipartBody.Part> ktpPart = new ArrayList<>();
    private List<MultipartBody.Part> simPart = new ArrayList<>();
    private List<MultipartBody.Part> skckPart = new ArrayList<>();
    private List<MultipartBody.Part> stnkPart = new ArrayList<>();
    private List<MultipartBody.Part> etcPart = new ArrayList<>();
    private MultipartBody.Part imagePart = null;
    private List<Media> mediaListKTP;
    private List<Media> mediaListSIM;
    private List<Media> mediaListSKCK;
    private List<Media> mediaListSTNK;
    private List<Media> mediaListEtc;
    private List<Media> mediaListDeletedKTP = new ArrayList<>();
    private List<Media> mediaListDeletedSIM = new ArrayList<>();
    private List<Media> mediaListDeletedSKCK = new ArrayList<>();
    private List<Media> mediaListDeletedSTNK = new ArrayList<>();
    private List<Media> mediaListDeletedETC = new ArrayList<>();

    private String birthDate = "";

    public EditAccountFragment() {

    }

    @Override public EditAccountPresenter createPresenter() {
        return new EditAccountPresenter(getContext(), getActivity());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_edit_account;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new SimpleAdapter();
        adapterKtp = new ImageScanAdapter(new ImageScanAdapter.ImageScanListener() {
            @Override public void onClickImage(int position) {
                showImagePreview(position, adapterKtp);
            }

            @Override public void onDeleteImage(int position) {
                adapterKtp.removeItem(position);
                if (mediaListKTP != null) {
                    if (position < mediaListKTP.size()) {
                        mediaListDeletedKTP.add(mediaListKTP.get(position));
                        mediaListKTP.remove(position);
                    } else {
                        ktpPart.remove(position - mediaListKTP.size());
                    }
                } else {
                    ktpPart.remove(position);
                }
                if (adapterKtp.getItemCount() < 2) {
                    btnUploadKTP.setVisibility(View.VISIBLE);
                }
            }
        });
        adapterSim = new ImageScanAdapter(new ImageScanAdapter.ImageScanListener() {
            @Override public void onClickImage(int position) {
                showImagePreview(position, adapterSim);
            }

            @Override public void onDeleteImage(int position) {
                adapterSim.removeItem(position);
                if (mediaListSIM != null) {
                    if (position < mediaListSIM.size()) {
                        mediaListDeletedSIM.add(mediaListSIM.get(position));
                        mediaListSIM.remove(position);
                    } else {
                        simPart.remove(position - mediaListSIM.size());
                    }
                } else {
                    simPart.remove(position);
                }
                if (adapterSim.getItemCount() < 2) {
                    btnUploadSIM.setVisibility(View.VISIBLE);
                }
            }
        });
        adapterSkck = new ImageScanAdapter(new ImageScanAdapter.ImageScanListener() {
            @Override public void onClickImage(int position) {
                showImagePreview(position, adapterSkck);
            }

            @Override public void onDeleteImage(int position) {
                adapterSkck.removeItem(position);
                if (mediaListSKCK != null) {
                    if (position < mediaListSKCK.size()) {
                        mediaListDeletedSKCK.add(mediaListSKCK.get(position));
                        mediaListSKCK.remove(position);
                    } else {
                        skckPart.remove(position - mediaListSKCK.size());
                    }
                } else {
                    skckPart.remove(position);
                }
                if (adapterSkck.getItemCount() < 2) {
                    btnUploadSKCK.setVisibility(View.VISIBLE);
                }
            }
        });
        adapterStnk = new ImageScanAdapter(new ImageScanAdapter.ImageScanListener() {
            @Override public void onClickImage(int position) {
                showImagePreview(position, adapterStnk);
            }

            @Override public void onDeleteImage(int position) {
                adapterStnk.removeItem(position);
                if (mediaListSTNK != null) {
                    if (position < mediaListSTNK.size()) {
                        mediaListDeletedSTNK.add(mediaListSTNK.get(position));
                        mediaListSTNK.remove(position);
                    } else {
                        stnkPart.remove(position - mediaListSTNK.size());
                    }
                } else {
                    stnkPart.remove(position);
                }
                if (adapterStnk.getItemCount() < 2) {
                    btnUploadSTNK.setVisibility(View.VISIBLE);
                }
            }
        });
        adapterEtc = new ImageScanAdapter(new ImageScanAdapter.ImageScanListener() {
            @Override public void onClickImage(int position) {
                showImagePreview(position, adapterEtc);
            }

            @Override public void onDeleteImage(int position) {
                adapterEtc.removeItem(position);
                if (mediaListEtc != null) {
                    if (position < mediaListEtc.size()) {
                        mediaListDeletedETC.add(mediaListEtc.get(position));
                        mediaListEtc.remove(position);
                    } else {
                        etcPart.remove(position - mediaListEtc.size());
                    }
                } else {
                    etcPart.remove(position);
                }
                if (adapterEtc.getItemCount() < 2) {
                    btnUploadSTNK.setVisibility(View.VISIBLE);
                }
            }
        });

        rvKtp.setNestedScrollingEnabled(true);
        ViewCompat.setNestedScrollingEnabled(rvKtp, false);
        rvSIM.setNestedScrollingEnabled(true);
        ViewCompat.setNestedScrollingEnabled(rvSIM, false);
        rvSKCK.setNestedScrollingEnabled(true);
        ViewCompat.setNestedScrollingEnabled(rvSKCK, false);
        rvSTNK.setNestedScrollingEnabled(true);
        ViewCompat.setNestedScrollingEnabled(rvSTNK, false);
        rvETC.setNestedScrollingEnabled(true);
        ViewCompat.setNestedScrollingEnabled(rvETC, false);

        rvKtp.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvSIM.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvSKCK.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvSTNK.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvETC.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false));
        rvKtp.setAdapter(adapterKtp);
        rvSIM.setAdapter(adapterSim);
        rvSKCK.setAdapter(adapterSkck);
        rvSTNK.setAdapter(adapterStnk);
        rvETC.setAdapter(adapterEtc);


        loadData(false);
    }

    private void showImagePreview(int position, ImageScanAdapter adapterKtp) {
        FragmentPhotoViewSingle fragmentPhotoView = new FragmentPhotoViewSingleBuilder(adapterKtp.getItem(position)).build();
        fragmentPhotoView.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
        fragmentPhotoView.show(getFragmentManager(), "photo");
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestCustomer data) {
        if (data.getStatus()) {
            customer = data.getCustomer();
            if (!TextUtils.isEmpty(data.getCustomer().getCustomerImage())) {
                Picasso.with(getContext()).load(data.getCustomer().getCustomerImage()).placeholder(R.drawable.person_default).into(ivCustomerImage);
            }
            tvCustomerId.setText(data.getCustomer().getCustomerKode());
            etName.setText(data.getCustomer().getCustomerName());
            etKtp.setText(Str.getText(data.getCustomer().getCustomerMdn(), ""));
            etEmail.setText(data.getCustomer().getCustomerEmail());
            etPhone.setText(CommonUtilities.getText(data.getCustomer().getCustomerMsisdn(), ""));
            etAddress.setText(CommonUtilities.getText(data.getCustomer().getCustomerAddress(), ""));
            etProvince.setText(CommonUtilities.getText(data.getCustomer().getCustomerProvince(), ""));
            birthDate = data.getCustomer().getCustomerBirthDate();
            try {
                etBirthDate.setText(CommonUtilities.getFormatedDate(birthDate, "yyyy-MM-dd", "dd / MM / yyyy"));
            } catch (NullPointerException ignore) {
            }
            if (!presenter.getPrefs().getPreferencesString(PLAN_CODE).equals(Plan.CODE_PLANA1)) {
                mediaListKTP = data.getMediaKtp();
                mediaListSIM = data.getMediaSim();
                mediaListSKCK = data.getMediaSkck();
                mediaListSTNK = data.getMediaStnk();
                mediaListEtc = data.getMediaEtc();
                if (mediaListKTP.size() > 0) {
                    for (Media media : mediaListKTP) {
                        adapterKtp.addItem(media.getMediaValue());
                    }
                }
                if (mediaListSIM.size() > 0) {
                    for (Media media : mediaListSIM) {
                        adapterSim.addItem(media.getMediaValue());
                    }
                }
                if (mediaListSKCK.size() > 0) {
                    for (Media media : mediaListSKCK) {
                        adapterSkck.addItem(media.getMediaValue());
                    }
                }
                if (mediaListSTNK.size() > 0) {
                    for (Media media : mediaListSTNK) {
                        adapterStnk.addItem(media.getMediaValue());
                    }
                }
                if (mediaListEtc.size() > 0) {
                    for (Media media : mediaListEtc) {
                        adapterEtc.addItem(media.getMediaValue());
                    }
                }


                layKtp.setVisibility(View.VISIBLE);
                laySIM.setVisibility(View.VISIBLE);
                laySkck.setVisibility(View.VISIBLE);
                if (presenter.getPrefs().getPreferencesString(PLAN_CODE).equals(Plan.CODE_PLANB1)) {
                    laySTNK.setVisibility(View.VISIBLE);
                } else {
                    laySTNK.setVisibility(View.GONE);
                }
                layEtc.setVisibility(View.VISIBLE);
            } else {
                layKtp.setVisibility(View.GONE);
                laySIM.setVisibility(View.GONE);
                laySkck.setVisibility(View.GONE);
                laySTNK.setVisibility(View.GONE);
                layEtc.setVisibility(View.GONE);
            }

        }

    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh);
    }

    @OnClick({R.id.etProvince, R.id.etBirthDate, R.id.tvUpdate, R.id.fabEditPhoto, R.id.btnUploadKTP, R.id.btnUploadSIM, R.id.btnUploadSKCK, R.id.btnUploadSTNK, R.id.btnUploadEtc}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etProvince:
                presenter.loadProvince();
                break;
            case R.id.etBirthDate:
                final Calendar calendar = Calendar.getInstance();
                if (!TextUtils.isEmpty(birthDate)) {
                    try {
                        @SuppressLint("SimpleDateFormat") SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                        try {
                            Date date = sdf.parse(birthDate);
                            calendar.setTime(date);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                    } catch (NullPointerException ignore) {
                    }
                }
                DatePickerDialog.OnDateSetListener dateSetListener = new DatePickerDialog.OnDateSetListener() {
                    @Override public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                        DecimalFormat mFormat = new DecimalFormat("00");
                        mFormat.setRoundingMode(RoundingMode.DOWN);
                        String day = mFormat.format(Double.valueOf(dayOfMonth));
                        String monthFormat = mFormat.format(Double.valueOf(month + 1));
                        birthDate = year + "-" + monthFormat + "-" + day;
                        etBirthDate.setText(new StringBuilder().append(day)
                                .append(" / ").append(monthFormat).append(" / ").append(year)
                                .append(" "));
                    }
                };

                new DatePickerDialog(
                        getActivity(),
                        dateSetListener,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                ).show();
                break;
            case R.id.tvUpdate:
                validationInput();
                break;
            case R.id.fabEditPhoto:
                imageType = IMAGE_TYPE_PROFILE;
                presenter.requestPermissionGallery();
                break;
            case R.id.btnUploadKTP:
                imageType = IMAGE_TYPE_KTP;
                presenter.requestPermissionGallery();
                break;
            case R.id.btnUploadSIM:
                imageType = IMAGE_TYPE_SIM;
                presenter.requestPermissionGallery();
                break;
            case R.id.btnUploadSKCK:
                imageType = IMAGE_TYPE_SKCK;
                presenter.requestPermissionGallery();
                break;
            case R.id.btnUploadSTNK:
                imageType = IMAGE_TYPE_STNK;
                presenter.requestPermissionGallery();
                break;
            case R.id.btnUploadEtc:
                imageType = IMAGE_TYPE_ETC;
                presenter.requestPermissionGallery();
                break;
        }
    }

    private void validationInput() {
        CommonUtilities.hideSoftKeyboard(getActivity());
        if (Validation.isEmpty(etName, "Nama lengkap tidak boleh kosong")) return;
        if (Validation.isEmpty(etPhone, "No HP tidak boleh kosong")) return;
        if (etPhone.getText().length() < 10) {
            etPhone.requestFocus();
            etPhone.setError("No HP tidak sesuai");
            return;
        }
        if (Validation.isEmpty(etKtp, "No. KTP / NIK wajib diisi")) return;
        if (etKtp.length() < 16) {
            etKtp.requestFocus();
            etKtp.setError("No. KTP / NIK tidak sesuai");
            return;
        }
        if (Validation.isEmpty(etBirthDate, "Tanggal lahir wajib diisi")) return;
        if (Validation.isEmpty(etAddress, "Alamat tidak boleh kosong")) return;
        if (Validation.isEmpty(getContext(), etProvince, "Provinsi harus dipilih")) return;

        HashMap<String, RequestBody> params = new HashMap<>();
        params.put("name", RequestBody.create(MediaType.parse("multipart/form-data"), etName.getText().toString()));
        params.put("msisdn", RequestBody.create(MediaType.parse("multipart/form-data"), etPhone.getText().toString()));
        params.put("address", RequestBody.create(MediaType.parse("multipart/form-data"), etAddress.getText().toString()));
        params.put("province", RequestBody.create(MediaType.parse("multipart/form-data"), etProvince.getText().toString()));
        params.put("photo", RequestBody.create(MediaType.parse("multipart/form-data"), customer.getCustomerImageName()));
        params.put("birthDate", RequestBody.create(MediaType.parse("multipart/form-data"), birthDate));
        params.put("ktp", RequestBody.create(MediaType.parse("multipart/form-data"), etKtp.getText().toString()));

        if (!presenter.getPrefs().getPreferencesString(PLAN_CODE).equals(Plan.CODE_PLANA1)) {
            params.put("media_ktp_delete", RequestBody.create(MediaType.parse("multipart/form-data"), new Gson().toJson(mediaListDeletedKTP)));
            params.put("media_sim_delete", RequestBody.create(MediaType.parse("multipart/form-data"), new Gson().toJson(mediaListDeletedSIM)));
            params.put("media_skck_delete", RequestBody.create(MediaType.parse("multipart/form-data"), new Gson().toJson(mediaListDeletedSKCK)));
            params.put("media_stnk_delete", RequestBody.create(MediaType.parse("multipart/form-data"), new Gson().toJson(mediaListDeletedSTNK)));
            params.put("media_etc_delete", RequestBody.create(MediaType.parse("multipart/form-data"), new Gson().toJson(mediaListDeletedETC)));
        }
        presenter.requestUpdateProfile(params, imagePart, ktpPart, simPart, skckPart, stnkPart, etcPart);
    }

    @Override public void showDialog(ArrayList<? extends SimpleList> data, String title) {
        if (adapter.getItemCount() > 0) {
            adapter.clear();
        }
        adapter.addAll(data);
        new DialogListOption(getContext(), title, adapter) {
            @Override public void onItemClicked(int position) {
                if (position != -1) {
                    etProvince.setText(adapter.getItem(position).getName());
                }
            }

            @Override public void onFilter(CharSequence s) {
                adapter.filter(s);
            }
        };
    }

    @Override public void imageFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        startActivityForResult(Intent.createChooser(intent, "Pilih Gambar"), CHOICE_IMAGE_FROM_GALLERY);
    }

    @Override public void showLoadingUpdate(boolean show) {
        if (show) {
            showLoading("Loading...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void gotoBack() {
        mListener.back();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CHOICE_IMAGE_FROM_GALLERY) {
                final Uri selectedUri = data.getData();
                if (selectedUri != null) {
                    startCrop(selectedUri);
                } else {
                    showToast("Tidak dapat mengambil gambar");
                }
            } else if (requestCode == UCrop.REQUEST_CROP) {
                onImageSelected(UCrop.getOutput(data));
            }
        }
        if (resultCode == UCrop.RESULT_ERROR) {
            handleCropError(data);
        }

    }

    private void onImageSelected(Uri output) {
        fileImage = CommonUtilities.imageFileFromOutput(output, getContext());
        if (fileImage == null) {
            Toast.makeText(getContext(), R.string.error_pick_image, Toast.LENGTH_SHORT).show();
        } else {
            new Handler().post(new Runnable() {
                public void run() {
                    switch (imageType) {
                        case IMAGE_TYPE_PROFILE:
                            imagePart = getRequestImage();
                            Picasso.with(getContext()).load(fileImage).into(ivCustomerImage);
                            break;
                        case IMAGE_TYPE_KTP:
                            ktpPart.add(getRequestImage());
                            adapterKtp.addItem(fileImage.getPath());
                            if (adapterKtp.getItemCount() == 2) {
                                btnUploadKTP.setVisibility(View.INVISIBLE);
                            }
                            break;
                        case IMAGE_TYPE_SIM:
                            simPart.add(getRequestImage());
                            adapterSim.addItem(fileImage.getPath());
                            if (adapterSim.getItemCount() == 2) {
                                btnUploadSIM.setVisibility(View.INVISIBLE);
                            }
                            break;
                        case IMAGE_TYPE_SKCK:
                            skckPart.add(getRequestImage());
                            adapterSkck.addItem(fileImage.getPath());
                            if (adapterSkck.getItemCount() == 2) {
                                btnUploadSKCK.setVisibility(View.INVISIBLE);
                            }
                            break;
                        case IMAGE_TYPE_STNK:
                            stnkPart.add(getRequestImage());
                            adapterStnk.addItem(fileImage.getPath());
                            if (adapterStnk.getItemCount() == 2) {
                                btnUploadSTNK.setVisibility(View.INVISIBLE);
                            }
                            break;
                        case IMAGE_TYPE_ETC:
                            etcPart.add(getRequestImage());
                            adapterEtc.addItem(fileImage.getPath());
                            break;
                    }
                }
            });
        }
    }


    private void startCrop(Uri uri) {
        if (imageType.equals(IMAGE_TYPE_PROFILE)) {
            UCrop.of(uri, getTarget())
                    .withAspectRatio(1, 1)
                    .withMaxResultSize(CUSTOMER_IMAGE_CROP_WIDTH, CUSTOMER_IMAGE_CROP_HEIGHT)
                    .start(getContext(), EditAccountFragment.this);
        } else {
            UCrop.of(uri, getTarget())
                    .withMaxResultSize(DOCUMENT_IMAGE_CROP_WIDTH, DOCUMENT_IMAGE_CROP_HEIGHT)
                    .start(getContext(), EditAccountFragment.this);
        }

    }

    private Uri getTarget() {
        cameraFileName = CommonUtilities.getTargetImage();
        return Uri.fromFile(new File(cameraFileName));
    }

    @SuppressWarnings("ThrowableResultOfMethodCallIgnored")
    private void handleCropError(@NonNull Intent result) {
        final Throwable cropError = UCrop.getError(result);
        if (cropError != null) {
            Toast.makeText(getContext(), cropError.getMessage(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), "Gagal memotong gambar", Toast.LENGTH_SHORT).show();
        }
    }

    public MultipartBody.Part getRequestImage() {
        if (fileImage != null) {
            return CommonUtilities.prepareFilePartFile(imageType, fileImage);
        }
        return null;
    }

    @Override public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString(CAMERA_FILE_NAME, cameraFileName);
    }

    @Override public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null) {
            cameraFileName = savedInstanceState.getString(CAMERA_FILE_NAME);
        }
    }

    @Nullable @Override public String getTitle() {
        return "Ubah Profil";
    }
}