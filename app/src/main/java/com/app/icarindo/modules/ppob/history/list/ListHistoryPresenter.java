package com.app.icarindo.modules.ppob.history.list;

import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.ppob.RequestHistory;

import java.util.HashMap;

import static com.app.icarindo.utility.Config.CUSTOMER_ID;

public class ListHistoryPresenter extends BaseRxLcePresenter<XListHistoryView, RequestHistory>
        implements XListHistoryPresenter {

    public ListHistoryPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(String filterType, String filterDate, boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "history");
        params.put("filterType", filterType);
        params.put("transDate", filterDate);
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(CUSTOMER_ID)));

        subscribe(App.getService().apiHistoryRequest(params), pullToRefresh);
    }
}