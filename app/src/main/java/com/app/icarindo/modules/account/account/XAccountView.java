package com.app.icarindo.modules.account.account;

import com.hannesdorfmann.mosby3.mvp.MvpView;

public interface XAccountView extends MvpView {

    void setAccount(String image, String name, String id, Integer saldo);
}