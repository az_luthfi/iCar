package com.app.icarindo.modules.splash.login;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.NestedScrollView;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.listeners.AlertListener;
import com.app.icarindo.models.customer.Customer;
import com.app.icarindo.modules.splash.BaseSplashFragment;
import com.app.icarindo.utility.Logs;
import com.app.icarindo.utility.Validation;
import com.facebook.FacebookSdk;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.rengwuxian.materialedittext.MaterialEditText;

import butterknife.BindView;
import butterknife.OnClick;
import cn.pedant.SweetAlert.SweetAlertDialog;

import static com.app.icarindo.utility.Config.MIN_LENGHT_PASSWORD;

public class LoginFragment extends BaseSplashFragment<XLoginView, LoginPresenter>
        implements XLoginView,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, AlertListener {
    private static final String TAG = LoginFragment.class.getSimpleName();
    private static final int RC_SIGN_IN_GOOGLE = 5755;
    @BindView(R.id.etEmail) MaterialEditText etEmail;
    @BindView(R.id.etPassword) MaterialEditText etPassword;
    @BindView(R.id.btnLogin) Button btnLogin;
    @BindView(R.id.tvForgetPassword) TextView tvForgetPassword;
    @BindView(R.id.tvOr) TextView tvOr;
    @BindView(R.id.btnFacebook) Button btnFacebook;
    @BindView(R.id.btnGoogle) Button btnGoogle;
    @BindView(R.id.container) NestedScrollView container;

    private BottomSheetDialog dialog = null;
    private GoogleApiClient mGoogleApiClient;

    public LoginFragment() {

    }

    @Override public LoginPresenter createPresenter() {
        return new LoginPresenter(getContext(), getActivity());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_login;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setAlertListener(this);
    }

    @OnClick({R.id.btnLogin, R.id.tvForgetPassword, R.id.btnFacebook, R.id.btnGoogle, R.id.btnJoin}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btnLogin:
                if (TextUtils.isEmpty(etEmail.getText())){
                    etEmail.setError("Email wajib diisi.");
                    etEmail.requestFocus();
                    return;
                }
                if (!Validation.isValidEmailAddress(etEmail.getText().toString())) {
                    etEmail.setError("Format email tidak valid");
                    etEmail.requestFocus();
                    return;
                }
                if (TextUtils.isEmpty(etPassword.getText())){
                    etPassword.setError("Password wajib diisi");
                    etPassword.requestFocus();
                    return;
                }
                if (etPassword.length() < MIN_LENGHT_PASSWORD) {
                    etPassword.setError("Password minimal " + MIN_LENGHT_PASSWORD);
                    etPassword.requestFocus();
                    return;
                }
                presenter.requestLogin(etEmail.getText().toString(), etPassword.getText().toString(), false);
                break;
            case R.id.tvForgetPassword:
                showForgetPasswordDialog();
                break;
            case R.id.btnFacebook:
                presenter.facebookLogin();
                break;
            case R.id.btnGoogle:
                initGoogleLogin();
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN_GOOGLE);
                break;
            case R.id.btnJoin:
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://icarindo.com/customer"));
                startActivity(browserIntent);
                break;
        }
    }

    private void initGoogleLogin() {
        if (mGoogleApiClient == null) {
            GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                    .requestIdToken(this.getString(R.string.default_web_client_id))
                    .requestEmail()
                    .build();
            mGoogleApiClient = new GoogleApiClient.Builder(getContext())
                    .enableAutoManage(getActivity() /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }
        mGoogleApiClient.registerConnectionCallbacks(this);
        mGoogleApiClient.connect();
    }

    @Override public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RC_SIGN_IN_GOOGLE) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                GoogleSignInAccount acct = result.getSignInAccount();
                if (acct != null){
                    presenter.requestLogin(acct.getEmail(), null, true);
                }
            }
        } else if (FacebookSdk.isFacebookRequestCode(requestCode)) { // splash activity
            presenter.onActivityResultFacebook(requestCode, resultCode, data);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void showForgetPasswordDialog() {
        dialog = new BottomSheetDialog(getContext(), R.style.BottomSheetDialog);
        dialog.setContentView(R.layout.dialog_forgot_password);
        Button button = (Button) dialog.findViewById(R.id.btnReset);
        final TextInputEditText etEmail = (TextInputEditText) dialog.findViewById(R.id.etEmail);
        dialog.show();


        if (button != null) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String emailReset = null;
                    if (etEmail != null) {
                        emailReset = etEmail.getText().toString();
                    }
                    if (!Validation.isValidEmailAddress(emailReset)) {
                        showToast("Format email salah");
                    }else{
                        presenter.resetPassword(emailReset);
                    }
                }
            });
        }
    }

    @Override public void showProgressDialog(boolean show) {
        if (show) {
            showLoading("Loading...", false);
        } else {
            hideLoading();
        }
    }

    @Override public void showError(String text) {
        final Snackbar snackbar = Snackbar.make(container, text, Snackbar.LENGTH_LONG);
        snackbar.setAction("Tutup", new View.OnClickListener() {
            @Override public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.show();
    }

    @Override public void onSuccessAuth(Customer customer) {
        splashListener.OnSuccessAuth(customer);
    }

    @Override public void onSuccessResetPassword(String text) {
        showConfirmDialog(SweetAlertDialog.SUCCESS_TYPE, "Reset Password", text, "OK", null, "reset-password-success", false);
    }

    @Override public void onConnected(@Nullable Bundle bundle) {
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status status) {
                            Logs.d(TAG, "onResult ==> _________SIGNOUT SUCCESS____________");
                        }
                    });
        }
    }

    @Override public void onConnectionSuspended(int i) {

    }

    @Override public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override public void onCancel(String action) {

    }

    @Override public void onSubmit(String action) {
        switch (action){
            case "reset-password-success":
                dialog.cancel();
                break;
        }
    }
}