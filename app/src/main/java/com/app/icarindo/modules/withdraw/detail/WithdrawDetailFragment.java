package com.app.icarindo.modules.withdraw.detail;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseMvpLceFragment;
import com.app.icarindo.models.withdraw.RequestWithdraw;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.GeneralErrorMessage;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import butterknife.BindView;

@FragmentWithArgs
public class WithdrawDetailFragment extends BaseMvpLceFragment<RelativeLayout, RequestWithdraw, XWithdrawDetailView, WithdrawDetailPresenter>
        implements XWithdrawDetailView {

    @Arg String withdrawId;

    @BindView(R.id.tvWithdrawId) TextView tvWithdrawId;
    @BindView(R.id.tvWithdrawAmount) TextView tvWithdrawAmount;
    @BindView(R.id.tvWithdrawStatus) TextView tvWithdrawStatus;
    @BindView(R.id.tvBankTo) TextView tvBankTo;
    @BindView(R.id.tvRekTo) TextView tvRekTo;
    @BindView(R.id.tvBankNameTo) TextView tvBankNameTo;
    @BindView(R.id.tvTitleSender) TextView tvTitleSender;
    @BindView(R.id.tvBankFrom) TextView tvBankFrom;
    @BindView(R.id.tvBankNameFrom) TextView tvBankNameFrom;
    @BindView(R.id.laySender) CardView laySender;
    @BindView(R.id.tvWithdrawCreateDate) TextView tvWithdrawCreateDate;
    @BindView(R.id.layWithdrawCreateDate) LinearLayout layWithdrawCreateDate;
    @BindView(R.id.tvWithdrawProcessDate) TextView tvWithdrawProcessDate;
    @BindView(R.id.layWithdrawProcessDate) LinearLayout layWithdrawProcessDate;
    @BindView(R.id.tvWithdrawFinishDate) TextView tvWithdrawFinishDate;
    @BindView(R.id.layWithdrawFinishDate) LinearLayout layWithdrawFinishDate;
    @BindView(R.id.tvWithdrawCancelDate) TextView tvWithdrawCancelDate;
    @BindView(R.id.layWithdrawCancelDate) LinearLayout layWithdrawCancelDate;
    @BindView(R.id.tvInfoText) TextView tvInfoText;
    @BindView(R.id.dataView) NestedScrollView dataView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.contentView) RelativeLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;

    public WithdrawDetailFragment() {

    }

    @Override public WithdrawDetailPresenter createPresenter() {
        return new WithdrawDetailPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_withdraw_detail;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        return GeneralErrorMessage.get(getContext(), pullToRefresh);
    }

    @Override public void setData(RequestWithdraw data) {
        tvWithdrawId.setText(new StringBuilder("Withdraw Id : ").append(data.getWithdraw().getWithdrawId()));
        tvWithdrawAmount.setText(CommonUtilities.toRupiahNumberFormat(data.getWithdraw().getWithdrawAmount()));
        tvWithdrawStatus.setText(data.getWithdraw().getWithdrawStatus().toUpperCase());

        tvBankTo.setText(data.getWithdraw().getWithdrawBankToAcc());
        tvRekTo.setText(data.getWithdraw().getWithdrawBankToNumber());
        tvBankNameTo.setText(data.getWithdraw().getWithdrawBankToName());

        if (data.getWithdraw().getWithdrawStatus().toLowerCase().equals("finish")){
            tvTitleSender.setVisibility(View.VISIBLE);
            laySender.setVisibility(View.VISIBLE);
            tvBankFrom.setText(data.getWithdraw().getWithdrawBankFromAcc());
            tvBankNameFrom.setText(data.getWithdraw().getWithdrawBankFromName());
        }

        tvWithdrawCreateDate.setText(data.getWithdraw().getWithdrawCreateDate());
        if (!TextUtils.isEmpty(data.getWithdraw().getWithdrawFinishDate())){
            layWithdrawFinishDate.setVisibility(View.VISIBLE);
            tvWithdrawFinishDate.setText(data.getWithdraw().getWithdrawFinishDate());
        }
        if (!TextUtils.isEmpty(data.getWithdraw().getWithdrawCancelDate())){
            layWithdrawCancelDate.setVisibility(View.VISIBLE);
            tvWithdrawCancelDate.setText(data.getWithdraw().getWithdrawCancelDate());
        }

        tvInfoText.setText(data.getWithdraw().getWithdrawDesc());
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(withdrawId, pullToRefresh);
    }
}