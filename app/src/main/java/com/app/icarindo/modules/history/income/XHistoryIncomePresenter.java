package com.app.icarindo.modules.history.income;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XHistoryIncomePresenter extends MvpPresenter<XHistoryIncomeView> {

    void loadData(String filterType, String filterDate, boolean pullToRefresh);

    void loadDataNextPage(String filterType, String filterDate, int page);
}