package com.app.icarindo.modules.ppob.airline;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.DialogFragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseFragment;
import com.app.icarindo.listeners.OnDateChanged;
import com.app.icarindo.models.airline.ApiStationRequest;
import com.app.icarindo.models.airline.Station;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.utility.Logs;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class TicketPesawat extends BaseFragment implements OnClickListener, MainActivity.OnChangeToolbar, OnDateChanged {
    @BindView(R.id.tvAsal)
    TextInputEditText tvAsal;
    @BindView(R.id.tvTujuan)
    TextInputEditText tvTujuan;
    @BindView(R.id.icChange)
    ImageButton icChange;
    @BindView(R.id.tvTglkeberangkatan)
    TextInputEditText tvTglkeberangkatan;
    @BindView(R.id.cbPP)
    CheckBox cbPP;
    @BindView(R.id.tvTglKembali)
    TextInputEditText tvTglKembali;
    @BindView(R.id.backDateContainer)
    LinearLayout backDateContainer;
    @BindView(R.id.countDewasa)
    TextView countDewasa;
    @BindView(R.id.btnMinus1)
    ImageButton btnMinus1;
    @BindView(R.id.btnPlus1)
    ImageButton btnPlus1;
    @BindView(R.id.countAnak)
    TextView countAnak;
    @BindView(R.id.btnMinus2)
    ImageButton btnMinus2;
    @BindView(R.id.btnPlus2)
    ImageButton btnPlus2;
    @BindView(R.id.countBayi)
    TextView countBayi;
    @BindView(R.id.btnMinus3)
    ImageButton btnMinus3;
    @BindView(R.id.btnPlus3)
    ImageButton btnPlus3;
    @BindView(R.id.btnSearchTicket)
    Button btnSearchTicket;
    @BindView(R.id.dewasa)
    LinearLayout dewasa;
    @BindView(R.id.anak)
    LinearLayout anak;
    @BindView(R.id.bayi)
    LinearLayout bayi;
    @BindView(R.id.scrollView9)
    NestedScrollView scrollView9;

    String tanggalBerangkat = null;
    Subscription subscription;

    String codeStasiunAsal = null;
    String codeStasiunTujuan = null;
    ProgressDialog progress;
    Calendar cMinDate;
    String tanggalKembali = null;
    String stasiunAsal;
    String stasiunTujuan;
    List<Station> stations;

    public TicketPesawat() {
        // Required empty public constructor
    }

    public static TicketPesawat newInstance() {
        TicketPesawat fragment = new TicketPesawat();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_ticket_pesawat;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        cbPP.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                Toast.makeText(getContext(), "Fitur ini masih dalam pengembangan", Toast.LENGTH_SHORT).show();
                cbPP.setChecked(false);
                if (isChecked) {
                    if (!backDateContainer.isShown()) {
                        backDateContainer.setVisibility(View.VISIBLE);
                    }
                } else {
                    backDateContainer.setVisibility(View.GONE);
                }
            }
        });

        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        tvTglkeberangkatan.setText(String.valueOf(day + 1) + "-" + (month + 1 ) + "-" + year);

        loadStation();

        setupToolbar();
    }

    private void loadStation() {
        progress = ProgressDialog.show(getContext(), null, "Silahkan tunggu.", false);
        Map<String, String> params = new HashMap<String, String>();
        params.put("act", "getStation");
        App.getService().apiStationRequest(params).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ApiStationRequest>() {

                    @Override
                    public void onError(Throwable e) {
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                        show_notif("Gagal mendapatkan data stasiun", null, "Coba Lagi", "loadStation");
                    }

                    @Override public void onComplete() {

                    }

                    @Override public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ApiStationRequest apiStationRequest) {
                        if (progress.isShowing()) {
                            progress.dismiss();
                        }
                        if (apiStationRequest.getStatus()) {
                            stations = apiStationRequest.getStations();

                            //set default
                            codeStasiunAsal = "DKI";
                            stasiunAsal = "Jakarta (Semua Bandara Jakarta)";
                            tvAsal.setText(codeStasiunAsal + " - " +stasiunAsal );

                            codeStasiunTujuan = "DPS";
                            stasiunTujuan = "Bali / Denpasar";
                            tvTujuan.setText(codeStasiunTujuan + " - " + stasiunTujuan);
                            //end set default
                        } else {
                            show_notif("Tidakdapat menghubungi server. Pastikan Anda terkoneksi dengan Internet", null, "Coba Lagi", "loadStation");
                        }
                    }
                });
    }

    private void show_notif(String message, String btnLabel, String retryLabel, final String callback) {
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
        builder.setMessage(message)
                .setCancelable(false)
                .setNegativeButton(btnLabel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        if (!retryLabel.equalsIgnoreCase("null")) {
            builder.setPositiveButton(retryLabel, new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (callback) {
                        case "loadStation":
                            loadStation();
                            break;
                    }
                    dialog.cancel();
                }
            });
        }
        android.support.v7.app.AlertDialog alert = builder.create();
        alert.show();
    }

    private void showStation(final String type) {
        final ArrayAdapter adapter = new ArrayAdapter<Station>(getContext(), R.layout.spinner_item, stations);

        new AlertDialog.Builder(getActivity()).setTitle("Pilih " + type).setAdapter(adapter, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (type.equalsIgnoreCase("Bandara Asal")) {
                    codeStasiunAsal = stations.get(i).getCode();
                    stasiunAsal = stations.get(i).getName();
                    tvAsal.setText(stations.get(i).toString());
                    Logs.d("TicketPesawat => onClick tvAsal ==> " + stations.get(i).toString());
                } else if (type.equalsIgnoreCase("Bandara Tujuan")) {
                    codeStasiunTujuan = stations.get(i).getCode();
                    stasiunTujuan = stations.get(i).getName();
                    tvTujuan.setText(stations.get(i).toString());
                    Logs.d("TicketPesawat => onClick tvTujuan ==> " + stations.get(i).toString());
                }
            }

        }).create().show();
    }

    @OnClick({R.id.tvTglkeberangkatan, R.id.tvTglKembali, R.id.btnMinus1, R.id.btnMinus2,
            R.id.btnMinus3, R.id.btnPlus1, R.id.btnPlus2, R.id.btnPlus3, R.id.btnSearchTicket,
            R.id.icChange, R.id.tvAsal, R.id.tvTujuan})
    public void onClick(View v) {
        int totalDewasa = Integer.parseInt(countDewasa.getText().toString());
        int totalAnak = Integer.parseInt(countAnak.getText().toString());
        int totalBayi = Integer.parseInt(countBayi.getText().toString());
        switch (v.getId()) {
            case R.id.tvAsal:
                showStation("Bandara Asal");
                break;
            case R.id.tvTujuan:
                showStation("Bandara Tujuan");
                break;
            case R.id.btnPlus1:
                if ((totalDewasa + totalAnak) == 7) {
                    Snackbar.make(scrollView9, "Jumlah Penumpang Dewasa dan Anak maksimal 7", Snackbar.LENGTH_LONG).show();
                } else {
                    btnMinus1.setEnabled(true);
                    add(countDewasa);
                }
                break;
            case R.id.btnPlus2:
                if ((totalDewasa + totalAnak) == 7) {
                    Snackbar.make(scrollView9, "Jumlah Penumpang Dewasa dan Anak maksimal 7", Snackbar.LENGTH_LONG).show();
                } else {
                    add(countAnak);
                }
                break;
            case R.id.btnPlus3:
                if (totalDewasa == totalBayi) {
                    Snackbar.make(scrollView9, "Jumlah Penumpang Bayi tidak boleh melebihi penumpang Dewasa", Snackbar.LENGTH_LONG).show();
                } else {
                    add(countBayi);
                }
                break;
            case R.id.btnMinus1:
                if (totalDewasa == 1) {
                    Snackbar.make(scrollView9, "Jumlah Penumpang Dewasa minimal 1", Snackbar.LENGTH_LONG).show();
                } else if (totalBayi == totalDewasa) {
                    Snackbar.make(scrollView9, "Jumlah Penumpang Bayi tidak boleh melebihi penumpang Dewasa", Snackbar.LENGTH_LONG).show();
                } else {
                    btnMinus1.setEnabled(false);
                    del(countDewasa);
                }
                break;
            case R.id.btnMinus2:
                if (totalAnak != 0) {
                    del(countAnak);
                }
                break;
            case R.id.btnMinus3:
                if (totalBayi != 0) {
                    del(countBayi);
                }
                break;
            case R.id.tvTglkeberangkatan:
                showDatePickerDialog(tvTglkeberangkatan, "tvTglkeberangkatan");
                break;
            case R.id.tvTglKembali:
                showDatePickerDialog(tvTglKembali, "tvTglKembali");
                break;
            case R.id.icChange:
                //set code
                String temp = codeStasiunAsal;
                String temp2 = stasiunAsal;
                //set asal
                codeStasiunAsal = codeStasiunTujuan;
                stasiunAsal = stasiunTujuan;
                //set tujuan
                codeStasiunTujuan = temp;
                stasiunTujuan = temp2;

                // textview
                if (!codeStasiunAsal.isEmpty() && !codeStasiunTujuan.isEmpty()) {
                    temp = tvAsal.getText().toString();
                    tvAsal.setText(tvTujuan.getText());
                    tvTujuan.setText(temp);
                }

                break;
            case R.id.btnSearchTicket:
                tanggalBerangkat = tvTglkeberangkatan.getText().toString();
                tanggalKembali = tvTglKembali.getText().toString();

                boolean flag = true;

                if (codeStasiunAsal == null) {
                    Toast.makeText(getContext(), "Silahkan isi asal keberangkatan", Toast.LENGTH_SHORT).show();
                    flag = false;
                }

                if (codeStasiunTujuan == null) {
                    flag = false;
                    Toast.makeText(getContext(), "Silahkan isi tujuan", Toast.LENGTH_SHORT).show();
                }

                if (tanggalBerangkat == null || tanggalBerangkat.isEmpty()) {
                    flag = false;
                    Toast.makeText(getContext(), "Silahkan isi tanggal keberangkatan", Toast.LENGTH_SHORT).show();
                }
                if ((totalDewasa + totalAnak + totalBayi) == 0) {
                    flag = false;
                    Toast.makeText(getContext(), "Silahkan isi jumlah penumpang", Toast.LENGTH_SHORT).show();

                }

                if (cbPP.isChecked() && (tanggalKembali == null || tanggalKembali.isEmpty())) {
                    flag = false;
                    Toast.makeText(getContext(), "Silahkan isi tanggal kembali", Toast.LENGTH_SHORT).show();
                }


                if (flag) {
                    ArrayList<String> params = new ArrayList<>();
                    params.add(codeStasiunAsal);
                    params.add(codeStasiunTujuan);
                    params.add(tanggalBerangkat);
                    params.add(tanggalKembali);
                    params.add(String.valueOf(totalDewasa));
                    params.add(String.valueOf(totalAnak));
                    params.add(String.valueOf(totalBayi));
                    if (cbPP.isChecked()) {
                        params.add("rt");
                    } else {
                        params.add("ow");
                    }

                    Toast.makeText(getContext(), "Maaf fitur ini masih dalam pengembangan.", Toast.LENGTH_SHORT).show();

//                    mListener.onMenuSelected("Depart Flight", SearchTicketFragment.newInstance(stasiunAsal + " - " + stasiunTujuan, tanggalBerangkat, params, null, null), false);
                }
                break;
        }
    }

    private void add(TextView tv) {
        int last = Integer.parseInt(tv.getText().toString());
        tv.setText(String.valueOf(last + 1));
    }

    private void del(TextView tv) {
        int last = Integer.parseInt(tv.getText().toString());
        if (last != 0) {
            tv.setText(String.valueOf(last - 1));
        }
    }

    public void showDatePickerDialog(EditText v, String tag) {
        DatePickerFragment newFragment;
        if (tag.equalsIgnoreCase("tvTglKembali")) {
            newFragment = new DatePickerFragment(cMinDate, this);
        } else {
            newFragment = new DatePickerFragment(null, this);

        }
        newFragment.show(getChildFragmentManager(), tag);


    }

    @Override
    public void onDateChanged(DatePicker view, int year, int month, int day, String tag) {
        String date = String.valueOf(day) + "-" + String.valueOf(month + 1) + "-" + String.valueOf(year);

        if (tag.equalsIgnoreCase("tvTglKembali")) {
            tvTglKembali.setText(date);

        } else {
            tvTglKembali.setText("");
            tvTglkeberangkatan.setText(date);
            cMinDate = Calendar.getInstance();
            cMinDate.set(Calendar.YEAR, year);
            cMinDate.set(Calendar.MONTH, month);
            cMinDate.set(Calendar.DAY_OF_MONTH, day);
        }
    }
    @SuppressLint("ValidFragment")
    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {
        private Calendar minDate = null;
        private DatePickerDialog datePicker;
        OnDateChanged listener;

        public DatePickerFragment(Calendar cMinDate, OnDateChanged l) {
            this.minDate = cMinDate;
            this.listener = l;
        }


        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            datePicker = new DatePickerDialog(getActivity(), this, year, month, day);
            if (minDate != null) {
                datePicker.getDatePicker().setMinDate(minDate.getTimeInMillis());
            } else {
                datePicker.getDatePicker().setMinDate(new Date().getTime());
            }
            return datePicker;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            // Do something with the date chosen by the user
            listener.onDateChanged(view, year, month, day, getTag());
        }

    }

    private void setupToolbar() {
//        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
//        ActionBar actionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setDisplayShowTitleEnabled(false);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        switch (id) {
//            case android.R.id.home:
//                Logs.d("Pembelian => onOptionsItemSelected ==> ");
//                mListener.back();
//                break;
//        }
//        return true;
//    }

    @Nullable @Override public String getTitle() {
        return "Tiket Pesawat";
    }
}
