package com.app.icarindo.modules.ppob.history.detail.deposit;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseLceRefreshFragment;
import com.app.icarindo.models.ppob.RequestHistory;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.utility.CommonUtilities;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import butterknife.BindView;

@FragmentWithArgs
public class DetailDepositFragment extends BaseLceRefreshFragment<SwipeRefreshLayout, RequestHistory, XDetailDepositView, DetailDepositPresenter>
        implements XDetailDepositView, MainActivity.OnChangeToolbar {

    @Arg String depositId;
    @BindView(R.id.tvDepositId) TextView tvDepositId;
    @BindView(R.id.tvDepositAmount) TextView tvDepositAmount;
    @BindView(R.id.tvDepositStatus) TextView tvDepositStatus;
    @BindView(R.id.tvDepositDate) TextView tvDepositDate;
    @BindView(R.id.ivIcon) ImageView ivIcon;
    @BindView(R.id.tvBankTo) TextView tvBankTo;
    @BindView(R.id.tvRekTo) TextView tvRekTo;
    @BindView(R.id.tvBankNameTo) TextView tvBankNameTo;
    @BindView(R.id.tvBankFrom) TextView tvBankFrom;
    @BindView(R.id.tvBankNameFrom) TextView tvBankNameFrom;
    @BindView(R.id.tvInfoText) TextView tvInfoText;
    @BindView(R.id.contentView) SwipeRefreshLayout contentView;
    @BindView(R.id.errorView) TextView errorView;
    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.emptyView) TextView emptyView;
    @BindView(R.id.dataView) NestedScrollView dataView;

    public DetailDepositFragment() {

    }

    @Override public DetailDepositPresenter createPresenter() {
        return new DetailDepositPresenter(getContext());
    }

    @Override protected int getLayoutRes() {
        return R.layout.fragment_detail_deposit;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData(false);
    }

    @Override protected String getErrorMessage(Throwable e, boolean pullToRefresh) {
        if (pullToRefresh) {
            return getContext().getString(R.string.error_network_light);
        }
        return getContext().getString(R.string.error_network);
    }

    @Override public void setData(RequestHistory data) {
        if (data.getStatus()) {
            dataView.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            tvDepositId.setText(new StringBuilder("Deposit ID : ").append(data.getDeposit().getDepositAmount()));
            tvDepositDate.setText(data.getDeposit().getDepositCreateDate());
            tvDepositAmount.setText(CommonUtilities.toRupiahFormat(data.getDeposit().getDepositAmount()));
            tvBankTo.setText(data.getDeposit().getDepositBankTo().toUpperCase());
            tvRekTo.setText(data.getDeposit().getDepositBankToNumber());
            tvBankNameTo.setText(data.getDeposit().getDepositBankToAcc());
            tvBankFrom.setText(data.getDeposit().getDepositBankFrom());
            tvBankNameFrom.setText(data.getDeposit().getDepositBankAcc());
            tvInfoText.setText(data.getDeposit().getDepositUpdateText());
            if (!TextUtils.isEmpty(data.getDeposit().getBank().getBankImage())){
                Picasso.with(getContext()).load(data.getDeposit().getBank().getBankImage()).into(ivIcon, new Callback() {
                    @Override public void onSuccess() {
                        ivIcon.setVisibility(View.VISIBLE);
                    }

                    @Override public void onError() {

                    }
                });
            }
            switch (data.getDeposit().getDepositStatus().toLowerCase()){
                case "pending":
                    tvDepositStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.status_pending));
                    break;
                case "cancel":
                    tvDepositStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.status_cancel));
                    break;
                case "new":
                    tvDepositStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.status_new));
                    break;
                default:
                    tvDepositStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.status_success));
                    break;
            }
        } else {
            dataView.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            emptyView.setText(data.getText());
        }
    }

    @Override public void loadData(boolean pullToRefresh) {
        presenter.loadData(pullToRefresh, depositId);
    }

    @Nullable @Override public String getTitle() {
        return "Detail Deposit";
    }
}