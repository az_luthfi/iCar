package com.app.icarindo.modules.account.edit;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface XEditAccountPresenter extends MvpPresenter<XEditAccountView> {

    void loadData(boolean pullToRefresh);

    void loadProvince();

    void requestPermissionGallery();

    void requestUpdateProfile(HashMap<String, RequestBody> params, MultipartBody.Part requestImage, List<MultipartBody.Part> skckPart, List<MultipartBody.Part> ktpPart, List<MultipartBody.Part> simPart, List<MultipartBody.Part> stnkPart, List<MultipartBody.Part> etcPart);
}