package com.app.icarindo.modules.withdraw.detail;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XWithdrawDetailPresenter extends MvpPresenter<XWithdrawDetailView> {

    void loadData(String withdrawId, boolean pullToRefresh);
}