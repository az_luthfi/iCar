package com.app.icarindo.modules.history;


import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseFragment;
import com.app.icarindo.modules.history.deposit.HistoryDepositFragment;
import com.app.icarindo.modules.history.income.HistoryIncomeFragment;
import com.app.icarindo.modules.history.product.HistoryProductFragment;
import com.app.icarindo.modules.history.register.HistoryRegisterFragment;
import com.app.icarindo.modules.history.transaction.HistoryTransactionFragment;
import com.app.icarindo.modules.history.withdraw.HistoryWithdrawFragment;
import com.app.icarindo.modules.home.HomeFragment;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Config;
import com.app.icarindo.utility.FragmentStack;
import com.app.icarindo.utility.Preferences;
import com.app.icarindo.utility.SmartFragmentStatePagerAdapter;
import com.rackspira.kristiawan.rackmonthpicker.RackMonthPicker;
import com.rackspira.kristiawan.rackmonthpicker.listener.DateMonthDialogListener;
import com.rackspira.kristiawan.rackmonthpicker.listener.OnCancelMonthDialogListener;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
public class HistoryFragment extends BaseFragment implements FragmentStack.OnBackPressedHandlingFragment{

    @BindView(R.id.layDatePrev) FrameLayout layDatePrev;
    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.layDateNext) FrameLayout layDateNext;
    @BindView(R.id.tvSaldo) TextView tvSaldo;
    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;

    private HistoryViewPager pagerAdapter;
    private Calendar calendar;
    private String filterDate;
    private SimpleDateFormat simpleDateFormat;
    private RackMonthPicker rackMonthPicker;

    public HistoryFragment() {
        // Required empty public constructor
    }


    @Override protected int getLayoutRes() {
        return R.layout.fragment_history;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Preferences prefs = new Preferences(getContext());
        tvSaldo.setText(CommonUtilities.toRupiahNumberFormat(prefs.getPreferencesInt(Config.CUSTOMER_SALDO)));
        calendar = Calendar.getInstance();
        simpleDateFormat =  new SimpleDateFormat("MMM yyyy");
        rackMonthPicker = new RackMonthPicker(getContext())
                .setLocale(Locale.ENGLISH)
                .setYear(calendar.get(Calendar.YEAR))
                .setSelectedMonth(calendar.get(Calendar.MONTH))
                .setColorTheme(R.color.colorPrimary)
                .setPositiveButton(new DateMonthDialogListener() {
                    @Override
                    public void onDateMonth(int month, int startDate, int endDate, int year) {
                        calendar.set(Calendar.YEAR, year);
                        calendar.set(Calendar.MONTH, month - 1);
                        updateFilterDate();
                    }
                })
                .setNegativeButton(new OnCancelMonthDialogListener() {
                    @Override
                    public void onCancel(AlertDialog dialog) {
                        dialog.dismiss();
                    }
                });


        pagerAdapter = new HistoryViewPager(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(0);
        viewPager.setOffscreenPageLimit(5);
        tabLayout.setupWithViewPager(viewPager);

        new Handler().postDelayed(new Runnable() {
            @Override public void run() {
                updateFilterDate();
            }
        }, 100);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override public void onPageSelected(int position) {
                Fragment currentFragment = pagerAdapter.getRegisteredFragment(position);
                if (currentFragment instanceof HistoryIncomeFragment){
                    ((HistoryIncomeFragment) currentFragment).updateData();
                }else if (currentFragment instanceof HistoryWithdrawFragment){
                    ((HistoryWithdrawFragment) currentFragment).updateData();
                }else if (currentFragment instanceof HistoryTransactionFragment){
                    ((HistoryTransactionFragment) currentFragment).updateData();
                }else if (currentFragment instanceof HistoryRegisterFragment){
                    ((HistoryRegisterFragment) currentFragment).updateData();
                }else if (currentFragment instanceof HistoryDepositFragment){
                    ((HistoryDepositFragment) currentFragment).updateData();
                }else if (currentFragment instanceof HistoryProductFragment){
                    ((HistoryProductFragment) currentFragment).updateData();
                }
            }

            @Override public void onPageScrollStateChanged(int state) {

            }
        });

    }

    @Override public boolean onBackPressed() {
        mListener.gotoPage(new HomeFragment(), false, null);
        return true;
    }

    private class HistoryViewPager extends SmartFragmentStatePagerAdapter {
        private String tabTitles[] =  new String[]{"Income", "Withdraw", "Deposit", "E-Transaksi", "Register", "Produk"};

        public HistoryViewPager(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override public Fragment getItem(int position) {
            switch (position) {
                case 1:
                    return new HistoryWithdrawFragment();
                case 2:
                    return new HistoryDepositFragment();
                case 3:
                    return new HistoryTransactionFragment();
                case 4:
                    return new HistoryRegisterFragment();
                case 5:
                    return new HistoryProductFragment();
                default:
                    return new HistoryIncomeFragment();
            }
        }

        @Override public int getCount() {
            return tabTitles.length;
        }

        @Override public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }

    @OnClick({R.id.layDatePrev, R.id.tvDate, R.id.layDateNext}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layDatePrev:
                calendar.add(Calendar.MONTH, -1);
                updateFilterDate();
                break;
            case R.id.tvDate:
                rackMonthPicker.setYear(calendar.get(Calendar.YEAR));
                rackMonthPicker.setSelectedMonth(calendar.get(Calendar.MONTH));
                rackMonthPicker.show();
                break;
            case R.id.layDateNext:
                calendar.add(Calendar.MONTH, 1);
                updateFilterDate();
                break;
        }
    }

    private void updateFilterDate(){
        filterDate = simpleDateFormat.format(calendar.getTime());
        tvDate.setText(filterDate);
        int position = viewPager.getCurrentItem();
        Fragment currentFragment = pagerAdapter.getRegisteredFragment(position);
        if (currentFragment instanceof HistoryIncomeFragment){
            ((HistoryIncomeFragment) currentFragment).updateData();
        }else if (currentFragment instanceof HistoryWithdrawFragment){
            ((HistoryWithdrawFragment) currentFragment).updateData();
        }else if (currentFragment instanceof HistoryTransactionFragment){
            ((HistoryTransactionFragment) currentFragment).updateData();
        }else if (currentFragment instanceof HistoryRegisterFragment){
            ((HistoryRegisterFragment) currentFragment).updateData();
        }else if (currentFragment instanceof HistoryDepositFragment){
            ((HistoryDepositFragment) currentFragment).updateData();
        }else if (currentFragment instanceof HistoryProductFragment){
            ((HistoryProductFragment) currentFragment).updateData();
        }
    }

    public String getFilterDate() {
        return filterDate;
    }
}
