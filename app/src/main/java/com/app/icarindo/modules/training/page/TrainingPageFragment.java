package com.app.icarindo.modules.training.page;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.webkit.WebView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseFragment;
import com.app.icarindo.models.training.Soal;
import com.app.icarindo.modules.training.parent.TrainingFragment;
import com.hannesdorfmann.fragmentargs.annotation.Arg;
import com.hannesdorfmann.fragmentargs.annotation.FragmentWithArgs;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * A simple {@link Fragment} subclass.
 */
@FragmentWithArgs
public class TrainingPageFragment extends BaseFragment {

    @Arg Soal soal;
    @Arg int position;
    @BindView(R.id.webView) WebView webView;
    @BindView(R.id.buttonA) RadioButton buttonA;
    @BindView(R.id.buttonB) RadioButton buttonB;
    @BindView(R.id.buttonC) RadioButton buttonC;
    @BindView(R.id.buttonD) RadioButton buttonD;
    @BindView(R.id.buttonE) RadioButton buttonE;
    @BindView(R.id.radio_answers) RadioGroup radioAnswers;

    public TrainingPageFragment() {
        // Required empty public constructor
    }


    @Override protected int getLayoutRes() {
        return R.layout.fragment_training_page;
    }

    @Override public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        buttonC.setVisibility(View.GONE);
        buttonD.setVisibility(View.GONE);
        buttonE.setVisibility(View.GONE);
        webView.loadData("</br></br>" + soal.getStPertanyaan() + "</br></br></br>", "text/html", "utf-8");
    }

    @OnClick({R.id.buttonA, R.id.buttonB, R.id.buttonC, R.id.buttonD, R.id.buttonE}) public void onViewClicked(View view) {
        boolean checked = ((RadioButton) view).isChecked();
        if (checked){
            switch (view.getId()) {
                case R.id.buttonA:
                    ((TrainingFragment) getParentFragment()).setAnswer(position, "A");
                    break;
                case R.id.buttonB:
                    ((TrainingFragment) getParentFragment()).setAnswer(position, "B");
                    break;
                case R.id.buttonC:
                    ((TrainingFragment) getParentFragment()).setAnswer(position, "C");
                    break;
                case R.id.buttonD:
                    ((TrainingFragment) getParentFragment()).setAnswer(position, "D");
                    break;
                case R.id.buttonE:
                    ((TrainingFragment) getParentFragment()).setAnswer(position, "E");
                    break;
            }
        }

    }
}
