package com.app.icarindo.modules.splash.login;

import android.content.Intent;
import android.support.annotation.Nullable;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XLoginPresenter extends MvpPresenter<XLoginView> {

    void resetPassword(String emailReset);

    void requestLogin(String email, @Nullable String password, boolean isSocial);

    void onActivityResultFacebook(int requestCode, int resultCode, Intent data);

    void facebookLogin();
}