package com.app.icarindo.modules.store.product.detail;

import android.content.Context;

import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.store.RequestProduct;
import com.app.icarindo.utility.Config;

import java.util.HashMap;

public class ProductDetailPresenter extends BaseRxLcePresenter<XProductDetailView, RequestProduct>
        implements XProductDetailPresenter {

    public ProductDetailPresenter(Context context) {
        super(context);
    }

    @Override public void loadData(String productId, boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("productId",productId);
        params.put("act", "productDetail");

        subscribe(App.getService().apiProduk(params), pullToRefresh);
    }
}