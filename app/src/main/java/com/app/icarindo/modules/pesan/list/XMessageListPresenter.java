package com.app.icarindo.modules.pesan.list;

import com.hannesdorfmann.mosby3.mvp.MvpPresenter;

public interface XMessageListPresenter extends MvpPresenter<XMessageListView> {

    void loadListPesan(boolean pullToRefresh);

    void loadDataNextPage(int page);
}