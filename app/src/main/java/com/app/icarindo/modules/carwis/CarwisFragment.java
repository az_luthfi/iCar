package com.app.icarindo.modules.carwis;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.app.icarindo.R;
import com.app.icarindo.base.BaseFragment;
import com.app.icarindo.modules.carwis.list.ListCarwisFragmentBuilder;
import com.app.icarindo.modules.main.MainActivity;
import com.app.icarindo.utility.SmartFragmentStatePagerAdapter;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 */
public class CarwisFragment extends BaseFragment implements MainActivity.OnChangeToolbar {


    @BindView(R.id.tabLayout) TabLayout tabLayout;
    @BindView(R.id.viewPager) ViewPager viewPager;

    public CarwisFragment() {
        // Required empty public constructor
    }


    @Override protected int getLayoutRes() {
        return R.layout.fragment_with_tab_layout;
    }

    @Override public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CarwisViewPager pagerAdapter = new CarwisViewPager(getChildFragmentManager());
        viewPager.setAdapter(pagerAdapter);
        viewPager.setCurrentItem(0);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Nullable @Override public String getTitle() {
        return "Carwis";
    }

    private class CarwisViewPager extends SmartFragmentStatePagerAdapter {
        private String tabTitles[] = new String[]{"UPCOMING", "FINISH"};

        public CarwisViewPager(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override public Fragment getItem(int position) {
            switch (position) {
                case 1:
                    return new ListCarwisFragmentBuilder("finish").build();
                default:
                    return new ListCarwisFragmentBuilder("upcoming").build();
            }
        }

        @Override public int getCount() {
            return tabTitles.length;
        }

        @Override public CharSequence getPageTitle(int position) {
            return tabTitles[position];
        }
    }
}
