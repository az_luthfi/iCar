package com.app.icarindo.modules.store.product.list;

import com.app.icarindo.models.store.RequestProduct;
import com.hannesdorfmann.mosby3.mvp.lce.MvpLceView;

public interface XProductListView extends MvpLceView<RequestProduct> {

}