package com.app.icarindo.modules.absensi.ijin;

import android.content.Context;

import com.app.icarindo.R;
import com.app.icarindo.app.App;
import com.app.icarindo.base.BaseRxLcePresenter;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.utility.Config;

import java.util.HashMap;

import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

public class CutiPresenter extends BaseRxLcePresenter<XCutiView, RequestNetwork>
        implements XCutiPresenter {

    public CutiPresenter(Context context) {
        super(context);
    }

    @Override public void loadDataCuti(boolean pullToRefresh) {
        HashMap<String, String> params = new HashMap<>();
        params.put("act", "absensi-izin");
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));

        subscribe(App.getService().apiNetwork(params), pullToRefresh);
    }

    @Override public void requestCuti(String startDate, String endDate, String etDesc) {
        if (isViewAttached()) {
            getView().showProgressDialog(true);
        }
        HashMap<String, String> params = new HashMap<>();
        params.put("startDate", startDate);
        params.put("endDate", endDate);
        params.put("desc", etDesc);
        params.put("customerId", String.valueOf(prefs.getPreferencesInt(Config.CUSTOMER_ID)));
        params.put("act", "input-cuti");
        Observer<RequestNetwork> observer = new Observer<RequestNetwork>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestNetwork data) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().onNextRequesCuti(data);
                }
            }

            @Override public void onError(Throwable e) {
                if (isViewAttached()) {
                    getView().showProgressDialog(false);
                    getView().showError(context.getString(R.string.error_network_light));
                }
            }

            @Override public void onComplete() {

            }
        };

        App.getService().apiNetwork(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }
}