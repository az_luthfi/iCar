package com.app.icarindo.apis;

import com.app.icarindo.models.RequestBase;
import com.app.icarindo.models.RequestHome;
import com.app.icarindo.models.airline.ApiStationRequest;
import com.app.icarindo.models.carwis.RequestCarwis;
import com.app.icarindo.models.content.RequestContent;
import com.app.icarindo.models.content.RequestContentCategory;
import com.app.icarindo.models.customer.RequestCustomer;
import com.app.icarindo.models.datalist.Province;
import com.app.icarindo.models.network.RequestNetwork;
import com.app.icarindo.models.pesan.RequestMessage;
import com.app.icarindo.models.ppob.RequestDeposit;
import com.app.icarindo.models.ppob.RequestHistory;
import com.app.icarindo.models.ppob.RequestInquiry;
import com.app.icarindo.models.ppob.RequestPay;
import com.app.icarindo.models.ppob.RequestPayment;
import com.app.icarindo.models.ppob.RequestProduct;
import com.app.icarindo.models.ppob.RequestProductCategory;
import com.app.icarindo.models.store.City;
import com.app.icarindo.models.store.RequestOrder;
import com.app.icarindo.models.store.RequestShipping;
import com.app.icarindo.models.training.RequestTraining;
import com.app.icarindo.models.withdraw.RequestWithdraw;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Response;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Streaming;

/**
 * Created by j3p0n on 3/21/2017.
 */

public interface ApiService {
    @FormUrlEncoded
    @POST("APIuser.php")
    Observable<RequestCustomer> apiCustomer(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("APIbisnismu.php")
    Observable<RequestProductCategory> apiPPOBProductCategory(@FieldMap Map<String, String> params);

    @POST("APIbisnismu.php")
    @FormUrlEncoded
    Observable<RequestProduct> apiPPOBProduct(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("APIrequest.php")
    Observable<RequestPayment> apiPurchaseRequest(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("APIinquiry.php")
    Observable<RequestInquiry> apiInquiryRequest(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("APIpayment.php")
    Observable<RequestPay> apiPaymentRequest(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("APIuser.php")
    Observable<RequestDeposit> apiDepositRequest(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("APIstruk.php")
    Observable<RequestBase> apiStruckRequest(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("APIbisnismu.php")
    Observable<RequestHistory> apiHistoryRequest(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("APIpesan.php")
    Observable<RequestMessage> apiMessageRequest(@FieldMap Map<String, String> params);

    @Multipart
    @POST("APIpesan.php")
    Observable<RequestMessage> sendMessage(
            @PartMap Map<String, RequestBody> params,
            @Part MultipartBody.Part image
    );

    @Multipart
    @POST("APIpesan.php")
    Observable<RequestMessage> sendConversation(
            @PartMap Map<String, RequestBody> params,
            @Part MultipartBody.Part image
    );

    @Multipart
    @POST("APIuser.php")
    Observable<RequestCustomer> apiRegisterMember(
            @PartMap Map<String, RequestBody> params,
            @Part MultipartBody.Part image,
            @Part List<MultipartBody.Part> ktp,
            @Part List<MultipartBody.Part> sim,
            @Part List<MultipartBody.Part> skck,
            @Part List<MultipartBody.Part> stnk,
            @Part List<MultipartBody.Part> etc
    );

    @FormUrlEncoded
    @POST("APIcontent.php")
    Observable<RequestHome> apiHomeRequest(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("APIcontent.php")
    Observable<RequestContent> apiContentRequest(@FieldMap HashMap<String, String> params);

    @FormUrlEncoded
    @POST("APIcontent.php")
    Observable<RequestContentCategory> apiContentCategoryRequest(@FieldMap Map<String, String> params);

    @FormUrlEncoded
    @POST("APIairline.php")
    Observable<ApiStationRequest> apiStationRequest(
            @FieldMap Map<String, String> options
    );

    @FormUrlEncoded
    @POST("APInetwork.php")
    Observable<RequestNetwork> apiNetwork(
            @FieldMap Map<String, String> options
    );

    @FormUrlEncoded
    @POST("APItraining.php")
    Observable<RequestTraining> apiTraining(
            @FieldMap Map<String, String> options
    );

    @FormUrlEncoded
    @POST("APIwithdraw.php")
    Observable<RequestWithdraw> apiWithdraw(
            @FieldMap Map<String, String> options
    );

    @FormUrlEncoded
    @POST("APItracking.php")
    Observable<RequestBase> apiTracking(
            @FieldMap Map<String, String> options
    );

    @FormUrlEncoded
    @POST("APIproduk.php")
    Observable<com.app.icarindo.models.store.RequestProduct> apiProduk(
            @FieldMap Map<String, String> options
    );

    @FormUrlEncoded
    @POST("APIshipping.php")
    Observable<RequestShipping> apiShipping(
            @FieldMap Map<String, String> options
    );

    @GET("{url}")
    @Streaming
    Observable<Response<ResponseBody>> downloadFile(
            @Path(value = "url", encoded = true) String url);

    @FormUrlEncoded
    @POST("APIshipping.php")
    Observable<ArrayList<Province>> apiShippingProvince(
            @FieldMap Map<String, String> options
    );

    @FormUrlEncoded
    @POST("APIshipping.php")
    Observable<ArrayList<City>> apiShippingCity(
            @FieldMap Map<String, String> options
    );

    @FormUrlEncoded
    @POST("APIorder.php")
    Observable<RequestOrder> apiOrder(
            @FieldMap Map<String, String> options
    );

    @FormUrlEncoded
    @POST("APIcarwis.php")
    Observable<RequestCarwis> apiCarwis(
            @FieldMap Map<String, String> options
    );
}
