package com.app.icarindo.listeners;

/**
 * Created by j3p0n on 3/21/2017.
 */

public interface AlertListener {
    public void onCancel(String action);
    public void onSubmit(String action);
}
