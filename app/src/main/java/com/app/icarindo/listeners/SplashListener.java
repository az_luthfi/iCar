package com.app.icarindo.listeners;

import com.app.icarindo.models.customer.Customer;

/**
 * Created by Luthfi on 02/08/2017.
 */

public interface SplashListener {
    void OnSuccessAuth(Customer customer);
}
