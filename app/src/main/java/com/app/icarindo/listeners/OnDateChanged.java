package com.app.icarindo.listeners;

import android.widget.DatePicker;

/**
 * Created by j3p0n on 12/9/2016.
 */

public interface OnDateChanged {
    void onDateChanged(DatePicker view, int year, int month, int day, String tag);
}
