package com.app.icarindo.holder.ppob;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.ppob.History;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemHistoryHolder extends RecyclerView.ViewHolder {

//    @BindView(R.id.tvTypeLabel) TextView tvTypeLabel;
    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.tvProductName) TextView tvProductName;
    @BindView(R.id.tvStatus) TextView tvStatus;
    @BindView(R.id.tvIdPel) TextView tvIdPel;
    @BindView(R.id.tvPrice) TextView tvPrice;

    public ItemHistoryHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_history, parent, false));
    }

    public ItemHistoryHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(History item) {
        tvDate.setText(item.getHistoryDate());
        tvStatus.setText(item.getHistoryStatus().toUpperCase());
        tvPrice.setText(new StringBuilder(item.getHistoryAmount()));
        tvProductName.setText(item.getHistoryProduct());
//        tvTypeLabel.setText(item.getHistoryType());
//        if (item.getHistoryType().toLowerCase().equals("deposit")){
//            tvIdPel.setVisibility(View.GONE);
//        }else{
            tvIdPel.setVisibility(View.VISIBLE);
            tvIdPel.setText(new StringBuilder("Id Pel : ").append(item.getHistoryPel()));
//        }
        setupStatusColor(item.getHistoryStatus());
    }

    private void setupStatusColor(String status) {
        switch (status) {
            case "success":
                tvStatus.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.status_success));
                break;
            case "cancel":
                tvStatus.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.status_cancel));
                break;
            case "failed":
                tvStatus.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.status_cancel));
                break;
            case "pending":
                tvStatus.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.status_pending));
                break;
            case "new":
                tvStatus.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.status_new));
                break;
            default:
                tvStatus.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.status_cancel));
                break;
        }
    }
}
