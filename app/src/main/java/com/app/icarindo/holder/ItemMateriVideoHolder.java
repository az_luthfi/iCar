package com.app.icarindo.holder;

import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.Media;
import com.app.icarindo.modules.youtube.YoutubeActivity;
import com.app.icarindo.utility.Str;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemMateriVideoHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvTitle) TextView tvTitle;
    @BindView(R.id.layContainer) LinearLayout layContainer;

    public ItemMateriVideoHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_materi_video, parent, false));
    }

    public ItemMateriVideoHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(final Media media){
        tvTitle.setText(Str.getText(media.getMediaName(), ""));
        layContainer.setOnClickListener(new View.OnClickListener() {
            @Override public void onClick(View v) {
                Intent i = new Intent(itemView.getContext(), YoutubeActivity.class);
                i.putExtra("url", media.getMediaValue());
                i.putExtra("title", media.getMediaName());
                itemView.getContext().startActivity(i);
            }
        });
    }
}
