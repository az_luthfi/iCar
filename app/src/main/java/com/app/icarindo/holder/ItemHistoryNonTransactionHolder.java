package com.app.icarindo.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.ItemHistory;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemHistoryNonTransactionHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvType) TextView tvType;
    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.tvNominal) TextView tvNominal;

    public ItemHistoryNonTransactionHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_history_non_transaction, parent, false));
    }

    public ItemHistoryNonTransactionHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(ItemHistory model) {
        tvType.setText(model.getHistoryType());
        switch (model.getHistoryMethod()) {
            case "deposit":
                switch (model.getHistoryType().toLowerCase()) {
                    case "new":
                        tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_blue_500));
                        break;
                    case "pending":
                        tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_orange_500));
                        break;
                    case "success":
                        tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_green_500));
                        break;
                    case "cancel":
                        tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_red_500));
                        break;
                }
                break;
            case "withdraw":
                switch (model.getHistoryType().toLowerCase()) {
                    case "process":
                        tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_orange_500));
                        break;
                    case "finish":
                        tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_green_500));
                        break;
                    case "cancel":
                        tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_red_500));
                        break;
                }
                break;
            case "register":
                tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_grey_600));
                break;
            case "trans_cashback":
            case "order_cashback":
            case "income":
                tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_grey_600));
                break;
            case "order":
                switch (model.getHistoryType().toLowerCase()) {
                    case "paid":
                        tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_blue_500));
                        break;
                    case "processing":
                        tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_orange_500));
                        break;
                    case "shipped":
                        tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_purple_500));
                        break;
                    case "finished":
                        tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_green_500));
                        break;
                    case "canceled":
                        tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_red_500));
                        break;
                }
            case "carwis":
                tvType.setTextColor(itemView.getContext().getResources().getColor(R.color.md_grey_600));
                break;
        }

        tvName.setText(model.getHistoryName());
        tvDate.setText(model.getHistoryDate());
        tvNominal.setText(model.getHistoryNominal());
    }
}
