package com.app.icarindo.holder.message;

import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.pesan.Conversation;
import com.app.icarindo.modules.common.FragmentPhotoViewSingle;
import com.app.icarindo.modules.common.FragmentPhotoViewSingleBuilder;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemConversationMeHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivImage) ImageView ivImage;
    @BindView(R.id.tvChatText) TextView tvChatText;
    @BindView(R.id.rlMsgContainer) RelativeLayout rlMsgContainer;
    @BindView(R.id.tvTime) TextView tvTime;

    private Conversation item;

    public ItemConversationMeHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_conversation_me, parent, false));
    }

    public ItemConversationMeHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(Conversation item) {
        this.item = item;
        tvTime.setText(item.getDate());
        tvChatText.setText(item.getIsi());

        if (!TextUtils.isEmpty(item.getImg())) {
            Picasso.with(itemView.getContext()).load(item.getImg()).resize(400, 0).into(ivImage);
            ivImage.setVisibility(View.VISIBLE);
        } else {
            ivImage.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.ivImage) public void onImageClicked() {
        FragmentPhotoViewSingle fragmentPhotoView = new FragmentPhotoViewSingleBuilder(item.getImg()).build();
        fragmentPhotoView.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
        fragmentPhotoView.show(((AppCompatActivity) itemView.getContext()).getSupportFragmentManager(), "photo");
    }
}
