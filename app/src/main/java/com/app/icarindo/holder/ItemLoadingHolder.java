/*
 * Copyright 2016 Hannes Dorfmann.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.app.icarindo.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.app.icarindo.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


/**
 * Simply shows a progress bar. This is typically used for pagination to indicate  that more items
 * are loading.
 *
 * @author Hannes Dorfmann
 */
public class ItemLoadingHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.loadingView) ProgressBar loadingView;
    @BindView(R.id.ivRefresh) ImageView ivRefresh;
    @BindView(R.id.tvErrorMsg) TextView tvErrorMsg;
    @BindView(R.id.errorView) RelativeLayout errorView;

    private ItemViewLoadingListener listener;

    public ItemLoadingHolder(LayoutInflater inflater, ViewGroup parent, ItemViewLoadingListener listener) {
        this(inflater.inflate(R.layout.item_loading, parent, false));
        this.listener = listener;
    }

    private ItemLoadingHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void setErrorNextPage(String msgErrorNextPage) {
        loadingView.setVisibility(View.GONE);
        errorView.setVisibility(View.VISIBLE);
        tvErrorMsg.setText(msgErrorNextPage);
    }

    public void setLoading() {
        loadingView.setVisibility(View.VISIBLE);
        errorView.setVisibility(View.GONE);
    }

    @OnClick(R.id.errorView) public void onViewClicked() {
        listener.onRetryNextPage();
    }

    public interface ItemViewLoadingListener {
        public void onRetryNextPage();
    }
}
