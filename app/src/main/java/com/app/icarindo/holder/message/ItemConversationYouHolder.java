package com.app.icarindo.holder.message;

import android.content.Context;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.icarindo.R;
import com.app.icarindo.models.pesan.Conversation;
import com.app.icarindo.modules.common.FragmentPhotoViewSingle;
import com.app.icarindo.modules.common.FragmentPhotoViewSingleBuilder;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemConversationYouHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivAvatar) ImageView ivAvatar;
    @BindView(R.id.ivImage) ImageView ivImage;
    @BindView(R.id.tvChatText) TextView tvChatText;
    @BindView(R.id.rlMsgContainer) RelativeLayout rlMsgContainer;
    @BindView(R.id.tvTime) TextView tvTime;

    private Conversation item;
    private final TextDrawable.IShapeBuilder builder;
    private Context context;

    public ItemConversationYouHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_conversation_you, parent, false));
    }

    public ItemConversationYouHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        builder = TextDrawable.builder()
                .beginConfig()
                .endConfig();
        this.context = itemView.getContext();
    }

    public void bindView(Conversation item){
        this.item = item;
        tvTime.setText(item.getDate());
        tvChatText.setText(item.getIsi());
        if (!TextUtils.isEmpty(item.getImg())){
            Picasso.with(context).load(item.getImg()).resize(400, 0).into(ivImage);
            ivImage.setVisibility(View.VISIBLE);
        }else{
            ivImage.setVisibility(View.GONE);
        }
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(item.getDivisi());
        String alias = "";
        switch (item.getDivisi()) {
            case "transaksi":
                alias = "T";
                break;
            case "kritik dan saran":
                alias = "KS";
                break;
            case "deposit":
                alias = "D";
                break;
        }
        TextDrawable ic1 = builder.buildRound(alias, color);
        ivAvatar.setImageDrawable(ic1);
    }

    @OnClick(R.id.ivImage) public void onViewClicked() {
        FragmentPhotoViewSingle fragmentPhotoView = new FragmentPhotoViewSingleBuilder(item.getImg()).build();
        fragmentPhotoView.setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogFragmentTheme);
        fragmentPhotoView.show(((AppCompatActivity) itemView.getContext()).getSupportFragmentManager(),"photo");
    }
}
