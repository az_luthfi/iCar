package com.app.icarindo.holder;

import android.os.Environment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.app.icarindo.R;
import com.app.icarindo.adapter.MateriAdapter;
import com.app.icarindo.models.Media;
import com.app.icarindo.utility.FileOpen;

import java.io.File;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemMateriFileHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivPerson) ImageView ivPerson;
    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.tvStatus) TextView tvStatus;
    @BindView(R.id.layStatus) LinearLayout layStatus;
    @BindView(R.id.layContainer) RelativeLayout layContainer;

    public ItemMateriFileHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_materi_file, parent, false));
    }

    public ItemMateriFileHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(final Media media, final MateriAdapter.MateriListener listener){
        final File fullFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS) + "/" + media.getMediaValue());
        tvName.setText(media.getMediaName());
        if (fullFile.exists()) {
            tvStatus.setText("Buka");
            layContainer.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    try {
                        FileOpen.openFile(itemView.getContext(), fullFile);
                    } catch (IOException e) {
                        Toast.makeText(itemView.getContext(), "Gagal membuka file", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
            });
        } else {
            tvStatus.setText("Download");
            layContainer.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {
                    listener.downloadFile(media.getMediaValueUrl(), media.getMediaValue());
                }
            });
        }
    }

}
