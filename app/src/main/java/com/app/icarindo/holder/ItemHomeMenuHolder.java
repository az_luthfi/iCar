package com.app.icarindo.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.HomeMenuAdapter;
import com.app.icarindo.models.HomeMenu;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemHomeMenuHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivIcon) ImageView ivIcon;
    @BindView(R.id.tvTitle) TextView tvTitle;
    @BindView(R.id.layContainer) LinearLayout layContainer;

    private HomeMenu model;
    private HomeMenuAdapter.HomeMenuListener listener;
    public ItemHomeMenuHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_home_menu, parent, false));
    }

    public ItemHomeMenuHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    @OnClick(R.id.layContainer) public void onViewClicked() {
        listener.onMenuClick(getAdapterPosition());
    }

    public void bindView(HomeMenu model, HomeMenuAdapter.HomeMenuListener listener) {
        this.listener = listener;
        this.model = model;
        tvTitle.setText(model.getTitle());

        int resourceId = itemView.getContext().getResources().getIdentifier(model.getIcon(), "drawable", itemView.getContext().getPackageName());
        if (resourceId != 0) {
            Picasso.with(itemView.getContext()).load(resourceId).into(ivIcon);
        }
    }
}
