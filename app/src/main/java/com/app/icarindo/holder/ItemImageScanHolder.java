package com.app.icarindo.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.ImageScanAdapter;
import com.app.icarindo.utility.Logs;
import com.squareup.picasso.Picasso;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemImageScanHolder extends RecyclerView.ViewHolder {
    private static final String TAG = ItemImageScanHolder.class.getSimpleName();
    @BindView(R.id.ivImage) ImageView ivImage;
    @BindView(R.id.layDelete) FrameLayout layDelete;

    private ImageScanAdapter.ImageScanListener listener;
    public ItemImageScanHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_image_scan, parent, false));
    }

    public ItemImageScanHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(String image, ImageScanAdapter.ImageScanListener listener) {
        Logs.d(TAG, "bindView ==> imagee");
        this.listener = listener;
        if (!image.contains("http")) {
            File ivFile = new File(image);
            Picasso.with(itemView.getContext())
                    .load(ivFile)
                    .into(ivImage);
        } else {

            Picasso.with(itemView.getContext())
                    .load(image)
                    .into(ivImage);
        }
    }

    @OnClick({R.id.ivImage, R.id.layDelete}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivImage:
                listener.onClickImage(getAdapterPosition());
                break;
            case R.id.layDelete:
                listener.onDeleteImage(getAdapterPosition());
                break;
        }
    }
}
