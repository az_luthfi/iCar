package com.app.icarindo.holder.ppob;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.ppob.PpobProduct;
import com.app.icarindo.utility.CommonUtilities;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.app.icarindo.utility.Config.FINANCE;
import static com.app.icarindo.utility.Config.HP_PASCA;
import static com.app.icarindo.utility.Config.PAKET_DATA;
import static com.app.icarindo.utility.Config.PDAM;
import static com.app.icarindo.utility.Config.PULSA;

public class ItemPpobProductLayoutHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.productName) TextView productName;
    @BindView(R.id.productAdmin) TextView productAdmin;
    @BindView(R.id.productFee) TextView productFee;
    @BindView(R.id.container) LinearLayout container;

    public ItemPpobProductLayoutHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_ppob_product_layout, parent, false));
    }

    public ItemPpobProductLayoutHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(PpobProduct item, String type) {
        productName.setText(item.getPpobProductName());
        switch (type) {
            case HP_PASCA:
            case FINANCE:
                productAdmin.setText(CommonUtilities.toRupiahFormat(item.getPpobProductModal()));
                productFee.setText(CommonUtilities.toRupiahFormat(item.getPpobProductFee()));
                break;
            case PDAM:
                productAdmin.setText(CommonUtilities.toRupiahFormat(item.getPpobProductAdminBank()));
                productFee.setText(CommonUtilities.toRupiahFormat(item.getPpobProductFee()));
                break;
            case PULSA:
            case PAKET_DATA:
                int price = Integer.parseInt(item.getPpobProductPrice()) + Integer.parseInt(item.getPpobProductModal());
                productAdmin.setText(CommonUtilities.toRupiahFormat(price));
                productFee.setText(CommonUtilities.toRupiahFormat(item.getPpobProductFee()));
                break;
        }
    }
}
