package com.app.icarindo.holder.ppob;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.ppob.Datum;
import com.app.icarindo.utility.CommonUtilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemInquiryPpobHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.tvLabel) TextView tvLabel;
    @BindView(R.id.tvValue) TextView tvValue;
    @BindView(R.id.tvTitle) TextView tvTitle;
    @BindView(R.id.contentLayout) LinearLayout contentLayout;

    public ItemInquiryPpobHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_inquiry_ppob, parent, false));
    }

    public ItemInquiryPpobHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(Datum item) {

        if (item.getLabel().toLowerCase().equalsIgnoreCase("textcenter")) {
            tvTitle.setText(CommonUtilities.toHtml(item.getValue()));
            tvTitle.setVisibility(View.VISIBLE);
            contentLayout.setVisibility(View.GONE);
        } else {
            contentLayout.setVisibility(View.VISIBLE);
            tvLabel.setText(item.getLabel());
            tvValue.setText(item.getValue());
        }

    }
}
