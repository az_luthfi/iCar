package com.app.icarindo.holder.message;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.app.icarindo.R;
import com.app.icarindo.models.pesan.Message;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemMessageHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivCustomerPhoto) ImageView ivCustomerPhoto;
    @BindView(R.id.tvCounter) TextView tvCounter;
    @BindView(R.id.rlPhotoContainer) RelativeLayout rlPhotoContainer;
    @BindView(R.id.tvTujuan) TextView tvTujuan;
    @BindView(R.id.tvTitle) TextView tvTitle;
    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.tvStatus) TextView tvStatus;
    @BindView(R.id.mainContainer) RelativeLayout mainContainer;

    private TextDrawable.IShapeBuilder builder;

    public ItemMessageHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_pesan, parent, false));
    }

    public ItemMessageHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
        builder = TextDrawable.builder()
                .beginConfig()
                .endConfig();
    }

    public void bindView(Message item) {
        ColorGenerator generator = ColorGenerator.MATERIAL;
        int color = generator.getColor(item.getDivisi().toLowerCase());
        String alias = "";
        switch (item.getDivisi().toLowerCase()) {
            case "transaksi":
                alias = "T";
                tvTujuan.setText("Transaksi");
                break;
            case "kritik dan saran":
                alias = "KS";
                tvTujuan.setText("Kritik & Saran");
                break;
            case "deposit":
                alias = "D";
                tvTujuan.setText("Deposit");
                break;
        }

        if (item.getStatus().equalsIgnoreCase("closed")) {
            tvStatus.setText("Closed");
            tvStatus.setVisibility(View.VISIBLE);
        } else if (item.getStatus().equalsIgnoreCase("terjawab")) {
            tvStatus.setText("Terjawab");
            tvStatus.setVisibility(View.VISIBLE);
            mainContainer.setSelected(true);
        } else if (item.getStatus().equalsIgnoreCase("open")) {
            tvStatus.setText("Baru");
            tvStatus.setVisibility(View.VISIBLE);
        } else if (item.getStatus().equalsIgnoreCase("terjawab_customer")) {
            tvStatus.setText("Menunggu");
            tvStatus.setVisibility(View.VISIBLE);
        } else if (item.getStatus().equalsIgnoreCase("terbaca")) {
            tvStatus.setText("Terbaca");
            tvStatus.setVisibility(View.VISIBLE);
        } else {
            mainContainer.setSelected(false);
            tvStatus.setVisibility(View.GONE);
        }

        TextDrawable ic1 = builder.buildRound(alias, color);
        ivCustomerPhoto.setImageDrawable(ic1);
        tvTitle.setText(item.getSubjek());
        tvDate.setText(item.getDate());
    }

}
