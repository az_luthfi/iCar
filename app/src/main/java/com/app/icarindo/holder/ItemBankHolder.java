package com.app.icarindo.holder;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.ppob.Bank;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemBankHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivBank) ImageView ivBank;
    @BindView(R.id.tvBankAccountName) TextView tvBankAccountName;
    @BindView(R.id.tvBankAccountNo) TextView tvBankAccountNo;
    @BindView(R.id.ivCheck) ImageView ivCheck;

    public ItemBankHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_bank, parent, false));
    }

    public ItemBankHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(Bank item) {
        if (!TextUtils.isEmpty(item.getBankImage())) {
            Picasso.with(itemView.getContext()).load(item.getBankImage()).into(ivBank);
        }
        tvBankAccountName.setText(item.getBankName());
        tvBankAccountNo.setText(item.getBankNumber());
        if (item.isSelected()){
            ivCheck.setVisibility(View.VISIBLE);
        }else{
            ivCheck.setVisibility(View.INVISIBLE);
        }

    }
}
