package com.app.icarindo.holder.carwis;

import android.annotation.SuppressLint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.carwis.Transaction;
import com.app.icarindo.utility.CommonUtilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemCarwisHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvPemesanName) TextView tvPemesanName;
    @BindView(R.id.tvBookingDate) TextView tvBookingDate;
    @BindView(R.id.tvTypeBooking) TextView tvTypeBooking;
    @BindView(R.id.tvStatusText) TextView tvStatusText;

    public ItemCarwisHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_carwis, parent, false));
    }

    public ItemCarwisHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    @SuppressLint("SetTextI18n") public void bindView(Transaction model) {
        tvPemesanName.setText(model.getContactName());
        tvStatusText.setText(CommonUtilities.toHtml(model.getCtStatusText()));
        tvTypeBooking.setText(model.getCtTypeText());
        tvBookingDate.setText("Booking : " + CommonUtilities.getFormatedDate(model.getCtBookingTime(), "yyyy-MM-dd HH:mm:ss", "dd MMM yyyy HH:mm"));
    }
}
