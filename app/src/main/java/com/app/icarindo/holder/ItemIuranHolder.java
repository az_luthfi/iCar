package com.app.icarindo.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.network.Iuran;
import com.app.icarindo.utility.CommonUtilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemIuranHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvNumber) TextView tvNumber;
    @BindView(R.id.tvNominal) TextView tvNominal;
    @BindView(R.id.tvDate) TextView tvDate;

    public ItemIuranHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_iuran, parent, false));
    }

    public ItemIuranHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(Iuran model) {
        tvNumber.setText(String.valueOf(getAdapterPosition() + 1));
        tvNominal.setText(CommonUtilities.toRupiahFormat(model.getInNominal()));
        tvDate.setText(model.getInCreateDate());
    }
}
