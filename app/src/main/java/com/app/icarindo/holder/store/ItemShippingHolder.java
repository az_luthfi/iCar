package com.app.icarindo.holder.store;

import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.adapter.store.ShippingAdapter;
import com.app.icarindo.models.store.CustomerShipping;
import com.app.icarindo.utility.CommonUtilities;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemShippingHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.tvDefault) TextView tvDefault;
    @BindView(R.id.tvPhone) TextView tvPhone;
    @BindView(R.id.tvFullAddress) TextView tvFullAddress;
    @BindView(R.id.tvAddressShipping) TextView tvAddressShipping;
    @BindView(R.id.layEdit) FrameLayout layEdit;
    @BindView(R.id.layDelete) FrameLayout layDelete;
    @BindView(R.id.actionView) LinearLayout actionView;
    @BindView(R.id.tvLabel) TextView tvLabel;
    @BindView(R.id.ivCheck) ImageView ivCheck;
    @BindView(R.id.layCheck) FrameLayout layCheck;

    private ShippingAdapter.ShippingListener listener;
    private CustomerShipping shipping;

    public ItemShippingHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_shipping, parent, false));
    }

    public ItemShippingHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(CustomerShipping item, ShippingAdapter.ShippingListener listener, boolean isPurchase, boolean setSelected) {
        this.shipping = item;
        this.listener = listener;
        tvLabel.setText(CommonUtilities.getText(item.getShippingLabel(), ""));
        tvName.setText(CommonUtilities.getText(item.getShippingName(), ""));
        if (isPurchase){
            layCheck.setVisibility(View.VISIBLE);
        }else{
            layCheck.setVisibility(View.GONE);
        }
        ivCheck.setVisibility(View.INVISIBLE);
        if (CommonUtilities.getText(item.getShippingDefault(), "0").equals("1")) {
            tvDefault.setVisibility(View.VISIBLE);
            if (isPurchase && !setSelected) {
                setSelected();
            }
        } else {
            tvDefault.setVisibility(View.GONE);
        }
        if (item.isSelected()) {
            setSelected();
        }
        tvPhone.setText(CommonUtilities.getText(item.getShippingPhone(), ""));
        tvFullAddress.setText(CommonUtilities.getText(item.getShippingAddress(), ""));
        String addressShipping = "";
        if (!TextUtils.isEmpty(item.getShippingProvinceName())) {
            addressShipping = item.getShippingProvinceName();
        }
        if (!TextUtils.isEmpty(item.getShippingCityName())) {
            addressShipping = addressShipping + ", " + item.getShippingCityName();
        }
        tvAddressShipping.setText(addressShipping);
    }

    @OnClick({R.id.layEdit, R.id.layDelete}) public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.layEdit:
                listener.onEdit(new Gson().toJson(shipping));
                break;
            case R.id.layDelete:
                listener.onDelete(getAdapterPosition());
                break;
        }
    }

    private void setSelected() {
        ivCheck.setVisibility(View.VISIBLE);
        shipping.setPositionAdapter(getAdapterPosition());
        listener.onSelectedAddress(shipping);
    }
}
