package com.app.icarindo.holder;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.network.Cuti;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemHistoryAbsensiIjinHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.tvDesc) TextView tvDesc;
    @BindView(R.id.tvStatus) TextView tvStatus;

    public ItemHistoryAbsensiIjinHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_history_absensi_ijin, parent, false));
    }

    public ItemHistoryAbsensiIjinHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(Cuti cuti){
        tvDate.setText(cuti.getCcDate());
        tvDesc.setText(cuti.getCcKeterangan());
        tvStatus.setText(cuti.getCcStatus());

        switch (cuti.getCcStatus().toLowerCase()){
            case "pending":
                tvStatus.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.md_orange_500));
                break;
            case "success":
                tvStatus.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.md_green_500));
                break;
            case "cancel":
                tvStatus.setTextColor(ContextCompat.getColor(itemView.getContext(), R.color.md_red_500));
                break;

        }
    }

}
