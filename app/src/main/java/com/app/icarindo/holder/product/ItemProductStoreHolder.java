package com.app.icarindo.holder.product;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.store.Product;
import com.app.icarindo.utility.CommonUtilities;
import com.app.icarindo.utility.Str;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemProductStoreHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.ivImage) ImageView ivImage;
    @BindView(R.id.tvProductName) TextView tvProductName;
    @BindView(R.id.tvProductPrice) TextView tvProductPrice;

    public ItemProductStoreHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_product_store, parent, false));
    }

    public ItemProductStoreHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(Product model) {
        if (!model.getProductImageLink().isEmpty() && model.getProductImageLink().contains("http")) {
            Picasso.with(itemView.getContext()).load(model.getProductImageLink()).into(ivImage);
        }
        tvProductName.setText(CommonUtilities.toHtml(Str.getText(model.getProductName(), "")));
        tvProductPrice.setText(CommonUtilities.toRupiahNumberFormat(Str.getText(model.getProductPricePublish(), "0")));
    }
}
