package com.app.icarindo.holder;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.icarindo.R;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;

/**
 * Created by j3p0n on 1/10/2017.
 */

public class SliderItem extends BaseSliderView {

    public SliderItem(Context context) {
        super(context);
    }

    @Override
    public View getView() {
        View v = LayoutInflater.from(getContext()).inflate(R.layout.item_slider_layout,null);
        ImageView target = (ImageView)v.findViewById(R.id.daimajia_slider_image);
        TextView description = (TextView)v.findViewById(R.id.description);
        description.setText(getDescription());
        bindEventAndShow(v, target);
        return v;
    }

}