package com.app.icarindo.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.network.Extra;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemHistoryAbsensiExtraHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvDate) TextView tvDate;

    public ItemHistoryAbsensiExtraHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_history_absensi_extra, parent, false));
    }

    public ItemHistoryAbsensiExtraHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(Extra model) {
        tvDate.setText(model.getBeDatePakai());
    }
}
