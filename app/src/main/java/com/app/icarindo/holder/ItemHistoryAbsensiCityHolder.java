package com.app.icarindo.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.network.KeluarKota;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemHistoryAbsensiCityHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvDate) TextView tvDate;
    @BindView(R.id.tvDesc) TextView tvDesc;

    public ItemHistoryAbsensiCityHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_history_absensi_city, parent, false));
    }

    public ItemHistoryAbsensiCityHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }


    public void bindView(KeluarKota model) {
        tvDate.setText(model.getCkkDate());
        tvDesc.setText(model.getCkkDesc());
    }
}
