package com.app.icarindo.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.network.BonusSponsor;
import com.app.icarindo.utility.CommonUtilities;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemBonusSponsorHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.tvNumber) TextView tvNumber;
    @BindView(R.id.tvName) TextView tvName;
    @BindView(R.id.tvNominal) TextView tvNominal;
    @BindView(R.id.tvDate) TextView tvDate;

    public ItemBonusSponsorHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_bonus_sponsor, parent, false));
    }

    public ItemBonusSponsorHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(BonusSponsor model){
        tvNumber.setText(String.valueOf(getAdapterPosition() + 1));
        tvName.setText(model.getCustomerName());
        tvNominal.setText(CommonUtilities.toRupiahFormat(model.getBsNominal()));
        tvDate.setText(model.getBsCreateDate());
    }
}
