package com.app.icarindo.holder;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.network.CustomerAkun;
import com.app.icarindo.utility.Str;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemCustomerAkunHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivLogo) ImageView ivLogo;
    @BindView(R.id.tvUserName) TextView tvUserName;
    @BindView(R.id.tvPassword) TextView tvPassword;

    public ItemCustomerAkunHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_customer_akun, parent, false));
    }

    public ItemCustomerAkunHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(CustomerAkun model) {
        tvUserName.setText(Str.getText(model.getCaUsername(), ""));
        int resourceId = itemView.getContext().getResources().getIdentifier(model.getCaType(), "drawable", itemView.getContext().getPackageName());
        if (resourceId != 0) {
            Picasso.with(itemView.getContext()).load(resourceId).into(ivLogo);
        }
    }
}
