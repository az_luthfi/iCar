package com.app.icarindo.holder.ppob;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.icarindo.R;
import com.app.icarindo.models.ppob.MenuPembayaran;
import com.squareup.picasso.Picasso;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ItemMenuPembayaranHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.ivMenu) ImageView ivMenu;
    @BindView(R.id.tvMenuPembayaran) TextView tvMenuPembayaran;

    public ItemMenuPembayaranHolder(LayoutInflater inflater, ViewGroup parent) {
        this(inflater.inflate(R.layout.item_menu_pembayaran, parent, false));
    }

    public ItemMenuPembayaranHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }

    public void bindView(MenuPembayaran item) {
        tvMenuPembayaran.setText(item.getTitle());
//        int resourceId = itemView.getContext().getResources().getIdentifier(item.getIcon().toLowerCase().replace(" ", "_").trim(), "drawable", itemView.getContext().getPackageName());
        int resourceId = itemView.getContext().getResources().getIdentifier(item.getIcon(), "drawable", itemView.getContext().getPackageName());
        if (resourceId != 0) {
            Picasso.with(itemView.getContext()).load(resourceId).into(ivMenu);
        }
    }
}
