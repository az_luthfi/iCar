package com.app.icarindo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.holder.ItemMateriFileHolder;
import com.app.icarindo.holder.ItemMateriVideoHolder;
import com.app.icarindo.models.Media;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 20/10/2017.
 */

public class MateriAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_VIDEO = 0;
    private static final int TYPE_OTHER = 1;
    protected ArrayList<Media> items = new ArrayList<>();
    private MateriListener listener;

    public MateriAdapter(MateriListener listener) {
        this.listener = listener;
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_VIDEO) {
            return new ItemMateriVideoHolder(inflater, parent);
        }
        return new ItemMateriFileHolder(inflater, parent);
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemMateriFileHolder) {
            ((ItemMateriFileHolder) holder).bindView(items.get(position), listener);
        } else if (holder instanceof ItemMateriVideoHolder) {
            ((ItemMateriVideoHolder) holder).bindView(items.get(position));
        }
    }

    @Override public int getItemCount() {
        return items.size();
    }

    @Override public int getItemViewType(int position) {
        if (items.get(position).getMediaType().equals("video")) {
            return TYPE_VIDEO;
        }
        return TYPE_OTHER;
    }

    public void setItems(ArrayList<Media> data) {
        int start = items.size();
        items.addAll(data);
        notifyItemRangeInserted(start, data.size());
    }

    public interface MateriListener {
        void downloadFile(String mediaValueUrl, String mediaValue);
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

}
