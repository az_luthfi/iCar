package com.app.icarindo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarindo.holder.ItemBonusSponsorHolder;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.network.BonusSponsor;

/**
 * Created by Luthfi Aziz on 23/10/2017.
 */

public class BonusSponsorAdapter extends BaseRecyclerViewLoadingAdapter<BonusSponsor> {

    public BonusSponsorAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemBonusSponsorHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, BonusSponsor model) {
        if (holder instanceof ItemBonusSponsorHolder){
            ((ItemBonusSponsorHolder) holder).bindView(model);
        }
    }
}
