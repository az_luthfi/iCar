package com.app.icarindo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.holder.ppob.ItemHistoryHolder;
import com.app.icarindo.models.ppob.History;

/**
 * Created by Luthfi Aziz on 31/10/2017.
 */

public class HistoryTransactionAdapter extends BaseRecyclerViewLoadingAdapter<History> {

    public HistoryTransactionAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemHistoryHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, History model) {
        if (holder instanceof ItemHistoryHolder) {
            ((ItemHistoryHolder) holder).bindView(model);
        }
    }
}
