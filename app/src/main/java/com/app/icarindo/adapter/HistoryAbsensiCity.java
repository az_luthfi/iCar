package com.app.icarindo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarindo.holder.ItemHistoryAbsensiCityHolder;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.network.KeluarKota;

/**
 * Created by Luthfi Aziz on 29/10/2017.
 */

public class HistoryAbsensiCity extends BaseRecyclerViewLoadingAdapter<KeluarKota> {

    public HistoryAbsensiCity(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemHistoryAbsensiCityHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, KeluarKota model) {
        if (holder instanceof ItemHistoryAbsensiCityHolder){
            ((ItemHistoryAbsensiCityHolder) holder).bindView(model);
        }
    }
}
