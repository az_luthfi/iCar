package com.app.icarindo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarindo.holder.ItemHistoryAbsensiIjinHolder;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.network.Cuti;

/**
 * Created by Luthfi Aziz on 25/10/2017.
 */

public class HistoryAbsensiIjinAdapter extends BaseRecyclerViewLoadingAdapter<Cuti> {

    public HistoryAbsensiIjinAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemHistoryAbsensiIjinHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, Cuti model) {
        if (holder instanceof ItemHistoryAbsensiIjinHolder){
            ((ItemHistoryAbsensiIjinHolder) holder).bindView(model);
        }
    }
}
