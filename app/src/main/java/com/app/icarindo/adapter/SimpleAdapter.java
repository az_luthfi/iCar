package com.app.icarindo.adapter;


import com.app.icarindo.base.BaseRecyclerViewAdapter;
import com.app.icarindo.holder.ItemSimpleHolder;
import com.app.icarindo.models.datalist.SimpleList;
import com.app.icarindo.utility.filter.SimpleListFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luthfi on 28/04/2017.
 */
public class SimpleAdapter extends BaseRecyclerViewAdapter<SimpleList,ItemSimpleHolder> {

    private final SimpleListFilter simpleListFilter;
    private int selected = -1;
    public SimpleAdapter() {
        super(ItemSimpleHolder.class);
        this.simpleListFilter = new SimpleListFilter(this);
    }

    @Override protected void populateViewHolder(ItemSimpleHolder viewHolder, SimpleList model, int position) {
        viewHolder.bindView(model, (position == selected));
    }

    public void addAll(ArrayList<? extends SimpleList> data){
        if (data != null){
            mList.addAll(data);
            simpleListFilter.setData(data);
            notifyDataSetChanged();
        }
    }

    public void filter(CharSequence charSequence) {
        simpleListFilter.filter(charSequence);
    }

    public void setFiltered(List<SimpleList> simpleListFiltered) {
        mList.addAll(simpleListFiltered);
        notifyDataSetChanged();
    }

    @Override public void clear() {
        super.clear();
        selected = -1;
    }

    public void setSelected(int selected) {
        this.selected = selected;
        notifyDataSetChanged();
    }
}