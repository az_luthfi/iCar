package com.app.icarindo.adapter.ppob;


import com.app.icarindo.base.BaseRecyclerViewAdapter;
import com.app.icarindo.holder.ppob.ItemInquiryPpobHolder;
import com.app.icarindo.models.ppob.Datum;

/**
 * Created by j3p0n on 5/10/2017.
 */
public class InquiryDataAdapter extends BaseRecyclerViewAdapter<Datum, ItemInquiryPpobHolder> {

    public InquiryDataAdapter() {
        super(ItemInquiryPpobHolder.class);
    }

    @Override protected void populateViewHolder(ItemInquiryPpobHolder viewHolder, Datum item, int position) {
        viewHolder.bindView(item);
    }
}
