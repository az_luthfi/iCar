package com.app.icarindo.adapter.ppob;

import com.app.icarindo.base.BaseRecyclerViewAdapter;
import com.app.icarindo.holder.ppob.ItemHistoryHolder;
import com.app.icarindo.models.ppob.History;

/**
 * Created by Luthfi on 10/08/2017.
 */
public class HistoryAdapter extends BaseRecyclerViewAdapter<History, ItemHistoryHolder> {

    public HistoryAdapter() {
        super(ItemHistoryHolder.class);
    }

    @Override protected void populateViewHolder(ItemHistoryHolder viewHolder, History item, int position) {
        viewHolder.bindView(item);
    }
}