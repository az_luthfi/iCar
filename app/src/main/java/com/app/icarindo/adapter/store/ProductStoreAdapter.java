package com.app.icarindo.adapter.store;

import com.app.icarindo.base.BaseRecyclerViewAdapter;
import com.app.icarindo.holder.product.ItemProductStoreHolder;
import com.app.icarindo.models.store.Product;

/**
 * Created by Luthfi Aziz on 04/11/2017.
 */
public class ProductStoreAdapter extends BaseRecyclerViewAdapter<Product, ItemProductStoreHolder> {

    public ProductStoreAdapter() {
        super(ItemProductStoreHolder.class);
    }

    @Override protected void populateViewHolder(ItemProductStoreHolder viewHolder, Product item, int position) {
        viewHolder.bindView(item);
    }
}