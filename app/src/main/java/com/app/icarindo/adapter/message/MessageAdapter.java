package com.app.icarindo.adapter.message;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.holder.message.ItemMessageHolder;
import com.app.icarindo.models.pesan.Message;


/**
 * Created by luthfi on 02/06/2017.
 */

public class MessageAdapter extends BaseRecyclerViewLoadingAdapter<Message> {

    public MessageAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    /**
     * @param position don't use 0, because is usage LOADING_VIEW
     * @return viewType
     */
    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemMessageHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, Message model) {
        ((ItemMessageHolder) holder).bindView(model);
    }

    public void addNewItem(Message message){
        this.items.add(0,message);
        notifyDataSetChanged();
    }
}
