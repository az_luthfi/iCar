package com.app.icarindo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarindo.holder.ItemHistoryNonTransactionHolder;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.ItemHistory;

/**
 * Created by Luthfi Aziz on 31/10/2017.
 */

public class HistoryNonTransactionAdapter extends BaseRecyclerViewLoadingAdapter<ItemHistory> {

    public HistoryNonTransactionAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemHistoryNonTransactionHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, ItemHistory model) {
        if (holder instanceof ItemHistoryNonTransactionHolder){
            ((ItemHistoryNonTransactionHolder) holder).bindView(model);
        }
    }
}
