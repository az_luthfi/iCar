package com.app.icarindo.adapter.carwis;


import com.app.icarindo.base.BaseRecyclerViewAdapter;
import com.app.icarindo.holder.carwis.ItemWisataCartPriceHolder;
import com.app.icarindo.models.carwis.Wisata;

/**
 * Created by Luthfi Aziz on 19/11/2017.
 */
public class WisataCartPriceAdapter extends BaseRecyclerViewAdapter<Wisata, ItemWisataCartPriceHolder> {

    public WisataCartPriceAdapter() {
        super(ItemWisataCartPriceHolder.class);
    }

    @Override protected void populateViewHolder(ItemWisataCartPriceHolder viewHolder, Wisata model, int position) {
        viewHolder.bindView(model);
    }
}