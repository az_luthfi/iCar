package com.app.icarindo.adapter;

import com.app.icarindo.base.BaseRecyclerViewAdapter;
import com.app.icarindo.holder.ItemCustomerAkunHolder;
import com.app.icarindo.models.network.CustomerAkun;

/**
 * Created by Luthfi Aziz on 29/10/2017.
 */
public class CustomerAkunAdapter extends BaseRecyclerViewAdapter<CustomerAkun, ItemCustomerAkunHolder> {

    public CustomerAkunAdapter() {
        super(ItemCustomerAkunHolder.class);
    }

    @Override protected void populateViewHolder(ItemCustomerAkunHolder viewHolder, CustomerAkun item, int position) {
        viewHolder.bindView(item);
    }
}