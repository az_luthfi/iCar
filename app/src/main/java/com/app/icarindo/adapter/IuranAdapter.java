package com.app.icarindo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarindo.holder.ItemIuranHolder;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.network.Iuran;

/**
 * Created by Luthfi Aziz on 23/10/2017.
 */

public class IuranAdapter extends BaseRecyclerViewLoadingAdapter<Iuran> {

    public IuranAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemIuranHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, Iuran model) {
        if (holder instanceof ItemIuranHolder){
            ((ItemIuranHolder) holder).bindView(model);
        }
    }
}
