package com.app.icarindo.adapter.content;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarindo.holder.ItemContentListHolder;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.content.Content;

/**
 * Created by j3p0n on 4/27/2017.
 */
public class ContentListAdapter extends BaseRecyclerViewLoadingAdapter<Content> {

    public ContentListAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemContentListHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, Content model) {
        if (holder instanceof  ItemContentListHolder){
            ((ItemContentListHolder) holder).bindView(model);
        }
    }
}