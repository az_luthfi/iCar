package com.app.icarindo.adapter;

import com.app.icarindo.base.BaseRecyclerViewAdapter;
import com.app.icarindo.holder.ItemImageScanHolder;

/**
 * Created by Luthfi Aziz on 19/10/2017.
 */
public class ImageScanAdapter extends BaseRecyclerViewAdapter<String, ItemImageScanHolder> {

    private ImageScanListener listener;

    public ImageScanAdapter(ImageScanListener listener) {
        super(ItemImageScanHolder.class);
        this.listener = listener;
    }

    @Override protected void populateViewHolder(ItemImageScanHolder viewHolder, String item, int position) {
        viewHolder.bindView(item, listener);
    }

    public interface ImageScanListener{
        void onClickImage(int position);
        void onDeleteImage(int position);
    }
}