package com.app.icarindo.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarindo.holder.ItemHistoryAbsensiExtraHolder;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.models.network.Extra;

/**
 * Created by Luthfi Aziz on 25/10/2017.
 */

public class HistoryAbsensiExtraAdapter extends BaseRecyclerViewLoadingAdapter<Extra> {

    public HistoryAbsensiExtraAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemHistoryAbsensiExtraHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, Extra model) {
        if (holder instanceof ItemHistoryAbsensiExtraHolder){
            ((ItemHistoryAbsensiExtraHolder) holder).bindView(model);
        }
    }
}
