package com.app.icarindo.adapter.ppob;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.holder.ppob.ItemPpobProductLayoutHolder;
import com.app.icarindo.models.ppob.PpobProduct;
import com.app.icarindo.utility.filter.ProductFilter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by j3p0n on 4/30/2017.
 */
public class PpobProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<PpobProduct> items = new ArrayList<>();
    private final ProductFilter pFIlter;
    private String type;

    public PpobProductAdapter() {
        pFIlter = new ProductFilter(this);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                      int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new ItemPpobProductLayoutHolder(inflater, parent);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        PpobProduct item = items.get(position);
        ItemPpobProductLayoutHolder itemHolder = (ItemPpobProductLayoutHolder) holder;
        itemHolder.bindView(item, type);
    }

    @Override
    public int getItemCount() {
        if (items == null) {
            return 0;
        }
        return items.size();
    }

    // get items by position
    public PpobProduct getItem(int position) {
        return items.get(position);
    }

    // Clean all elements of the recycler
    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    // Add items of items
    public void addAll(List<PpobProduct> datas) {
        if (datas != null) {
            items.addAll(datas);
            pFIlter.setData(datas);
            notifyDataSetChanged();
        }
    }

    public List<PpobProduct> getAll() {
        return items;
    }


    public void filter(CharSequence charSequence) {
        pFIlter.filter(charSequence);
    }

    public void setFiltered(List<PpobProduct> filtered) {
        items.addAll(filtered);
        notifyDataSetChanged();
    }

    public void setType(String type) {
        this.type = type;
    }
}