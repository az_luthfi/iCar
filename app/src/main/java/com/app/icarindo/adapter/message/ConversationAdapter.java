package com.app.icarindo.adapter.message;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.holder.message.ItemConversationMeHolder;
import com.app.icarindo.holder.message.ItemConversationYouHolder;
import com.app.icarindo.models.pesan.Conversation;

import java.util.ArrayList;

/**
 * Created by luthfi on 05/06/2017.
 */

public class ConversationAdapter  extends BaseRecyclerViewLoadingAdapter<Conversation> {


    public ConversationAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }
    /**
     * @param position don't use 0, because is usage LOADING_VIEW
     * @return viewType
     */
    @Override protected int getItemViewTypeChild(int position) {
        return items.get(position).getType().equalsIgnoreCase("admin") ? 1 : 2;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        if (viewType == 1){
            return new ItemConversationYouHolder(inflater, parent);
        }
        return new ItemConversationMeHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, Conversation model) {
        if (holder instanceof ItemConversationYouHolder){
            ((ItemConversationYouHolder) holder).bindView(model);
        }else if (holder instanceof ItemConversationMeHolder){
            ((ItemConversationMeHolder) holder).bindView(model);
        }
    }

    public String getLastId(){
        if(!items.isEmpty())
        {
            return items.get(0).getId();
        }
        return "0";
    }

    public void addNewItems(ArrayList<Conversation> data) {
        this.items.addAll(0,data);
        notifyDataSetChanged();
    }
}
