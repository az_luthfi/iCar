package com.app.icarindo.adapter.ppob;

import com.app.icarindo.base.BaseRecyclerViewAdapter;
import com.app.icarindo.holder.ppob.ItemMenuPembayaranHolder;
import com.app.icarindo.models.ppob.MenuPembayaran;

/**
 * Created by Luthfi on 07/08/2017.
 */
public class MenuPembayaranAdapter extends BaseRecyclerViewAdapter<MenuPembayaran, ItemMenuPembayaranHolder> {

    public MenuPembayaranAdapter() {
        super(ItemMenuPembayaranHolder.class);
    }

    @Override protected void populateViewHolder(ItemMenuPembayaranHolder viewHolder, MenuPembayaran item, int position) {
        viewHolder.bindView(item);
    }
}