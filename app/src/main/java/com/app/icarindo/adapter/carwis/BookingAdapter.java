package com.app.icarindo.adapter.carwis;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.base.BaseRecyclerViewLoadingAdapter;
import com.app.icarindo.holder.ItemLoadingHolder;
import com.app.icarindo.holder.carwis.ItemCarwisHolder;
import com.app.icarindo.models.carwis.Transaction;


/**
 * Created by Luthfi Aziz on 09/12/2017.
 */

public class BookingAdapter extends BaseRecyclerViewLoadingAdapter<Transaction> {

    public BookingAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        super(loadingListener);
    }

    @Override protected int getItemViewTypeChild(int position) {
        return 1;
    }

    @Override protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater) {
        return new ItemCarwisHolder(inflater, parent);
    }

    @Override protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, Transaction model) {
        if (holder instanceof ItemCarwisHolder){
            ((ItemCarwisHolder) holder).bindView(model);
        }
    }
}
