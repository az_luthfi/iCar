package com.app.icarindo.adapter.store;


import com.app.icarindo.base.BaseRecyclerViewAdapter;
import com.app.icarindo.holder.store.ItemShippingHolder;
import com.app.icarindo.models.store.CustomerShipping;

/**
 * Created by luthfi on 10/07/2017.
 */

public class ShippingAdapter extends BaseRecyclerViewAdapter<CustomerShipping, ItemShippingHolder> {

    private final ShippingListener listener;
    private final boolean isPurchase;
    private boolean setSelected = false;

    public ShippingAdapter(ShippingListener listener, boolean isPurchase) {
        super(ItemShippingHolder.class);
        this.listener = listener;
        this.isPurchase = isPurchase;
    }

    @Override protected void populateViewHolder(ItemShippingHolder viewHolder, CustomerShipping model, int position) {
        viewHolder.bindView(model, listener, isPurchase, setSelected);
    }

    public void setSelected(boolean selected){
        this.setSelected = selected;
    }

    public interface ShippingListener {

        void onEdit(String jsonShipping);

        void onDelete(int position);

        void onSelectedAddress(CustomerShipping shipping);
    }
}
