package com.app.icarindo.adapter;

import com.app.icarindo.base.BaseRecyclerViewAdapter;
import com.app.icarindo.holder.ItemHomeMenuHolder;
import com.app.icarindo.models.HomeMenu;

/**
 * Created by Luthfi Aziz on 22/10/2017.
 */
public class HomeMenuAdapter extends BaseRecyclerViewAdapter<HomeMenu, ItemHomeMenuHolder> {

    private HomeMenuListener listener;
    public HomeMenuAdapter(HomeMenuListener listener) {
        super(ItemHomeMenuHolder.class);
        this.listener = listener;
    }

    @Override protected void populateViewHolder(ItemHomeMenuHolder viewHolder, HomeMenu item, int position) {
        viewHolder.bindView(item, listener);
    }

    public interface HomeMenuListener{
        void onMenuClick(int position);
    }
}