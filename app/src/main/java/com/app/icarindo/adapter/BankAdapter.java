package com.app.icarindo.adapter;

import com.app.icarindo.base.BaseRecyclerViewAdapter;
import com.app.icarindo.holder.ItemBankHolder;
import com.app.icarindo.models.ppob.Bank;

/**
 * Created by Luthfi on 02/08/2017.
 */
public class BankAdapter extends BaseRecyclerViewAdapter<Bank, ItemBankHolder> {

    public BankAdapter() {
        super(ItemBankHolder.class);
    }

    @Override protected void populateViewHolder(ItemBankHolder viewHolder, Bank item, int position) {
        viewHolder.bindView(item);
    }
}