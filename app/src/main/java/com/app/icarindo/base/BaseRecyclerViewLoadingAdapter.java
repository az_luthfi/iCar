package com.app.icarindo.base;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.app.icarindo.holder.ItemLoadingHolder;

import java.util.ArrayList;

/**
 * Created by luthfi on 26/04/2017.
 */

public abstract class BaseRecyclerViewLoadingAdapter<M> extends  RecyclerView.Adapter<RecyclerView.ViewHolder>{
    public static final int LOADING_VIEW = 0;

    protected ArrayList<M> items = new ArrayList<>();

    private boolean isLoadingNextPage = false;
    private String msgErrorNextPage = null;
    private ItemLoadingHolder.ItemViewLoadingListener loadingListener;

    public BaseRecyclerViewLoadingAdapter(ItemLoadingHolder.ItemViewLoadingListener loadingListener) {
        this.loadingListener = loadingListener;
    }

    @Override public int getItemViewType(int position) {
        if (isLoadingNextPage && position == items.size()) {
            return LOADING_VIEW;
        }
        return getItemViewTypeChild(position);
    }

    @Override public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == LOADING_VIEW){
            return new ItemLoadingHolder(inflater, parent, loadingListener);
        }else{
            return onCreateViewHolderChild(parent, viewType, inflater);
        }
    }

    @Override public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        // initialization for loading view (loading/retry)
        if (holder instanceof ItemLoadingHolder) {
            if (msgErrorNextPage != null) {
                ((ItemLoadingHolder) holder).setErrorNextPage(msgErrorNextPage);
            } else {
                ((ItemLoadingHolder) holder).setLoading();
            }
            if (holder.itemView.getLayoutParams() instanceof  StaggeredGridLayoutManager.LayoutParams){
                StaggeredGridLayoutManager.LayoutParams layoutParams = (StaggeredGridLayoutManager.LayoutParams) ((ItemLoadingHolder) holder).itemView.getLayoutParams();
                layoutParams.setFullSpan(true);
            }
        }else{
            M model = getItem(position);
            onBindViewHolderChild(holder, position, model);
        }
    }

    public void setItems(ArrayList<M> data) {
        int start = items.size();
        items.addAll(data);
        notifyItemRangeInserted(start, data.size());
    }

    public void addItem(M t) {
        int index = items.size();
        items.add(index, t);
        notifyItemInserted(index);
    }

    public void updateItem(int position, M m) {
        items.set(position, m);
        notifyItemChanged(position);
    }

    public void removeItem(int position) {
        items.remove(position);
        notifyItemRemoved(position);
    }

    public void clear() {
        items.clear();
        notifyDataSetChanged();
    }

    @Override public int getItemCount() {
        return items == null ? 0 : (items.size() + (isLoadingNextPage ? 1 : 0));
    }

    public M getItem(int position) {
        return items.get(position);
    }

    public boolean isLoading() {
        return isLoadingNextPage;
    }

    /**
     * @param loadingNextPage true = insert view loading for next page, false = remove view loading next page
     */
    public void setLoadingNextPage(boolean loadingNextPage) {
//        isLoadingNextPage = loadingNextPage;
        msgErrorNextPage = null;

        if (loadingNextPage) {
            if (!isLoading()){
                isLoadingNextPage = true;
                notifyItemInserted(items.size());
            }
        } else {
            if (isLoading()){
                isLoadingNextPage = false;
                notifyItemRemoved(items.size());
            }
        }
    }

    /**
     * @param msgError not null String for change loading to retry button
     */
    public void setErrorNextPage(String msgError) {
        msgErrorNextPage = msgError;
        if (isLoading()) {
            isLoadingNextPage = true;
            notifyItemChanged(items.size());
        }else{
            isLoadingNextPage = true;
            notifyItemInserted(items.size());
        }
    }

    /**
     * @param position don't use 0, because is usage LOADING_VIEW
     * @return viewType
     */
    abstract protected int getItemViewTypeChild(int position);

    abstract protected RecyclerView.ViewHolder onCreateViewHolderChild(ViewGroup parent, int viewType, LayoutInflater inflater);

    abstract protected void onBindViewHolderChild(RecyclerView.ViewHolder holder, int position, M model);

}
