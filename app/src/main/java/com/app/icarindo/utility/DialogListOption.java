package com.app.icarindo.utility;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;

import com.app.icarindo.R;
import com.app.icarindo.adapter.SimpleAdapter;
import com.app.icarindo.utility.listview.DividerItemDecoration;
import com.app.icarindo.utility.listview.ItemClickSupport;


/**
 * Created by luthfi on 28/04/2017.
 */

public abstract class DialogListOption {

    public DialogListOption(Context context, String title, SimpleAdapter adapter) {
        final Dialog dialog = new Dialog(context, android.R.style.DeviceDefault_Light_ButtonBar);
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setContentView(R.layout.fullscreen_dialog_simple);
        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
        EditText searchEditText = (EditText) dialog.findViewById(R.id.tvSearch);
        searchEditText.setHint("Cari " + title);
        recyclerView.setAdapter(adapter);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        recyclerView.addItemDecoration(new DividerItemDecoration(context, DividerItemDecoration.LIST_VERTICAL));

        ItemClickSupport.addTo(recyclerView).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                DialogListOption.this.onItemClicked(position);
                dialog.dismiss();
            }
        });

        searchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                DialogListOption.this.onFilter(s);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        dialog.show();
    }

    public abstract void onItemClicked(int position);

    public abstract void onFilter(CharSequence s);
}
