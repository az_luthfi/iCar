package com.app.icarindo.utility.listview;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Creasif on 9/21/2016.
 */

public class GridSpaceItemDecoration extends RecyclerView.ItemDecoration {
    private final int mSpace;
    private final int spanCount;

    public GridSpaceItemDecoration(int space, int spanCount) {
        this.mSpace = space;
        this.spanCount = spanCount;

    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        outRect.right = mSpace;
        outRect.bottom = mSpace;
        if (parent.getChildAdapterPosition(view) < spanCount) {

            outRect.top = mSpace;

        }

        parent.setPadding(mSpace,0,0,0);

        /*if ((parent.getChildAdapterPosition(view)+1) % spanCount == 1) {

            outRect.left = mSpace;

        }*/
    }
}