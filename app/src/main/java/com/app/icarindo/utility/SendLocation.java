package com.app.icarindo.utility;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.app.icarindo.app.App;
import com.app.icarindo.models.RequestBase;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;
import com.patloew.rxlocation.RxLocation;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Consumer;
import io.reactivex.functions.Function;
import io.reactivex.plugins.RxJavaPlugins;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Luthfi on 17/08/2017.
 */

public class SendLocation {
    private static final String TAG = SendLocation.class.getSimpleName();

    private Context context;
    private RxLocation rxLocation;
    private LocationRequest locationRequest;
    private CompositeDisposable disposable;
    private String customerId;
    private LatLng latLng;

    public SendLocation(Context context, String customerId) {
        this.context = context;
        this.customerId = customerId;
        getLocation();
    }

    private void getLocation() {
        if (rxLocation == null) {
            rxLocation = new RxLocation(context);
            rxLocation.setDefaultTimeout(15, TimeUnit.SECONDS);
        }
        if (disposable == null) {
            disposable = new CompositeDisposable();
        }
        if (locationRequest == null) {
            locationRequest = LocationRequest.create()
                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                    .setInterval(5000)
                    .setNumUpdates(1);
        }

        RxJavaPlugins.setErrorHandler(new Consumer<Throwable>() {
            @Override public void accept(@NonNull Throwable throwable) throws Exception {
                Log.e(TAG, "MapMyLocation => RxJavaPlugins.setErrorHandler => " + throwable.getMessage());
            }
        });
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.d(TAG, "Permissions not allowed");
        } else {
            disposable.add(rxLocation.settings().checkAndHandleResolution(locationRequest)
                    .doOnError(new Consumer<Throwable>() {
                        @Override public void accept(@NonNull Throwable throwable) throws Exception {
                            Log.d(TAG, "on Error location");
                        }
                    })
                    .flatMapObservable(new Function<Boolean, ObservableSource<Address>>() {
                        @Override public ObservableSource<Address> apply(@NonNull Boolean aBoolean) throws Exception {
                            return getLocationObservable(aBoolean);
                        }
                    })
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Consumer<Address>() {
                        @Override public void accept(@NonNull Address address) throws Exception {
                            onAddressUpdate(address);
                        }
                    }));
        }
    }


    private Observable<Address> getLocationObservable(final boolean success) {
        if (success) {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return null;
            }
            return rxLocation.location().updates(locationRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnNext(new Consumer<Location>() {
                        @Override public void accept(@NonNull Location location) throws Exception {
                            onLocationUpdate(new LatLng(location.getLatitude(), location.getLongitude()));
                        }
                    }).flatMap(new Function<Location, ObservableSource<Address>>() {
                        @Override public ObservableSource<Address> apply(@NonNull Location location) throws Exception {
                            return getAddressFromLocation(location);
                        }
                    });
        }

        return rxLocation.location().lastLocation()
                .doOnSuccess(new Consumer<Location>() {
                    @Override public void accept(@NonNull Location location) throws Exception {
                        onLocationUpdate(new LatLng(location.getLatitude(), location.getLongitude()));
                    }
                })
                .flatMapObservable(new Function<Location, ObservableSource<? extends Address>>() {
                    @Override public ObservableSource<? extends Address> apply(@NonNull Location location) throws Exception {
                        return getAddressFromLocation(location);
                    }
                });
    }

    private Observable<Address> getAddressFromLocation(Location location) {
        return rxLocation.geocoding().fromLocation(location).toObservable()
                .subscribeOn(Schedulers.io());
    }

    private void onLocationUpdate(LatLng latLng) {
        this.latLng = latLng;
    }

    private void onAddressUpdate(final Address address) {
        if (latLng != null){
            sendNotification(latLng, address);
            disposable.clear();
        }else{
            new Handler().postDelayed(new Runnable() {
                @Override public void run() {
                    onAddressUpdate(address);
                }
            }, 200);
        }
    }

    private void sendNotification(LatLng latLng, Address address) {
        HashMap<String, String> hashMap = new HashMap<>();
        hashMap.put("lat", String.valueOf(latLng.latitude));
        hashMap.put("long", String.valueOf(latLng.longitude));
        hashMap.put("customerId", customerId);
        hashMap.put("address", getAddressText(address));
        hashMap.put("act", "update");
        Observer<RequestBase> observer = new Observer<RequestBase>() {
            @Override public void onSubscribe(Disposable d) {

            }

            @Override public void onNext(RequestBase data) {

            }

            @Override public void onError(Throwable e) {

            }

            @Override public void onComplete() {

            }
        };
        App.getService().apiTracking(hashMap)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    private String getAddressText(Address address) {
        StringBuilder addressText = new StringBuilder();
        final int maxAddressLineIndex = address.getMaxAddressLineIndex();

        for(int i=0; i<=maxAddressLineIndex; i++) {
            addressText.append(address.getAddressLine(i));
            if(i != maxAddressLineIndex) { addressText.append("\n"); }
        }

        return addressText.toString();
    }
}
