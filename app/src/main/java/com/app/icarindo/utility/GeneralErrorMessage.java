package com.app.icarindo.utility;

import android.content.Context;

import com.app.icarindo.R;


/**
 * Created by Luthfi on 23/08/2017.
 */

public final class GeneralErrorMessage {
    public static String get(Context c, boolean pullToRefresh) {
        if (pullToRefresh) {
            return c.getString(R.string.error_network_light);
        }
        return c.getString(R.string.error_network);
    }
}
