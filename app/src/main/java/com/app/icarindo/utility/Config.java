package com.app.icarindo.utility;

import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by j3p0n on 12/3/2016.
 */
public class Config {

    public static final String BASE_URL = "http://icarindo.com/apiv2prod/";
    static final String APP_NAME = ".-iCARindo-.";
    public static final String APP_NAME_BASE = "iCARindo";
    static final boolean DEBUG_MODE = true;
    static final String LOG_TAG = ".-iCar-.";
    public static final String GOOGLE_SIGN_IN_API = ".-iCar-.";
    static final String AUTH_KEY = "49b3aa4b7e9732b75e4a4908994822fb";
    public static final String APP_FILE_PROVIDER = "com.app.icarindo.fileprovider";
    public static final String YOUTUBE_API_KEY = "AIzaSyArFYYp41CNdPgK4YEb5qfREP2nbC8BfFk";
    public static final int ALARM_ID = 1010;
    public static final int MINIMAL_WITHDRAW = 50000;
    public static final int PAGINATION_LIMIT = 12;

    //Konstanta
    public static final String CUSTOMER_ID = "customer_id";
    public static final String CUSTOMER_EMAIL = "customer_email";
    public static final String CUSTOMER_NAME = "customer_name";

    public static final String NO_DATA = "no_data";
    public static final String TRAINING = "training";
    public static final String NO_AKUN = "no_akun";

    public static final String CUSTOMER_KODE = "customer_kode";
    public static final String PLAN_CODE = "plan_code";
    public static final String CUSTOMER_REG_ID = "customer_reg_id";
    public static final String CUSTOMER_SALDO = "customer_saldo";
    public static final String CUSTOMER_AVATAR = "customer_image";
    public static final String CUSTOMER_PIN = "customer_pin";
    public static final String AVATAR_UPLOADED = "avatar_uploaded";
    public static final String ECOMMERCE_MODE = "ecommerce_mode";
    public static final String ECOMMERCE_STATUS = "ecommerce_status";
    public static final int NOTIFICATION_ID = 575855;
    public static final int NOTIFICATION_MESSAGE_ID = 575856;

    public static final String APP_PREFIX = "JFSP";
    public static final String ID_PREFIX = "88753";

    public static final int MIN_LENGHT_PASSWORD = 6;
    //Product type
    public static final String PT_PLN_TOKEN = "pln_prabayar";
    public static final String PT_PLN_PASCA = "pln_pascabayar";
    public static final String PDAM = "pdam";
    public static final String HP_PASCA = "hppasca";
    public static final String TELKOM = "telkom";
    public static final String TV_CABLE = "tvcable";
    public static final String PULSA = "pulsa";
    public static final String PAKET_DATA = "data";
    public static final String FINANCE = "finance";
    public static final String VOUCHER = "voucher";
    public static final String BPJS = "bpjs";

    // Json data notification firebase
    public static final String JSON_DATA_NOTIFICATION = "json_data_notification";

    public static HttpLoggingInterceptor.Level getLogLevel() {
        if (DEBUG_MODE) {
            return HttpLoggingInterceptor.Level.BODY;
        } else {
            return HttpLoggingInterceptor.Level.NONE;
        }
    }

    public static final int MESSAGE_IMAGE_CROP_WIDTH = 640; // 1:1
    public static final int MESSAGE_IMAGE_CROP_HEIGHT = 640; // 1:1

    public static final int CUSTOMER_IMAGE_CROP_WIDTH = 640; // 1:1
    public static final int CUSTOMER_IMAGE_CROP_HEIGHT = 640; // 1:1

    public static final int DOCUMENT_IMAGE_CROP_WIDTH = 1024; // 1:1
    public static final int DOCUMENT_IMAGE_CROP_HEIGHT = 1024; // 1:1
}
