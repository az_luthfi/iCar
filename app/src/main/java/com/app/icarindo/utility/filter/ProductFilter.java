package com.app.icarindo.utility.filter;

import android.widget.Filter;

import com.app.icarindo.adapter.ppob.PpobProductAdapter;
import com.app.icarindo.models.ppob.PpobProduct;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by j3p0n on 1/22/2017.
 */

public class ProductFilter extends Filter {

    private List<PpobProduct> products;
    private List<PpobProduct> productsFiltered;
    private final PpobProductAdapter adapter;

    public ProductFilter(PpobProductAdapter productRecyclerAdapter) {
        this.adapter = productRecyclerAdapter;
        productsFiltered = new ArrayList<>();
    }

    public void setData(List<PpobProduct> products){
        this.products = products;
    }

    @Override
    protected FilterResults performFiltering(CharSequence charSequence) {
        productsFiltered.clear();
        final FilterResults results = new FilterResults();
        //here you need to add proper items do filteredContactList
        for (final PpobProduct item : products) {
            if (item.getPpobProductName().toLowerCase().trim().contains(charSequence)) {
                productsFiltered.add(item);
            }
        }

        results.values = productsFiltered;
        results.count = productsFiltered.size();
        return results;
    }

    @Override
    protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
        adapter.clear();
        adapter.setFiltered(productsFiltered);
    }
}
