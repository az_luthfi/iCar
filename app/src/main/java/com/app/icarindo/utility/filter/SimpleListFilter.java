package com.app.icarindo.utility.filter;

import android.widget.Filter;

import com.app.icarindo.adapter.SimpleAdapter;
import com.app.icarindo.models.datalist.SimpleList;
import com.app.icarindo.utility.Logs;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by luthfi on 28/04/2017.
 */

public class SimpleListFilter extends Filter {
    private static final String TAG = SimpleListFilter.class.getSimpleName();
    private List<? extends SimpleList> simpleList;
    private List<SimpleList> simpleListFiltered;
    private final SimpleAdapter adapter;

    public SimpleListFilter(SimpleAdapter adapter) {
        this.adapter = adapter;
        simpleListFiltered = new ArrayList<>();
    }

    public void setData(List<? extends SimpleList> simpleList) {
        this.simpleList = simpleList;
    }

    @Override protected FilterResults performFiltering(CharSequence constraint) {
        Logs.d(TAG, "Size " + simpleList.size());
        simpleListFiltered.clear();
        final FilterResults results = new FilterResults();
        //here you need to add proper items do filteredContactList
        for (final SimpleList item : simpleList) {
            if (item.getName().toLowerCase().trim().contains(constraint)) {
                simpleListFiltered.add(item);
            }
        }

        results.values = simpleListFiltered;
        results.count = simpleListFiltered.size();
        return results;
    }

    @Override protected void publishResults(CharSequence constraint, FilterResults results) {
        adapter.clear();
        adapter.setFiltered(simpleListFiltered);
    }
}
