package com.app.icarindo.models.carwis;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 09/12/2017.
 */

public class Rental {

    @SerializedName("ccr_id")
    @Expose
    private String ccrId;
    @SerializedName("ct_id")
    @Expose
    private String ctId;
    @SerializedName("ccp_hour")
    @Expose
    private String ccpHour;
    @SerializedName("ccp_amount")
    @Expose
    private String ccpAmount;
    @SerializedName("ccr_name")
    @Expose
    private String ccrName;
    @SerializedName("ccr_nopol")
    @Expose
    private String ccrNopol;
    @SerializedName("ccr_capacity")
    @Expose
    private String ccrCapacity;
    @SerializedName("ccr_type")
    @Expose
    private String ccrType;
    @SerializedName("ccr_extra")
    @Expose
    private String ccrExtra;
    @SerializedName("ccr_is_driver")
    @Expose
    private String ccrIsDriver;
    @SerializedName("ccr_is_gasoline")
    @Expose
    private String ccrIsGasoline;

    public String getCcrId() {
        return ccrId;
    }

    public void setCcrId(String ccrId) {
        this.ccrId = ccrId;
    }

    public String getCtId() {
        return ctId;
    }

    public void setCtId(String ctId) {
        this.ctId = ctId;
    }

    public String getCcpHour() {
        return ccpHour;
    }

    public void setCcpHour(String ccpHour) {
        this.ccpHour = ccpHour;
    }

    public String getCcpAmount() {
        return ccpAmount;
    }

    public void setCcpAmount(String ccpAmount) {
        this.ccpAmount = ccpAmount;
    }

    public String getCcrName() {
        return ccrName;
    }

    public void setCcrName(String ccrName) {
        this.ccrName = ccrName;
    }

    public String getCcrNopol() {
        return ccrNopol;
    }

    public void setCcrNopol(String ccrNopol) {
        this.ccrNopol = ccrNopol;
    }

    public String getCcrCapacity() {
        return ccrCapacity;
    }

    public void setCcrCapacity(String ccrCapacity) {
        this.ccrCapacity = ccrCapacity;
    }

    public String getCcrType() {
        return ccrType;
    }

    public void setCcrType(String ccrType) {
        this.ccrType = ccrType;
    }

    public String getCcrExtra() {
        return ccrExtra;
    }

    public void setCcrExtra(String ccrExtra) {
        this.ccrExtra = ccrExtra;
    }

    public String getCcrIsDriver() {
        return ccrIsDriver;
    }

    public void setCcrIsDriver(String ccrIsDriver) {
        this.ccrIsDriver = ccrIsDriver;
    }

    public String getCcrIsGasoline() {
        return ccrIsGasoline;
    }

    public void setCcrIsGasoline(String ccrIsGasoline) {
        this.ccrIsGasoline = ccrIsGasoline;
    }
}
