package com.app.icarindo.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 23/10/2017.
 */

public class BonusSponsor {
    @SerializedName("bs_id")
    @Expose
    private String bsId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("from_customer_id")
    @Expose
    private String fromCustomerId;
    @SerializedName("bs_nominal")
    @Expose
    private String bsNominal;
    @SerializedName("bs_create_date")
    @Expose
    private String bsCreateDate;
    @SerializedName("customer_name")
    @Expose
    private String customerName;

    public String getBsId() {
        return bsId;
    }

    public void setBsId(String bsId) {
        this.bsId = bsId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getFromCustomerId() {
        return fromCustomerId;
    }

    public void setFromCustomerId(String fromCustomerId) {
        this.fromCustomerId = fromCustomerId;
    }

    public String getBsNominal() {
        return bsNominal;
    }

    public void setBsNominal(String bsNominal) {
        this.bsNominal = bsNominal;
    }

    public String getBsCreateDate() {
        return bsCreateDate;
    }

    public void setBsCreateDate(String bsCreateDate) {
        this.bsCreateDate = bsCreateDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }
}
