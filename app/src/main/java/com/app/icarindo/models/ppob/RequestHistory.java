package com.app.icarindo.models.ppob;

import com.app.icarindo.models.ItemHistory;
import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Luthfi on 09/08/2017.
 */

public class RequestHistory extends RequestBase {
    @SerializedName("yearly")
    @Expose
    private List<HistoryFilterYearly> yearly = new ArrayList<>();
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("total_monthly")
    @Expose
    private String totalMonthly;
    @SerializedName("histories")
    @Expose
    private ArrayList<History> histories = new ArrayList<>();
    @SerializedName("deposit")
    @Expose
    private Deposit deposit;
    @SerializedName("transaction")
    @Expose
    private Transaction transaction;
    @SerializedName("nominal_month")
    @Expose
    private String nominalMonth;
    @SerializedName("nominal_total")
    @Expose
    private String nominalTotal;
    @SerializedName("item_histories")
    @Expose
    private ArrayList<ItemHistory> itemHistories = new ArrayList<>();


    public List<HistoryFilterYearly> getYearly() {
        return yearly;
    }

    public void setYearly(List<HistoryFilterYearly> yearly) {
        this.yearly = yearly;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getTotalMonthly() {
        return totalMonthly;
    }

    public void setTotalMonthly(String totalMonthly) {
        this.totalMonthly = totalMonthly;
    }

    public ArrayList<History> getHistories() {
        return histories;
    }

    public Deposit getDeposit() {
        return deposit;
    }

    public Transaction getTransaction() {
        return transaction;
    }

    public String getNominalMonth() {
        return nominalMonth;
    }

    public String getNominalTotal() {
        return nominalTotal;
    }

    public ArrayList<ItemHistory> getItemHistories() {
        return itemHistories;
    }
}
