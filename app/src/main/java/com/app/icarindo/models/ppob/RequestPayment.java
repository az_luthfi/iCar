package com.app.icarindo.models.ppob;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by j3p0n on 1/18/2017.
 */

public class RequestPayment extends RequestInquiry {

    @SerializedName("StruckDate")
    @Expose
    private String struckDate;
    @SerializedName("SwReferenceNumber")
    @Expose
    private String swReferenceNumber;
    @SerializedName("BillerRefNumber")
    @Expose
    private String billerRefNumber;
    @SerializedName("Ppn")
    @Expose
    private String ppn;
    @SerializedName("TextInfo1")
    @Expose
    private String textInfo1;
    @SerializedName("TextInfo2")
    @Expose
    private String textInfo2;
    @SerializedName("TextInfo3")
    @Expose
    private String textInfo3;
    @SerializedName("ServiceUnit")
    @Expose
    private String serviceUnit;
    @SerializedName("transaction")
    @Expose
    private Transaction transaction;

    public String getStruckDate() {
        return struckDate;
    }

    public String getSwReferenceNumber() {
        return swReferenceNumber;
    }

    public String getBillerRefNumber() {
        return billerRefNumber;
    }

    public String getPpn() {
        return ppn;
    }

    public String getTextInfo1() {
        return textInfo1;
    }

    public String getTextInfo2() {
        return textInfo2;
    }

    public String getTextInfo3() {
        return textInfo3;
    }

    public String getServiceUnit() {
        return serviceUnit;
    }

    public Transaction getTransaction() {
        return transaction;
    }
}