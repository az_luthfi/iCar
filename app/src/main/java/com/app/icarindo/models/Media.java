package com.app.icarindo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by j3p0n on 7/26/2017.
 */

public class Media {

    @SerializedName("media_id")
    @Expose
    private Integer mediaId;
    @SerializedName("section_id")
    @Expose
    private Integer sectionId;
    @SerializedName("data_id")
    @Expose
    private Integer dataId;
    @SerializedName("media_name")
    @Expose
    private String mediaName;
    @SerializedName("media_alias")
    @Expose
    private String mediaAlias;
    @SerializedName("media_desc")
    @Expose
    private String mediaDesc;
    @SerializedName("media_type")
    @Expose
    private String mediaType;
    @SerializedName("media_value")
    @Expose
    private String mediaValue;
    @SerializedName("media_size")
    @Expose
    private String mediaSize;
    @SerializedName("media_primary")
    @Expose
    private String mediaPrimary;
    @SerializedName("media_status")
    @Expose
    private String mediaStatus;
    @SerializedName("media_create_date")
    @Expose
    private String mediaCreateDate;
    @SerializedName("media_value_url")
    @Expose
    private String mediaValueUrl;

    public Integer getMediaId() {
        return mediaId;
    }

    public void setMediaId(Integer mediaId) {
        this.mediaId = mediaId;
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public Integer getDataId() {
        return dataId;
    }

    public void setDataId(Integer dataId) {
        this.dataId = dataId;
    }

    public String getMediaName() {
        return mediaName;
    }

    public void setMediaName(String mediaName) {
        this.mediaName = mediaName;
    }

    public String getMediaAlias() {
        return mediaAlias;
    }

    public void setMediaAlias(String mediaAlias) {
        this.mediaAlias = mediaAlias;
    }

    public String getMediaDesc() {
        return mediaDesc;
    }

    public void setMediaDesc(String mediaDesc) {
        this.mediaDesc = mediaDesc;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getMediaValue() {
        return mediaValue;
    }

    public void setMediaValue(String mediaValue) {
        this.mediaValue = mediaValue;
    }

    public String getMediaSize() {
        return mediaSize;
    }

    public void setMediaSize(String mediaSize) {
        this.mediaSize = mediaSize;
    }

    public String getMediaPrimary() {
        return mediaPrimary;
    }

    public void setMediaPrimary(String mediaPrimary) {
        this.mediaPrimary = mediaPrimary;
    }

    public String getMediaStatus() {
        return mediaStatus;
    }

    public void setMediaStatus(String mediaStatus) {
        this.mediaStatus = mediaStatus;
    }

    public String getMediaCreateDate() {
        return mediaCreateDate;
    }

    public void setMediaCreateDate(String mediaCreateDate) {
        this.mediaCreateDate = mediaCreateDate;
    }

    public String getMediaValueUrl() {
        return mediaValueUrl;
    }
}