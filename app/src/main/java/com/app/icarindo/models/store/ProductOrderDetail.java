package com.app.icarindo.models.store;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 05/11/2017.
 */

public class ProductOrderDetail {
    @SerializedName("od_id")
    @Expose
    private String odId;
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("vendor_id")
    @Expose
    private String vendorId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_image")
    @Expose
    private String productImage;
    @SerializedName("od_desc")
    @Expose
    private String odDesc;
    @SerializedName("od_qty")
    @Expose
    private String odQty;
    @SerializedName("od_item_price")
    @Expose
    private String odItemPrice;
    @SerializedName("od_item_price_fee")
    @Expose
    private String odItemPriceFee;
    @SerializedName("od_discount")
    @Expose
    private String odDiscount;
    @SerializedName("product_weigh")
    @Expose
    private String productWeigh;

    public String getOdId() {
        return odId;
    }

    public void setOdId(String odId) {
        this.odId = odId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getOdDesc() {
        return odDesc;
    }

    public void setOdDesc(String odDesc) {
        this.odDesc = odDesc;
    }

    public String getOdQty() {
        return odQty;
    }

    public void setOdQty(String odQty) {
        this.odQty = odQty;
    }

    public String getOdItemPrice() {
        return odItemPrice;
    }

    public void setOdItemPrice(String odItemPrice) {
        this.odItemPrice = odItemPrice;
    }

    public String getOdItemPriceFee() {
        return odItemPriceFee;
    }

    public void setOdItemPriceFee(String odItemPriceFee) {
        this.odItemPriceFee = odItemPriceFee;
    }

    public String getOdDiscount() {
        return odDiscount;
    }

    public void setOdDiscount(String odDiscount) {
        this.odDiscount = odDiscount;
    }

    public String getProductWeigh() {
        return productWeigh;
    }

    public void setProductWeigh(String productWeigh) {
        this.productWeigh = productWeigh;
    }
}
