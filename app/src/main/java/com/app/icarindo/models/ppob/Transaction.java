package com.app.icarindo.models.ppob;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by j3p0n on 1/18/2017.
 */

public class Transaction {

    @SerializedName("trans_id")
    @Expose
    private String transId;
    @SerializedName("section_id")
    @Expose
    private String sectionId;
    @SerializedName("data_id")
    @Expose
    private String dataId;
    @SerializedName("trans_trx_id")
    @Expose
    private String transTrxId;
    @SerializedName("ppob_product_code")
    @Expose
    private String ppobProductCode;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("trans_customer_number")
    @Expose
    private String transCustomerNumber;
    @SerializedName("trans_type")
    @Expose
    private String transType;
    @SerializedName("trans_price")
    @Expose
    private String transPrice;
    @SerializedName("trans_modal")
    @Expose
    private String transModal;
    @SerializedName("trans_admin_bank")
    @Expose
    private String transAdminBank;
    @SerializedName("trans_fee")
    @Expose
    private String transFee;
    @SerializedName("trans_status")
    @Expose
    private String transStatus;
    @SerializedName("trans_create_date")
    @Expose
    private String transCreateDate;
    @SerializedName("trans_respon_code")
    @Expose
    private String transResponCode;
    @SerializedName("trans_respon_reqnum")
    @Expose
    private String transResponReqnum;
    @SerializedName("trans_respon_refnum")
    @Expose
    private String transResponRefnum;
    @SerializedName("trans_respon_msg")
    @Expose
    private String transResponMsg;
    @SerializedName("trans_respon_note")
    @Expose
    private String transResponNote;
    @SerializedName("trans_respon_date")
    @Expose
    private String transResponDate;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("ppob_cat_id")
    @Expose
    private String ppobCatId;
    @SerializedName("ppob_product_id")
    @Expose
    private String ppobProductId;
    @SerializedName("ppob_product_name")
    @Expose
    private String ppobProductName;
    @SerializedName("ppob_product_type")
    @Expose
    private String ppobProductType;
    @SerializedName("vendor_name")
    @Expose
    private String vendorName;
    @SerializedName("ppob_product_parent_cat")
    @Expose
    private String ppobProductParentCat;
    @SerializedName("ppob_product_image")
    @Expose
    private String ppobProductImage;
    @SerializedName("total")
    @Expose
    private Integer total;

    public String getTransId() {
        return transId;
    }

    public void setTransId(String transId) {
        this.transId = transId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getDataId() {
        return dataId;
    }

    public void setDataId(String dataId) {
        this.dataId = dataId;
    }

    public String getTransTrxId() {
        return transTrxId;
    }

    public void setTransTrxId(String transTrxId) {
        this.transTrxId = transTrxId;
    }

    public String getPpobProductCode() {
        return ppobProductCode;
    }

    public void setPpobProductCode(String ppobProductCode) {
        this.ppobProductCode = ppobProductCode;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTransCustomerNumber() {
        return transCustomerNumber;
    }

    public void setTransCustomerNumber(String transCustomerNumber) {
        this.transCustomerNumber = transCustomerNumber;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getTransPrice() {
        return transPrice;
    }

    public void setTransPrice(String transPrice) {
        this.transPrice = transPrice;
    }

    public String getTransModal() {
        return transModal;
    }

    public void setTransModal(String transModal) {
        this.transModal = transModal;
    }

    public String getTransAdminBank() {
        return transAdminBank;
    }

    public void setTransAdminBank(String transAdminBank) {
        this.transAdminBank = transAdminBank;
    }

    public String getTransFee() {
        return transFee;
    }

    public void setTransFee(String transFee) {
        this.transFee = transFee;
    }

    public String getTransStatus() {
        return transStatus;
    }

    public void setTransStatus(String transStatus) {
        this.transStatus = transStatus;
    }

    public String getTransCreateDate() {
        return transCreateDate;
    }

    public void setTransCreateDate(String transCreateDate) {
        this.transCreateDate = transCreateDate;
    }

    public String getTransResponCode() {
        return transResponCode;
    }

    public void setTransResponCode(String transResponCode) {
        this.transResponCode = transResponCode;
    }

    public String getTransResponReqnum() {
        return transResponReqnum;
    }

    public void setTransResponReqnum(String transResponReqnum) {
        this.transResponReqnum = transResponReqnum;
    }

    public String getTransResponRefnum() {
        return transResponRefnum;
    }

    public void setTransResponRefnum(String transResponRefnum) {
        this.transResponRefnum = transResponRefnum;
    }

    public String getTransResponMsg() {
        return transResponMsg;
    }

    public void setTransResponMsg(String transResponMsg) {
        this.transResponMsg = transResponMsg;
    }

    public String getTransResponNote() {
        return transResponNote;
    }

    public void setTransResponNote(String transResponNote) {
        this.transResponNote = transResponNote;
    }

    public String getTransResponDate() {
        return transResponDate;
    }

    public void setTransResponDate(String transResponDate) {
        this.transResponDate = transResponDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getPpobCatId() {
        return ppobCatId;
    }

    public void setPpobCatId(String ppobCatId) {
        this.ppobCatId = ppobCatId;
    }

    public String getPpobProductId() {
        return ppobProductId;
    }

    public void setPpobProductId(String ppobProductId) {
        this.ppobProductId = ppobProductId;
    }

    public String getPpobProductName() {
        return ppobProductName;
    }

    public void setPpobProductName(String ppobProductName) {
        this.ppobProductName = ppobProductName;
    }

    public String getPpobProductType() {
        return ppobProductType;
    }

    public void setPpobProductType(String ppobProductType) {
        this.ppobProductType = ppobProductType;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getPpobProductParentCat() {
        return ppobProductParentCat;
    }

    public void setPpobProductParentCat(String ppobProductParentCat) {
        this.ppobProductParentCat = ppobProductParentCat;
    }

    public String getPpobProductImage() {
        return ppobProductImage;
    }

    public void setPpobProductImage(String ppobProductImage) {
        this.ppobProductImage = ppobProductImage;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

}
