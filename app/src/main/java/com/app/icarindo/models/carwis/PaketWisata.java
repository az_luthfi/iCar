package com.app.icarindo.models.carwis;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 22/11/2017.
 */

public class PaketWisata {
    @SerializedName("cpw_id")
    @Expose
    private String cpwId;
    @SerializedName("cpw_name")
    @Expose
    private String cpwName;
    @SerializedName("cpw_amount")
    @Expose
    private Double cpwAmount;
    @SerializedName("cpw_start_date")
    @Expose
    private String cpwStartDate;
    @SerializedName("cpw_desc")
    @Expose
    private String cpwDesc;
    @SerializedName("cpw_create_date")
    @Expose
    private String cpwCreateDate;
    @SerializedName("media_value")
    @Expose
    private String mediaValue;

    public String getCpwId() {
        return cpwId;
    }

    public void setCpwId(String cpwId) {
        this.cpwId = cpwId;
    }

    public String getCpwName() {
        return cpwName;
    }

    public void setCpwName(String cpwName) {
        this.cpwName = cpwName;
    }

    public Double getCpwAmount() {
        return cpwAmount;
    }

    public void setCpwAmount(Double cpwAmount) {
        this.cpwAmount = cpwAmount;
    }

    public String getCpwStartDate() {
        return cpwStartDate;
    }

    public void setCpwStartDate(String cpwStartDate) {
        this.cpwStartDate = cpwStartDate;
    }

    public String getCpwDesc() {
        return cpwDesc;
    }

    public void setCpwDesc(String cpwDesc) {
        this.cpwDesc = cpwDesc;
    }

    public String getCpwCreateDate() {
        return cpwCreateDate;
    }

    public void setCpwCreateDate(String cpwCreateDate) {
        this.cpwCreateDate = cpwCreateDate;
    }

    public String getMediaValue() {
        return mediaValue;
    }

    public void setMediaValue(String mediaValue) {
        this.mediaValue = mediaValue;
    }
}
