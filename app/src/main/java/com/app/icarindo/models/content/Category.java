package com.app.icarindo.models.content;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by j3p0n on 2/13/2017.
 */

public class Category {

    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("section_id")
    @Expose
    private String sectionId;
    @SerializedName("cat_name")
    @Expose
    private String catName;
    @SerializedName("cat_alias")
    @Expose
    private String catAlias;
    @SerializedName("cat_desc")
    @Expose
    private String catDesc;
    @SerializedName("cat_image")
    @Expose
    private String catImage;
    @SerializedName("cat_hits")
    @Expose
    private String catHits;
    @SerializedName("cat_parent")
    @Expose
    private String catParent;
    @SerializedName("cat_level")
    @Expose
    private String catLevel;
    @SerializedName("cat_status")
    @Expose
    private String catStatus;
    @SerializedName("cat_root")
    @Expose
    private String catRoot;
    @SerializedName("cat_order")
    @Expose
    private String catOrder;
    @SerializedName("cat_is_parent")
    @Expose
    private boolean catIsParent;
    @SerializedName("contents")
    @Expose
    private ArrayList<Content> contents = new ArrayList<>();
    @SerializedName("categories")
    @Expose
    private ArrayList<Category> categories = new ArrayList<>();

    public String getCatId() {
        return catId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public String getCatName() {
        return catName;
    }

    public String getCatAlias() {
        return catAlias;
    }

    public String getCatDesc() {
        return catDesc;
    }

    public String getCatImage() {
        return catImage;
    }

    public String getCatHits() {
        return catHits;
    }

    public String getCatParent() {
        return catParent;
    }

    public String getCatLevel() {
        return catLevel;
    }

    public String getCatStatus() {
        return catStatus;
    }

    public String getCatRoot() {
        return catRoot;
    }

    public String getCatOrder() {
        return catOrder;
    }

    public ArrayList<Content> getContents() {
        return contents;
    }

    public void setContents(ArrayList<Content> contents) {
        this.contents = contents;
    }

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public void setCategories(ArrayList<Category> categories) {
        this.categories = categories;
    }

    public boolean isCatIsParent() {
        return catIsParent;
    }

    public void setCatIsParent(boolean catIsParent) {
        this.catIsParent = catIsParent;
    }
}