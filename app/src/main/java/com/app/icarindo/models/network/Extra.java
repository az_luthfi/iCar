package com.app.icarindo.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 25/10/2017.
 */

public class Extra {
    @SerializedName("be_id")
    @Expose
    private String beId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("be_date_pakai")
    @Expose
    private String beDatePakai;
    @SerializedName("be_create_date")
    @Expose
    private String beCreateDate;
    @SerializedName("can_cancel")
    @Expose
    private Boolean canCancel;

    public String getBeId() {
        return beId;
    }

    public void setBeId(String beId) {
        this.beId = beId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getBeDatePakai() {
        return beDatePakai;
    }

    public void setBeDatePakai(String beDatePakai) {
        this.beDatePakai = beDatePakai;
    }

    public String getBeCreateDate() {
        return beCreateDate;
    }

    public void setBeCreateDate(String beCreateDate) {
        this.beCreateDate = beCreateDate;
    }

    public Boolean getCanCancel() {
        return canCancel;
    }
}
