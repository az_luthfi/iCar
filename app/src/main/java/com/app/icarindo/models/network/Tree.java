package com.app.icarindo.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Luthfi Aziz on 13/10/2017.
 */

public class Tree implements Serializable{
    @SerializedName("customer_id_parent")
    @Expose
    private String customerIdParent;
    @SerializedName("cn_kaki")
    @Expose
    private String cnKaki;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("customer_kode")
    @Expose
    private String customerKode;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_image")
    @Expose
    private String customerImage;

    public String getCustomerIdParent() {
        return customerIdParent;
    }

    public void setCustomerIdParent(String customerIdParent) {
        this.customerIdParent = customerIdParent;
    }

    public String getCnKaki() {
        return cnKaki;
    }

    public void setCnKaki(String cnKaki) {
        this.cnKaki = cnKaki;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCustomerKode() {
        return customerKode;
    }

    public void setCustomerKode(String customerKode) {
        this.customerKode = customerKode;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerImage() {
        return customerImage;
    }

    public void setCustomerImage(String customerImage) {
        this.customerImage = customerImage;
    }
}
