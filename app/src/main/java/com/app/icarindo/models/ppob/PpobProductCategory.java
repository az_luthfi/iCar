package com.app.icarindo.models.ppob;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by j3p0n on 4/29/2017.
 */

public class PpobProductCategory {

    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("section_id")
    @Expose
    private String sectionId;
    @SerializedName("cat_name")
    @Expose
    private String catName;
    @SerializedName("cat_alias")
    @Expose
    private String catAlias;
    @SerializedName("cat_desc")
    @Expose
    private String catDesc;
    @SerializedName("cat_model")
    @Expose
    private String catModel;
    @SerializedName("cat_type")
    @Expose
    private String catType;
    @SerializedName("cat_status_fee")
    @Expose
    private String catStatusFee;
    @SerializedName("cat_image")
    @Expose
    private String catImage;
    @SerializedName("cat_hits")
    @Expose
    private String catHits;
    @SerializedName("cat_parent")
    @Expose
    private String catParent;
    @SerializedName("cat_level")
    @Expose
    private String catLevel;
    @SerializedName("cat_status")
    @Expose
    private String catStatus;
    @SerializedName("cat_root")
    @Expose
    private String catRoot;
    @SerializedName("cat_order")
    @Expose
    private String catOrder;
    @SerializedName("ppob_products")
    @Expose
    private List<PpobProduct> ppobProducts = null;

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getCatName() {
        return catName;
    }

    public void setCatName(String catName) {
        this.catName = catName;
    }

    public String getCatAlias() {
        return catAlias;
    }

    public void setCatAlias(String catAlias) {
        this.catAlias = catAlias;
    }

    public String getCatDesc() {
        return catDesc;
    }

    public void setCatDesc(String catDesc) {
        this.catDesc = catDesc;
    }

    public String getCatModel() {
        return catModel;
    }

    public void setCatModel(String catModel) {
        this.catModel = catModel;
    }

    public String getCatType() {
        return catType;
    }

    public void setCatType(String catType) {
        this.catType = catType;
    }

    public String getCatStatusFee() {
        return catStatusFee;
    }

    public void setCatStatusFee(String catStatusFee) {
        this.catStatusFee = catStatusFee;
    }

    public String getCatImage() {
        return catImage;
    }

    public void setCatImage(String catImage) {
        this.catImage = catImage;
    }

    public String getCatHits() {
        return catHits;
    }

    public void setCatHits(String catHits) {
        this.catHits = catHits;
    }

    public String getCatParent() {
        return catParent;
    }

    public void setCatParent(String catParent) {
        this.catParent = catParent;
    }

    public String getCatLevel() {
        return catLevel;
    }

    public void setCatLevel(String catLevel) {
        this.catLevel = catLevel;
    }

    public String getCatStatus() {
        return catStatus;
    }

    public void setCatStatus(String catStatus) {
        this.catStatus = catStatus;
    }

    public String getCatRoot() {
        return catRoot;
    }

    public void setCatRoot(String catRoot) {
        this.catRoot = catRoot;
    }

    public String getCatOrder() {
        return catOrder;
    }

    public void setCatOrder(String catOrder) {
        this.catOrder = catOrder;
    }

    public List<PpobProduct> getPpobProducts() {
        return ppobProducts;
    }

    public void setPpobProducts(List<PpobProduct> ppobProducts) {
        this.ppobProducts = ppobProducts;
    }

}