package com.app.icarindo.models.content;

import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by j3p0n on 4/25/2017.
 */

public class RequestContent extends RequestBase {
    @SerializedName("contents")
    @Expose
    private ArrayList<Content> contents = new ArrayList<>();

    @SerializedName("content")
    @Expose
    private Content content;


    public ArrayList<Content> getContents() {
        return contents;
    }

    public void setContents(ArrayList<Content> contents) {
        this.contents = contents;
    }

    public Content getContent() {
        return content;
    }

    public void setContent(Content content) {
        this.content = content;
    }

}
