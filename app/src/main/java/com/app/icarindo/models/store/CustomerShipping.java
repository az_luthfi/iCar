package com.app.icarindo.models.store;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 05/11/2017.
 */

public class CustomerShipping {
    @SerializedName("shipping_id")
    @Expose
    private String shippingId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("shipping_label")
    @Expose
    private String shippingLabel;
    @SerializedName("shipping_name")
    @Expose
    private String shippingName;
    @SerializedName("shipping_email")
    @Expose
    private String shippingEmail;
    @SerializedName("shipping_phone")
    @Expose
    private String shippingPhone;
    @SerializedName("shipping_address")
    @Expose
    private String shippingAddress;
    @SerializedName("shipping_city")
    @Expose
    private String shippingCity;
    @SerializedName("shipping_zip_code")
    @Expose
    private String shippingZipCode;
    @SerializedName("shipping_province")
    @Expose
    private String shippingProvince;
    @SerializedName("shipping_country")
    @Expose
    private String shippingCountry;
    @SerializedName("shipping_default")
    @Expose
    private String shippingDefault;
    @SerializedName("shipping_city_name")
    @Expose
    private String shippingCityName;
    @SerializedName("shipping_province_name")
    @Expose
    private String shippingProvinceName;
    private boolean selected;
    private int positionAdapter;

    public String getShippingId() {
        return shippingId;
    }

    public void setShippingId(String shippingId) {
        this.shippingId = shippingId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getShippingLabel() {
        return shippingLabel;
    }

    public void setShippingLabel(String shippingLabel) {
        this.shippingLabel = shippingLabel;
    }

    public String getShippingName() {
        return shippingName;
    }

    public void setShippingName(String shippingName) {
        this.shippingName = shippingName;
    }

    public String getShippingEmail() {
        return shippingEmail;
    }

    public void setShippingEmail(String shippingEmail) {
        this.shippingEmail = shippingEmail;
    }

    public String getShippingPhone() {
        return shippingPhone;
    }

    public void setShippingPhone(String shippingPhone) {
        this.shippingPhone = shippingPhone;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public String getShippingCity() {
        return shippingCity;
    }

    public void setShippingCity(String shippingCity) {
        this.shippingCity = shippingCity;
    }

    public String getShippingZipCode() {
        return shippingZipCode;
    }

    public void setShippingZipCode(String shippingZipCode) {
        this.shippingZipCode = shippingZipCode;
    }

    public String getShippingProvince() {
        return shippingProvince;
    }

    public void setShippingProvince(String shippingProvince) {
        this.shippingProvince = shippingProvince;
    }

    public String getShippingCountry() {
        return shippingCountry;
    }

    public void setShippingCountry(String shippingCountry) {
        this.shippingCountry = shippingCountry;
    }

    public String getShippingDefault() {
        return shippingDefault;
    }

    public void setShippingDefault(String shippingDefault) {
        this.shippingDefault = shippingDefault;
    }

    public String getShippingCityName() {
        return shippingCityName;
    }

    public void setShippingCityName(String shippingCityName) {
        this.shippingCityName = shippingCityName;
    }

    public String getShippingProvinceName() {
        return shippingProvinceName;
    }

    public void setShippingProvinceName(String shippingProvinceName) {
        this.shippingProvinceName = shippingProvinceName;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public int getPositionAdapter() {
        return positionAdapter;
    }

    public void setPositionAdapter(int positionAdapter) {
        this.positionAdapter = positionAdapter;
    }
}
