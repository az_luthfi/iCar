package com.app.icarindo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


/**
 * Created by j3p0n on 1/6/2017.
 */

public class RequestBase {
    @SerializedName("status")
    @Expose
    protected Boolean status;
    @SerializedName("method")
    @Expose
    protected String method;
    @SerializedName("user_status")
    @Expose
    protected String userStatus;
    @SerializedName("text")
    @Expose
    protected String text;
    @SerializedName("customer_saldo")
    @Expose
    private int customerSaldo;
    @SerializedName("no_data")
    @Expose
    private Boolean noData = false;
    @SerializedName("training")
    @Expose
    private Boolean training = false;
    @SerializedName("count")
    @Expose
    private Integer count = 0;
    @SerializedName("plan_code")
    @Expose
    protected String planCode;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }

    public int getCustomerSaldo() {
        return customerSaldo;
    }

    public void setCustomerSaldo(int customerSaldo) {
        this.customerSaldo = customerSaldo;
    }

    public Boolean getNoData() {
        return noData;
    }

    public Boolean getTraining() {
        return training;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Integer getCount() {
        return count;
    }

    public String getPlanCode() {
        return planCode;
    }
}