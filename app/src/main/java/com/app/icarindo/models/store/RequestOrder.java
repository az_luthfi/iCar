package com.app.icarindo.models.store;

import com.app.icarindo.models.ItemHistory;
import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 05/11/2017.
 */

public class RequestOrder extends RequestBase {

    @SerializedName("nominal_month")
    @Expose
    private String nominalMonth;
    @SerializedName("nominal_total")
    @Expose
    private String nominalTotal;
    @SerializedName("item_histories")
    @Expose
    private ArrayList<ItemHistory> itemHistories = new ArrayList<>();
    @SerializedName("product_order")
    @Expose
    private ProductOrder productOrder;
    @SerializedName("order_id")
    @Expose
    private String orderId;

    public String getNominalMonth() {
        return nominalMonth;
    }

    public String getNominalTotal() {
        return nominalTotal;
    }

    public ArrayList<ItemHistory> getItemHistories() {
        return itemHistories;
    }

    public ProductOrder getProductOrder() {
        return productOrder;
    }

    public String getOrderId() {
        return orderId;
    }
}
