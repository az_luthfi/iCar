
package com.app.icarindo.models.content;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("unused")
public class Content {
    @SerializedName("cat_id")
    private String mCatId;
    @SerializedName("content_alias")
    private String mContentAlias;
    @SerializedName("content_create_date")
    private String mContentCreateDate;
    @SerializedName("content_desc")
    private String mContentDesc;
    @SerializedName("content_hits")
    private String mContentHits;
    @SerializedName("content_id")
    private String mContentId;
    @SerializedName("content_image")
    private String mContentImage;
    @SerializedName("content_name")
    private String mContentName;
    @SerializedName("content_publish_date")
    private String mContentPublishDate;
    @SerializedName("content_status")
    private String mContentStatus;
    @SerializedName("content_tags")
    private String mContentTags;
    @SerializedName("section_id")
    private String mSectionId;
    @SerializedName("user_id")
    private String mUserId;

    public String getCatId() {
        return mCatId;
    }

    public void setCatId(String catId) {
        mCatId = catId;
    }

    public String getContentAlias() {
        return mContentAlias;
    }

    public void setContentAlias(String contentAlias) {
        mContentAlias = contentAlias;
    }

    public String getContentCreateDate() {
        return mContentCreateDate;
    }

    public void setContentCreateDate(String contentCreateDate) {
        mContentCreateDate = contentCreateDate;
    }

    public String getContentDesc() {
        return mContentDesc;
    }

    public void setContentDesc(String contentDesc) {
        mContentDesc = contentDesc;
    }

    public String getContentHits() {
        return mContentHits;
    }

    public void setContentHits(String contentHits) {
        mContentHits = contentHits;
    }

    public String getContentId() {
        return mContentId;
    }

    public void setContentId(String contentId) {
        mContentId = contentId;
    }

    public String getContentImage() {
        return mContentImage;
    }

    public void setContentImage(String contentImage) {
        mContentImage = contentImage;
    }

    public String getContentName() {
        return mContentName;
    }

    public void setContentName(String contentName) {
        mContentName = contentName;
    }

    public String getContentPublishDate() {
        return mContentPublishDate;
    }

    public void setContentPublishDate(String contentPublishDate) {
        mContentPublishDate = contentPublishDate;
    }

    public String getContentStatus() {
        return mContentStatus;
    }

    public void setContentStatus(String contentStatus) {
        mContentStatus = contentStatus;
    }

    public String getContentTags() {
        return mContentTags;
    }

    public void setContentTags(String contentTags) {
        mContentTags = contentTags;
    }

    public String getSectionId() {
        return mSectionId;
    }

    public void setSectionId(String sectionId) {
        mSectionId = sectionId;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String userId) {
        mUserId = userId;
    }

}
