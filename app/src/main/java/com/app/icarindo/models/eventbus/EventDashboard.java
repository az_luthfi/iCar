package com.app.icarindo.models.eventbus;

/**
 * Created by Luthfi on 12/08/2017.
 */

public class EventDashboard {
    private int position;

    public EventDashboard(int position) {
        this.position = position;
    }

    public int getPosition() {
        return position;
    }
}
