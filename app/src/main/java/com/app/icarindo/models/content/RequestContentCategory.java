package com.app.icarindo.models.content;

import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by j3p0n on 4/25/2017.
 */

public class RequestContentCategory extends RequestBase {
    @SerializedName("categories")
    @Expose
    private ArrayList<Category> categories = new ArrayList<>();

    @SerializedName("category")
    @Expose
    private Category category;

    public ArrayList<Category> getCategories() {
        return categories;
    }

    public Category getCategory() {
        return category;
    }
}
