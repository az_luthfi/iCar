package com.app.icarindo.models;

import com.app.icarindo.models.content.Content;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Luthfi on 03/08/2017.
 */

public class RequestHome extends RequestBase{
    @SerializedName("contents")
    private List<Content> mContents;
    @SerializedName("total_cashback")
    private String mTotalCashback;
    @SerializedName("total_deposit")
    private String mTotalDeposit;
    @SerializedName("total_transaction")
    private String mTotalTransaction;

    public List<Content> getContents() {
        return mContents;
    }

    public void setContents(List<Content> contents) {
        mContents = contents;
    }

    public String getTotalCashback() {
        return mTotalCashback;
    }

    public void setTotalCashback(String totalCashback) {
        mTotalCashback = totalCashback;
    }

    public String getTotalDeposit() {
        return mTotalDeposit;
    }

    public void setTotalDeposit(String totalDeposit) {
        mTotalDeposit = totalDeposit;
    }

    public String getTotalTransaction() {
        return mTotalTransaction;
    }

    public void setTotalTransaction(String totalTransaction) {
        mTotalTransaction = totalTransaction;
    }
}
