package com.app.icarindo.models.training;

import com.app.icarindo.models.Media;
import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 18/10/2017.
 */

public class RequestTraining extends RequestBase {
    @SerializedName("soal")
    @Expose
    private ArrayList<Soal> soal = new ArrayList<>();
    @SerializedName("medias")
    @Expose
    private ArrayList<Media> medias = new ArrayList<>();

    public ArrayList<Soal> getSoal() {
        return soal;
    }

    public ArrayList<Media> getMedias() {
        return medias;
    }
}
