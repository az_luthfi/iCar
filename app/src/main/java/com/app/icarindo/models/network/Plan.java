package com.app.icarindo.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 14/10/2017.
 */

public class Plan {
    public static final String CODE_PLANA1 = "PLANA1";
    public static final String CODE_PLANB1 = "PLANB1";
    public static final String CODE_PLANA2 = "PLANA2";
    public static final String CODE_PLANB2 = "PLANB2";

    public static final String NAME_PLANA1 = "PLAN A";
    public static final String NAME_PLANB1 = "PLAN B";
    public static final String NAME_PLANA2 = "PLAN B+";
    public static final String NAME_PLANB2 = "PLAN A+";

    @SerializedName("pp_id")
    @Expose
    private String ppId;
    @SerializedName("pp_code")
    @Expose
    private String ppCode;
    @SerializedName("pp_nama")
    @Expose
    private String ppNama;
    @SerializedName("pp_harga")
    @Expose
    private String ppHarga;
    @SerializedName("pp_potongan")
    @Expose
    private String ppPotongan;
    @SerializedName("pp_fas_1")
    @Expose
    private String ppFas1;
    @SerializedName("pp_fas_2")
    @Expose
    private String ppFas2;
    @SerializedName("pp_fas_3")
    @Expose
    private String ppFas3;
    @SerializedName("pp_is_carwis")
    @Expose
    private String ppIsCarwis;
    @SerializedName("pp_fas_4")
    @Expose
    private String ppFas4;
    @SerializedName("pp_is_asuransi")
    @Expose
    private String ppIsAsuransi;
    @SerializedName("pp_fas_5")
    @Expose
    private String ppFas5;
    @SerializedName("pp_is_join")
    @Expose
    private String ppIsJoin;

    public String getPpId() {
        return ppId;
    }

    public void setPpId(String ppId) {
        this.ppId = ppId;
    }

    public String getPpCode() {
        return ppCode;
    }

    public void setPpCode(String ppCode) {
        this.ppCode = ppCode;
    }

    public String getPpNama() {
        return ppNama;
    }

    public void setPpNama(String ppNama) {
        this.ppNama = ppNama;
    }

    public String getPpHarga() {
        return ppHarga;
    }

    public void setPpHarga(String ppHarga) {
        this.ppHarga = ppHarga;
    }

    public String getPpPotongan() {
        return ppPotongan;
    }

    public void setPpPotongan(String ppPotongan) {
        this.ppPotongan = ppPotongan;
    }

    public String getPpFas1() {
        return ppFas1;
    }

    public void setPpFas1(String ppFas1) {
        this.ppFas1 = ppFas1;
    }

    public String getPpFas2() {
        return ppFas2;
    }

    public void setPpFas2(String ppFas2) {
        this.ppFas2 = ppFas2;
    }

    public String getPpFas3() {
        return ppFas3;
    }

    public void setPpFas3(String ppFas3) {
        this.ppFas3 = ppFas3;
    }

    public String getPpIsCarwis() {
        return ppIsCarwis;
    }

    public void setPpIsCarwis(String ppIsCarwis) {
        this.ppIsCarwis = ppIsCarwis;
    }

    public String getPpFas4() {
        return ppFas4;
    }

    public void setPpFas4(String ppFas4) {
        this.ppFas4 = ppFas4;
    }

    public String getPpIsAsuransi() {
        return ppIsAsuransi;
    }

    public void setPpIsAsuransi(String ppIsAsuransi) {
        this.ppIsAsuransi = ppIsAsuransi;
    }

    public String getPpFas5() {
        return ppFas5;
    }

    public void setPpFas5(String ppFas5) {
        this.ppFas5 = ppFas5;
    }

    public String getPpIsJoin() {
        return ppIsJoin;
    }

    public void setPpIsJoin(String ppIsJoin) {
        this.ppIsJoin = ppIsJoin;
    }
}
