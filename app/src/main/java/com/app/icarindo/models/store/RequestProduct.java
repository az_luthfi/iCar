package com.app.icarindo.models.store;

import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 04/11/2017.
 */

public class RequestProduct extends RequestBase{
    @SerializedName("products")
    @Expose
    private ArrayList<Product> products = new ArrayList<>();
    @SerializedName("product")
    @Expose
    private Product product;

    public ArrayList<Product> getProducts() {
        return products;
    }

    public Product getProduct() {
        return product;
    }
}
