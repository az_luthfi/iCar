package com.app.icarindo.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 29/10/2017.
 */

public class KeluarKota {
    @SerializedName("ckk_id")
    @Expose
    private String ckkId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("ckk_date")
    @Expose
    private String ckkDate;
    @SerializedName("ckk_desc")
    @Expose
    private String ckkDesc;
    @SerializedName("ckk_create_date")
    @Expose
    private String ckkCreateDate;

    public String getCkkId() {
        return ckkId;
    }

    public void setCkkId(String ckkId) {
        this.ckkId = ckkId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCkkDate() {
        return ckkDate;
    }

    public void setCkkDate(String ckkDate) {
        this.ckkDate = ckkDate;
    }

    public String getCkkDesc() {
        return ckkDesc;
    }

    public void setCkkDesc(String ckkDesc) {
        this.ckkDesc = ckkDesc;
    }

    public String getCkkCreateDate() {
        return ckkCreateDate;
    }

    public void setCkkCreateDate(String ckkCreateDate) {
        this.ckkCreateDate = ckkCreateDate;
    }
}
