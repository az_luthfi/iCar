package com.app.icarindo.models.ppob;

import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi on 07/08/2017.
 */

public class RequestProduct extends RequestBase {
    @SerializedName("ppob_products")
    @Expose
    private ArrayList<PpobProduct> ppobProducts = null;

    public ArrayList<PpobProduct> getPpobProducts() {
        return ppobProducts;
    }

    public void setPpobProducts(ArrayList<PpobProduct> ppobProducts) {
        this.ppobProducts = ppobProducts;
    }
}
