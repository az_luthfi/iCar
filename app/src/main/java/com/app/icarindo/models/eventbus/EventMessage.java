package com.app.icarindo.models.eventbus;

import com.app.icarindo.models.pesan.Message;

/**
 * Created by luthfi on 05/06/2017.
 */

public class EventMessage {
    public static final int NEW = 0;
    public static final int NOTIFICATION = 1;
    private Message message;
    private int type;

    public EventMessage(int type, Message message) {
        this.type = type;
        this.message = message;
    }

    public int getType() {
        return type;
    }

    public Message getMessage() {
        return message;
    }
}
