package com.app.icarindo.models.withdraw;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 29/10/2017.
 */

public class CustomerBank {
    @SerializedName("cb_id")
    @Expose
    private String cbId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("cb_name")
    @Expose
    private String cbName;
    @SerializedName("cb_acc")
    @Expose
    private String cbAcc;
    @SerializedName("cb_number")
    @Expose
    private String cbNumber;
    @SerializedName("cb_create_date")
    @Expose
    private String cbCreateDate;

    public String getCbId() {
        return cbId;
    }

    public void setCbId(String cbId) {
        this.cbId = cbId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCbName() {
        return cbName;
    }

    public void setCbName(String cbName) {
        this.cbName = cbName;
    }

    public String getCbAcc() {
        return cbAcc;
    }

    public void setCbAcc(String cbAcc) {
        this.cbAcc = cbAcc;
    }

    public String getCbNumber() {
        return cbNumber;
    }

    public void setCbNumber(String cbNumber) {
        this.cbNumber = cbNumber;
    }

    public String getCbCreateDate() {
        return cbCreateDate;
    }

    public void setCbCreateDate(String cbCreateDate) {
        this.cbCreateDate = cbCreateDate;
    }
}
