package com.app.icarindo.models.store;

import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 05/11/2017.
 */

public class RequestShipping extends RequestBase {
    @SerializedName("customer_shipping")
    @Expose
    private ArrayList<CustomerShipping> customerShipping = new ArrayList<>();

    public ArrayList<CustomerShipping> getCustomerShipping() {
        return customerShipping;
    }
}
