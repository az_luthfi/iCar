package com.app.icarindo.models.customer;

import com.app.icarindo.models.Media;
import com.app.icarindo.models.RequestBase;
import com.app.icarindo.models.ppob.Bank;
import com.app.icarindo.models.ppob.Deposit;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by luthfi on 21/07/2017.
 */

public class RequestCustomer extends RequestBase {
    @SerializedName("customer")
    @Expose
    private Customer customer;
    @SerializedName("customer_password")
    @Expose
    private String customerPassword;
    @SerializedName("deposit")
    @Expose
    private Deposit deposit;
    @SerializedName("banks")
    @Expose
    private ArrayList<Bank> banks = new ArrayList<>();
    @SerializedName("media_ktp")
    @Expose
    private ArrayList<Media> mediaKtp = new ArrayList<>();
    @SerializedName("media_sim")
    @Expose
    private ArrayList<Media> mediaSim = new ArrayList<>();
    @SerializedName("media_skck")
    @Expose
    private ArrayList<Media> mediaSkck = new ArrayList<>();
    @SerializedName("media_stnk")
    @Expose
    private ArrayList<Media> mediaStnk = new ArrayList<>();
    @SerializedName("media_etc")
    @Expose
    private ArrayList<Media> mediaEtc = new ArrayList<>();

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public String getCustomerPassword() {
        return customerPassword;
    }

    public void setCustomerPassword(String customerPassword) {
        this.customerPassword = customerPassword;
    }

    public Deposit getDeposit() {
        return deposit;
    }

    public ArrayList<Bank> getBanks() {
        return banks;
    }

    public ArrayList<Media> getMediaKtp() {
        return mediaKtp;
    }

    public ArrayList<Media> getMediaSim() {
        return mediaSim;
    }

    public ArrayList<Media> getMediaSkck() {
        return mediaSkck;
    }

    public ArrayList<Media> getMediaStnk() {
        return mediaStnk;
    }

    public ArrayList<Media> getMediaEtc() {
        return mediaEtc;
    }
}
