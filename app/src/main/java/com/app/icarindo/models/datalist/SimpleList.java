package com.app.icarindo.models.datalist;

/**
 * Created by luthfi on 28/04/2017.
 */

public interface SimpleList {
    String getName();

    String getId();

    String getOther();
}
