package com.app.icarindo.models.eventbus;


import com.app.icarindo.models.ppob.Bank;

import java.util.ArrayList;

/**
 * Created by Luthfi on 14/08/2017.
 */

public class EventDataBank {
    private ArrayList<Bank> banks = new ArrayList<>();

    public EventDataBank(ArrayList<Bank> banks) {
        this.banks = banks;
    }

    public ArrayList<Bank> getBanks() {
        return banks;
    }
}
