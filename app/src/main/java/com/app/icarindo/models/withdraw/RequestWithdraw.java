package com.app.icarindo.models.withdraw;

import com.app.icarindo.models.ItemHistory;
import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 29/10/2017.
 */

public class RequestWithdraw extends RequestBase {
    @SerializedName("customer_bank")
    @Expose
    private CustomerBank customerBank;
    @SerializedName("nominal_month")
    @Expose
    private String nominalMonth;
    @SerializedName("nominal_total")
    @Expose
    private String nominalTotal;
    @SerializedName("item_histories")
    @Expose
    private ArrayList<ItemHistory> itemHistories = new ArrayList<>();
    @SerializedName("withdraw")
    @Expose
    private Withdraw withdraw;

    public CustomerBank getCustomerBank() {
        return customerBank;
    }

    public String getNominalMonth() {
        return nominalMonth;
    }

    public String getNominalTotal() {
        return nominalTotal;
    }

    public ArrayList<ItemHistory> getItemHistories() {
        return itemHistories;
    }

    public Withdraw getWithdraw() {
        return withdraw;
    }
}
