package com.app.icarindo.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 23/10/2017.
 */

public class Iuran {
    @SerializedName("in_id")
    @Expose
    private String inId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("in_nominal")
    @Expose
    private String inNominal;
    @SerializedName("customer_saldo")
    @Expose
    private String customerSaldo;
    @SerializedName("in_create_date")
    @Expose
    private String inCreateDate;

    public String getInId() {
        return inId;
    }

    public void setInId(String inId) {
        this.inId = inId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getInNominal() {
        return inNominal;
    }

    public void setInNominal(String inNominal) {
        this.inNominal = inNominal;
    }

    public String getCustomerSaldo() {
        return customerSaldo;
    }

    public void setCustomerSaldo(String customerSaldo) {
        this.customerSaldo = customerSaldo;
    }

    public String getInCreateDate() {
        return inCreateDate;
    }

    public void setInCreateDate(String inCreateDate) {
        this.inCreateDate = inCreateDate;
    }

}
