package com.app.icarindo.models.carwis;

import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 13/12/2017.
 */

public class RequestCarwis extends RequestBase {

    @SerializedName("transactions")
    @Expose
    private ArrayList<Transaction> transactions = new ArrayList<>();
    @SerializedName("transaction")
    @Expose
    private Transaction transaction;

    public ArrayList<Transaction> getTransactions() {
        return transactions;
    }

    public Transaction getTransaction() {
        return transaction;
    }
}
