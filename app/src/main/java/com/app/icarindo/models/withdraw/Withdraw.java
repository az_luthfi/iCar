package com.app.icarindo.models.withdraw;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 05/11/2017.
 */

public class Withdraw {
    @SerializedName("withdraw_id")
    @Expose
    private String withdrawId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("withdraw_amount")
    @Expose
    private String withdrawAmount;
    @SerializedName("withdraw_desc")
    @Expose
    private String withdrawDesc;
    @SerializedName("withdraw_note")
    @Expose
    private String withdrawNote;
    @SerializedName("withdraw_bank_to_name")
    @Expose
    private String withdrawBankToName;
    @SerializedName("withdraw_bank_to_acc")
    @Expose
    private String withdrawBankToAcc;
    @SerializedName("withdraw_bank_to_number")
    @Expose
    private String withdrawBankToNumber;
    @SerializedName("withdraw_bank_from_name")
    @Expose
    private String withdrawBankFromName;
    @SerializedName("withdraw_bank_from_acc")
    @Expose
    private String withdrawBankFromAcc;
    @SerializedName("withdraw_bank_from_number")
    @Expose
    private String withdrawBankFromNumber;
    @SerializedName("withdraw_status")
    @Expose
    private String withdrawStatus;
    @SerializedName("withdraw_create_date")
    @Expose
    private String withdrawCreateDate;
    @SerializedName("withdraw_finish_date")
    @Expose
    private String withdrawFinishDate;
    @SerializedName("withdraw_cancel_date")
    @Expose
    private String withdrawCancelDate;
    @SerializedName("customer_name")
    @Expose
    private String customerName;
    @SerializedName("customer_kode")
    @Expose
    private String customerKode;
    @SerializedName("customer_status")
    @Expose
    private String customerStatus;

    public String getWithdrawId() {
        return withdrawId;
    }

    public void setWithdrawId(String withdrawId) {
        this.withdrawId = withdrawId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getWithdrawAmount() {
        return withdrawAmount;
    }

    public void setWithdrawAmount(String withdrawAmount) {
        this.withdrawAmount = withdrawAmount;
    }

    public String getWithdrawDesc() {
        return withdrawDesc;
    }

    public void setWithdrawDesc(String withdrawDesc) {
        this.withdrawDesc = withdrawDesc;
    }

    public String getWithdrawNote() {
        return withdrawNote;
    }

    public void setWithdrawNote(String withdrawNote) {
        this.withdrawNote = withdrawNote;
    }

    public String getWithdrawBankToName() {
        return withdrawBankToName;
    }

    public void setWithdrawBankToName(String withdrawBankToName) {
        this.withdrawBankToName = withdrawBankToName;
    }

    public String getWithdrawBankToAcc() {
        return withdrawBankToAcc;
    }

    public void setWithdrawBankToAcc(String withdrawBankToAcc) {
        this.withdrawBankToAcc = withdrawBankToAcc;
    }

    public String getWithdrawBankToNumber() {
        return withdrawBankToNumber;
    }

    public void setWithdrawBankToNumber(String withdrawBankToNumber) {
        this.withdrawBankToNumber = withdrawBankToNumber;
    }

    public String getWithdrawBankFromName() {
        return withdrawBankFromName;
    }

    public void setWithdrawBankFromName(String withdrawBankFromName) {
        this.withdrawBankFromName = withdrawBankFromName;
    }

    public String getWithdrawBankFromAcc() {
        return withdrawBankFromAcc;
    }

    public void setWithdrawBankFromAcc(String withdrawBankFromAcc) {
        this.withdrawBankFromAcc = withdrawBankFromAcc;
    }

    public String getWithdrawBankFromNumber() {
        return withdrawBankFromNumber;
    }

    public void setWithdrawBankFromNumber(String withdrawBankFromNumber) {
        this.withdrawBankFromNumber = withdrawBankFromNumber;
    }

    public String getWithdrawStatus() {
        return withdrawStatus;
    }

    public void setWithdrawStatus(String withdrawStatus) {
        this.withdrawStatus = withdrawStatus;
    }

    public String getWithdrawCreateDate() {
        return withdrawCreateDate;
    }

    public void setWithdrawCreateDate(String withdrawCreateDate) {
        this.withdrawCreateDate = withdrawCreateDate;
    }

    public String getWithdrawFinishDate() {
        return withdrawFinishDate;
    }

    public void setWithdrawFinishDate(String withdrawFinishDate) {
        this.withdrawFinishDate = withdrawFinishDate;
    }

    public String getWithdrawCancelDate() {
        return withdrawCancelDate;
    }

    public void setWithdrawCancelDate(String withdrawCancelDate) {
        this.withdrawCancelDate = withdrawCancelDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerKode() {
        return customerKode;
    }

    public void setCustomerKode(String customerKode) {
        this.customerKode = customerKode;
    }

    public String getCustomerStatus() {
        return customerStatus;
    }

    public void setCustomerStatus(String customerStatus) {
        this.customerStatus = customerStatus;
    }
}
