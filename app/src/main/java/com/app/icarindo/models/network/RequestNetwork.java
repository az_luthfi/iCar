package com.app.icarindo.models.network;

import com.app.icarindo.models.ItemHistory;
import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 12/10/2017.
 */

public class RequestNetwork extends RequestBase {
    @SerializedName("trees")
    @Expose
    private ArrayList<Tree> trees = new ArrayList<>();
    @SerializedName("plans")
    @Expose
    private ArrayList<Plan> plans = new ArrayList<>();
    @SerializedName("bonus_sponsors")
    @Expose
    private ArrayList<BonusSponsor> bonusSponsors = new ArrayList<>();
    @SerializedName("iurans")
    @Expose
    private ArrayList<Iuran> iurans = new ArrayList<>();
    @SerializedName("count_can_use_extra")
    @Expose
    private Integer countCanUseExtra;
    @SerializedName("date_can_cuti")
    @Expose
    private String dateCanCuti;
    @SerializedName("count_cuti_available_in_month")
    @Expose
    private Integer countCutiAvailableInMonth;
    @SerializedName("cutis")
    @Expose
    private ArrayList<Cuti> cutis = new ArrayList<>();
    @SerializedName("extras")
    @Expose
    private ArrayList<Extra> extras = new ArrayList<>();
    @SerializedName("keluar_kotas")
    @Expose
    private ArrayList<KeluarKota> keluarKotas = new ArrayList<>();
    @SerializedName("plan")
    @Expose
    private Plan plan;
    @SerializedName("customer_kode")
    @Expose
    private String customerKode;
    @SerializedName("customer_akun")
    @Expose
    private ArrayList<CustomerAkun> customerAkun = new ArrayList<>();
    @SerializedName("nominal_month")
    @Expose
    private String nominalMonth;
    @SerializedName("nominal_total")
    @Expose
    private String nominalTotal;
    @SerializedName("item_histories")
    @Expose
    private ArrayList<ItemHistory> itemHistories = new ArrayList<>();

    public ArrayList<Tree> getTrees() {
        return trees;
    }

    public void setTrees(ArrayList<Tree> trees) {
        this.trees = trees;
    }

    public ArrayList<Plan> getPlans() {
        return plans;
    }

    public ArrayList<BonusSponsor> getBonusSponsors() {
        return bonusSponsors;
    }

    public ArrayList<Iuran> getIurans() {
        return iurans;
    }

    public Integer getCountCanUseExtra() {
        return countCanUseExtra;
    }

    public String getDateCanCuti() {
        return dateCanCuti;
    }

    public Integer getCountCutiAvailableInMonth() {
        return countCutiAvailableInMonth;
    }

    public ArrayList<Cuti> getCutis() {
        return cutis;
    }

    public ArrayList<Extra> getExtras() {
        return extras;
    }

    public ArrayList<KeluarKota> getKeluarKotas() {
        return keluarKotas;
    }

    public Plan getPlan() {
        return plan;
    }

    public String getCustomerKode() {
        return customerKode;
    }

    public ArrayList<CustomerAkun> getCustomerAkun() {
        return customerAkun;
    }

    public String getNominalMonth() {
        return nominalMonth;
    }

    public String getNominalTotal() {
        return nominalTotal;
    }

    public ArrayList<ItemHistory> getItemHistories() {
        return itemHistories;
    }
}
