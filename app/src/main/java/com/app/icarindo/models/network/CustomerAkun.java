package com.app.icarindo.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 29/10/2017.
 */

public class CustomerAkun {
    @SerializedName("ca_id")
    @Expose
    private String caId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("ca_nomer")
    @Expose
    private String caNomer;
    @SerializedName("ca_type")
    @Expose
    private String caType;
    @SerializedName("ca_username")
    @Expose
    private String caUsername;
    @SerializedName("ca_password")
    @Expose
    private String caPassword;
    @SerializedName("ca_create_date")
    @Expose
    private String caCreateDate;

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCaNomer() {
        return caNomer;
    }

    public void setCaNomer(String caNomer) {
        this.caNomer = caNomer;
    }

    public String getCaType() {
        return caType;
    }

    public void setCaType(String caType) {
        this.caType = caType;
    }

    public String getCaUsername() {
        return caUsername;
    }

    public void setCaUsername(String caUsername) {
        this.caUsername = caUsername;
    }

    public String getCaPassword() {
        return caPassword;
    }

    public void setCaPassword(String caPassword) {
        this.caPassword = caPassword;
    }

    public String getCaCreateDate() {
        return caCreateDate;
    }

    public void setCaCreateDate(String caCreateDate) {
        this.caCreateDate = caCreateDate;
    }

}
