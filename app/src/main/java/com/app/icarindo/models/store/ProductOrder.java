package com.app.icarindo.models.store;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 05/11/2017.
 */

public class ProductOrder {
    @SerializedName("order_id")
    @Expose
    private String orderId;
    @SerializedName("vendor_id")
    @Expose
    private String vendorId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("order_code")
    @Expose
    private String orderCode;
    @SerializedName("order_desc")
    @Expose
    private String orderDesc;
    @SerializedName("order_tax")
    @Expose
    private String orderTax;
    @SerializedName("order_discount")
    @Expose
    private String orderDiscount;
    @SerializedName("order_voucher_id")
    @Expose
    private String orderVoucherId;
    @SerializedName("order_voucher_amount")
    @Expose
    private String orderVoucherAmount;
    @SerializedName("order_shipping_courier")
    @Expose
    private String orderShippingCourier;
    @SerializedName("order_shipping_number")
    @Expose
    private String orderShippingNumber;
    @SerializedName("order_shipping_address")
    @Expose
    private String orderShippingAddress;
    @SerializedName("order_shipping_cost")
    @Expose
    private String orderShippingCost;
    @SerializedName("order_sub_total")
    @Expose
    private String orderSubTotal;
    @SerializedName("order_cashback")
    @Expose
    private String orderCashback;
    @SerializedName("order_total")
    @Expose
    private String orderTotal;
    @SerializedName("order_status")
    @Expose
    private String orderStatus;
    @SerializedName("order_payment_method")
    @Expose
    private String orderPaymentMethod;
    @SerializedName("order_payment_note")
    @Expose
    private String orderPaymentNote;
    @SerializedName("order_create_date")
    @Expose
    private String orderCreateDate;
    @SerializedName("order_cancel_date")
    @Expose
    private String orderCancelDate;
    @SerializedName("order_paid_date")
    @Expose
    private String orderPaidDate;
    @SerializedName("order_process_date")
    @Expose
    private String orderProcessDate;
    @SerializedName("order_shipping_date")
    @Expose
    private String orderShippingDate;
    @SerializedName("order_delivered_date")
    @Expose
    private String orderDeliveredDate;
    @SerializedName("order_delivered_note")
    @Expose
    private String orderDeliveredNote;
    @SerializedName("order_note")
    @Expose
    private String orderNote;
    @SerializedName("order_item_qty")
    @Expose
    private Integer orderItemQty;
    @SerializedName("product_order_details")
    @Expose
    private ArrayList<ProductOrderDetail> productOrderDetails = new ArrayList<>();

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getOrderCode() {
        return orderCode;
    }

    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    public String getOrderDesc() {
        return orderDesc;
    }

    public void setOrderDesc(String orderDesc) {
        this.orderDesc = orderDesc;
    }

    public String getOrderTax() {
        return orderTax;
    }

    public void setOrderTax(String orderTax) {
        this.orderTax = orderTax;
    }

    public String getOrderDiscount() {
        return orderDiscount;
    }

    public void setOrderDiscount(String orderDiscount) {
        this.orderDiscount = orderDiscount;
    }

    public String getOrderVoucherId() {
        return orderVoucherId;
    }

    public void setOrderVoucherId(String orderVoucherId) {
        this.orderVoucherId = orderVoucherId;
    }

    public String getOrderVoucherAmount() {
        return orderVoucherAmount;
    }

    public void setOrderVoucherAmount(String orderVoucherAmount) {
        this.orderVoucherAmount = orderVoucherAmount;
    }

    public String getOrderShippingCourier() {
        return orderShippingCourier;
    }

    public void setOrderShippingCourier(String orderShippingCourier) {
        this.orderShippingCourier = orderShippingCourier;
    }

    public String getOrderShippingNumber() {
        return orderShippingNumber;
    }

    public void setOrderShippingNumber(String orderShippingNumber) {
        this.orderShippingNumber = orderShippingNumber;
    }

    public String getOrderShippingAddress() {
        return orderShippingAddress;
    }

    public void setOrderShippingAddress(String orderShippingAddress) {
        this.orderShippingAddress = orderShippingAddress;
    }

    public String getOrderShippingCost() {
        return orderShippingCost;
    }

    public void setOrderShippingCost(String orderShippingCost) {
        this.orderShippingCost = orderShippingCost;
    }

    public String getOrderSubTotal() {
        return orderSubTotal;
    }

    public void setOrderSubTotal(String orderSubTotal) {
        this.orderSubTotal = orderSubTotal;
    }

    public String getOrderCashback() {
        return orderCashback;
    }

    public void setOrderCashback(String orderCashback) {
        this.orderCashback = orderCashback;
    }

    public String getOrderTotal() {
        return orderTotal;
    }

    public void setOrderTotal(String orderTotal) {
        this.orderTotal = orderTotal;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        this.orderStatus = orderStatus;
    }

    public String getOrderPaymentMethod() {
        return orderPaymentMethod;
    }

    public void setOrderPaymentMethod(String orderPaymentMethod) {
        this.orderPaymentMethod = orderPaymentMethod;
    }

    public String getOrderPaymentNote() {
        return orderPaymentNote;
    }

    public void setOrderPaymentNote(String orderPaymentNote) {
        this.orderPaymentNote = orderPaymentNote;
    }

    public String getOrderCreateDate() {
        return orderCreateDate;
    }

    public void setOrderCreateDate(String orderCreateDate) {
        this.orderCreateDate = orderCreateDate;
    }

    public String getOrderCancelDate() {
        return orderCancelDate;
    }

    public void setOrderCancelDate(String orderCancelDate) {
        this.orderCancelDate = orderCancelDate;
    }

    public String getOrderPaidDate() {
        return orderPaidDate;
    }

    public void setOrderPaidDate(String orderPaidDate) {
        this.orderPaidDate = orderPaidDate;
    }

    public String getOrderProcessDate() {
        return orderProcessDate;
    }

    public void setOrderProcessDate(String orderProcessDate) {
        this.orderProcessDate = orderProcessDate;
    }

    public String getOrderShippingDate() {
        return orderShippingDate;
    }

    public void setOrderShippingDate(String orderShippingDate) {
        this.orderShippingDate = orderShippingDate;
    }

    public String getOrderDeliveredDate() {
        return orderDeliveredDate;
    }

    public void setOrderDeliveredDate(String orderDeliveredDate) {
        this.orderDeliveredDate = orderDeliveredDate;
    }

    public String getOrderDeliveredNote() {
        return orderDeliveredNote;
    }

    public void setOrderDeliveredNote(String orderDeliveredNote) {
        this.orderDeliveredNote = orderDeliveredNote;
    }

    public String getOrderNote() {
        return orderNote;
    }

    public void setOrderNote(String orderNote) {
        this.orderNote = orderNote;
    }

    public Integer getOrderItemQty() {
        return orderItemQty;
    }

    public void setOrderItemQty(Integer orderItemQty) {
        this.orderItemQty = orderItemQty;
    }

    public ArrayList<ProductOrderDetail> getProductOrderDetails() {
        return productOrderDetails;
    }

    public void setProductOrderDetails(ArrayList<ProductOrderDetail> productOrderDetails) {
        this.productOrderDetails = productOrderDetails;
    }
}
