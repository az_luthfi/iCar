package com.app.icarindo.models.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 25/10/2017.
 */

public class Cuti {
    @SerializedName("cc_id")
    @Expose
    private String ccId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("cc_date")
    @Expose
    private String ccDate;
    @SerializedName("cc_keterangan")
    @Expose
    private String ccKeterangan;
    @SerializedName("cc_status")
    @Expose
    private String ccStatus;
    @SerializedName("cc_create_date")
    @Expose
    private String ccCreateDate;

    public String getCcId() {
        return ccId;
    }

    public void setCcId(String ccId) {
        this.ccId = ccId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getCcDate() {
        return ccDate;
    }

    public void setCcDate(String ccDate) {
        this.ccDate = ccDate;
    }

    public String getCcKeterangan() {
        return ccKeterangan;
    }

    public void setCcKeterangan(String ccKeterangan) {
        this.ccKeterangan = ccKeterangan;
    }

    public String getCcStatus() {
        return ccStatus;
    }

    public void setCcStatus(String ccStatus) {
        this.ccStatus = ccStatus;
    }

    public String getCcCreateDate() {
        return ccCreateDate;
    }

    public void setCcCreateDate(String ccCreateDate) {
        this.ccCreateDate = ccCreateDate;
    }
}
