package com.app.icarindo.models.ppob;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Ryan on 5/9/2016.
 */
public class HistoryFilterYearly {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("monthly")
    @Expose
    private ArrayList<HistoryFilterMonthly> historyFilterMonthlies = new ArrayList<>();


    public String getType() {
        return type;
    }


    public void setType(String type) {
        this.type = type;
    }


    public String getText() {
        return text;
    }


    public void setText(String text) {
        this.text = text;
    }

    public ArrayList<HistoryFilterMonthly> getHistoryFilterMonthlies() {
        return historyFilterMonthlies;
    }

    public void setHistoryFilterMonthlies(ArrayList<HistoryFilterMonthly> historyFilterMonthlies) {
        this.historyFilterMonthlies = historyFilterMonthlies;
    }
}
