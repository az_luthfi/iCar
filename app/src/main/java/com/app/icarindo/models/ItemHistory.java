package com.app.icarindo.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 31/10/2017.
 */

public class ItemHistory {
    @SerializedName("history_id")
    @Expose
    private String historyId;
    @SerializedName("history_method")
    @Expose
    private String historyMethod;
    @SerializedName("history_type")
    @Expose
    private String historyType;
    @SerializedName("history_name")
    @Expose
    private String historyName;
    @SerializedName("history_date")
    @Expose
    private String historyDate;
    @SerializedName("history_nominal")
    @Expose
    private String historyNominal;

    public String getHistoryId() {
        return historyId;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public String getHistoryMethod() {
        return historyMethod;
    }

    public void setHistoryMethod(String historyMethod) {
        this.historyMethod = historyMethod;
    }

    public String getHistoryType() {
        return historyType;
    }

    public void setHistoryType(String historyType) {
        this.historyType = historyType;
    }

    public String getHistoryName() {
        return historyName;
    }

    public void setHistoryName(String historyName) {
        this.historyName = historyName;
    }

    public String getHistoryDate() {
        return historyDate;
    }

    public void setHistoryDate(String historyDate) {
        this.historyDate = historyDate;
    }

    public String getHistoryNominal() {
        return historyNominal;
    }
}
