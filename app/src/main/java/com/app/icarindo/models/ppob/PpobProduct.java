package com.app.icarindo.models.ppob;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by j3p0n on 4/29/2017.
 */

public class PpobProduct {

    @SerializedName("ppob_product_id")
    @Expose
    private String ppobProductId;
    @SerializedName("ppob_cat_id")
    @Expose
    private String ppobCatId;
    @SerializedName("ppob_vendor_id")
    @Expose
    private String ppobVendorId;
    @SerializedName("ppob_product_code")
    @Expose
    private String ppobProductCode;
    @SerializedName("ppob_product_name")
    @Expose
    private String ppobProductName;
    @SerializedName("ppob_product_alias")
    @Expose
    private String ppobProductAlias;
    @SerializedName("ppob_product_desc")
    @Expose
    private String ppobProductDesc;
    @SerializedName("ppob_product_type")
    @Expose
    private String ppobProductType;
    @SerializedName("ppob_product_nominal")
    @Expose
    private String ppobProductNominal;
    @SerializedName("ppob_product_modal")
    @Expose
    private String ppobProductModal;
    @SerializedName("ppob_product_price")
    @Expose
    private String ppobProductPrice;
    @SerializedName("ppob_product_admin_bank")
    @Expose
    private String ppobProductAdminBank;
    @SerializedName("ppob_product_fee")
    @Expose
    private String ppobProductFee;
    @SerializedName("ppob_product_input")
    @Expose
    private String ppobProductInput;
    @SerializedName("ppob_product_status")
    @Expose
    private String ppobProductStatus;
    @SerializedName("ppob_product_create_date")
    @Expose
    private String ppobProductCreateDate;

    public String getPpobProductId() {
        return ppobProductId;
    }

    public void setPpobProductId(String ppobProductId) {
        this.ppobProductId = ppobProductId;
    }

    public String getPpobCatId() {
        return ppobCatId;
    }

    public void setPpobCatId(String ppobCatId) {
        this.ppobCatId = ppobCatId;
    }

    public String getPpobVendorId() {
        return ppobVendorId;
    }

    public void setPpobVendorId(String ppobVendorId) {
        this.ppobVendorId = ppobVendorId;
    }

    public String getPpobProductCode() {
        return ppobProductCode;
    }

    public void setPpobProductCode(String ppobProductCode) {
        this.ppobProductCode = ppobProductCode;
    }

    public String getPpobProductName() {
        return ppobProductName;
    }

    public void setPpobProductName(String ppobProductName) {
        this.ppobProductName = ppobProductName;
    }

    public String getPpobProductAlias() {
        return ppobProductAlias;
    }

    public void setPpobProductAlias(String ppobProductAlias) {
        this.ppobProductAlias = ppobProductAlias;
    }

    public String getPpobProductDesc() {
        return ppobProductDesc;
    }

    public void setPpobProductDesc(String ppobProductDesc) {
        this.ppobProductDesc = ppobProductDesc;
    }

    public String getPpobProductType() {
        return ppobProductType;
    }

    public void setPpobProductType(String ppobProductType) {
        this.ppobProductType = ppobProductType;
    }

    public String getPpobProductNominal() {
        return ppobProductNominal;
    }

    public void setPpobProductNominal(String ppobProductNominal) {
        this.ppobProductNominal = ppobProductNominal;
    }

    public String getPpobProductModal() {
        return ppobProductModal;
    }

    public void setPpobProductModal(String ppobProductModal) {
        this.ppobProductModal = ppobProductModal;
    }

    public String getPpobProductPrice() {
        return ppobProductPrice;
    }

    public void setPpobProductPrice(String ppobProductPrice) {
        this.ppobProductPrice = ppobProductPrice;
    }

    public String getPpobProductAdminBank() {
        return ppobProductAdminBank;
    }

    public void setPpobProductAdminBank(String ppobProductAdminBank) {
        this.ppobProductAdminBank = ppobProductAdminBank;
    }

    public String getPpobProductFee() {
        return ppobProductFee;
    }

    public void setPpobProductFee(String ppobProductFee) {
        this.ppobProductFee = ppobProductFee;
    }

    public String getPpobProductInput() {
        return ppobProductInput;
    }

    public void setPpobProductInput(String ppobProductInput) {
        this.ppobProductInput = ppobProductInput;
    }

    public String getPpobProductStatus() {
        return ppobProductStatus;
    }

    public void setPpobProductStatus(String ppobProductStatus) {
        this.ppobProductStatus = ppobProductStatus;
    }

    public String getPpobProductCreateDate() {
        return ppobProductCreateDate;
    }

    public void setPpobProductCreateDate(String ppobProductCreateDate) {
        this.ppobProductCreateDate = ppobProductCreateDate;
    }

}