package com.app.icarindo.models.ppob;

import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by j3p0n on 5/19/2017.
 */

public class RequestPay extends RequestBase {

    @SerializedName("data_struk")
    @Expose
    private String dataStruk;


    public String getDataStruk() {
        return dataStruk;
    }

    public void setDataStruk(String dataStruk) {
        this.dataStruk = dataStruk;
    }

}
