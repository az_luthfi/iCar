package com.app.icarindo.models.ppob;

import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by j3p0n on 1/14/2017.
 */

public class RequestDeposit extends RequestBase {

    @SerializedName("deposit")
    @Expose
    private Deposit deposit;
    @SerializedName("min_date")
    @Expose
    private String minDate;
    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("total")
    @Expose
    private String total;
    @SerializedName("deposits")
    @Expose
    private ArrayList<Deposit> deposits = new ArrayList<>();
    @SerializedName("banks")
    @Expose
    private ArrayList<Bank> banks = new ArrayList<>();

    public Deposit getDeposit() {
        return deposit;
    }

    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }

    public String getMinDate() {
        return minDate;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public ArrayList<Deposit> getDeposits() {
        return deposits;
    }

    public void setDeposits(ArrayList<Deposit> deposits) {
        this.deposits = deposits;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public ArrayList<Bank> getBanks() {
        return banks;
    }

    public void setBanks(ArrayList<Bank> banks) {
        this.banks = banks;
    }
}
