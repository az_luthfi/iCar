package com.app.icarindo.models.ppob;

import com.app.icarindo.models.datalist.SimpleList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by j3p0n on 1/13/2017.
 */

public class Bank implements SimpleList{

    @SerializedName("bank_id")
    @Expose
    private String bankId;
    @SerializedName("bank_name")
    @Expose
    private String bankName;
    @SerializedName("bank_account")
    @Expose
    private String bankAccount;
    @SerializedName("bank_number")
    @Expose
    private String bankNumber;
    @SerializedName("bank_branch")
    @Expose
    private String bankBranch;
    @SerializedName("bank_image")
    @Expose
    private String bankImage;
    @SerializedName("bank_status")
    @Expose
    private String bankStatus;
    @SerializedName("bank_order")
    @Expose
    private String bankOrder;
    private boolean selected;

    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAccount() {
        return bankAccount;
    }

    public void setBankAccount(String bankAccount) {
        this.bankAccount = bankAccount;
    }

    public String getBankNumber() {
        return bankNumber;
    }

    public void setBankNumber(String bankNumber) {
        this.bankNumber = bankNumber;
    }

    public String getBankBranch() {
        return bankBranch;
    }

    public void setBankBranch(String bankBranch) {
        this.bankBranch = bankBranch;
    }

    public String getBankImage() {
        return bankImage;
    }

    public void setBankImage(String bankImage) {
        this.bankImage = bankImage;
    }

    public String getBankStatus() {
        return bankStatus;
    }

    public void setBankStatus(String bankStatus) {
        this.bankStatus = bankStatus;
    }

    public String getBankOrder() {
        return bankOrder;
    }

    public void setBankOrder(String bankOrder) {
        this.bankOrder = bankOrder;
    }

    @Override
    public String toString() {
        return bankName;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    @Override public String getName() {
        return bankName;
    }

    @Override public String getId() {
        return bankId;
    }

    @Override public String getOther() {
        return null;
    }
}