package com.app.icarindo.models.carwis;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 09/12/2017.
 */

public class Transaction {
    @SerializedName("ct_id")
    @Expose
    private String ctId;
    @SerializedName("promo_id")
    @Expose
    private String promoId;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("driver_id")
    @Expose
    private String driverId;
    @SerializedName("payment_method_id")
    @Expose
    private String paymentMethodId;
    @SerializedName("payment_method_group")
    @Expose
    private String paymentMethodGroup;
    @SerializedName("payment_method_name")
    @Expose
    private String paymentMethodName;
    @SerializedName("payment_method_desc")
    @Expose
    private String paymentMethodDesc;
    @SerializedName("ct_type")
    @Expose
    private String ctType;
    @SerializedName("contact_name")
    @Expose
    private String contactName;
    @SerializedName("contact_email")
    @Expose
    private String contactEmail;
    @SerializedName("contact_phone")
    @Expose
    private String contactPhone;
    @SerializedName("pickup_address")
    @Expose
    private String pickupAddress;
    @SerializedName("pickup_note")
    @Expose
    private String pickupNote;
    @SerializedName("pickup_lat")
    @Expose
    private String pickupLat;
    @SerializedName("pickup_long")
    @Expose
    private String pickupLong;
    @SerializedName("return_address")
    @Expose
    private String returnAddress;
    @SerializedName("return_note")
    @Expose
    private String returnNote;
    @SerializedName("return_lat")
    @Expose
    private String returnLat;
    @SerializedName("return_long")
    @Expose
    private String returnLong;
    @SerializedName("ch_visitor_child")
    @Expose
    private String chVisitorChild;
    @SerializedName("ch_visitor_adult")
    @Expose
    private String chVisitorAdult;
    @SerializedName("ct_amount_subtotal")
    @Expose
    private String ctAmountSubtotal;
    @SerializedName("ct_amount_voucher")
    @Expose
    private String ctAmountVoucher;
    @SerializedName("ct_amount_margin")
    @Expose
    private String ctAmountMargin;
    @SerializedName("ct_amount_payment_admin")
    @Expose
    private String ctAmountPaymentAdmin;
    @SerializedName("ct_amount_total")
    @Expose
    private String ctAmountTotal;
    @SerializedName("ct_status")
    @Expose
    private String ctStatus;
    @SerializedName("ct_note")
    @Expose
    private String ctNote;
    @SerializedName("ct_booking_code")
    @Expose
    private String ctBookingCode;
    @SerializedName("ct_booking_time")
    @Expose
    private String ctBookingTime;
    @SerializedName("ct_create_date")
    @Expose
    private String ctCreateDate;
    @SerializedName("ct_panding_date")
    @Expose
    private String ctPandingDate;
    @SerializedName("ct_pending_note")
    @Expose
    private String ctPendingNote;
    @SerializedName("ct_paid_date")
    @Expose
    private String ctPaidDate;
    @SerializedName("ct_paid_note")
    @Expose
    private String ctPaidNote;
    @SerializedName("ct_process_date")
    @Expose
    private String ctProcessDate;
    @SerializedName("ct_process_note")
    @Expose
    private String ctProcessNote;
    @SerializedName("ct_cancel_date")
    @Expose
    private String ctCancelDate;
    @SerializedName("ct_cancel_note")
    @Expose
    private String ctCancelNote;
    @SerializedName("ct_refund_date")
    @Expose
    private String ctRefundDate;
    @SerializedName("ct_refund_note")
    @Expose
    private String ctRefundNote;
    @SerializedName("ct_finish_date")
    @Expose
    private String ctFinishDate;
    @SerializedName("ct_finish_note")
    @Expose
    private String ctFinishNote;
    @SerializedName("ct_status_text")
    @Expose
    private String ctStatusText;
    @SerializedName("ct_status_info_text")
    @Expose
    private String ctStatusInfoText;
    @SerializedName("ct_type_text")
    @Expose
    private String ctTypeText;
    @SerializedName("rental")
    @Expose
    private Rental rental;
    @SerializedName("wisatas")
    @Expose
    private ArrayList<Wisata> wisatas = new ArrayList<>();
    @SerializedName("paket")
    @Expose
    private PaketWisata paket;

    public String getCtId() {
        return ctId;
    }

    public String getPromoId() {
        return promoId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public String getDriverId() {
        return driverId;
    }

    public String getPaymentMethodId() {
        return paymentMethodId;
    }

    public String getPaymentMethodGroup() {
        return paymentMethodGroup;
    }

    public String getPaymentMethodName() {
        return paymentMethodName;
    }

    public String getPaymentMethodDesc() {
        return paymentMethodDesc;
    }

    public String getCtType() {
        return ctType;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public String getPickupAddress() {
        return pickupAddress;
    }

    public String getPickupNote() {
        return pickupNote;
    }

    public String getPickupLat() {
        return pickupLat;
    }

    public String getPickupLong() {
        return pickupLong;
    }

    public String getReturnAddress() {
        return returnAddress;
    }

    public String getReturnNote() {
        return returnNote;
    }

    public String getReturnLat() {
        return returnLat;
    }

    public String getReturnLong() {
        return returnLong;
    }

    public String getChVisitorChild() {
        return chVisitorChild;
    }

    public String getChVisitorAdult() {
        return chVisitorAdult;
    }

    public String getCtAmountSubtotal() {
        return ctAmountSubtotal;
    }

    public String getCtAmountVoucher() {
        return ctAmountVoucher;
    }

    public String getCtAmountMargin() {
        return ctAmountMargin;
    }

    public String getCtAmountPaymentAdmin() {
        return ctAmountPaymentAdmin;
    }

    public String getCtAmountTotal() {
        return ctAmountTotal;
    }

    public String getCtStatus() {
        return ctStatus;
    }

    public String getCtNote() {
        return ctNote;
    }

    public String getCtBookingCode() {
        return ctBookingCode;
    }

    public String getCtBookingTime() {
        return ctBookingTime;
    }

    public String getCtCreateDate() {
        return ctCreateDate;
    }

    public String getCtPandingDate() {
        return ctPandingDate;
    }

    public String getCtPendingNote() {
        return ctPendingNote;
    }

    public String getCtPaidDate() {
        return ctPaidDate;
    }

    public String getCtPaidNote() {
        return ctPaidNote;
    }

    public String getCtProcessDate() {
        return ctProcessDate;
    }

    public String getCtProcessNote() {
        return ctProcessNote;
    }

    public String getCtCancelDate() {
        return ctCancelDate;
    }

    public String getCtCancelNote() {
        return ctCancelNote;
    }

    public String getCtRefundDate() {
        return ctRefundDate;
    }

    public String getCtRefundNote() {
        return ctRefundNote;
    }

    public String getCtFinishDate() {
        return ctFinishDate;
    }

    public String getCtFinishNote() {
        return ctFinishNote;
    }

    public String getCtStatusText() {
        return ctStatusText;
    }

    public String getCtStatusInfoText() {
        return ctStatusInfoText;
    }

    public String getCtTypeText() {
        return ctTypeText;
    }

    public Rental getRental() {
        return rental;
    }

    public ArrayList<Wisata> getWisatas() {
        return wisatas;
    }

    public PaketWisata getPaket() {
        return paket;
    }
}
