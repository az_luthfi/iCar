package com.app.icarindo.models.store;

import com.app.icarindo.models.datalist.SimpleList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi Aziz on 05/11/2017.
 */

public class City implements SimpleList {
    @SerializedName("city_id")
    @Expose
    private String cityId;
    @SerializedName("province_id")
    @Expose
    private String provinceId;
    @SerializedName("city_province")
    @Expose
    private String cityProvince;
    @SerializedName("city_name")
    @Expose
    private String cityName;
    @SerializedName("city_code")
    @Expose
    private String cityCode;
    @SerializedName("type")
    @Expose
    private String type;

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(String provinceId) {
        this.provinceId = provinceId;
    }

    public String getCityProvince() {
        return cityProvince;
    }

    public void setCityProvince(String cityProvince) {
        this.cityProvince = cityProvince;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    public String getCityCode() {
        return cityCode;
    }

    public void setCityCode(String cityCode) {
        this.cityCode = cityCode;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override public String getName() {
        return cityName;
    }

    @Override public String getId() {
        return cityId;
    }

    @Override public String getOther() {
        return cityCode;
    }
}
