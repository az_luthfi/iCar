package com.app.icarindo.models.store;

import com.app.icarindo.models.Media;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Luthfi Aziz on 04/11/2017.
 */

public class Product {
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("section_id")
    @Expose
    private String sectionId;
    @SerializedName("cat_id")
    @Expose
    private String catId;
    @SerializedName("vendor_id")
    @Expose
    private String vendorId;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("product_alias")
    @Expose
    private String productAlias;
    @SerializedName("product_desc")
    @Expose
    private String productDesc;
    @SerializedName("product_hits")
    @Expose
    private String productHits;
    @SerializedName("product_status")
    @Expose
    private String productStatus;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("product_price_market")
    @Expose
    private String productPriceMarket;
    @SerializedName("product_price_merchant")
    @Expose
    private String productPriceMerchant;
    @SerializedName("product_discount")
    @Expose
    private String productDiscount;
    @SerializedName("product_price_publish")
    @Expose
    private String productPricePublish;
    @SerializedName("product_fee")
    @Expose
    private String productFee;
    @SerializedName("product_stock_ready")
    @Expose
    private String productStockReady;
    @SerializedName("product_stock")
    @Expose
    private String productStock;
    @SerializedName("product_weigh")
    @Expose
    private String productWeigh;
    @SerializedName("product_label")
    @Expose
    private String productLabel;
    @SerializedName("product_option")
    @Expose
    private String productOption;
    @SerializedName("product_assurance")
    @Expose
    private String productAssurance;
    @SerializedName("product_assurance_amount")
    @Expose
    private String productAssuranceAmount;
    @SerializedName("product_create_date")
    @Expose
    private String productCreateDate;
    @SerializedName("product_condition")
    @Expose
    private String productCondition;
    @SerializedName("product_cashback")
    @Expose
    private String productCashback;
    @SerializedName("product_rating")
    @Expose
    private String productRating;
    @SerializedName("product_image_link")
    @Expose
    private String productImageLink;
    @SerializedName("medias")
    @Expose
    private ArrayList<Media> medias = new ArrayList<>();
    @SerializedName("qty_buy")
    @Expose
    private Integer qtyBuy;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getSectionId() {
        return sectionId;
    }

    public void setSectionId(String sectionId) {
        this.sectionId = sectionId;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public String getVendorId() {
        return vendorId;
    }

    public void setVendorId(String vendorId) {
        this.vendorId = vendorId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductAlias() {
        return productAlias;
    }

    public void setProductAlias(String productAlias) {
        this.productAlias = productAlias;
    }

    public String getProductDesc() {
        return productDesc;
    }

    public void setProductDesc(String productDesc) {
        this.productDesc = productDesc;
    }

    public String getProductHits() {
        return productHits;
    }

    public void setProductHits(String productHits) {
        this.productHits = productHits;
    }

    public String getProductStatus() {
        return productStatus;
    }

    public void setProductStatus(String productStatus) {
        this.productStatus = productStatus;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductPriceMarket() {
        return productPriceMarket;
    }

    public void setProductPriceMarket(String productPriceMarket) {
        this.productPriceMarket = productPriceMarket;
    }

    public String getProductPriceMerchant() {
        return productPriceMerchant;
    }

    public void setProductPriceMerchant(String productPriceMerchant) {
        this.productPriceMerchant = productPriceMerchant;
    }

    public String getProductDiscount() {
        return productDiscount;
    }

    public void setProductDiscount(String productDiscount) {
        this.productDiscount = productDiscount;
    }

    public String getProductPricePublish() {
        return productPricePublish;
    }

    public void setProductPricePublish(String productPricePublish) {
        this.productPricePublish = productPricePublish;
    }

    public String getProductFee() {
        return productFee;
    }

    public void setProductFee(String productFee) {
        this.productFee = productFee;
    }

    public String getProductStockReady() {
        return productStockReady;
    }

    public void setProductStockReady(String productStockReady) {
        this.productStockReady = productStockReady;
    }

    public String getProductStock() {
        return productStock;
    }

    public void setProductStock(String productStock) {
        this.productStock = productStock;
    }

    public String getProductWeigh() {
        return productWeigh;
    }

    public void setProductWeigh(String productWeigh) {
        this.productWeigh = productWeigh;
    }

    public String getProductLabel() {
        return productLabel;
    }

    public void setProductLabel(String productLabel) {
        this.productLabel = productLabel;
    }

    public String getProductOption() {
        return productOption;
    }

    public void setProductOption(String productOption) {
        this.productOption = productOption;
    }

    public String getProductAssurance() {
        return productAssurance;
    }

    public void setProductAssurance(String productAssurance) {
        this.productAssurance = productAssurance;
    }

    public String getProductAssuranceAmount() {
        return productAssuranceAmount;
    }

    public void setProductAssuranceAmount(String productAssuranceAmount) {
        this.productAssuranceAmount = productAssuranceAmount;
    }

    public String getProductCreateDate() {
        return productCreateDate;
    }

    public void setProductCreateDate(String productCreateDate) {
        this.productCreateDate = productCreateDate;
    }

    public String getProductCondition() {
        return productCondition;
    }

    public void setProductCondition(String productCondition) {
        this.productCondition = productCondition;
    }

    public String getProductCashback() {
        return productCashback;
    }

    public void setProductCashback(String productCashback) {
        this.productCashback = productCashback;
    }

    public String getProductRating() {
        return productRating;
    }

    public void setProductRating(String productRating) {
        this.productRating = productRating;
    }

    public String getProductImageLink() {
        return productImageLink;
    }

    public void setProductImageLink(String productImageLink) {
        this.productImageLink = productImageLink;
    }

    public ArrayList<Media> getMedias() {
        return medias;
    }

    public void setMedias(ArrayList<Media> medias) {
        this.medias = medias;
    }

    public Integer getQtyBuy() {
        return qtyBuy;
    }

    public void setQtyBuy(Integer qtyBuy) {
        this.qtyBuy = qtyBuy;
    }
}
