package com.app.icarindo.models.ppob;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Luthfi on 10/08/2017.
 */

public class History {
    @SerializedName("history_id")
    @Expose
    private String historyId;
    @SerializedName("history_type")
    @Expose
    private String historyType;
    @SerializedName("history_product")
    @Expose
    private String historyProduct;
    @SerializedName("history_date")
    @Expose
    private String historyDate;
    @SerializedName("history_amount")
    @Expose
    private String historyAmount;
    @SerializedName("history_status")
    @Expose
    private String historyStatus;
    @SerializedName("history_pel")
    @Expose
    private String historyPel;

    public String getHistoryId() {
        return historyId;
    }

    public void setHistoryId(String historyId) {
        this.historyId = historyId;
    }

    public String getHistoryType() {
        return historyType;
    }

    public void setHistoryType(String historyType) {
        this.historyType = historyType;
    }

    public String getHistoryProduct() {
        return historyProduct;
    }

    public void setHistoryProduct(String historyProduct) {
        this.historyProduct = historyProduct;
    }

    public String getHistoryDate() {
        return historyDate;
    }

    public void setHistoryDate(String historyDate) {
        this.historyDate = historyDate;
    }

    public String getHistoryAmount() {
        return historyAmount;
    }

    public void setHistoryAmount(String historyAmount) {
        this.historyAmount = historyAmount;
    }

    public String getHistoryStatus() {
        return historyStatus;
    }

    public void setHistoryStatus(String historyStatus) {
        this.historyStatus = historyStatus;
    }

    public String getHistoryPel() {
        return historyPel;
    }

    public void setHistoryPel(String historyPel) {
        this.historyPel = historyPel;
    }
}
