package com.app.icarindo.models.pesan;

import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by luthfi on 02/06/2017.
 */

public class RequestMessage extends RequestBase {
    @SerializedName("messages")
    @Expose
    private ArrayList<Message> messages = new ArrayList<>();
    @SerializedName("message")
    @Expose
    private Message message;
    @SerializedName("conversations")
    @Expose
    private ArrayList<Conversation> conversations = new ArrayList<>();

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }

    public Message getMessage() {
        return message;
    }

    public void setMessage(Message message) {
        this.message = message;
    }

    public ArrayList<Conversation> getConversations() {
        return conversations;
    }

    public void setConversations(ArrayList<Conversation> conversations) {
        this.conversations = conversations;
    }
}
