package com.app.icarindo.models.ppob;

import com.app.icarindo.models.RequestBase;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by j3p0n on 1/11/2017.
 */

public class RequestProductCategory extends RequestBase {

    @SerializedName("category")
    @Expose
    private PpobProductCategory ppobProductCategory;

    @SerializedName("categories")
    @Expose
    private ArrayList<PpobProductCategory> ppobProductCategories = new ArrayList<>();

    public PpobProductCategory getPpobProductCategory() {
        return ppobProductCategory;
    }

    public void setPpobProductCategory(PpobProductCategory ppobProductCategory) {
        this.ppobProductCategory = ppobProductCategory;
    }

    public ArrayList<PpobProductCategory> getPpobProductCategories() {
        return ppobProductCategories;
    }

    public void setPpobProductCategories(ArrayList<PpobProductCategory> ppobProductCategories) {
        this.ppobProductCategories = ppobProductCategories;
    }
}