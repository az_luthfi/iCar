package com.app.icarindo.models.training;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Luthfi Aziz on 18/10/2017.
 */

public class Soal implements Serializable{
    @SerializedName("st_id")
    @Expose
    private String stId;
    @SerializedName("st_pertanyaan")
    @Expose
    private String stPertanyaan;
    @SerializedName("st_benar")
    @Expose
    private String stBenar;
    @SerializedName("jawaban")
    @Expose
    private String jawaban;

    public String getStId() {
        return stId;
    }

    public void setStId(String stId) {
        this.stId = stId;
    }

    public String getStPertanyaan() {
        return stPertanyaan;
    }

    public void setStPertanyaan(String stPertanyaan) {
        this.stPertanyaan = stPertanyaan;
    }

    public String getStBenar() {
        return stBenar;
    }

    public void setStBenar(String stBenar) {
        this.stBenar = stBenar;
    }

    public String getJawaban() {
        return jawaban;
    }

    public void setJawaban(String jawaban) {
        this.jawaban = jawaban;
    }
}
